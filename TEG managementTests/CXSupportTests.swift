import XCTest
@testable import TEG_management

class CXSupportTests: XCTestCase {
   let supportViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("CXSupport") as! CXSupportViewController
   
   override func setUp() {
      super.setUp()
      
      UIApplication.shared.keyWindow!.rootViewController = supportViewController
      let _ = supportViewController.view
   }
   
   override func tearDown() {
      CXUser.sharedInstance.signOut()
      super.tearDown()
   }
   
   func testSendSupportEmail() {
      let expectation = expectationWithDescription(CXHTTPManager.Method.inProgress)
      
      CXUser.sharedInstance.signIn("mykolag_admin", password: "secret")
      CXHTTPManager.sharedInstance.sendSupportEmail(CXGeneral, comment: CXComments) { (response, error) -> Void in
         if error == nil {
            XCTAssertNotNil(response)
         } else {
            XCTFail("Error")
         }
         
         expectation.fulfill()
      }
      
      waitForExpectationsWithTimeout(CXDefaultTimeout, handler: nil)
   }
   
   func testCheckTextFieldsTrue() {
      supportViewController.textFieldSubject?.text = CXGeneral
      supportViewController.textViewComment?.text = CXComments
      XCTAssertTrue(supportViewController.checkTextFieldsEmpty())
   }
   
   func testCheckTextFieldsFalse() {
      supportViewController.textFieldSubject?.text = ""
      supportViewController.textViewComment?.text = ""
      XCTAssertFalse(supportViewController.checkTextFieldsEmpty())
   }
}
