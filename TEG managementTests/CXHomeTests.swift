import XCTest
@testable import TEG_management

class CXHomeTests: XCTestCase {
   let homeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("CXHome") as! CXHomeViewController
   
   override func setUp() {
      super.setUp()
      
      UIApplication.shared.keyWindow!.rootViewController = homeViewController
      let _ = homeViewController.view
      homeViewController.arrayLoads = [["deliverBy": "2016-02-24T09:00:00Z", "distance":100, "eta":["distanceInMiles":20, "eta":"2016-02-24T09:00:00Z"], "from":["town":"London"], "to":["town":"Manchester"], "reference":0000001], ["deliverBy": "2016-02-24T09:00:00Z", "distance":100, "from":["town":"London"], "to":["town":"Manchester"], "reference":0000002], ["deliverBy": "2016-02-24T11:00:00Z", "distance":100, "eta":["distanceInMiles":20, "eta":"2016-02-24T10:00:00Z"], "from":["town":"London"], "to":["town":"Manchester"], "reference":0000003], ["deliverBy": "2016-02-24T10:00:00Z", "distance":100, "eta":["distanceInMiles":20, "eta":"2016-02-24T17:00:00Z"], "from":["town":"London"], "to":["town":"Manchester"], "reference":0000004], ["deliverBy": "2016-02-24T09:00:00Z", "distance":100, "from":["town":"London"], "to":["town":"Manchester"], "reference":0000005], ["deliverBy": "2016-02-24T09:00:00Z", "distance":100, "eta":["distanceInMiles":20, "eta":"2016-02-24T09:10:00Z"], "from":["town":"London"], "to":["town":"Manchester"], "reference":0000006]]
   }
   
   override func tearDown() {
      CXUser.sharedInstance.signOut()      
      super.tearDown()
   }
   
   func testLoads() {
      let expectation = expectationWithDescription(CXHTTPManager.Method.inProgress)
      
      CXUser.sharedInstance.signIn("mykolag_admin", password: "secret")
      CXHTTPManager.sharedInstance.inProgressWithCompletion { (response, error) -> Void in
         if error == nil {
            XCTAssertNotNil(response)
         } else {
            XCTFail("Error")
         }
         
         expectation.fulfill()
      }
      
      waitForExpectationsWithTimeout(CXDefaultTimeout, handler: nil)
   }
   
   func testLogout() {
      let expectation = expectationWithDescription(CXHTTPManager.Method.logout)
      
      CXUser.sharedInstance.signIn("mykolag_admin", password: "secret")
      CXHTTPManager.sharedInstance.logoutWithCompletion { (response, error) -> Void in
         if error == nil {
            XCTAssertNotNil(response)
         } else {
            XCTFail("Error")
         }
         
         expectation.fulfill()
      }
      
      waitForExpectationsWithTimeout(CXDefaultTimeout, handler: nil)
   }
   
   func testSortLoadsFilterTotal() {
      homeViewController.filter = .Total
      homeViewController.updateLoads()
      homeViewController.sortLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?[0]["reference"], 0000001)
   }
   
   func testSortLoadsFilterOnTime() {
      homeViewController.filter = .OnTime
      homeViewController.updateLoads()
      homeViewController.sortLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?[0]["reference"], 0000001)
   }
   
   func testSortLoadsFilterBehindETA() {
      homeViewController.filter = .BehindETA
      homeViewController.updateLoads()
      homeViewController.sortLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?[0]["reference"], 0000006)
   }
   
   func testSortLoadsFilterLate() {
      homeViewController.filter = .Late
      homeViewController.updateLoads()
      homeViewController.sortLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?[0]["reference"], 0000004)
   }
   
   func testSortLoadsFilterNotTracked() {
      homeViewController.filter = .NotTracked
      homeViewController.updateLoads()
      homeViewController.sortLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?[0]["reference"], 0000002)
   }
   
   func testUpdateLoadsFilterTotal() {
      homeViewController.filter = .Total
      homeViewController.updateLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?.count, homeViewController.arrayLoads?.count)
   }
   
   func testUpdateLoadsFilterOnTime() {
      homeViewController.filter = .OnTime
      homeViewController.updateLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?.count, 2)
   }
   
   func testUpdateLoadsFilterBehindETA() {
      homeViewController.filter = .BehindETA
      homeViewController.updateLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?.count, 1)
   }
   
   func testUpdateLoadsFilterLate() {
      homeViewController.filter = .Late
      homeViewController.updateLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?.count, 1)
   }
   
   func testUpdateLoadsFilterNotTracked() {
      homeViewController.filter = .NotTracked
      homeViewController.updateLoads()
      XCTAssertEqual(homeViewController.arrayLoadsFiltered?.count, 2)
   }
   
   func testButtonFilter() {
      XCTAssertTrue(homeViewController.buttonFilter().isKindOfClass(CXButton.self))
   }
   
   func testLabelFilter() {
      XCTAssertTrue(homeViewController.labelFilter().isKindOfClass(UILabel.self))
   }
}
