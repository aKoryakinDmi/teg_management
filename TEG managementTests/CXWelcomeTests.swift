import XCTest
@testable import TEG_management

class CXWelcomeTests: XCTestCase {
   let welcomeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("CXWelcome") as! CXWelcomeViewController
   
   override func setUp() {
      super.setUp()
    
      UIApplication.shared.keyWindow!.rootViewController = welcomeViewController
      let _ = welcomeViewController.view
   }
   
   override func tearDown() {
      CXUser.sharedInstance.signOut()
      
      super.tearDown()
   }
   
   func testLogin() {
      let expectation = expectationWithDescription(CXHTTPManager.Method.managerLogin)
      
      CXUser.sharedInstance.signIn("mykolag_admin", password: "secret")
      CXHTTPManager.sharedInstance.managerLoginWithCompletion { (response, error) -> Void in
         if error == nil
         {
            XCTAssertNotNil(response)
            let companySettings = (response as! Dictionary<String, AnyObject>)[CXKeyCompanySettings] as! Dictionary<String, AnyObject>
            XCTAssertNotNil(companySettings)
            XCTAssertNotNil(companySettings[CXKeyBehindTime])
            XCTAssertNotNil(companySettings[CXKeyRefreshInterval])
         } else {
            XCTFail("Error")
         }
         
         expectation.fulfill()
      }
      
      waitForExpectationsWithTimeout(CXDefaultTimeout, handler: nil)
   }
   
   func testCheckTextFieldsTrue() {
      welcomeViewController.textFieldLogin?.text = CXUsername
      welcomeViewController.textFieldPassword?.text = CXPassword
      XCTAssertTrue(welcomeViewController.checkTextFieldsEmpty())
   }
   
   func testCheckTextFieldsFalse() {
      welcomeViewController.textFieldLogin?.text = ""
      welcomeViewController.textFieldPassword?.text = ""
      XCTAssertFalse(welcomeViewController.checkTextFieldsEmpty())
   }
   
   func testIsEmailTrue() {
      let email = "email@gmail.com"
      XCTAssertTrue(email.isEmail())
   }
   
   func testIsEmailFalse() {
      let email = "email@gmail.c"
      XCTAssertFalse(email.isEmail())
   }
   
   func testFromBase64() {
      let value = "VEVH"
      XCTAssertEqual(value.fromBase64(), "TEG")
   }
   
   func testToBase64() {
      let value = "TEG"
      XCTAssertEqual(value.toBase64(), "VEVH")
   }
   
   func testToMD5() {
      let value = "TEG"
      XCTAssertEqual(value.toMD5(), "8249001e2d27ab6e3ded1f44cb3660db")
   }
   
   func testKeychainItemWrapperSignIn() {
      CXUser.sharedInstance.signIn(CXUsername, password: CXPassword)
      XCTAssertNotNil(CXUser.sharedInstance.username())
   }
   
   func testKeychainItemWrapperSignOut() {
      CXUser.sharedInstance.signOut()
      XCTAssertNil(CXUser.sharedInstance.username())
   }
}
