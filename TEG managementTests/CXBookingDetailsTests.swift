import XCTest
@testable import TEG_management

class CXBookingDetailsTests: XCTestCase {
   let bookingDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("CXBookingDetails") as! CXBookingDetailsViewController
   
   func testDifferenceBetween() {
      let dateFormat = DateFormatter()
      dateFormat.dateFormat = CXDateFormat
      let fromDate = dateFormat.date(from: "2016-03-07T09:00:00Z")
      let toDate = dateFormat.date(from: "2016-03-08T10:10:00Z")
      let difference = bookingDetailsViewController.differenceBetween(fromDate!, toDate: toDate!)
      XCTAssertEqual(difference, "1d 1h 10m")
   }
   
   func testLoadStatusOnTime() {
      bookingDetailsViewController.load = ["deliverBy": "2016-02-24T09:00:00Z", "distance":100, "eta":["distanceInMiles":20, "eta":"2016-02-24T09:00:00Z"]
         , "from":["town":"London"], "to":["town":"Manchester"], "reference":0000001]
      bookingDetailsViewController.loadStatus()
      XCTAssertEqual(bookingDetailsViewController.status, .OnTime)
   }
   
   func testLoadStatusBehindETA() {
      bookingDetailsViewController.load = ["deliverBy": "2016-02-24T09:00:00Z", "distance":100, "eta":["distanceInMiles":20, "eta":"2016-02-24T09:10:00Z"], "from":["town":"London"], "to":["town":"Manchester"], "reference":0000006]
      bookingDetailsViewController.loadStatus()
      XCTAssertEqual(bookingDetailsViewController.status, .BehindETA)
   }
   
   func testLoadStatusLate() {
      bookingDetailsViewController.load = ["deliverBy": "2016-02-24T10:00:00Z", "distance":100, "eta":["distanceInMiles":20, "eta":"2016-02-24T17:00:00Z"], "from":["town":"London"], "to":["town":"Manchester"], "reference":0000004]
      bookingDetailsViewController.loadStatus()
      XCTAssertEqual(bookingDetailsViewController.status, .Late)
   }
   
   func testLoadStatusNotTracked() {
      bookingDetailsViewController.load = ["deliverBy": "2016-02-24T09:00:00Z", "distance":100, "from":["town":"London"], "to":["town":"Manchester"], "reference":0000002]
      bookingDetailsViewController.loadStatus()
      XCTAssertEqual(bookingDetailsViewController.status, .NotTracked)
   }
}
