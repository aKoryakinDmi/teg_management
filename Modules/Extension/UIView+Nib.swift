//
//  UIView+Nib.swift
//  cx4i
//
//  Created by EvGeniy Lell on 08.04.16.
//
//

import Foundation

extension UIView {
    
    class func defaultNibName() -> String {
        return String(describing: self)
    }
    
    class func loadFromNib(named name: String? = nil, bundle : Bundle? = nil) -> Self? {
        let trueName: String
        if let newName = name { trueName = newName } else { trueName = self.defaultNibName() }
        return loadFromNibHelper(named: trueName, bundle: bundle)
    }
    
    private class func loadFromNibHelper<T>(named name: String, bundle : Bundle? = nil) -> T? {
        let instantiate = UINib(nibName: name, bundle: bundle).instantiate(withOwner: nil, options: nil)
        if instantiate.count > 0 {
            return instantiate[0] as? T
        }
        return nil
    }
    
    func copyPropertiesFrom(prototype: UIView) {
        self.frame = prototype.frame
        self.autoresizingMask = prototype.autoresizingMask
        self.translatesAutoresizingMaskIntoConstraints = prototype.translatesAutoresizingMaskIntoConstraints
        var constraints = [NSLayoutConstraint]()
        for constraint in prototype.constraints {
            var firstItem = constraint.firstItem
            if firstItem as? NSObject == prototype { firstItem = self }
            var secondItem = constraint.secondItem
            if secondItem as? NSObject == prototype { secondItem = self }
            constraints.append(NSLayoutConstraint(item: firstItem, attribute: constraint.firstAttribute, relatedBy: constraint.relation, toItem: secondItem, attribute: constraint.secondAttribute, multiplier: constraint.multiplier, constant: constraint.constant))
        }
        for subview in prototype.subviews {
            self.addSubview(subview)
        }
        self.addConstraints(constraints)
    }
    
    func replacementObject(object: Any?, forClass classType: Any? = nil, fromNibNamed nibName: String? = nil, bundle : Bundle? = nil) -> Any? {
        var trueType = classType as? UIView.Type
        if trueType == nil {
            trueType = type(of: self)
        }
                if let newLoadedView = trueType?.loadFromNib(named: nibName, bundle: bundle) {
                    newLoadedView.copyPropertiesFrom(prototype: self)
                    return newLoadedView
                }
        return object
    }
}
