//
//  Date+Formatter.swift
//  cx4i
//
//  Created by EvGeniy Lell on 09.04.16.
//
//

import Foundation


extension Date {
    
    func formattedWith(_ format: String) -> String {
        let formatter = DateFormatter()
        var localeIdentifier = formatter.locale.identifier
        if !localeIdentifier.hasSuffix("_POSIX") {
            localeIdentifier = localeIdentifier+"_POSIX"
            let locale = Locale(identifier: localeIdentifier)
            formatter.locale = locale
        }
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    var formattedAsTime:String {
        return self.formattedWith("HH:mm")
    }
    
    var formattedAsDate:String {
        return self.formattedWith("dd/M/yyyy")
    }
    
    var formattedAsDateTime:String {
        return self.formattedWith("dd/M/yyyy, H:mm")
    }
    
    func formattedByPeriod(date: Date = Date()) -> String {
        let accuracy = accuracyToDate(date)
        switch accuracy {
        case .Second, .Minute, .Hour, .Day:
            return self.formattedWith("HH:mm")
        case .Week:
            return self.formattedWith("EEEE")
        case .Month:
            return self.formattedWith("dd/M")
        case .Year:
            return self.formattedWith("dd/M/yyyy")
        default:
            return self.formattedWith("dd/M/yyyy, H:mm")
        }
    }
    
    func formattedShortByPeriod(date: Date = Date()) -> String {
        let accuracy = accuracyToDate(date)
        switch accuracy {
        case .Second, .Minute, .Hour, .Day:
            return self.formattedWith("HH:mm")
        case .Week:
            return self.formattedWith("EE")
        case .Month, .Year:
            return self.formattedWith("dd.MM")
        default:
            return self.formattedWith("yyyy")
        }
    }
    
}

enum NSAccuracyCalendarUnit {
    case Unknown
    case Year
    case Month
    case Week
    case Day
    case Hour
    case Minute
    case Second
}

extension Date {
    func accuracyToDate(_ dateToCompare: Date = Date(), roundWeeks: Bool = true) -> NSAccuracyCalendarUnit {
        let calendar = Calendar.current
        
        let unitFlags = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second, .weekOfYear])
        
        let selfComponents = calendar.dateComponents(unitFlags, from: self)
        let compareComponents = calendar.dateComponents(unitFlags, from: dateToCompare)
        
        var internalAccuracy: NSAccuracyCalendarUnit = .Unknown

        if roundWeeks && countDaysFrom(date: dateToCompare) > -7 { internalAccuracy = .Week }
        
        if internalAccuracy == .Unknown &&
            selfComponents.year == compareComponents.year { internalAccuracy = .Year }
        if internalAccuracy == .Year &&
            selfComponents.weekOfYear == compareComponents.weekOfYear { internalAccuracy = .Week }
        if internalAccuracy == .Year &&
            selfComponents.month == compareComponents.month { internalAccuracy = .Month }
        if internalAccuracy == .Week &&
            selfComponents.day == compareComponents.day { internalAccuracy = .Day }
        if internalAccuracy == .Day &&
            selfComponents.hour == compareComponents.hour { internalAccuracy = .Hour }
        if internalAccuracy == .Hour &&
            selfComponents.minute == compareComponents.minute { internalAccuracy = .Minute }
        if internalAccuracy == .Minute &&
            selfComponents.second == compareComponents.second { internalAccuracy = .Second }
        
        return internalAccuracy
    }
}

extension Date {
    func isOneDayWithDate(dateToCompare: Date = Date()) -> Bool {
        let calendar = Calendar.current
        
        let unitFlags = Set<Calendar.Component>([.year, .month, .day])
        let selfComponents = calendar.dateComponents(unitFlags, from: self)
        let compareComponents = calendar.dateComponents(unitFlags, from: dateToCompare)
        var isOneDay = false
        if selfComponents.day == compareComponents.day &&
            selfComponents.month == compareComponents.month &&
            selfComponents.year == compareComponents.year {
            isOneDay = true
        }
        return isOneDay
    }

    func isGreaterThanDate(dateToCompare: Date = Date()) -> Bool {
        var isGreater = false
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: Date = Date()) -> Bool {
        var isLess = false
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        return isLess
    }
    
    func equalToDate(dateToCompare: Date = Date()) -> Bool {
        var isEqualTo = false
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        return isEqualTo
    }
    
    func addDays(addDays: Int) -> Date {
        let secondsInDays: TimeInterval = Double(addDays) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        return dateWithDaysAdded
    }
    
    func addHours(addHours: Int) -> Date {
        let secondsInHours: TimeInterval = Double(addHours) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        return dateWithHoursAdded
    }
    
    func countDaysFrom(date: Date) -> Int {
        let unitFlags = Set<Calendar.Component>([.day])
        return Calendar.current.dateComponents(unitFlags, from: date, to: self).day!
    }
}
