//
//  FullCompany.swift
//  cx4i
//
//  Created by EvGeniy Lell on 07.06.16.
//
//

import Foundation

protocol FullCompany {
    var companyName: String? { get }
    var companyId: String? { get }
}

extension FullCompany {
    var fullCompany: String? {
        if let companyName = self.companyName {
            if let companyId = self.companyId {
                return companyName + " (" + companyId + ")"
            } else {
                return companyName
            }
        } else if let lastName = self.companyId {
            return "(" + lastName + ")"
        }
        return nil
    }
}
