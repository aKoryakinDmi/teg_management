//
//  UITableView+Dispatch.swift
//  cx4i
//
//  Created by EvGeniy Lell on 14.04.16.
//
//

import Foundation

func doAfterDelayInSeconds(_ delayInSeconds: Double, block: (() -> Void)?) {
    let delayTime = DispatchTime.now() + (delayInSeconds * Double(NSEC_PER_SEC))
    DispatchQueue.main.asyncAfter(deadline: delayTime) {
        if block != nil {
            block!()
        }
    }
}
extension UITableView {
    
    func deselectRowAtIndexPath(_ indexpath: IndexPath, animated: Bool, delayInSeconds: Double, block: (() -> Void)?) {
        let delayTime = DispatchTime.now() + (delayInSeconds * Double(NSEC_PER_SEC))
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.deselectRow(at: indexpath, animated: animated)
            if block != nil {
                block!()
            }
        }
    }
}
