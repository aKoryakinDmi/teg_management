//
//  ErrorType.swift
//  cx4i
//
//  Created by EvGeniy Lell on 28.04.16.
//
//

import Foundation

extension Error {
    static var domain: String { get { return String(describing: self) } }
}
