//
//  String+Validation.swift
//  cx4i
//
//  Created by EvGeniy Lell on 19.05.16.
//
//

import Foundation

extension String {
    //Mark: Email
    func isValideEmail() -> Bool {
        return String.validateEmail(string: self)
    }
    static func validateEmail(string: String) -> Bool {
        let emailRegex = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", argumentArray: [emailRegex])
        return emailTest.evaluate(with: string)
    }
    
}
