//
//  FullName.swift
//  cx4i
//
//  Created by EvGeniy Lell on 25.04.16.
//
//

import Foundation

protocol FullName {
    var firstName: String? { get }
    var lastName: String? { get }
}

extension FullName {
    var fullName: String? {
        if let firstName = self.firstName {
            if let lastName = self.lastName {
                return firstName + " " + lastName
            } else {
                return firstName
            }
        } else if let lastName = self.lastName {
            return lastName
        }
        return nil
    }
}
