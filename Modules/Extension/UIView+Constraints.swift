//
//  UIView+Constraints.swift
//  cx4i
//
//  Created by EvGeniy Lell on 07.04.16.
//
//

import Foundation

extension UIView {
    func addConstraintsToFillSuperview() {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let superview = self.superview {
            superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view" : self]))
            superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view" : self]))
        }
    }
    func addConstraintsToCenteringOnSuperview() {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let superview = self.superview {
            superview.addConstraints([
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1.0, constant: 0.0),
                NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1.0, constant: 0.0),
                ])
        }
    }
}

