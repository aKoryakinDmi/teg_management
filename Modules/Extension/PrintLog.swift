//
//  debugPrint+Log.swift
//  cx4i
//
//  Created by EvGeniy Lell on 14.05.16.
//
//

import Foundation
//\(XcodeColor.escape)fg\(fg.0),\(fg.1),\(fg.2)
//public static let escape = "\u{001b}["
public enum PrintLogLevel : Int {
    case Debug      = 0
    case Info       = 1
    case Notice     = 2
    case Warning    = 3
    case Error      = 4
    case Always     = 5
}

class PrintLog : NSObject{
    static let sharedInstance = PrintLog()
    var level: PrintLogLevel = .Warning
    private var fileLevel = [String: PrintLogLevel]()
    
    private func adaptateFileName(_ fileName: String) -> String {
        return URL(fileURLWithPath: fileName).deletingPathExtension().lastPathComponent
    }
    
    func setLevel(_ level: PrintLogLevel?, fileName: String = #file) {
        let fileName = adaptateFileName(fileName)
        if let level = level {
            fileLevel[fileName] = level
        } else {
            fileLevel.removeValue(forKey: fileName)
        }
    }
    
    func getLevel(_ fileName: String = #file) -> PrintLogLevel {
        let fileName = adaptateFileName(fileName)
        let fileLevel = self.fileLevel[fileName] ?? level
        return fileLevel
    }
    
    func printLog(level: PrintLogLevel, items: Any..., infoSeparator: String? = nil, separator: String? = nil, terminator: String? = nil,
                  functionName: String = #function,
                  fileName: String = #file,
                  lineNumber: Int = #line) {
        let fileLevel = getLevel(fileName)
        let fileName = adaptateFileName(fileName)
        if fileLevel.rawValue <= level.rawValue {
            let infoSeparator = infoSeparator ?? " "
            let separator = separator ?? " "
            let terminator = terminator ?? "\n"
            
            if level != .Always {
                print("[\(level)] ",terminator: "")
            }
            print("[\(fileName): \(lineNumber)]:",terminator: infoSeparator)
            if let wrapper = items.first as? Array<Any>, items.count == 1{
                for item in wrapper {
                    debugPrint(item, separator: separator, terminator: separator)
                }
            } else {
                debugPrint(items, separator: separator, terminator: separator)
            }
            debugPrint(terminator: terminator)
        }
    }

    override init() {
        super.init()
        self.readFromExtensions()
    }
    
    func readFromExtensions() {
        var methodCount: UInt32 = 0
        let methodList = class_copyMethodList(type(of: self), &methodCount)
        typealias setLogLevelFunction = @convention(c) (AnyObject, Selector) -> Void
        for i in 0..<Int(methodCount) {
            let selector = method_getName(methodList?[i])
            let selectorName = sel_getName(selector)
            let methodName = String(cString: selectorName!, encoding: String.Encoding.utf8)
            if (methodName?.hasPrefix("set"))!
                && (methodName?.hasSuffix("LogLevel"))! {
                let implementation = method_getImplementation(methodList?[i])
                let curriedImplementation = unsafeBitCast(implementation, to: setLogLevelFunction.self)
                curriedImplementation(self, selector!)
            }
        }
        free(methodList)
    }
}

public func printLog(_ level: PrintLogLevel = .Always, items: Any..., separator: String? = nil, terminator: String? = nil,
                     functionName: String = #function,
                     fileName: String = #file,
                     lineNumber: Int = #line) {
    let newItems:[Any]
    if items.count == 0 {
        newItems = [functionName]
    } else {
        newItems = items
    }
    PrintLog.sharedInstance.printLog(level: level, items: newItems, separator: separator, terminator: terminator,
        functionName:functionName, fileName:fileName, lineNumber:lineNumber)
}



