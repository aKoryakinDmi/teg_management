//
//  UIView+Subviews.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.05.16.
//
//

import Foundation

extension UIView {
    func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
}