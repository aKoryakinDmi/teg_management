//
//  CoreDataManager.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import Foundation
import CoreData

class CoreDataManager {
    
    let modelName: String
    var contextNeedWait: Bool = true
    
    init(modelName: String) {
        self.modelName = modelName
        DispatchQueue.global().sync {
            self.privateManagedObjectContext = self.newManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        }
        self.defaultManagedObjectContext = self.newManagedObjectContext(concurrencyType: .mainQueueConcurrencyType, parentContext: self.privateManagedObjectContext)
    }
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.evg.swiftCoreData" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("\(self.modelName).sqlite")
        //        // TODO: DEBUG - CLEAR BASE
        //        do { try NSFileManager.defaultManager().removeItemAtURL(url) } catch { /* nothing */}
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            do {
                try FileManager.default.removeItem(at: url)
                try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
            } catch {
                // Report any error we got.
                var dict = [String: Any]()
                dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
                dict[NSLocalizedFailureReasonErrorKey] = failureReason
                dict[NSUnderlyingErrorKey] = error
                let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
                // Replace this with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
                abort()
            }
        }
        
        return coordinator
    }()
    
    private func newManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType = .mainQueueConcurrencyType, parentContext: NSManagedObjectContext? = nil) -> NSManagedObjectContext{
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let resultManagedObjectContext = NSManagedObjectContext(concurrencyType: concurrencyType)
        if let parentContext = parentContext {
            resultManagedObjectContext.parent = parentContext
        } else {
            resultManagedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        }
        return resultManagedObjectContext
    }
    
    func newBacgroundManagedObjectContext() -> NSManagedObjectContext {
        return self.newManagedObjectContext(concurrencyType: .privateQueueConcurrencyType, parentContext: defaultManagedObjectContext)
    }
    
    private var privateManagedObjectContext: NSManagedObjectContext!
    var defaultManagedObjectContext: NSManagedObjectContext!
    
    // MARK: - Core Data Saving support
    
    func saveDefaultContext () {
        saveBackgroundContext(context: defaultManagedObjectContext, needWait: false)
        saveBackgroundContext(context: privateManagedObjectContext)
    }
    
    func saveBackgroundContext(context: NSManagedObjectContext, needWait: Bool = true) {
        if context.hasChanges {
            func saveContext() {
                do {
                    try context.save()
                } catch {
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    abort()
                }
            }
            if needWait {
                context.performAndWait {
                    saveContext()
                }
            } else {
                context.perform {
                    saveContext()
                }
            }
        }
    }
}
