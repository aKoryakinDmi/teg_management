//
//  NSManagedObject+Helper.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import Foundation
import CoreData


public extension NSManagedObject {
    
    public class func primaryKeys() -> [String]? {
        return nil
    }
    public class func mapping() -> [String: String]? {
        return nil
    }
    
    
    //Querying
    
    public static func all<T:NSManagedObject>(_ context: NSManagedObjectContext, withSort sort: Any? = nil) -> [T] {
        return self.fetch(context: context, sort: sort)
    }
    
    public static func findAndUpdateOrCreate<T:NSManagedObject>(_ primaryKeys: [String]? = nil, properties: [String: Any], context: NSManagedObjectContext) -> T {
        
        let transformed = self.transformProperties(properties, context: context)
        var transformedPK = [String: Any]()
        if let primaryKeys = primaryKeys ?? self.primaryKeys()  {
            for pKey in primaryKeys {
                if let value = transformed[pKey] {
                    transformedPK[pKey] = value
                }
            }
        } else {
            transformedPK = transformed
        }
        let existing: T? = self.query(transformedPK, context: context).first
        if let existing = existing {
            existing.update(transformed)
        }
        return existing ?? self.create(properties: transformed, context:context)
    }
    
    public static func findOrCreate<T:NSManagedObject>(_ properties: [String: Any], context: NSManagedObjectContext) -> T {
        let transformed = self.transformProperties(properties, context: context)
        let existing: T? = self.query(transformed, context: context).first
        return existing ?? self.create(properties: transformed, context:context)
    }
    
    public static func find<T:NSManagedObject>(_ condition: Any, context: NSManagedObjectContext) -> T? {
        return query(condition, context: context, sort:nil, limit:1).first
    }
    
    public static func query<T:NSManagedObject>(_ condition: Any, context: NSManagedObjectContext, sort: Any? = nil, limit: Int? = nil) -> [T] {
        return fetch(condition, context: context, sort: sort, limit: limit)
    }
    
    // Aggregation
    public static func count(_ query: Any? = nil, context: NSManagedObjectContext) -> Int {
        var predicate: NSPredicate? = nil
        if let query = query {
            predicate = self.predicate(query)
        }
        return countForFetch(predicate, context: context)
    }
    
    // Creation / Deletion
    public static func create<T:NSManagedObject>(properties: [String: Any]? = nil, context: NSManagedObjectContext) -> T {
        let newEntity: T = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! T
        if let properties = properties {
            newEntity.update(properties)
        }
        return newEntity
    }
    
    public func update(_ properties: [String: Any], context: NSManagedObjectContext? = nil) {
        if (properties.count == 0) {
            return
        }
        guard let context = context ?? self.managedObjectContext else {
            return
        }
        let transformed = type(of: self).transformProperties(properties, context: context)
        //Finish
        for (key, value) in transformed {
            self.willChangeValue(forKey: key)
            self.setSafe(value: value, forKey: key)
            self.didChangeValue(forKey: key)
        }
    }
    
    //MARK: Save
    public static func save(_ context: NSManagedObjectContext) -> Bool {
        do {
            try context.save()
            return true
        } catch let e as NSError {
            print("Save Error: \(e)")
            return false
        }
    }
    
    public func save() -> Bool {
        return self.saveTheContext()
    }
    
    private func saveTheContext() -> Bool {
        if self.managedObjectContext == nil || !self.managedObjectContext!.hasChanges {
            return true
        }
        
        do {
            try self.managedObjectContext!.save()
        } catch let error as NSError {
            print("Unresolved error in saving context for entity:")
            print(self)
            print("!\nError: " + error.debugDescription)
            return false
        }
        
        return true
    }
    
    
    public func delete() {
        self.managedObjectContext?.delete(self)
    }
    
    public static func deleteAll(_ context: NSManagedObjectContext) {
        for o in self.all(context) {
            o.delete()
        }
    }
    
    public static var entityName: String {
        var name = NSStringFromClass(self)
        if name.range(of: ".") != nil {
            let comp = name.characters.split {$0 == "."}.map { String($0) }
            if let last = comp.last {
                name = last
            }
        }
        
        let nonUpperCase = CharacterSet.uppercaseLetters.inverted
        if let letters = name.components(separatedBy: nonUpperCase).first {
            if letters.characters.count > 1 {
                let index = name.index(name.startIndex, offsetBy: letters.characters.count-1)
                name = name.substring(from: index)
            }
        }
        if name.range(of: "_") != nil {
            var comp = name.characters.split {$0 == "_"}.map { String($0) }
            var last: String = ""
            var remove = -1
            for (i,s) in comp.reversed().enumerated() {
                if last == s {
                    remove = i
                }
                last = s
            }
            if remove > -1 {
                comp.remove(at: remove)
                name = comp.joined(separator: "_")
            }
        }
        return name
    }
    
    // F
    
    public static func newFetchedResultsControllerFor(_ cacheName: String, condition: Any? = nil, batchSize: Int = 20, sort: Any? = nil, sectionNameKeyPath: String? = nil, context: NSManagedObjectContext) -> NSFetchedResultsController<NSFetchRequestResult> {
        let fetchRequest = self.createFetchRequest(context)
        fetchRequest.fetchBatchSize = batchSize
        if let condition: Any = condition {
            fetchRequest.predicate = self.predicate(condition)
        }
        if let sort: Any = sort {
            fetchRequest.sortDescriptors = self.sortDescriptors(sort)
        }
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: sectionNameKeyPath, cacheName: cacheName)
        
        do {
            try aFetchedResultsController.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        return aFetchedResultsController
    }
    
    //Private
    
    private static func transformProperties(_ properties: [String: Any], context: NSManagedObjectContext) -> [String: Any]{
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)!
        let attrs = entity.attributesByName
        let rels = entity.relationshipsByName
        
        var transformed = [String: Any]()
        for (key, value) in properties {
            let localKey = self.key(forRemote: key, context: context)
            if attrs[localKey] != nil {
                transformed[localKey] = value
            } else if let rel = rels[localKey] {
                print(rel)
            }
        }
        return transformed
    }
    
    
    private static func predicate(_ condition: Any, args: [Any]? = nil) -> NSPredicate {
        if let predicate = condition as? NSPredicate {
            return predicate
        }
        if let format = condition as? String {
            return NSPredicate(format: format, argumentArray: args)
        }
        if let properties = condition as? [String: Any] {
            return self.predicate(properties: properties)
        }
        return NSPredicate()
    }
    
    private static func predicate(properties: [String: Any]) -> NSPredicate {
        var preds = [NSPredicate]()
        for (key, value) in properties {
            if key.hasSuffix(" >") {
                let index = key.index(key.endIndex, offsetBy: -2)
                let subKey = key.substring(to: index)
                preds.append(NSPredicate(format: "%K > %@", argumentArray: [subKey, value]))
            }
            else if key.hasSuffix(" <") {
                let index = key.index(key.endIndex, offsetBy: -2)
                let subKey = key.substring(to: index)
                preds.append(NSPredicate(format: "%K < %@", argumentArray: [subKey, value]))
            }
            else if key.hasSuffix(" !=") {
                let index = key.index(key.endIndex, offsetBy: -3)
                let subKey = key.substring(to: index)
                preds.append(NSPredicate(format: "%K != %@", argumentArray: [subKey, value]))
            }
            else {
                preds.append(NSPredicate(format: "%K = %@", argumentArray: [key, value]))
            }
        }
        return NSCompoundPredicate(type: .and, subpredicates: preds)
    }
    
    
    //MARK: - Sorts Methods
    private static func sortDescriptor(_ d: Any) -> NSSortDescriptor {
        if let string = d as? String {
            return sortDescriptor(string: string)
        }
        if let descriptor = d as? NSSortDescriptor {
            return sortDescriptor(descriptor: descriptor)
        }
        if let dictionary = d as? [String: Any] {
            return sortDescriptor(dictionary: dictionary)
        }
        return NSSortDescriptor()
    }
    
    private static func sortDescriptor(string: String) -> NSSortDescriptor {
        var key = string
        let components = string.characters.split {$0 == " "}.map { String($0) }
        var isAscending = true
        if (components.count > 1) {
            key = components[0]
            isAscending = components[1] == "ASC"
        }
        return NSSortDescriptor(key: key, ascending: isAscending)
    }
    
    private static func sortDescriptor(descriptor: NSSortDescriptor) -> NSSortDescriptor {
        return descriptor
    }
    
    private static func sortDescriptor(dictionary: [String: Any]) -> NSSortDescriptor {
        if let (key, value) = dictionary.first {
            if let value = value as? String {
                let isAscending = value.uppercased() != "DESC"
                return NSSortDescriptor(key: key, ascending: isAscending)
            }
        }
        return NSSortDescriptor()
    }
    
    //MARK: Sorts Methods
    private static func sortDescriptors(_ d: Any) -> [NSSortDescriptor] {
        if let string = d as? String {
            return self.sortDescriptors(string: string)
        }
        if let descriptors = d as? [NSSortDescriptor] {
            return sortDescriptors(descriptors: descriptors)
        }
        if let dictionaries = d as? [[String: Any]] {
            return self.sortDescriptors(dictionaries: dictionaries)
        }
        return [self.sortDescriptor(d)]
    }
    
    private static func sortDescriptors(string: String) -> [NSSortDescriptor] {
        let components = string.characters.split {$0 == ","}.map { String($0) }
        var descriptors = [NSSortDescriptor]()
        for substring in components {
            descriptors.append(sortDescriptor(string: substring))
        }
        return descriptors
    }
    
    private static func sortDescriptors(descriptors: [NSSortDescriptor]) -> [NSSortDescriptor] {
        return descriptors
    }
    
    private static func sortDescriptors(dictionaries: [[String: Any]]) -> [NSSortDescriptor] {
        var sortDescriptors: [NSSortDescriptor] = []
        for dictionary in dictionaries {
            let temp = sortDescriptor(dictionary: dictionary)
            sortDescriptors.append(temp)
        }
        return sortDescriptors
    }
    
    //MARK: Fetch
    private static func createFetchRequest(_ context: NSManagedObjectContext) -> NSFetchRequest<NSFetchRequestResult> {
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        return request
    }
    
    private static func fetch<T:NSManagedObject>(_ condition: Any? = nil, context: NSManagedObjectContext, sort: Any? = nil, limit: Int? = nil) -> [T] {
        let request = self.createFetchRequest(context)
        
        if let cond: Any = condition {
            request.predicate = self.predicate(cond)
        }
        
        if let ord: Any = sort {
            request.sortDescriptors = self.sortDescriptors(ord)
        }
        
        if let lim = limit {
            request.fetchLimit = lim
        }
        
        var result : [T]
        
        do {
            var fetchResult : [Any]
            try fetchResult = context.fetch(request)
            
            if let fetchResultTyped = fetchResult as? [T] {
                result = fetchResultTyped
            } else {
                throw NSError(domain: "Fetch results unable to be casted to [NSManagedObject]", code: 0, userInfo: nil)
            }
        } catch let error as NSError {
            print("Error executing fetch request \(request): " + error.description)
            result = [T]()
        }
        
        return result
    }
    
    private static func countForFetch(_ predicate: NSPredicate?, context: NSManagedObjectContext) -> Int {
        let request = self.createFetchRequest(context)
        request.predicate = predicate
        do {
            let count = try context.count(for: request)
            return count
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return 0
        }
        //        return context.countForFetchRequest(request, error: nil)
    }
    
    private static func count(_ predicate: NSPredicate, context: NSManagedObjectContext) -> Int {
        let request = self.createFetchRequest(context)
        request.predicate = predicate
        do {
            let count = try context.count(for: request)
            return count
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return 0
        }
        //        return context.countForFetchRequest(request, error: nil)
    }
    
    
    private func setSafe(value: Any?, forKey key: String) {
        if (value == nil || value is NSNull) {
            self.setNilValueForKey(key)
            return
        }
        //        let val: Any = value!
        if let attr = self.entity.attributesByName[key] {
            let attrType = attr.attributeType
            switch attrType {
            case .integer16AttributeType,
                 .integer32AttributeType,
                 .integer64AttributeType:
                if let p = value as? NSNumber {
                    self.setPrimitiveValue(NSNumber(value: p.intValue), forKey: key)
                    return
                } else
                    if let p = value as? NSString {
                        self.setPrimitiveValue(NSNumber(value: p.integerValue), forKey: key)
                        return
                }
            case .decimalAttributeType,
                 .doubleAttributeType:
                if let p = value as? NSNumber {
                    self.setPrimitiveValue(NSNumber(value: p.doubleValue), forKey: key)
                    return
                } else
                    if let p = value as? NSString {
                        self.setPrimitiveValue(NSNumber(value: p.doubleValue), forKey: key)
                        return
                }
            case .floatAttributeType:
                if let p = value as? NSNumber {
                    self.setPrimitiveValue(NSNumber(value: p.floatValue), forKey: key)
                    return
                } else
                    if let p = value as? NSString {
                        self.setPrimitiveValue(NSNumber(value: p.floatValue), forKey: key)
                        return
                }
            case .stringAttributeType:
                if let p = value as? NSNumber {
                    self.setPrimitiveValue(NSString(string: p.stringValue), forKey: key)
                    return
                } else
                    if let p = value as? NSString {
                        self.setPrimitiveValue(p, forKey: key)
                        return
                }
            case .booleanAttributeType:
                if let p = value as? NSNumber {
                    self.setPrimitiveValue(NSNumber(value: p.boolValue), forKey: key)
                    return
                } else
                    if let p = value as? NSString {
                        self.setPrimitiveValue(NSNumber(value: p.boolValue), forKey: key)
                        return
                }
            case .dateAttributeType:
                if let p = value as? NSNumber {
                    var d = type(of: self).dateFormatter.date(from: p.stringValue)
                    if (d == nil) {
                        d = Date(timeIntervalSince1970: p.doubleValue)
                    }
                    self.setPrimitiveValue(d, forKey: key)
                    return
                } else
                    if let p = value as? String {
                        self.setPrimitiveValue(type(of: self).dateFormatter.date(from: p), forKey: key)
                        return
                }
            case .binaryDataAttributeType:
                //self.setPrimitiveValue(value, forKey: key)
                fatalError("need implementation")
            default:
                self.setNilValueForKey(key)
            }
        }
    }
    
    private func isIntegerAttributeType(_ attrType: NSAttributeType) -> Bool {
        return attrType == NSAttributeType.integer16AttributeType || attrType == NSAttributeType.integer32AttributeType || attrType == NSAttributeType.integer64AttributeType
    }
    
    private static var dateFormatter: DateFormatter {
        if _dateFormatter == nil {
            _dateFormatter = DateFormatter()
            _dateFormatter!.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        }
        return _dateFormatter!
    }
    private static var _dateFormatter: DateFormatter?
}

