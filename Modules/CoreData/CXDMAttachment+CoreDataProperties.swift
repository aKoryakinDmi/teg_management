//
//  CXDMAttachment+CoreDataProperties.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.04.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CXDMAttachment {

    @NSManaged var contentType: String?
    @NSManaged var isDownloaded: NSNumber?
    @NSManaged var isDownloading: NSNumber?
    @NSManaged var name: String?
    @NSManaged var uid: String?
    @NSManaged var url: String?

}
