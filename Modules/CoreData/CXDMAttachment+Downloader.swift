//
//  CXDMAttachment+Downloader.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.04.16.
//
//

import Foundation

import CoreData

extension CXDMAttachment: CXDownloadableAttachmentProtocol {
    
    var isNeedDownload: Bool {
        get {
            return (self.isDownloading?.boolValue == false && self.isDownloaded?.boolValue == false)
        }
    }
    
    var filePath: URL? {
        get {
            if let uid = self.uid {
                return type(of: self).folderPath.appendingPathComponent(uid)
            }
            return nil
        }
    }
    
    class var folderPath: URL {
        get {
            return URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("/Documents/Attachments/")
        }
    }
    
}
