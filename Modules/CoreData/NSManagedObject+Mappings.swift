//
//  NSManagedObject+Mappings.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import Foundation
import CoreData

public extension NSManagedObject {
    
    public class func mappings() -> [String: String] {
        return [String: String]()
    }
    
    public static func key(forRemote remote: String, context: NSManagedObjectContext) -> String {
        if let s = cachedMappings[remote] {
            return s
        }
        
        if let mapping = self.mapping(),
            let localKey = mapping[remote] {
            _cachedMappings![remote] = localKey
            return localKey
        }
        
        let entity = NSEntityDescription.entity(forEntityName: self.entityName, in: context)!
        let properties = entity.propertiesByName
        if properties[remote] != nil {
            _cachedMappings![remote] = remote
            return remote
        }
        
        _cachedMappings![remote] = remote
        return remote
    }
    
    private static var cachedMappings: [String: String] {
        if let m = _cachedMappings {
            return m
        } else {
            var m = [String: String]()
            for (key, value) in mappings() {
                m[value] = key
            }
            _cachedMappings = m
            return m
        }
    }
    
    private static var _cachedMappings: [String: String]?
    
    //    private static func generateObject(rel: NSRelationshipDescription, dict: [String: AnyObject], context: NSManagedObjectContext) -> NSManagedObject {
    //        let entity = rel.destinationEntity!
    //
    //        let cls: NSManagedObject.Type = NSClassFromString(entity.managedObjectClassName) as! NSManagedObject.Type
    //        return cls.findOrCreate(dict, context: context)
    //    }
    //
    //    public static func primaryKey() -> String {
    //        NSException(name: "Primary key undefined in " + NSStringFromClass(self), reason: "Override primaryKey if you want to support automatic creation, otherwise disable this feature", userInfo: nil).raise()
    //        return ""
    //    }
}
