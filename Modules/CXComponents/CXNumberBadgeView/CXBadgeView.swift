//
//  CXBadgeView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 09.04.16.
//
//

import Foundation

class NIB_CXBadgeView: CXBadgeView {
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        let obj = super.awakeAfter(using: aDecoder)
        return self.replacementObject(object: obj, forClass: CXBadgeView.self)
    }
}

public class CXBadgeView: UIView {
    public var text: String? {
        get { return self.label.text }
        set(value) { self.label.text = value }
    }
    public var textColor: UIColor! {
        get { return self.label.textColor }
        set(value) { self.label.textColor = value }
    }
    public var highlightedTextColor: UIColor? {
        get { return self.label.highlightedTextColor }
        set(value) { self.label.highlightedTextColor = value }
    }
    public var font: UIFont! {
        get { return self.label.font }
        set(value) { self.label.font = value }
    }
    public var alignment: CXBadgeViewAlignment = .Center {
        didSet {
            updateAlignment()
        }
    }
    private func updateAlignment() {
//        if #available(iOS 8.0, *) {
//            wrapperLeadingConstraint?.active = (alignment == .Left)
//            wrapperCenterXConstraint?.active = (alignment == .Center)
//            wrapperTrailingConstraint?.active = (alignment == .Right)
//        } else {
            wrapperLeadingConstraint?.priority = (alignment == .Left) ? 1000 : 1
            wrapperCenterXConstraint?.priority = (alignment == .Center) ? 1000 : 1
            wrapperTrailingConstraint?.priority = (alignment == .Right) ? 1000 : 1
//        }
        self.setNeedsLayout()
        
    }
    public var badgeBackgroundColor: UIColor? {
        get { return self.wrapperView.backgroundColor }
        set(value) { self.wrapperView.backgroundColor = value }
    }
    
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var wrapperView: UIView!
    
    @IBOutlet weak var labelHeihgtIndentConstraint: NSLayoutConstraint!
    @IBOutlet private weak var labelWidthIndentConstraint: NSLayoutConstraint!
    @IBOutlet private weak var wrapperLeadingConstraint: NSLayoutConstraint?
    @IBOutlet private weak var wrapperCenterXConstraint: NSLayoutConstraint?
    @IBOutlet private weak var wrapperTrailingConstraint: NSLayoutConstraint?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        fatalError("Cannot be instantiated from code")
    }
    
//    class func loadWithNib(nibName: String? = nil, owner: AnyObject? = nil, options: [NSObject : AnyObject]? = nil) -> CXBadgeView? {
//        let nibName = nibName ?? "CXBadgeView"
//        let objectsList = NSBundle.mainBundle().loadNibNamed(nibName, owner: owner, options: options)
//        if let badge = objectsList[0] as? CXBadgeView {
//            return badge
//        }
//        return nil
//    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setNeedsLayout()
        updateAlignment()
    }
    
    func viewDidLoad() {
        
    }
    
    
    public override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        let result = super.awakeAfter(using: aDecoder)
        wrapperLeadingConstraint?.constant = 0
        wrapperCenterXConstraint?.constant = 0
        wrapperTrailingConstraint?.constant = 0
        alignment = .Center
        return result
    }
    
    private func setup() {
//        defer {
//        wrapperLeadingConstraint.constant = 0
//        wrapperCenterXConstraint.constant = 0
//        wrapperTrailingConstraint.constant = 0
//        alignment = .Center
//        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let wrapperHeight = wrapperView.frame.height;
        wrapperView.layer.cornerRadius = wrapperHeight / 2.0;
    }
    
}

@objc public enum CXBadgeViewAlignment : Int {
    case Left
    case Center
    case Right
}
