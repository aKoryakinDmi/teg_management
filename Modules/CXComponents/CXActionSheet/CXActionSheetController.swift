//
//  CXActionSheetController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 10.04.16.
//
//

import Foundation

internal class CXActionSheetController: UIViewController, UIViewControllerTransitioningDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet private(set) weak var wrapperView: UIView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var wrapperViewHeightConstraint: NSLayoutConstraint!
    private var cellReuseIdentifier: String = "defaultCXActionSheetCell"
    internal var items: [CXSheetActionItem]!
    internal var selectRowAtIndexPathHandler: ((_ indexPath: Int) -> ())?

    var wrapperViewHeight: CGFloat {
        get { return wrapperViewHeightConstraint.constant }
        set(value) { wrapperViewHeightConstraint.constant = value }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalPresentationStyle = UIModalPresentationStyle.custom
        self.transitioningDelegate = self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        if let contentView = self.view as? CXActionSheetControllerContentView {
            contentView.canCatchHitTestHandler = { () -> (Bool) in
                self.hideSheet()
                return true
            }
        }
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 61
    }
    
    func registerNib(nib: UINib) {
        self.cellReuseIdentifier = "customCXActionSheetCell"
        self.tableView.register(nib, forCellReuseIdentifier: self.cellReuseIdentifier)
        if self.view.window != nil {
            self.tableView.reloadData()
        }
    }

    private func hideSheet(handleItemIndex index: Int? = nil) {
        self.dismiss(animated: true) {
            if let handler = self.selectRowAtIndexPathHandler,
                let index = index {
                handler(index)
            }
        }
    }

    // MARK: UIViewControllerTransitioningDelegate Methods
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CXActionSheetTransitioningAnimation(isPresenting: true)
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CXActionSheetTransitioningAnimation(isPresenting: false)
    }
    
    // MARK: UITableViewDelegate & DataSource Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if #available(iOS 9.0, *) {
            return tableView.rowHeight
        } else {
            return tableView.estimatedRowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) {
            if let actionItemCell = cell as? CXSheetActionItemCell {
                actionItemCell.setActionItem(item: self.items[indexPath.row])
            } else {
                fatalError("Cell mast be comfort to protocol CXActionItemCell")
            }
            return cell
        }
        fatalError("Cell not fond for Identifier \(cellReuseIdentifier)")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.hideSheet(handleItemIndex: indexPath.row)
//        self.hideSheet()
//        if let handler = self.selectRowAtIndexPathHandler {
//            handler(indexPath: indexPath.row)
//        }
    }
}


