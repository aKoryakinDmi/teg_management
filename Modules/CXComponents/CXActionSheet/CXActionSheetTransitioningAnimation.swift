//
//  CXActionSheetTransitioningAnimation.swift
//  cx4i
//
//  Created by EvGeniy Lell on 11.04.16.
//
//

import Foundation

class CXActionSheetTransitioningAnimation : NSObject, UIViewControllerAnimatedTransitioning {
    
    let isPresenting: Bool
    
    init(isPresenting: Bool) {
        self.isPresenting = isPresenting
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        if (isPresenting) {
            return 0.45
        } else {
            return 0.25
        }
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if (isPresenting) {
            self.presentAnimateTransition(transitionContext: transitionContext)
        } else {
            self.dismissAnimateTransition(transitionContext: transitionContext)
        }
    }
    
    func presentAnimateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let alertController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as! CXActionSheetController
        let containerView = transitionContext.containerView
        
        alertController.wrapperView.alpha = 0.0
        alertController.wrapperView.transform = CGAffineTransform(translationX: 0, y: alertController.wrapperView.frame.height)
        
        containerView.addSubview(alertController.view)
        
        UIView.animate(withDuration: 0.25,
                       animations: {
                        alertController.wrapperView.alpha = 1.0
                        let bounce = alertController.wrapperView.frame.height / 480 * 10.0 + 10.0
                        alertController.wrapperView.transform = CGAffineTransform(translationX: 0, y: -bounce)
        },
                       completion: { finished in
                        UIView.animate(withDuration: 0.2,
                                       animations: {
                                        alertController.wrapperView.transform = CGAffineTransform.identity
                        },
                                       completion: { finished in
                                        if (finished) {
                                            transitionContext.completeTransition(true)
                                        }
                        })
        })
    }
    
    func dismissAnimateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let alertController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as! CXActionSheetController
        
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
                       animations: {
                        alertController.wrapperView.alpha = 0.0
                        alertController.wrapperView.transform = CGAffineTransform(translationX: 0, y: alertController.wrapperView.frame.height)
        },
                       completion: { finished in
                        transitionContext.completeTransition(true)
        })
    }
}
