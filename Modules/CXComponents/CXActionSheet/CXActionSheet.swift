//
//  CXActionSheet.swift
//  cx4i
//
//  Created by EvGeniy Lell on 10.04.16.
//
//

import Foundation

public class CXActionSheet {
    private typealias internalType = CXActionSheetController

    let rootController: UIViewController
    let actionSheetController: CXActionSheetController
    
    var didSelectItemAtIndexHandler: ((_ indexPath: Int) -> ())? {
        get {
            return actionSheetController.selectRowAtIndexPathHandler
        }
        set(value) {
            actionSheetController.selectRowAtIndexPathHandler = value
        }
    }


    public required init?(controller: UIViewController, items: [CXSheetActionItem], storyboard: UIStoryboard? = nil) {
        if let internalStoriboard = (storyboard != nil) ? storyboard : UIStoryboard(name: "CXActionSheetController", bundle: nil) {
            if let internalController = internalStoriboard.instantiateInitialViewController() as? internalType {
                rootController = controller
                actionSheetController = internalController
                actionSheetController.items = items
            } else { return nil }
        } else { return nil }
    }
    
    public func registerNib(nib: UINib) {
        actionSheetController.registerNib(nib: nib)
    }
    
    public func present() {
        let wvHight = CGFloat(min((actionSheetController.items.count * 61 + 56),300))
        actionSheetController.wrapperViewHeight = wvHight
        rootController.present(actionSheetController, animated: true, completion: nil)
    }
}
