//
//  CXActionSheetItemCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 12.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXActionSheetItemCell: UITableViewCell, CXSheetActionItemCell {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont(SFUIType: .Regular, size: 20)
        titleLabel.textColor = MSGColor.text
    }
    
    func setActionItem(item: CXSheetActionItem) {
        iconView.image = item.icon
        titleLabel.text = item.title
    }
}