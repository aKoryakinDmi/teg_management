//
//  ApnController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 24.05.16.
//
//

import Foundation

//extension PrintLog { func set_ApnController_LogLevel() { setLevel(.Debug) } }

private let ApnController_SharedInstance = ApnController()

@objc class ApnController : NSObject {
    static let sharedInstance = ApnController_SharedInstance
    private (set) var deviceToken: String?
    
    
    func initPushNotifications(application: UIApplication) {
        if #available(iOS 8.0, *) {
            let categories: Set<UIUserNotificationCategory>?
            if #available(iOS 9.0, *) {
                categories = [
                    newMessageCategory(),
                ]
            } else {
                categories = nil
            }
            
            let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: categories)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            // Fallback on earlier versions
            let type: UIRemoteNotificationType = [.sound, .alert, .badge]
            application.registerForRemoteNotifications(matching: type)
        }
    }
    
    func didRegisterForRemoteNotificationsWithDeviceToken(deviceToken: NSData) {
        var tokenString = deviceToken.description.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        tokenString = tokenString.replacingOccurrences(of: " ", with: "")
        
        self.deviceToken = tokenString
        
        printLog(items: "APN token \(tokenString)")
    }
    
    func didFailToRegisterForRemoteNotificationsWithError(error: NSError) {
        printLog(items: "Applicatiion. didFailToRegisterForRemoteNotificationsWithError. \(error.localizedDescription)")
        
        printLog(items: "Can't register app to receive push notification. Reason: \(error.localizedDescription)")
        
    }
    
    func didReceiveRemoteNotification(userInfo: [AnyHashable: Any]) {
        printLog(items: "remote notification: \(userInfo.description)")
        if let aps = userInfo["aps"] as? [String: AnyObject],
            let category = aps["category"] as? String {
            switch category {
            case ApnControllerNewMessageSilentCategoryIdentifier:
                handleNewMessageSilent(userInfo: userInfo)
            case ApnControllerNewMessageCategoryIdentifier:
                handleNewMessage(userInfo: userInfo)
            default:
                printLog(items: "Unknown category: \(category)")
                break
            }
        }
    }
    
    func handleActionWithIdentifier(identifier: String?, userInfo: [AnyHashable: Any], withResponseInfo responseInfo: [AnyHashable: Any]) {
        printLog(items: "handle Action: \(String(describing: identifier))", userInfo.description, responseInfo)
        if #available(iOS 9.0, *) {
            if let identifier = identifier {
                switch identifier {
                case ApnControllerInlineMessageReplyActionIdentifier:
                    handleInlineChatReply(userInfo: userInfo, withResponseInfo: responseInfo)
                default:
                    printLog(items: "Unknown identifier: \(identifier)")
                    break
                }
            }
        }
    }
    
}

