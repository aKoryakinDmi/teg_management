//
//  ApnController+Localized.swift
//  cx4i
//
//  Created by EvGeniy Lell on 09.06.16.
//
//

import Foundation

extension CXLocalized {
    class var apnController: CXLTApnController.Type {
        get { return CXLTApnController.self }
    }
}

class CXLTApnController: CXLocalized.Table {
}

extension CXLTApnController {
    static func newMessageReplyTitle() -> String {
        let result = localized()
        return result
    }
//    static func newMessageReplyTitleWithCount(count: String, price: String) -> String {
//        let result = localized()
//        return result
//    }
//    static func newMessageReplyTitle(count count: String, price: String) -> String {
//        let result = localized()
//        return result
//    }    
}

