//
//  ApnController+UserNotificationCategory.swift
//  cx4i
//
//  Created by EvGeniy Lell on 26.05.16.
//
//

import Foundation

// MARK: - UserNotificationCategory
let ApnControllerInlineMessageReplyActionIdentifier = "INLINE_MESSAGE_REPLY_ACTION"
let ApnControllerNewMessageCategoryIdentifier = "NEW_MESSAGE_CATEGORY"
let ApnControllerNewMessageSilentCategoryIdentifier = "NEW_MESSAGE_SILENTCATEGORY"

private let Localized = CXLocalized.apnController

extension ApnController {
    
    @available(iOS 9.0, *)
    func newMessageCategory() -> UIMutableUserNotificationCategory {

        let replyAction = UIMutableUserNotificationAction()
        replyAction.title = Localized.newMessageReplyTitle()
        replyAction.identifier = ApnControllerInlineMessageReplyActionIdentifier
        replyAction.activationMode = .background
        replyAction.isAuthenticationRequired = false
        replyAction.behavior = .textInput
        
        let category = UIMutableUserNotificationCategory()
        category.identifier = ApnControllerNewMessageCategoryIdentifier
        category.setActions([
            replyAction,
            ], for: UIUserNotificationActionContext.default)
        
        return category
    }
    
    func newMessageLocalNotification(alertBody: String, userInfo: [AnyHashable : Any]? = nil) {
        let localNotification = UILocalNotification()
        localNotification.fireDate = Date(timeIntervalSinceNow: 3)
        localNotification.timeZone = NSTimeZone.local
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.alertBody = alertBody
        localNotification.userInfo = userInfo
        if #available(iOS 9.0, *) {
            localNotification.category = ApnControllerNewMessageCategoryIdentifier
        } else {
            // Fallback on earlier versions
        }
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
    }

    
    @available(iOS 9.0, *)
    func handleInlineChatReply(userInfo: [AnyHashable : Any], withResponseInfo responseInfo: [AnyHashable : Any]) {
        if let payload = userInfo["payload"] as? [String: Any],
            let userHash = payload["to"] as? String,
            let contactHash = payload["from"] as? String,
            let responseText = responseInfo[UIUserNotificationActionResponseTypedTextKey] as? String {
            let chat = CXMessengerChatMachine.sharedInstance
            if let userUid = chat.user.uid, userUid == userHash {
                chat.createAndSilentSendMessage(forContactHash: contactHash, userHash: userHash, text: responseText)
            }
        }
    }
    
    func handleNewMessage(userInfo: [AnyHashable : Any]) {
        if let payload = userInfo["payload"] as? [String: Any],
            let userHash = payload["to"] as? String,
            let contactHash = payload["from"] as? String {
            printLog(items: userHash, contactHash)
            
            
            guard UIApplication.shared.applicationState == UIApplicationState.inactive && AppLogin.sharedInstance.logined else {
                    return
            }

            CXMessengerChatMachine.sharedInstance.appendUnreadMessages(toContactHash: contactHash, withUserHash: userHash)
            CXMessengerChatMachine.sharedInstance.updateApplicationBadge()
            
            func openChat(messengerNavigationController: CXMessengerNavigationController) {
                let childs = messengerNavigationController.childViewControllers
                for controller in childs.reversed() {
                    if let roster = controller as? CXMessengerRosterController {
                        messengerNavigationController.popToViewController(controller, animated: true)
                        roster.toChat(contactHash: contactHash)
                        break
                    } else
                        if let chat = controller as? CXMessengerChatController, chat.contact?.uid == contactHash  {
                            messengerNavigationController.popToViewController(controller, animated: true)
                            break
                    }
                }
            }

            if AppLogin.sharedInstance.logined {
                #if TypeProgect_cx4i
                    let rootViewController = cx4iAppDelegate.getCX4iApplication().window.rootViewController
                    if let messengerNavigationController = rootViewController as? CXMessengerNavigationController {
                        openChat(messengerNavigationController)

                    } else {
                        cx4iAppDelegate.getCX4iApplication().toMessages(rootViewController)
                        let rootViewController = cx4iAppDelegate.getCX4iApplication().window.rootViewController
                        if let messengerNavigationController = rootViewController as? CXMessengerNavigationController,
                            let roster = messengerNavigationController.childViewControllers.first as? CXMessengerRosterController {
                            roster.toChat(contactHash: contactHash)
                        }
                    }
                #elseif TypeProgect_TEG_management
                    if let mainTabBarController = AppDelegate.sharedInstance.mainTabBarController() {
                        mainTabBarController.selectedIndex = CXTabChat
                        if let messengerNavigationController = mainTabBarController.selectedViewController as? CXMessengerNavigationController {
                            openChat(messengerNavigationController: messengerNavigationController)
                        }
                    }
                #endif
            }
        }
    }
    
    func handleNewMessageSilent(userInfo: [AnyHashable : Any]) {
        if let payload = userInfo["payload"] as? [String: Any],
//            let userHash = payload["to"] as? String,
//            let contactHash = payload["from"] as? String,
            let text = payload["text"] as? String,
            let name = payload["name"] as? String {
            let alertBody = "\(UIApplication.shared.applicationState.rawValue): \(text)"
            newMessageLocalNotification(alertBody: alertBody, userInfo: payload)
            if UIApplication.shared.applicationState != .active {
                let alertBody = "\(name): \(text)"
                newMessageLocalNotification(alertBody: alertBody, userInfo: payload)
            }
        }
    }
}
