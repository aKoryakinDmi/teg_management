//
//  CXNavigationDropdownMenuViewController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.04.16.
//
//

import Foundation

typealias CXNDMViewController = CXNavigationDropdownMenuViewController

internal class CXNavigationDropdownMenuViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    //MARK: - Interface Builder
    @IBOutlet private      var titleView: CXNavigationDropdownMenuTitleView!
    @IBOutlet private      var defaultTitleContentView: CXNavigationDropdownTitleContentView!
    @IBOutlet private weak var wrapperView: UIView!
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var tableView: CXNavigationDropdownMenuTableView!
    @IBOutlet private weak var topSeparatorView: UIView!
    
    @IBAction private func tapOnTitleViewAction(_ sender: Any) {
        if isAnimated == false {
            if items.count > 1{
                isShown == true ? hideMenu() : showMenu()
            } else {
                if let handler = selectRowAtIndexPathHandler {
                    handler(self, 0)
                }
            }
        }
    }
    
    @IBAction private func tapOnWrapperViewAction(_ sender: Any) {
        hideMenu()
    }
    
    @IBOutlet private weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var wrapperViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topSeparatorHeightConstraint: NSLayoutConstraint!
    //MARK: - Properties
    private var contentViewTopConstraint: NSLayoutConstraint?
    private var cellReuseIdentifier: String = "defaultCXDropdownMenuCell"
    
    
    //MARK: configuration
    private(set) var configuration: CXNDMConfiguration = CXNDMConfiguration() {
        didSet {
            updateConfiguration()
        }
    }
    
    func updateConfiguration() {
        wrapperView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        titleView.updateConfiguration()
        tableView.tableHeaderView?.backgroundColor = configuration.color.withAlphaComponent(configuration.opacity)
        topSeparatorView.backgroundColor = configuration.topSeparatorColor
        topSeparatorHeightConstraint.constant = 0.5
    }
    
    
    private(set) var items: [CXDropdownItem]! {
        didSet {
            titleView.hiddenArrow = (items.count <= 1)
            for (index, item) in items.enumerated() {
                if item.checkmarked == true {
                    selectedIndexPath = index;
                    return
                }
            }
            // else
            selectedIndexPath = 0
        }
    }
    
    private var selectedIndexPath: Int! {
        didSet {
            updateTitleView()
        }
    }

    private var storeSelectedIndexPath: Bool = true
    
    private(set) var isShown: Bool!
    private(set) var isAnimated: Bool!
    private(set) weak var relationNavigationController: UINavigationController?
    
    typealias SelectRowAtIndexPathHandler = (_ menu: CXNavigationDropdownMenuViewController, _ indexPath: Int) -> ()
    var selectRowAtIndexPathHandler: (SelectRowAtIndexPathHandler)?
    
    //MARK: - Bind
    func bind(controller: UIViewController, items aItems: [CXDropdownItem]) {
        
        relationNavigationController = controller.navigationController
        items = aItems
        isShown = false
        isAnimated = false
        
        putControllerViewOnWindow()
        
        if let contentView = view as? CXNavigationDropdownMenuContentView {
            weak var blockSelf = self
            contentView.canCatchHitTestHandler = { () -> (Bool) in
                if blockSelf?.isShown == true && blockSelf?.isAnimated == false {
                    blockSelf?.hideMenu()
                    return false
                }
                return true
            }
        }
        
        defaultTitleContentView.titleColor = navigationTitleColor
        setTitleContentView(view: defaultTitleContentView)
        updateTitleView(configuration: configuration)
        controller.navigationItem.titleView = titleView
        
        makeTableViewViews()
        
        hideMenu(animation: false)
        
        updateConfiguration()
    }
    
    func setTitleContentView(view: CXNavigationDropdownTitleContentView) {
        titleView.contentView = view
    }
    
    func registerNib(nib: UINib) {
        cellReuseIdentifier = "customCXDropdownMenuCell"
        tableView.register(nib, forCellReuseIdentifier: cellReuseIdentifier)
        if view.window != nil {
            tableView.reloadData()
        }
    }
    
    func setItemHeight(height: CGFloat) {
        tableView.rowHeight = height
    }
    
    func setStoredSelected(value: Bool) {
        storeSelectedIndexPath = value
    }
    
    func update(items aItems: [CXDropdownItem]) {
        items = aItems
        tableView.reloadData()
        if let handler = selectRowAtIndexPathHandler {
            handler(self, 0)
        }
    }
    
    var navigationTitleColor: UIColor = UIColor.black {
        didSet {
            defaultTitleContentView.titleColor = navigationTitleColor
            titleView.contentView?.titleColor = navigationTitleColor
        }
    }



    
    //MARK: Helpers
    func putControllerViewOnWindow() {
        let window = UIApplication.shared.keyWindow!
        window.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let bottom =
            NSLayoutConstraint(item: view,
                               attribute: NSLayoutAttribute.bottom,
                               relatedBy: NSLayoutRelation.equal,
                               toItem: window,
                               attribute: NSLayoutAttribute.bottom,
                               multiplier: 1.0,
                               constant: 0)
        let top =
            NSLayoutConstraint(item: view,
                               attribute: NSLayoutAttribute.top,
                               relatedBy: NSLayoutRelation.equal,
                               toItem: window,
                               attribute: NSLayoutAttribute.top,
                               multiplier: 1.0,
                               constant: 0)
        let left =
            NSLayoutConstraint(item: view,
                               attribute: NSLayoutAttribute.left,
                               relatedBy: NSLayoutRelation.equal,
                               toItem: window,
                               attribute: NSLayoutAttribute.left,
                               multiplier: 1.0,
                               constant: 0)
        let right =
            NSLayoutConstraint(item: view,
                               attribute: NSLayoutAttribute.right,
                               relatedBy: NSLayoutRelation.equal,
                               toItem: window,
                               attribute: NSLayoutAttribute.right,
                               multiplier: 1.0,
                               constant: 0)
        
        window.addConstraints([top,bottom,left,right]);
        contentViewTopConstraint = top;
    }
    
    func updateTitleView(configuration: CXNDMConfiguration? = nil) {
        titleView.title = items[selectedIndexPath].title;
        if let newConfiguration = configuration {
            titleView.configuration = newConfiguration;
        }
    }
    
    //MARK: Menu
    func showMenu(animation:Bool = true) {
        isShown = true
        wrapperView.isHidden = false
        view.isHidden = false
        //      contentViewTopConstraint?.constant = relationNavigationController!.navigationBar.frame.maxY
        if let relationNavigationController = relationNavigationController {
            wrapperViewTopConstraint?.constant = relationNavigationController.navigationBar.frame.maxY
            titleView.rotateArrowToOpenPosition(animation: animation)
            
            tableView.reloadData()
            //      wrapperView.superview?.bringSubviewToFront(wrapperView)
            
            if animation {
                isAnimated = true
                // Animation
                backgroundView.alpha = 0
                tableViewTopConstraint.constant = tableTopPositionClose()
                tableView.layoutIfNeeded()
                tableViewTopConstraint.constant = tableTopPositionOpen()
                weak var blockSelf = self
                UIView.animate(
                    withDuration: configuration.animationDuration * 1.5,
                    delay: 0,
                    usingSpringWithDamping: 0.7,
                    initialSpringVelocity: 0.5,
                    options: [],
                    animations: {
                        if let blockSelf = blockSelf {
                            blockSelf.tableView.layoutIfNeeded()
                            blockSelf.backgroundView.alpha = blockSelf.configuration.backgroundOpacity
                        }
                    }, completion: { _ in
                        blockSelf?.isAnimated = false
                    }
                )
            } else {
                tableViewTopConstraint.constant = tableTopPositionOpen()
                tableView.layoutIfNeeded()
                backgroundView.alpha = configuration.backgroundOpacity
            }
        }
    }
    
    func hideMenu(animation:Bool = true) {
        isShown = false
        titleView.rotateArrowToClosePosition(animation: animation)
        backgroundView.alpha = configuration.backgroundOpacity
        
        if animation {
            isAnimated = true
            // Animation
            tableViewTopConstraint.constant = tableTopPositionOpen()
            tableView.layoutIfNeeded()
            tableViewTopConstraint.constant = tableTopPositionOpen() * 0.6
            weak var blockSelf = self
            UIView.animate(
                withDuration: configuration.animationDuration * 1.5,
                delay: 0,
                usingSpringWithDamping: 0.7,
                initialSpringVelocity: 0.5,
                options: [],
                animations: {
                    blockSelf?.tableView.layoutIfNeeded()
                }, completion: { _ in
                    blockSelf?.isAnimated = false
                }
            )
            // Animation
            tableViewTopConstraint.constant = tableTopPositionClose()
            UIView.animate(withDuration: configuration.animationDuration, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                blockSelf?.tableView.layoutIfNeeded()
                blockSelf?.backgroundView.alpha = 0
                }, completion: { _ in
                    blockSelf?.wrapperView.isHidden = true
                    blockSelf?.view.isHidden = true;
                }
            )
        } else {
            tableViewTopConstraint.constant = tableTopPositionClose()
            tableView.layoutIfNeeded()
            wrapperView.isHidden = true
            view.isHidden = true;
        }
    }
    
    //MARK: - UIGestureRecognizer Delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isKind(of: CXNDMWrapperView.self) != true) {
            return false
        }
        return true
    }
    
    //MARK: - UITableView Helpers
    func makeTableViewViews() {
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 300))
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func tableViewHeaderHeight() -> CGFloat {
        if let view = tableView.tableHeaderView {
            return view.frame.height
        }
        return CGFloat(0)
    }
    
    func tableTopPositionClose() -> CGFloat {
        return -(CGFloat(items.count) * tableView.rowHeight) - tableViewHeaderHeight()
    }
    
    func tableTopPositionOpen() -> CGFloat {
        return -tableViewHeaderHeight()
    }
    
    //MARK: UITableView DataSource & Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) {
            if let actionItemCell = cell as? CXDropdownItemCell {
                if var defcell = cell as? CXNDMConfigurationProtocol {
                    defcell.configuration = configuration
                }
                actionItemCell.setActionItem(item: items[indexPath.row])
            } else {
                fatalError("Cell mast be comfort to protocol CXActionItemCell")
            }
            return cell
        }
        fatalError("Cell not fond for Identifier \(cellReuseIdentifier)")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndexPath != indexPath.row {
            selectedIndexPath = indexPath.row
            let selectedItem = items[indexPath.row]
            for item in items {
                if selectedItem === item {
                    item.checkmarked = true
                } else {
                    item.checkmarked = false
                }
            }
            if let handler = selectRowAtIndexPathHandler {
                handler(self, indexPath.row)
            }
            tableView.reloadData()
            updateTitleView()
        }
        if !storeSelectedIndexPath {
            if let handler = selectRowAtIndexPathHandler {
                handler(self, indexPath.row)
            }
        }
        hideMenu()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        items[indexPath.row].checkmarked = false
    }   
}
