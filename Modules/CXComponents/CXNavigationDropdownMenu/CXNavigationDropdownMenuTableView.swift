//
//  CXNavigationDropdownMenuTableView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.04.16.
//
//

import Foundation

typealias CXNDMTableView = CXNavigationDropdownMenuTableView

class CXNavigationDropdownMenuTableView: UITableView {
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if let hitView = hitView {
            if hitView.superview?.isKind(of: UITableViewCell.self) == true ||
                hitView.superview?.superview?.isKind(of: UITableViewCell.self) == true {
                return hitView;
            }
        }
        return nil;
    }
    
}
