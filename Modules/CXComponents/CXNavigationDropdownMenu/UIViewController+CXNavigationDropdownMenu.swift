//
//  UIViewController+CXNavigationDropdownMenu.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.04.16.
//
//

import Foundation

extension UIViewController {
   var topPresentedViewController: UIViewController? {
      var target: UIViewController? = self
      while (target?.presentedViewController != nil) {
         target = target?.presentedViewController
      }
      return target
   }
   var topVisibleViewController: UIViewController? {
      if let navigation = self as? UINavigationController {
         if let visibleViewController = navigation.visibleViewController {
            return visibleViewController.topVisibleViewController
         }
      }
      if let tab = self as? UITabBarController {
         if let selectedViewController = tab.selectedViewController {
            return selectedViewController.topVisibleViewController
         }
      }
      return self
   }
   var topMostViewController: UIViewController? {
      return self.topPresentedViewController?.topVisibleViewController
   }
}