//
//  CXNavigationDropdownMenuConfiguration.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

typealias CXNDMConfiguration = CXNavigationDropdownMenuConfiguration

protocol CXNDMConfigurationProtocol {
    var configuration: CXNDMConfiguration? { get set }
}

class CXNavigationDropdownMenuConfiguration {
   
   var titleColor: UIColor!
   var color: UIColor!
   var highlightedColor: UIColor!
   var opacity: CGFloat!
   var backgroundColor: UIColor!
   var backgroundOpacity: CGFloat!
   var itemTextColor: UIColor!
   var selectedItemTextColor: UIColor!
   var animationDuration: TimeInterval!
   var topSeparatorColor: UIColor!

   init() {
      self.defaultValue()
   }
   
   func defaultValue() {
      self.titleColor = UIColor.black
      self.color = MSGColor.navigationBarView
      self.highlightedColor = UIColor.yellow
      self.opacity = CGFloat(0.95)
      self.backgroundColor = UIColor.darkGray
      self.backgroundOpacity = CGFloat(0.95)
      self.itemTextColor = MSGColor.navigationBarText
      self.selectedItemTextColor = MSGColor.navigationBarSelectedText
      self.animationDuration = TimeInterval(0.5)
      self.topSeparatorColor = MSGColor.navigationBarSepatator
   }
}
