//
//  CXNavigationDropdownMenuWrapperView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.04.16.
//
//

import Foundation

typealias CXNDMWrapperView = CXNavigationDropdownMenuWrapperView

class CXNavigationDropdownMenuWrapperView: UIView {

}

typealias CXNDMContentView = CXNavigationDropdownMenuContentView

class CXNavigationDropdownMenuContentView: UIView {
    //
    var canCatchHitTestHandler: (() -> (Bool))?
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if hitView == self && self.canCatchHitTestHandler != nil{
            let canCatch = self.canCatchHitTestHandler!()
            if canCatch != true {
                return nil
            }
        }
        return hitView
    }
}
