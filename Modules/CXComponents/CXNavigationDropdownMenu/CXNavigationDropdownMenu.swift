//
//  CXNavigationDropdownMenu.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.04.16.
//
//

import Foundation

public class CXNavigationDropdownMenu {
    
    private typealias internalType = CXNavigationDropdownMenuViewController
    private let internalController: internalType
    unowned let rootController: UIViewController
    
    var didSelectItemAtIndexHandler: (CXNavigationDropdownMenuViewController.SelectRowAtIndexPathHandler)? {
        get {
            return internalController.selectRowAtIndexPathHandler
        }
        set(value) {
            internalController.selectRowAtIndexPathHandler = value
        }
    }
    
    
    public required init?(controller: UIViewController, items: [CXDropdownItem], storyboard: UIStoryboard? = nil) {
        if let internalStoriboard = (storyboard != nil) ? storyboard : UIStoryboard(name: "CXNavigationDropdownMenuViewController", bundle: nil) {
            if let tInternalController = internalStoriboard.instantiateInitialViewController() as? internalType {
                rootController = controller
                internalController = tInternalController
                internalController.bind(controller: controller, items: items)
            } else { return nil }
        } else { return nil }
    }
    
    func setTitleContentView(view: CXNavigationDropdownTitleContentView) {
        internalController.setTitleContentView(view: view)
    }

    public func registerNib(nib: UINib) {
        internalController.registerNib(nib: nib)
    }

    public func setItemHeight(height: CGFloat) {
        internalController.setItemHeight(height: height)
    }

    public func setStoredSelected(value: Bool) {
        internalController.setStoredSelected(value: value)
    }

    public func update(items: [CXDropdownItem]) {
        internalController.update(items: items)
    }

    public var titleColor: UIColor {
        get {
            return internalController.navigationTitleColor
        }
        set(value){
            internalController.navigationTitleColor = value
        }
    }

}
