//
//  CXNavigationDropdownMenuTitleView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.04.16.
//
//

import Foundation

typealias CXNDMTitleView = CXNavigationDropdownMenuTitleView


class CXNavigationDropdownTitleContentView: UIView {
    @IBOutlet private weak var titleLabel: UILabel?
    var title: String? {
        get { return self.titleLabel?.text }
        set(value) { self.titleLabel?.text = value }
    }
    var titleColor: UIColor? {
        get { return self.titleLabel?.textColor }
        set(value) { self.titleLabel?.textColor = value ?? UIColor.black }
    }
    var titleFont: UIFont? {
        get { return self.titleLabel?.font }
        set(value) { self.titleLabel?.font = value ?? UIFont.systemFont(ofSize: 20) }
    }
    func updateConfiguration() {
        
    }
}


internal class CXNavigationDropdownMenuTitleView: UIView, UIGestureRecognizerDelegate {
    
    //MARK: - Interface Builder
    @IBOutlet private weak var wrapperContentView: UIView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var centerXConstraint: NSLayoutConstraint!
    
    
    //MARK: - Property
    var contentView: CXNavigationDropdownTitleContentView? {
        get { return wrapperContentView.subviews.first as? CXNavigationDropdownTitleContentView }
        set(view) {
            wrapperContentView.removeAllSubviews()
            if let view = view {
                wrapperContentView.addSubview(view)
                view.addConstraintsToFillSuperview()
            }
        }
    }
    
    var title: String? {
        get { return contentView?.title }
        set(value) { contentView?.title = value }
    }
    
//    func setContentView(view: CXNavigationDropdownTitleContentView) {
//        wrapperContentView.removeAllSubviews()
//        wrapperContentView.addSubview(view)
//        view.addConstraintsToFillSuperview()
//    }
    
    
    //MARK: configuration
    var configuration: CXNDMConfiguration! {
        didSet {
            updateConfiguration()
        }
    }
    
    func updateConfiguration() {
        contentView?.updateConfiguration()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setOnCenter()
    }
    
    override var frame: CGRect {
        didSet {
            self.setOnCenter()
        }
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.setOnCenter()
    }
    
    func setOnCenter() {
        if #available(iOS 9.0, *) {
            if let window = self.window {
                let windowMid = window.frame.midX
                let selfMid = self.frame.midX
                var const = windowMid - selfMid
                const = min(max(const, -20), 20)
                centerXConstraint.constant = const
            } else {
                if centerXConstraint != nil {
                    centerXConstraint.constant = 0
                }
            }
        }
    }
    
    var hiddenArrow:Bool {
        get { return iconImageView.isHidden }
        set(value) { iconImageView.isHidden = value }
    }
    func rotateArrowToOpenPosition(animation:Bool = true) {
        let angle = 180 * CGFloat(M_PI/180)
        rotateArrow(angle:angle, animation: animation)
    }
    func rotateArrowToClosePosition(animation:Bool = true) {
        let angle = 0 * CGFloat(M_PI/180)
        rotateArrow(angle:angle, animation: animation)
    }
    private func rotateArrow(angle:CGFloat, animation:Bool) {
        if animation {
            UIView.animate(withDuration: self.configuration.animationDuration) {
                self.iconImageView.transform = CGAffineTransform(rotationAngle: angle)
            }
        } else {
            self.iconImageView.transform = CGAffineTransform(rotationAngle: angle)
        }
    }
}
