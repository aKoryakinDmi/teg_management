//
//  CXNavigationDropdownMenuTableViewCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.04.16.
//
//

import Foundation

typealias CXNDMTableViewCell = CXNavigationDropdownMenuTableViewCell

class CXNavigationDropdownMenuTableViewCell: UITableViewCell, CXDropdownItemCell, CXNDMConfigurationProtocol {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var checkmarkImageView: UIImageView!
    
    private var item: CXDropdownItem?
    
    //MARK: configuration
    var configuration: CXNDMConfiguration? {
        didSet {
            updateConfiguration()
        }
    }
    
    func updateConfiguration() {
        if let configuration = configuration {
            self.backgroundColor = configuration.color.withAlphaComponent(configuration.opacity)
            self.selectedBackgroundView = UIView(frame: CGRect.zero)
            self.selectedBackgroundView?.backgroundColor = configuration.color.withAlphaComponent(configuration.opacity)
        }
    }
    
    func setActionItem(item: CXDropdownItem) {
        self.item = item
        reloadData()
    }
    
    func reloadData() {
        if let item = self.item {
            self.title = item.title
            if let checkmarked = item.checkmarked {
                self.checkmarked = checkmarked
            }
        }
    }
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        set(value) {
            self.titleLabel.text = value
        }
    }
    
    var checkmarked: Bool {
        get {
            return !self.checkmarkImageView.isHidden
        }
        set(value) {
            self.checkmarkImageView.isHidden = !value
            self.titleLabel.textColor = (value == false) ? self.configuration?.itemTextColor : self.configuration?.selectedItemTextColor
        }
    }
    
}
