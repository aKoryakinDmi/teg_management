//
//  CXNavigationDropdownMenuViewController+Configuration.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.04.16.
//
//
//  var\s(.*):.*
// 
//$0 {
//   get { return self.configuration.$1 }
//   set(value) {self.configuration.$1 = value}
//}

import Foundation

extension CXNDMViewController {
   var titleColor: UIColor! {
      get { return self.configuration.titleColor }
      set(value) {self.configuration.titleColor = value}
   }
   
   var color: UIColor! {
      get { return self.configuration.color }
      set(value) {self.configuration.color = value}
   }
   
   var highlightedColor: UIColor! {
      get { return self.configuration.highlightedColor }
      set(value) {self.configuration.highlightedColor = value}
   }
   
   var opacity: CGFloat! {
      get { return self.configuration.opacity }
      set(value) {self.configuration.opacity = value}
   }
   
   var backgroundColor: UIColor! {
      get { return self.configuration.backgroundColor }
      set(value) {self.configuration.backgroundColor = value}
   }
   
   var backgroundOpacity: CGFloat! {
      get { return self.configuration.backgroundOpacity }
      set(value) {self.configuration.backgroundOpacity = value}
   }
   
   var itemTextColor: UIColor! {
      get { return self.configuration.itemTextColor }
      set(value) {self.configuration.itemTextColor = value}
   }
   
   var selectedItemTextColor: UIColor! {
      get { return self.configuration.selectedItemTextColor }
      set(value) {self.configuration.selectedItemTextColor = value}
   }
   
   var animationDuration: TimeInterval! {
      get { return self.configuration.animationDuration }
      set(value) {self.configuration.animationDuration = value}
   }

   var topSeparatorColor: UIColor! {
      get { return self.configuration.topSeparatorColor }
      set(value) {self.configuration.topSeparatorColor = value}
   }
   
}
