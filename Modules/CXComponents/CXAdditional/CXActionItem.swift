//
//  CXActionItem.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

import Foundation

//public protocol CXActionItem {
//    var title: String { get }
//    var icon: UIImage? { get }
//    var checkmarked: Bool? { get set }
//}
//
//extension CXActionItem {
//    public var icon: UIImage? {
//        get {
//            return nil
//        }
//    }
//    public var checkmarked: Bool? {
//        get {
//            return nil
//        }
//        set(value) {
//            return
//        }
//    }
//}


// Mark: - Default
public protocol CXDropdownItemCell {
    func setActionItem(item: CXDropdownItem)
}

public class CXDropdownItem {
    public var title: String
    public var checkmarked: Bool? = false
    init(title: String) {
        self.title = title
    }
}

// Mark: - Default
public protocol CXSheetActionItemCell {
    func setActionItem(item: CXSheetActionItem)
}

public class CXSheetActionItem {
    let imageName: String!
    public var title: String
    public var checkmarked: Bool? = false
    public var icon: UIImage? {
        get { return UIImage(named:self.imageName) }
    }
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}
