//
//  ByProject.swift
//  TEG management
//
//  Created by EvGeniy Lell on 13.06.16.
//

import Foundation

func byProject<T>(cx: T, tagm: T) -> T {
    #if TypeProgect_cx4i
        return cx
    #elseif TypeProgect_TEG_management
        return tagm
    #endif
}
