//
//  CXMessengerStatusBadgeView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 07.04.16.
//
//

import Foundation

enum CXMessengerStatus: String {
    case Unknown
    case Offline
    case NotAvaliable
    case AwayFromDesk
    case Avaliable

    var imageName: String {
        get {
            return rawValue
        }
    }    
}

enum CXMessengerStatusFace: String {
    case Automatic
    case Regular = "Status"
    case Bold = "BoldStatus"
}
class CXMessengerStatusBadgeView: UIImageView {
    var status:CXMessengerStatus {
        didSet { self.updateBadgeIcon() }
    }
    var statusFace:CXMessengerStatusFace {
        didSet { self.updateBadgeIcon() }
    }
    
    func updateBadgeIcon() {
        var prefix = statusFace.rawValue
        if statusFace == .Automatic {
            switch frame.height {
            case 14...19: prefix = CXMessengerStatusFace.Regular.rawValue
            case 20...44: prefix = CXMessengerStatusFace.Bold.rawValue
            default:
                fatalError("Status size no conform to automatic face")
            }
        }
        let imageName = prefix + status.imageName
        if let newImage = UIImage(named: imageName) {
            self.image = newImage
        }
    }
    
    //MARK: - Life cycle
    required init?(coder aDecoder: NSCoder) {
        self.status = .Unknown
        self.statusFace = .Regular
        super.init(coder: aDecoder)
        self.updateBadgeIcon()
    }
    
    init() {
        self.status = .Unknown
        self.statusFace = .Regular
        super.init(frame: CGRect.zero)
        self.updateBadgeIcon()
    }
    
}
