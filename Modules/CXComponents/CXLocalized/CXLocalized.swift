//
//  Localizable.swift
//  cx4i
//
//  Created by EvGeniy Lell on 09.06.16.
//
//

import Foundation


class CXLocalized {
    private class func name(selfType: String, prefix: String) -> String? {
        let name: String?
        if selfType.hasPrefix(prefix) {
            let index = selfType.index(selfType.startIndex, offsetBy: prefix.characters.count)
            name = selfType.substring(from: index)
        } else {
            name = nil
        }
        return name
    }
    
    private class func functionToKey(value: String) ->String {
        var result = value
        result = result.replacingOccurrences(of: "()", with: "")
        result = result.replacingOccurrences(of: "(_:", with: "_")
        result = result.replacingOccurrences(of: "(", with: "_")
        result = result.replacingOccurrences(of: ":)", with: "")
        result = result.replacingOccurrences(of: ":", with: "_")
        return result
    }
    
    private class func localized(key: String, tableName: String? = nil, bundle: Bundle? = nil, value: String? = nil, comment: String? = nil, arguments: [CVarArg]) -> String {
        let bundle = bundle ?? Bundle.main
        let value = value ?? ""
        let comment = comment ?? ""
        let format = NSLocalizedString(key, tableName: tableName, bundle: bundle, value: value, comment: comment)
        let result = String.init(format: format, arguments: arguments)
        print("[\(tableName ?? "")]\(key) = \(result)")
        return result
    }

    class Table {
        class Block {
            private static let blockPrefix = "CXLTB"
            class func name() -> String? {
                return CXLocalized.name(selfType: String(describing: self), prefix: blockPrefix)
            }
            class func table() -> CXLocalized.Table.Type {
                return CXLocalized.Table.self
            }
            class func localized(key: String = #function, tableName: String? = nil, bundle: Bundle? = nil, value: String? = nil, comment: String? = nil, arguments: CVarArg...) -> String {
                return localized(key: key, tableName: tableName, bundle: bundle, value: value, comment: comment, arguments: arguments)
            }
            class func localized(key: String = #function, tableName: String? = nil, bundle: Bundle? = nil, value: String? = nil, comment: String? = nil, arguments: [CVarArg]) -> String {
                let bkey: String
                if let name = name() {
                    bkey = "\(name).\(CXLocalized.functionToKey(value: key))"
                } else {
                    bkey = CXLocalized.functionToKey(value: key)
                }
                let tableName = tableName ?? table().name()
                return CXLocalized.localized(key: bkey, tableName: tableName, bundle: bundle, value: value, comment: comment, arguments: arguments)
            }
        }
        private static let tablePrefix = "CXLT"
        class func name() -> String? {
            return CXLocalized.name(selfType: String(describing: self), prefix: tablePrefix)
        }
        class func localized(key: String = #function, tableName: String? = nil, bundle: Bundle? = nil, value: String? = nil, comment: String? = nil, arguments: CVarArg...) -> String {
            return localized(key: key, tableName: tableName, bundle: bundle, value: value, comment: comment, arguments: arguments)
        }
        class func localized(key: String = #function, tableName: String? = nil, bundle: Bundle? = nil, value: String? = nil, comment: String? = nil, arguments: [CVarArg]) -> String {
            let key = CXLocalized.functionToKey(value: key)
            let tableName = tableName ?? name()
            return CXLocalized.localized(key: key, tableName: tableName, bundle: bundle, value: value, comment: comment, arguments: arguments)
        }
    }
    
}



