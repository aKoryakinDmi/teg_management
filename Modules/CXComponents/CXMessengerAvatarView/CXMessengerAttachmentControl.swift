//
//  CXMessengerAttachmentControl.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.04.16.
//
//

import Foundation

class CXMessengerAttachmentControl: UIControl {
   
   //MARK: - Properties
   var attachment:CXDMAttachment? {
      get {
         return self.view.attachment
      }
      set(value) {
         self.view.attachment = value
      }
   }
   var placeholderImageName: String {
      get {
         return self.view.placeholderImageName
      }
      set(value) {
         self.view.placeholderImageName = value
      }
   }
   var placeholderImage: UIImage? {
      get {
         return self.view.placeholderImage
      }
   }
   private(set) var view = CXMessengerAttachmentView() {
      didSet {
         
      }
   }

   //MARK: - Life cycle
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.setup()
   }
   
   init() {
      super.init(frame: CGRect.zero)
      self.setup()
   }
   
   func setup() {
//      self.view = CXMessengerAttachmentView()
      self.addSubview(self.view)
      self.view.addConstraintsToFillSuperview()
      self.backgroundColor = UIColor.clear
   }

}
