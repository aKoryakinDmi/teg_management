//
//  CXMessengerAttachmentView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.04.16.
//
//

import Foundation

public class CXMessengerAttachmentView: UIImageView {
    //TODO: replace it
    //FIXME: replace it
    let APIAttachmentDownloadedNotification = "APIAttachmentDownloadedNotification"//???
    
    //MARK: - Properties
    var attachment: CXDMAttachment? {
        didSet {
            self.addObserverIfNeed()
            self.setImageIfCan()
        }
        
    }
    
    var placeholderImageName: String! {
        didSet {
            self.setImageIfCan()
        }
    }
    
    
    var placeholderImage: UIImage? {
        get {
            return UIImage(named: self.placeholderImageName)
        }
    }
    
    //MARK: - Life cycle
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    override public func removeFromSuperview() {
        self.removeObserver()
        super.removeFromSuperview()
    }
    
    deinit {
        self.removeObserver()
    }
    
    //MARK:
    func setup() {
        self.placeholderImageName = "placeholderImageName"
    }
    
    //MARK:
    //MARK: NSNotification Observer
    func addObserverIfNeed() {
        self.removeObserver()
        if self.attachment?.isDownloaded?.boolValue == false {
            NotificationCenter.default.addObserver(self, selector: #selector(CXMessengerAttachmentView.attachmentDownloaded(_:)), name: NSNotification.Name(rawValue: APIAttachmentDownloadedNotification), object: nil)
        }
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: NSNotification Called
    func attachmentDownloaded(_ notification:NSNotification) {
        if let newAttachment = notification.userInfo?["attachment"] as? CXDMAttachment {
            if self.attachment?.uid == newAttachment.uid {
                self.attachment = newAttachment
            }
        }
    }
    
    //MARK:
    func setImageIfCan() {
        if self.attachment?.isDownloaded?.boolValue == true {
            self.removeObserver()
            let dispatchQueue = DispatchQueue(label: "UIAttachmentView.imageWithContentsOfFile")
            dispatchQueue.async(execute: {
                if let filePath = self.attachment?.filePath {
                    let image = UIImage(contentsOfFile: filePath.absoluteString)
                    DispatchQueue.main.sync(execute: {
                        self.image = (image != nil) ? image : self.placeholderImage;
                    });
                }
            });
        } else {
            self.image = self.placeholderImage
        }
    }
    
}
