//
//  CXMessengerAvatarView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.04.16.
//
//

import Foundation

// FOR Reuse Storyboard
class NIB_CXMessengerAvatarView: CXMessengerAvatarView {
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        let obj = super.awakeAfter(using: aDecoder)
        return self.replacementObject(object: obj, forClass: CXMessengerAvatarView.self)
    }
}

class CXMessengerAvatarView: UIView {
    
    @IBOutlet private weak var control: CXMessengerAvatarAttachmentControl!
    @IBOutlet private weak var statusView: CXMessengerStatusBadgeView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NotificationCenter.default.addObserver(self, selector: #selector(socketManagerNotificationContactStatusDidUpdate(_:)), name:NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)
    }
    
    override init(frame: CGRect) {
        fatalError("Cannot be instantiated from code")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    private func setup() {
        self.control.addTarget(self, action: #selector(CXMessengerAvatarView.controlTaped(_:)), for: .touchUpInside)
    }
    
    @objc private func controlTaped(_ sender: UIControl) {
        if let handler = self.didTapHandler {
            handler()
        }
    }
    func addTarget(target: AnyObject?, action: Selector, forControlEvents controlEvents: UIControlEvents = .touchUpInside) {
        self.control.addTarget(target, action: action, for: controlEvents)
    }
    
    var didTapHandler: (() -> ())?
    
    @IBInspectable var status: CXMessengerStatus {
        get { return self.statusView.status }
        set(value) { self.statusView.status = value }
    }
    
    @IBInspectable var hiddenStatus: Bool {
        get { return self.statusView.isHidden }
        set(value) { self.statusView.isHidden = value; if value { updateStatus(contact: self.contact) } }
    }
    
    // MARK: - Chat Machine
    var contact: CXDMContact? {
        didSet { updateStatus(contact: self.contact) }
    }
    
    func socketManagerNotificationContactStatusDidUpdate(_ notification: NSNotification) {
        if !hiddenStatus {
            if let _ = notification.userInfo?["all"] {
                updateStatus(contact: self.contact)
            } else
            if let updatedUid = notification.userInfo?["uid"] as? String,
                let contactUid = contact?.uid {
                if updatedUid == contactUid {
                    updateStatus(contact: self.contact)
                }
            }
        }
    }
    
    func updateStatus(contact: CXDMContact?) {
        if let status = contact?.status {
            self.status = CXMessengerStatus(rawValue: status) ?? CXMessengerStatus.Offline
        } else {
            self.status = CXMessengerStatus.Offline
        }
    }
    
}
