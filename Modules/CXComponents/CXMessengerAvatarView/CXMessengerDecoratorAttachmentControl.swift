//
//  CXMessengerDecoratorAttachmentControl.swift
//  cx4i
//
//  Created by EvGeniy Lell on 07.04.16.
//
//


import Foundation


class CXMessengerDecoratorAttachmentControl: CXMessengerAttachmentControl {
    //MARK: - Properties
    private(set) var overBorder = UIView()
    
    private(set) var shadowView = UIView()
    
    var borderWidth:CGFloat {
        get { return self.view.layer.borderWidth }
        set(value) { self.view.layer.borderWidth = value }
    }
    
    var borderColor:UIColor? {
        get {
            if let borderColor = self.view.layer.borderColor {
                return UIColor(cgColor: borderColor)
            }
            return nil
        }
        set(value) { self.view.layer.borderColor = value?.cgColor }
    }
    
    var outsideBorderWidth:CGFloat {
        get { return self.overBorder.layer.borderWidth }
        set(value) { self.overBorder.layer.borderWidth = value }
    }
    
    var outsideBorderColor:UIColor? {
        get {
            if let borderColor = self.overBorder.layer.borderColor {
                return UIColor(cgColor: borderColor)
            }
            return nil
        }
        set(value) { self.overBorder.layer.borderColor = value?.cgColor }
    }
    
    var shadowColor:UIColor? {
        get {
            if let borderColor = self.shadowView.layer.borderColor {
                return UIColor(cgColor: borderColor)
            }
            return nil
        }
        set(value) { self.shadowView.layer.borderColor = value?.cgColor }
    }
    
    var shadowRadius:CGFloat {
        get { return self.shadowView.layer.shadowRadius }
        set(value) { self.shadowView.layer.shadowRadius = value }
    }
    
    var shadowOpacity:CGFloat {
        get { return CGFloat(self.shadowView.layer.shadowOpacity) }
        set(value) { self.shadowView.layer.shadowOpacity = Float(value) }
    }
    
    var shadowOffset:CGSize {
        get { return self.shadowView.layer.shadowOffset }
        set(value) { self.shadowView.layer.shadowOffset = value }
    }
    
    var cornerRadius:CGFloat {
        get { return self.view.layer.cornerRadius }
        set(value) {
            self.view.layer.cornerRadius = cornerRadius;
            self.overBorder.layer.cornerRadius = cornerRadius;
            self.shadowView.layer.cornerRadius = cornerRadius;
        }
    }
    
    //MARK: - Life cycle
    override func setup() {
        super.setup()
        self.view.layer.masksToBounds = true;
        
        self.overBorder.backgroundColor = UIColor.clear
        self.overBorder.layer.masksToBounds = true
        self.overBorder.isUserInteractionEnabled = false
        
        self.shadowView.backgroundColor = UIColor.clear
        self.shadowView.layer.masksToBounds = true
        self.shadowView.isUserInteractionEnabled = false
        
        self.addSubview(self.shadowView)
        self.addSubview(self.view)
        self.addSubview(self.overBorder)
        
        self.decoratorSetup()
        self.reframe()
    }
    
    override var frame: CGRect {
        didSet {
            self.reframe()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.reframe()
    }
    
    //MARK: - Helper
    func decoratorSetup() {
        self.borderColor = UIColor.white
        self.borderWidth = 0.0
        self.outsideBorderColor = UIColor.white
        self.outsideBorderWidth = 2.0
        self.shadowRadius = 2.0
        self.shadowOpacity = 0.4
        self.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.shadowColor = UIColor.black
    }
    
    func reframe() {
        self.view.frame = self.bounds;
        self.overBorder.frame = self.bounds.insetBy(dx: -self.outsideBorderWidth, dy: -self.outsideBorderWidth);
        self.shadowView.frame = self.bounds
        
        self.view.layer.cornerRadius = radiusByRect(rect: self.view.bounds)
        self.overBorder.layer.cornerRadius = radiusByRect(rect: self.overBorder.bounds)
        self.shadowView.layer.cornerRadius = radiusByRect(rect: self.shadowView.bounds)
    }
    
    func radiusByRect(rect:CGRect) -> CGFloat {
        return min(rect.height,rect.width)/2.0
    }

}
