//
//  CXMessengerAvatarAttachmentControl.swift
//  cx4i
//
//  Created by EvGeniy Lell on 07.04.16.
//
//

import Foundation

class CXMessengerAvatarAttachmentControl: CXMessengerDecoratorAttachmentControl {
    
    override func decoratorSetup() {
        self.borderColor = UIColor.clear
        self.borderWidth = 0.0
        self.outsideBorderColor = UIColor.clear
        self.outsideBorderWidth = 0.0
        self.shadowRadius = 0.0
        self.shadowOpacity = 0.0
        self.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.shadowColor = UIColor.clear
    }
    
    override func reframe() {
        super.reframe()
        if self.frame.size.height > 44 {
            self.placeholderImageName = "MSGSmallAvatarHolder"
        } else {
            self.placeholderImageName = "MSGAvatarHolder"
        }
    }
}
