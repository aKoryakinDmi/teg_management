//
//  CXAPIAttachmentDownloader.swift
//  cx4i
//
//  Created by EvGeniy Lell on 07.04.16.
//
//

import Foundation
import CoreData

protocol CXDownloadableAttachmentProtocol {
    var isNeedDownload: Bool { get }
    var filePath: URL? { get }
    static var folderPath: URL { get }
    
    var isDownloading: NSNumber? { get }
    var isDownloaded: NSNumber? { get }
    
    var contentType: String? { get }
}

class CXAPIAttachmentDownloader {
    //MARK: - public
//    static let defaultDownloader = CXAPIAttachmentDownloader()
    
    func resetFailedDownloads() {
    }
    
    func downloadAllAttachments() {
    }
    
    func downloadAttachment(Attachment: CXDownloadableAttachmentProtocol) {
    }
    
    init<T: CXDownloadableAttachmentProtocol> (context: NSManagedObjectContext, attachmentType: T.Type) {
        self.attachmentsDirectory = attachmentType.folderPath
        self.context = context
        self.operationQueue.maxConcurrentOperationCount = 4
        self.createAttachmentsDirectory()
    }
    
    //MARK: - private
    private let context: NSManagedObjectContext
    private let operationQueue = OperationQueue()
    private let attachmentsDirectory: URL
    
    private func createAttachmentsDirectory() {
        _ = try? FileManager.default.createDirectory(at: self.attachmentsDirectory, withIntermediateDirectories: true, attributes: nil)
    }
    
    private func operationFinished(operation:CXAPIRequestAttachmentOperation) {
        
    }
    
    private func operationFailed(operation: CXAPIRequestAttachmentOperation) {
        
    }

    
}
