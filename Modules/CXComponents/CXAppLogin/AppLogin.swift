//
//  AppLogin.swift
//  cx4i
//
//  Created by EvGeniy Lell on 27.05.16.
//
//

import Foundation

private let AppLogin_SharedInstance = AppLogin()

@objc class AppLogin : NSObject {
    static let sharedInstance = AppLogin_SharedInstance
    
    var jwt: String?
    var userUid: String?
    var logined: Bool = false
    
    func login(jwt aJwt: String, userUid aUserUid: String) {
        jwt = aJwt
        userUid = aUserUid
        logined = true
    }

    func logout() {
        jwt = nil
        userUid = nil
        logined = false
    }
}
