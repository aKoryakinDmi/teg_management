import UIKit
import MessageUI

var CXErrorHandlerMessageHeight: CGFloat = 64

class CXErrorNotificationView: UIView {
    private var titleLabel: UILabel?
    var userInfo: [String: Any]?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    fileprivate func addTitleLabel(withMessage message: String) -> UILabel {
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 20, width: self.frame.width, height: 44))
        label.backgroundColor = UIColor.clear
        label.font = UIFont(name: "SFUIDisplay-Regular", size: 12)
        label.numberOfLines = 0
        label.text = message
        label.textAlignment = .center
        label.textColor = UIColor.white
        self.addSubview(label)
        return label
    }
    
}

class CXErrorHandler: NSObject {
    static let sharedInstance = CXErrorHandler()
    
    private func makeEmailSend(controller: UIViewController, subject: String = "CXErrorHandler", message: String, delegate: MFMailComposeViewControllerDelegate? = nil) {

        if !MFMailComposeViewController.canSendMail() {
            printLog(items:"Mail services are not available")
            return
        }
        let composeVC = MFMailComposeViewController()
        if let delegate = delegate  {
            composeVC.mailComposeDelegate = delegate
        } else if let delegate = controller as? MFMailComposeViewControllerDelegate {
            composeVC.mailComposeDelegate = delegate
        }
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([
            "maxim.sabarnya@ardas.dp.ua",
            ])
        composeVC.setSubject(subject)
        composeVC.setMessageBody(message, isHTML: false)
        
        // Present the view controller modally.
        controller.present(composeVC, animated: true, completion: nil)
        
    }

    func actionTap(_ sender: Any) {
        var view: CXErrorNotificationView? = nil
        
        if let timer = sender as? Timer {
            view = timer.userInfo as? CXErrorNotificationView
        } else if let gest = sender as? UITapGestureRecognizer {
            view = gest.view as? CXErrorNotificationView
            #if !ConfigurationProgect_AppStore
                if UIApplication.shared.windows.count >= 1 {
                    let window = UIApplication.shared.windows[0]
                    if let topViewController = window.rootViewController?.topMostViewController,
                        let userInfo = view?.userInfo {
                        makeEmailSend(controller: topViewController, message: "\(userInfo.description)", delegate: self)
                    }
                }
            #endif
            
        }
        
//        #if ConfigurationProgect_Live
//            let a = 1
//            print(a)
//        #elseif ConfigurationProgect_Dev
//            let a = 2
//            print(a)
//        #elseif ConfigurationProgect_AppStore
//            let a = 3
//            print(a)
//        #else
//            let a = 4
//            print(a)
//        #endif
//        print("\(a)")
        
        if let view = view {
            UIView.animate(withDuration: 0.4, delay: 0, options: .transitionFlipFromBottom, animations: {() -> Void in
                view.frame = CGRect(x: view.frame.origin.x, y: -CXErrorHandlerMessageHeight, width: view.frame.width, height: view.frame.height)
            }, completion: {(finished: Bool) -> Void in
                view.removeFromSuperview()
            })
        }
    }
    
    func drawMessage(message: String, userInfo: [String: Any]? = nil, backgroundColor: UIColor, complete: (() -> Void)? = nil) {
        print("ERROR: \(message)")
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(actionTap))
        let width = UIScreen.main.bounds.width
        let view = CXErrorNotificationView(frame: CGRect(x: 0, y: -CXErrorHandlerMessageHeight, width: width, height:  CXErrorHandlerMessageHeight))
        view.backgroundColor = backgroundColor
        view.addGestureRecognizer(tapGestureRecognizer)
        
        UIApplication.shared.keyWindow!.addSubview(view)
        UIView.animate(withDuration: 0.4, delay: 0, options: .transitionFlipFromTop, animations: {() -> Void in
            view.frame = CGRect(x: view.frame.origin.x, y: 0, width: view.frame.width, height: view.frame.height)
            }, completion: {(finished: Bool) -> Void in
                _ = view.addTitleLabel(withMessage: message)
                view.userInfo = userInfo
                Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.actionTap), userInfo: view, repeats: false)
        })

    }
    
    func handleError(error: Error) {
        var message = "Unexpected error"
        if let reason = (error._userInfo?[AFNetworkingOperationFailingURLResponseErrorKey] as? HTTPURLResponse)?.allHeaderFields["CX-ERROR-REASON"] as? String {
            message = reason.fromBase64()
        } else {
            if !error.localizedDescription.isEmpty {
                message = error.localizedDescription
            }
        }
        self.drawMessage(message: message, backgroundColor: UIColor.red)
    }
}

extension CXErrorHandler : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
