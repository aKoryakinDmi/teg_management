//
//  CXSocket+Ping.swift
//  cx4i
//
//  Created by EvGeniy Lell on 19.04.16.
//
//

import Foundation

//extension PrintLog { func set_CXSocket_Ping_LogLevel() { setLevel(.Info) } }

let CXSocketPingInterval: TimeInterval = 5

let CXSocketPing = "[\"{\\\"type\\\":\\\"ping\\\"}\"]"


extension CXSocket {
    func ping() {
        let pingTask = CXSocketTask(message: CXSocketPing, uuid: randomStringUUID(), completion: { (task, response, error) in
            // noting
        }, type: .Ping)
        send(task: pingTask)
        printLog(items:"socket send Ping")
    }

    func timerEnable() {
        if pingTimer != nil {
            self.timerDisable()
        }
        pingTimer = Timer.scheduledTimer(timeInterval: CXSocketPingInterval, target: self, selector: #selector(ping), userInfo: nil, repeats: true)
    }
    
    func timerDisable() {
        pingTimer?.invalidate()
        pingTimer = nil
    }
    
    func handlePingMessage(message: String) -> Bool {
        if message == "h" {
            self.ping()
            return true
        }
        if message == "o" {
            self.ping()
            return true
        }
        return false
    }
}
