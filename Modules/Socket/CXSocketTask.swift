//
//  CXSocket+Web.swift
//  cx4i
//
//  Created by EvGeniy Lell on 19.04.16.
//
//

import Foundation

func ==(lhs: CXSocketTask, rhs: CXSocketTask) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

enum CXSocketTaskType {
    case Ping
    case Send
    case Register
    case Unregister
    case Publish
}

class CXSocketTask: Hashable, CustomStringConvertible {
    let message: String
    let uuid: String
    var completion: CXSocketClosure
    var type: CXSocketTaskType {
        didSet {
            inspectTimeoutTimer()
        }
    }
    var timeoutTimer: Timer? {
        didSet {
            if oldValue != nil {
                oldValue?.invalidate()
            }
            if timeoutTimer != nil {
                inspectTimeoutTimer()
            }
        }
    }
    var userInfo: [String: AnyObject]?
    var action:String? {
        get {
            var result = message.match(pattern: "\\\\\\\"body\\\\\\\":\\{.*?action.*?\\\\\\\":\\\\\\\"(.*?)\\\\\\\"", result: "$1").first
            if result == nil {
                result = message.match(pattern: "\\\\\\\"address\\\\\\\":\\\\\\\"(.*?)\\\\\\\"", result: "$1").first
            }
            return result
        }
    }

    let hashValue: Int
    var description: String {
        get { return "\(type(of: self)):\(uuid) - \(String(describing: action))" }
    }

    init(message: String, uuid: String, completion: @escaping CXSocketClosure, type: CXSocketTaskType = .Send) {
        self.message = message
        self.uuid = uuid
        self.completion = completion
        self.type = type
        
        typealias UnsafeBitCastType = (hi: UnsafePointer<Int>, lo: UnsafePointer<Int>)
        let p = unsafeBitCast(completion,to: UnsafeBitCastType.self)
        self.hashValue = "\(p.lo) - \(uuid)".hashValue
    }
    
    private func inspectTimeoutTimer() {
        switch type {
        case .Register, .Unregister, .Publish, .Ping:
            timeoutTimer = nil
        case .Send:
            break
        }
    }
    
}

extension String {
    func rangeFromNSRange(aRange: NSRange) -> Range<String.Index> {
        let s = self.index(self.startIndex, offsetBy: aRange.location, limitedBy: self.endIndex)
        let e = self.index(self.startIndex, offsetBy: aRange.location + aRange.length, limitedBy: self.endIndex)
        return s!..<e!
    }
    
    func match(pattern: String, result: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let checkResultsList = regex.matches(in: self, options: [], range: NSMakeRange(0, self.characters.count))
            return checkResultsList.map { (checkResult) -> String in
                var string = result
                let R0 = rangeFromNSRange(aRange: checkResult.range)
                let S0 = self.substring(with: R0)
                string = string.replacingOccurrences(of: "$0", with: S0)
                for index in 0..<checkResult.numberOfRanges {
                    let Ri = rangeFromNSRange(aRange: checkResult.rangeAt(index))
                    let Si = self.substring(with: Ri)
                    string = string.replacingOccurrences(of: "$\(index)", with: Si)
                }
                return string
            }
        } catch {
            return [String]()
        }
    }
}

extension Set where Element : CXSocketTask {
    func filter(uuid: String) -> [Element] {
        return self.filter({ (element) -> Bool in
            return element.uuid == uuid
        })
    }
    func filter(task: CXSocketTask) -> [Element] {
        return self.filter({ (element) -> Bool in
            return element == task
        })
    }
}
