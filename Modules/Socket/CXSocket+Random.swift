//
//  CXSocket+Random.swift
//  cx4i
//
//  Created by EvGeniy Lell on 19.04.16.
//
//

import Foundation

extension CXSocket {
    
    func randomString(quantity: Int) -> String {
        let letters: NSString = "abcdefghijklmnopqrstuvwxyz0123456789"
        let randomString = NSMutableString()
        
        while(randomString.length < quantity) {
            let rand = arc4random_uniform(UInt32(letters.length))
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString as String
    }
    
    func randomString3DC() -> String {
        return String(Int(arc4random_uniform(899) + 100))
    }
    
    func randomStringUUID() -> String {
        let letters: NSString = "0123456789abcdef"
        let randomString = NSMutableString()
        
        while(randomString.length < 32) {
            let rand = arc4random_uniform(UInt32(letters.length))
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        randomString.insert("-", at: 8)
        randomString.insert("-", at: 13)
        randomString.insert("-", at: 18)
        randomString.insert("-", at: 23)
        
        return randomString as String
    }
}
