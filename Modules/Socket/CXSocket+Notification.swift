//
//  CXSocket+Notification.swift
//  cx4i
//
//  Created by EvGeniy Lell on 19.04.16.
//
//

import Foundation

let CXSocketManagerNotificationDidOpen = "CXSocketManagerNotificationDidOpen"
let CXSocketManagerNotificationDidResponseError = "CXSocketManagerNotificationDidResponseError"
let CXSocketManagerNotificationDidFailWithError = "CXSocketManagerNotificationDidFailWithError"
let CXSocketManagerNotificationDidCloseWithCode = "CXSocketManagerNotificationDidCloseWithCode"


extension CXSocket {
    
    func webSocket(_ webSocket: SRWebSocket, didReceiveMessage message: Any) {
        printLog(items: "webSocket didReceiveMessage")
        if message is String {
            self.handleMessage(message: message as! String)
        }
    }
    
    func webSocketDidOpen(_ webSocket: SRWebSocket) {
        printLog(items: "webSocketDidOpen")
        self.timerEnable()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXSocketManagerNotificationDidOpen), object: nil)
    }
    
    func webSocket(_ webSocket: SRWebSocket, didFailWithError error: Error) {
        printLog(items: "webSocket didFailWithError: \(error)")
        self.timerDisable()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXSocketManagerNotificationDidFailWithError), object: error)
    }
    
    func webSocket(_ webSocket: SRWebSocket, didCloseWithCode code: Int, reason: String, wasClean: Bool) {
        printLog(items: "webSocket didCloseWithCode: \(code) reason: \(reason) wasClean: \(wasClean)")
        self.timerDisable()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXSocketManagerNotificationDidCloseWithCode), object: code)
    }
}

