import Foundation

//extension PrintLog { func set_CXSocket_LogLevel() { setLevel(.Debug) } }

typealias CXSocketClosure = (_ task: CXSocketTask, _ response: AnyObject?, _ error: CXSocketError?) -> Void


let CXSocketTimeoutInterval: TimeInterval = 30


enum CXSocketError: Error {
    case InvalidResponse
    case TaskCanceled
    case InvalidBody
    case TimedOut(forTask: CXSocketTask)
}

let CXSocketAddress = "address"
let CXSocketBody = "body"

class CXSocket: NSObject, SRWebSocketDelegate {
    
    static let sharedInstance = CXSocket()
    
    var socket: SRWebSocket?
    var tasksList = Set<CXSocketTask>()
    var pingTimer: Timer?
    
    class Parametrs: NSObject {
        var jwt: String? {
            get { return AppLogin.sharedInstance.jwt }
        }
        var userHash: String? {
            get { return AppLogin.sharedInstance.userUid }
        }
        
        func isValid() -> Bool {
            print(jwt as Any, userHash as Any)
            return (jwt != nil && userHash != nil)
        }
        
    }
    
    private(set) var parametrs = Parametrs()
    
    func connect() {
//        if socket?.readyState == .CONNECTING ||
//            socket?.readyState == .OPEN {
//        }
        self.disconnect()

        printLog(items:"socket connect")

        parametrs = Parametrs()

        if parametrs.isValid() {
            #if TypeProgect_cx4i
                let url = Config.getChatUrlWith3DC(randomString3DC(), and8CC: randomString(8))
            #elseif TypeProgect_TEG_management
                let url = CXHTTPManager.Chat.baseUrl(a3ds: randomString3DC(), a8CC: randomString(quantity: 8))
            #endif
            printLog(items:"socket url: \(url.absoluteString)")
            var request = URLRequest(url: url)
            request.addValue(parametrs.jwt!, forHTTPHeaderField: "jwt")
            socket = SRWebSocket(urlRequest: request)
            socket?.delegate = self
            socket?.requestCookies = [HTTPCookie(properties: [
                HTTPCookiePropertyKey.domain: "im-vertx.transportexchangegroup.com",
                HTTPCookiePropertyKey.path: "/mobilews/",
                HTTPCookiePropertyKey.name: "jwt",
                HTTPCookiePropertyKey.value: parametrs.jwt!,
                HTTPCookiePropertyKey.secure: "TRUE",
                ])!]
            socket?.open()
        } else {
            printLog(items:"Parametrs not valide (jwt, userHash)")
        }
    }
    
    func disconnect() {
        printLog(items:"socket disconnect")
        tasksList.removeAll()
        socket?.delegate = nil
        socket?.close()
        
        timerDisable()
    }
    
    func send(task: CXSocketTask) {
        if socket?.readyState != .OPEN {
            task.completion(task, nil, CXSocketError.TaskCanceled)
            return
        }
        let needSendMessage = (tasksList.filter(uuid: task.uuid).count == 0)
        if needSendMessage {
            tasksList.insert(task)
        }
        if needSendMessage {
            socket?.send(task.message)
            printLog(items: "socket send message: \(task.message)")
            task.timeoutTimer = Timer.scheduledTimer(timeInterval: CXSocketTimeoutInterval, target: self, selector: #selector(handleTimeout(_:)), userInfo: task, repeats: false)
        }
    }
    
    func handleMessage(message: String) {
        printLog(items:"handle message \(message)")
        if handlePingMessage(message: message) {
            printLog(items:"socket handle Ping")
            return
        }
        
        if message.hasPrefix("a") {
            var response = message
            response.remove(at: response.startIndex)
            if response.hasPrefix("[\"") {
                response.removeSubrange(response.startIndex..<response.index(response.startIndex, offsetBy: 2))
            }
            if response.hasSuffix("\"]") {
                response.removeSubrange(response.index(response.endIndex, offsetBy: -2)..<response.endIndex)
            }
            
            response = response.replacingOccurrences(of: "\\\"", with: "\"")
            response = response.replacingOccurrences(of: "\\\\", with: "\\")
            
            let data: [String: Any]!
            do {
                data = try dictionaryFromString(json: response) //response.convertToDictionary(),
            } catch let error {
                printLog(items:"ERRORESCAPE:\n\(response)\n\(error)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXSocketManagerNotificationDidResponseError), object: CXSocketError.InvalidResponse as NSError)
                return
            }
            
            guard
                (data != nil),
                let key = data[CXSocketAddress] as? String else {
                    return
            }
            
            let tasksListByUuid = tasksList.filter({ (task) -> Bool in return task.uuid == key })
            for task in tasksListByUuid {
                if let body = data[CXSocketBody] {
                    printLog(items:"socket handle message: \(body)\nuserInfo: \(String(describing: task.userInfo))")
                    task.completion(task, body as AnyObject, nil)
                } else {
                    task.completion(task, nil, CXSocketError.InvalidBody)
                }
                removeTaskIfNeed(task: task)
            }
        }
    }
    
    private func removeTaskIfNeed(task: CXSocketTask) {
        switch task.type {
        case .Register:
            break
        default:
            tasksList.remove(task)
        }
    }
    
    private func dictionaryFromString(json: String) throws -> [String: AnyObject]? {
        if let data = json.data(using: String.Encoding.utf8) {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        }
        return nil
    }

    
    func handleTimeout(_ timer: Timer) {
        guard let task = timer.userInfo as? CXSocketTask else {
            return
        }
        
        if tasksList.contains(task) {
            printLog(items:"TIMEOUT for meassage:\(task.message)")
            task.completion(task, nil, CXSocketError.TimedOut(forTask: task))
            removeTaskIfNeed(task: task)
        }
    }
    
}


