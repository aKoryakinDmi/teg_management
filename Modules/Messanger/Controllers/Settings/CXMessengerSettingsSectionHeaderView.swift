//
//  CXMessengerSettingsSectionHeaderView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 23.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerSettingsSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet private weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = UIView()
        backgroundView?.backgroundColor = UIColor.white
        
        titleLabel.textColor = MSGColor.profileItemTitleText
        titleLabel.font = UIFont(SFUIType: .Regular, size: 14)

    }
    
    var title: String? {
        get { return titleLabel.text }
        set(value) { titleLabel.text = value }
    }
}
