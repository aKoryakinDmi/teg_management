//
//  CXMessengerSettingsController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 20.05.16.
//
//

import Foundation

private let Localized = CXLocalized.Messenger.Settings

class CXMessengerSettingsController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView?
    
    var user: CXDMUser? {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        

        addMessengerObserver()
        setupTableView()
        setupNavigationItem()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    deinit {
        removeMessengerObserver()
        
        _ = user?.save()
        CXMessengerChatMachine.sharedInstance.sendUpdateUserSettings()
    }
    

    private func setupTableView() {
        tableView?.register(UINib(nibName: "CXMessengerSettingsSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "sectionHeader")

        tableView?.allowsSelection = false
        
        tableView?.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.estimatedRowHeight = 44
        
        tableView?.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView?.estimatedSectionHeaderHeight = 60
        
        tableView?.reloadData()
    }
    
    private func setupNavigationItem() {
        let backButton = UIBarButtonItem(image: UIImage(named: "NBCloseIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CXMessengerSettingsController.goBack(_:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.title = Localized.NavigationTitle()
    }

    @objc private func goBack(_ sender: Any) {
//        self.navigationController?.popViewControllerAnimated(true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

}

// MARK: - NSNotification
extension CXMessengerSettingsController  {
    
    func addMessengerObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(messangerUserDidUpdateNotification(_:)), name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUserDidUpdate), object: nil)
    }
    
    func removeMessengerObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUserDidUpdate), object: nil)
    }
    
    @objc private func messangerUserDidUpdateNotification(_ notification: NSNotification) {
        tableView?.reloadData()
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension CXMessengerSettingsController: UITableViewDelegate, UITableViewDataSource {
    
    // UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return (user != nil) ? 2 : 0

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: // visible settings
            return 2
        case 1: // sound settings
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if #available(iOS 9.0, *) {
            return UITableViewAutomaticDimension
        } else {
            switch indexPath.section {
            case 0:
                return 55
            case 1:
                return 55
            default:
                return 44
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if #available(iOS 9.0, *) {
            return UITableViewAutomaticDimension
        } else {
            switch section {
            case 0:
                return 60
            case 1:
                return 60
            default:
                return 60
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        weak var blockSelf = self
        weak var blockTable = tableView
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "VisibleSettingsCell") as? CXMessengerSettingsRadioButtonCell  {
                switch indexPath.row {
                case 0: cell.checked = (user?.searchRestrictionAsType == .Anyone)
                    cell.title = Localized.VisibleAny()
                    cell.didTapHandler(handler: { (cell) in
                        blockSelf?.user?.searchRestrictionAsType = .Anyone
                        blockTable?.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
                    })
                case 1: cell.checked = (user?.searchRestrictionAsType == .MyCompany)
                    cell.title = Localized.VisibleMyCompany()
                    cell.didTapHandler(handler: { (cell) in
                        blockSelf?.user?.searchRestrictionAsType = .MyCompany
                        blockTable?.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
                    })
                default:
                    break
                }
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SoundSettingsCell") as?
                CXMessengerSettingsCheckboxCell  {
                switch indexPath.row {
                case 0:
                    cell.checked = (user?.soundOn?.boolValue == true)
                    cell.title = Localized.Sound()
                    cell.didTapHandler(handler: { (cell) in
                        blockSelf?.user?.soundOn = NSNumber(value: !(blockSelf?.user?.soundOn?.boolValue == true))
                        blockTable?.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
                    })
                default:
                    break
                }
                return cell
            }

        default:
            break
        }
        fatalError("not found Cell or data")
    }
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
//        switch indexPath.section {
//        case 0:
//            switch indexPath.row {
//            case 0: user?.searchRestrictionAsType = .Anyone
//            case 1: user?.searchRestrictionAsType = .MyCompany
//            default:
//                break
//            }
//        case 1:
//            switch indexPath.row {
//            case 0: user?.soundOn = NSNumber(bool: !(user?.soundOn?.boolValue == true))
//            default:
//                break
//            }
//        default:
//            break
//        }
//        tableView.reloadData()
//    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "sectionHeader") as? CXMessengerSettingsSectionHeaderView {
            switch section {
            case 0: header.title = Localized.ConfidentialityHeader()
            case 1: header.title = Localized.NotificationHeader()
            default:
                break
            }
            return header
        }
        fatalError("not found Header")
    }
}
