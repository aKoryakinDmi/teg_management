//
//  CXMessengerSettingsRadioButtonCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 21.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerSettingsRadioButtonCell: UITableViewCell {
    
    @IBOutlet private weak var radiobutton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBAction private func valueChangedAction(_ sender: Any) {
        if let handler = handler {
            handler(self)
        }

    }

    override func awakeFromNib() {
        super.awakeFromNib()

        radiobutton.setImage(UIImage(named: "UIRadiobuttonSelected"), for: .selected)
        radiobutton.setImage(UIImage(named: "UIRadiobutton"), for: .normal)
        
        titleLabel.textColor = MSGColor.text
        titleLabel.font = UIFont(SFUIType: .Regular, size: 16)
    }
    
    var title: String? {
        get { return titleLabel.text }
        set(value) { titleLabel.text = value }
    }
    
    var checked: Bool {
        get { return radiobutton.isSelected }
        set(value) { radiobutton.isSelected = value }
    }

    typealias HandlerType = (_ cell: CXMessengerSettingsRadioButtonCell?) -> ()
    private var handler: HandlerType?
    func didTapHandler(handler: (HandlerType)?) {
        self.handler = handler
    }

}
