//
//  CXMessengerSettingsCheckboxCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 21.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerSettingsCheckboxCell: UITableViewCell {
    
    @IBOutlet private weak var checkbox: UISwitch!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBAction private func valueChangedAction(_ sender: Any) {
        if let handler = handler {
            handler(self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.textColor = MSGColor.text
        titleLabel.font = UIFont(SFUIType: .Regular, size: 16)
        checkbox.tintColor = MSGColor.navigationBarView
        checkbox.tintColor = MSGColor.navigationBarView
        checkbox.onTintColor = MSGColor.navigationBarView
    }
    
    var title: String? {
        get { return titleLabel.text }
        set(value) { titleLabel.text = value }
    }
    
    var checked: Bool {
        get { return checkbox.isSelected }
        set(value) {
            checkbox.setOn(value, animated: true)
        }
    }
    
    typealias HandlerType = (_ cell: CXMessengerSettingsCheckboxCell?) -> ()
    private var handler: HandlerType?
    func didTapHandler(handler: (HandlerType)?) {
        self.handler = handler
    }
    
//    class func heightForMessage(message: String?, inTableView tableView: UITableView) -> CGFloat {
//        let widthShift: CGFloat = 135.0
//        let heightShift: CGFloat = 28.0
//        let heightMin: CGFloat = 70.0
//        let heightMax: CGFloat = 250.0
//        
//        if let message = message {
//            let width: CGFloat = tableView.frame.width - widthShift
//            let constraintRect = CGSize(width: width, height: CGFloat.max)
//            
//            let boundingBox = message.boundingRectWithSize(constraintRect, options: [.UsesLineFragmentOrigin, .UsesFontLeading], attributes: CXMessengerChatMessageCell_MessageLabelAttribytes, context: nil)
//            let result = max(heightMin,min(heightMax, boundingBox.height + heightShift))
//            printLog(.Info, items: result)
//            return result
//        }
//        return heightMin
//        
//    }



}
