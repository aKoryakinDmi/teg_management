//
//  CXMessengerChatSectionHeaderView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 09.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerChatSectionHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet private weak var halfBackgroundView: UIView!
    @IBOutlet private weak var wrapperView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        halfBackgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        titleLabel.textColor = MSGColor.chatSectionHeaderText
        titleLabel.font = UIFont(SFUIType: .Regular, size: 13)
        
        wrapperView.backgroundColor = MSGColor.chatSectionHeaderBadgeView
        wrapperView.layer.cornerRadius = wrapperView.frame.height / 2
    }
    
    var title: String? {
        get { return titleLabel.text }
        set(value) { titleLabel.text = value }
    }
}
