//
//  CXMessengerChatDropdownTitleContentView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 05.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

@objc class CXMessengerChatDropdownTitleContentView: CXNavigationDropdownTitleContentView {

    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var statusView: CXMessengerStatusBadgeView!
    @IBOutlet private weak var statusLabel: UILabel!
    
    var contact: CXDMContact? {
        didSet {
            update()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fullNameLabel.font = UIFont(SFUIType: .Regular, size: 20)
        fullNameLabel.textColor = MSGColor.navigationBarText
        statusLabel.font = UIFont(SFUIType: .Regular, size: 12)
        statusLabel.textColor = MSGColor.navigationBarSubText
        
        NotificationCenter.default.addObserver(self, selector: #selector(socketManagerNotificationContactStatusDidUpdate(_:)), name:NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)
    }


    private func update() {
        fullNameLabel.text = contact?.fullName
        updateStatusFromContact(contact)
    }
    
    override func updateConfiguration() {
        super.updateConfiguration()
    }
    
    func socketManagerNotificationContactStatusDidUpdate(_ notification: NSNotification) {
        if let _ = notification.userInfo?["all"] {
            updateStatusFromContact(contact)
        } else
            if let updatedUid = notification.userInfo?["uid"] as? String,
                let contactUid = contact?.uid {
                if updatedUid == contactUid {
                    updateStatusFromContact(contact)
                }
        }
    }
    
    func updateStatusFromContact(_ contact: CXDMContact?) {
        if let status = contact?.status {
            statusView.status = CXMessengerStatus(rawValue: status) ?? CXMessengerStatus.Offline
        } else {
            statusView.status = CXMessengerStatus.Offline
        }
        statusLabel.text = CXLocalized.Messenger.ChatStatus.Status(statusView.status)
    }


}
