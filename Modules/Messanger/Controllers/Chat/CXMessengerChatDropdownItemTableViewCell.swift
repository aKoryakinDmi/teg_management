//
//  CXMessengerChatDropdownItemTableViewCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 16.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerChatDropdownItemTableViewCell: UITableViewCell, CXDropdownItemCell, CXNDMConfigurationProtocol {
    
    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    private var item: CXMessengerChatDropdownItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont(SFUIType: .Regular, size: 17)
        titleLabel.textColor = MSGColor.navigationBarText
    }

    //MARK: configuration
    var configuration: CXNDMConfiguration? {
        didSet {
            updateConfiguration()
        }
    }
    
    func updateConfiguration() {
        if let configuration = configuration {
            self.backgroundColor = configuration.color.withAlphaComponent(configuration.opacity)
            self.selectedBackgroundView = UIView(frame: CGRect.zero)
            self.selectedBackgroundView?.backgroundColor = configuration.color.withAlphaComponent(configuration.opacity)
        }
    }
    
    func setActionItem(item: CXDropdownItem) {
        if let item = item as? CXMessengerChatDropdownItem {
            self.item = item
            reloadData()
        }
    }
    
    func reloadData() {
        if let item = self.item {
            self.titleLabel.text = item.title
            self.iconView.image = item.icon
        }
    }
    
}
