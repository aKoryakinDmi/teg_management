//
//  CXMessengerChatMessageLabel.swift
//  cx4i
//
//  Created by EvGeniy Lell on 02.06.16.
//
//

import Foundation

class CXMessengerChatMessageLabel: UILabel {
    private let StringTagAttribut = "StringTagAttribut"
    private let StringTagLink = "Link"
    
    private var tapGesture: UITapGestureRecognizer?

    private let layoutManager = NSLayoutManager()
    private let textContainer = NSTextContainer()
    private var textStorage: NSTextStorage?

    private func initConfig() {
        setTapGesture()
        
        textContainer.lineFragmentPadding = 0.0;
        textContainer.lineBreakMode = lineBreakMode;
        textContainer.maximumNumberOfLines = numberOfLines;

        layoutManager.addTextContainer(textContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initConfig()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initConfig()
    }
    
    private func setTapGesture() {
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        if let tapGesture = tapGesture {
            self.addGestureRecognizer(tapGesture)
        }
    }
    
    func handleTap(_ tapGesture: UITapGestureRecognizer) {
        if let textStorage = textStorage, let attributedText = attributedText {
            textContainer.size = bounds.size
            let locationOfTouchInLabel = tapGesture.location(in: self)
            let labelSize = self.bounds.size
            let textBoundingBox = layoutManager.usedRect(for: textContainer)
            //            let scale = 1.0 / UIScreen.mainScreen().scale
            let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y:                                             (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
            let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
            let characterIndex = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
            if (characterIndex < textStorage.length) {
                var range = NSRange()
                let linkParametrs = attributedText.attribute(StringTagAttribut, at: characterIndex, effectiveRange: &range)
                if let tag = linkParametrs as? String {
                    switch tag {
                    case StringTagLink:
                        let stringUrl = attributedText.attributedSubstring(from: range).string
                        if let url = URL(string: stringUrl) {
                            UIApplication.shared.openURL(url);
                        }
                    default:
                        break
                    }
                }
            }
        }
    }
    
    func updateTextStorage() {
        if let attributedText = attributedText {
            textStorage = NSTextStorage(attributedString: attributedText);
            textStorage?.addLayoutManager(layoutManager)
        } else {
            textStorage = nil
        }
    }
    
    override var text: String? {
        get { return super.text }
        set(value) {
            if value != text {
                super.text = value
                dataDetect()
            }
        }
    }
    
    func dataDetect() {
        let dispatchQueue = DispatchQueue(label: "UILinkedLabel.dataDetect")
        weak var blockSelf = self
        dispatchQueue.async() {
            var result: NSMutableAttributedString? = nil
            if let value = self.text,
             let blockSelf = blockSelf {
                var aText = NSMutableAttributedString(string: value)
                blockSelf.detectLink(aText: &aText)
                result = aText
            }
            DispatchQueue.main.async() {
                blockSelf?.attributedText = result
                blockSelf?.updateTextStorage()
            }
        }
    }

    private func detectLink( aText: inout NSMutableAttributedString) {
        detectFor(pattern: "https?:\\/\\/[^\\s]+", attributes: [
            NSForegroundColorAttributeName: UIColor(hex: 0x0000FF),
            NSUnderlineStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue),
            StringTagAttribut: StringTagLink as AnyObject,
            ], aText: &aText)
    }
    
    private func detectFor(pattern: String, attributes:[String: AnyObject], aText: inout NSMutableAttributedString) {
        let text = aText.string
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let checkResultsList = regex.matches(in: text, options: [], range: NSMakeRange(0, text.characters.count))
            for checkResult in checkResultsList {
                let range = checkResult.range
                if range.location != NSNotFound {
                    aText.setAttributes(attributes, range: range)
                }
            }
        } catch {
            return
        }
    }
}
