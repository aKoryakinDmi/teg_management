//
//  CXMessengerChatContactHeaderView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerChatContactHeaderView : UITableViewHeaderFooterView {
    
    @IBOutlet private weak var avatarView: CXMessengerAvatarView!
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var companyNameLabel: UILabel!
    @IBOutlet private weak var jobTitleLabel: UILabel!
    
    @IBAction func touchAction(_ sender: Any) {
        if let didTapHandler = didTapHandler {
            didTapHandler()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarView.hiddenStatus = true
        
        fullNameLabel.textColor = MSGColor.text
        fullNameLabel.font = UIFont(SFUIType: .Regular, size: 20)

        companyNameLabel.textColor = MSGColor.disableText
        companyNameLabel.font = UIFont(SFUIType: .Regular, size: 14)
        jobTitleLabel.textColor = MSGColor.disableText
        jobTitleLabel.font = UIFont(SFUIType: .Regular, size: 14)
    }
    
    var contact: CXDMContact? {
        didSet { update() }
    }
    
    var didTapHandler: (() -> ())?
    
    func update() {
        avatarView.contact = contact
        fullNameLabel.text = contact?.fullName
        companyNameLabel.text = contact?.companyDescription
        jobTitleLabel.text = contact?.jobTitle
    }
    
}
