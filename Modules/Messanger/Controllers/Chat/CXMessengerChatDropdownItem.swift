//
//  CXMessengerChatDropdownItem.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.05.16.
//
//

import Foundation

class CXMessengerChatDropdownItem: CXDropdownItem {
    private let imageName: String
    
    init(title: String, imageName: String) {
        self.imageName = imageName
        super.init(title: title)
    }
    
    var icon: UIImage? {
        get {
            return UIImage(named: self.imageName)
        }
    }
}

