//
//  CXMessengerChatController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.05.16.
//
//

import Foundation
import CoreData

//extension PrintLog { func set_CXMessengerChatController_LogLevel() { setLevel(.Debug) } }

private let Localized = CXLocalized.Messenger.Chat


class CXMessengerChatController: UITableViewController {
    
    @IBOutlet fileprivate var dropdownTitleContentView: CXMessengerChatDropdownTitleContentView!
    @IBOutlet fileprivate var contactHeaderView: CXMessengerChatContactHeaderView!
    @IBOutlet fileprivate var chatInputAccessoryView: CXMessengerChatInputAccessoryView!
    
    // MARK: chat
    let chatMachine = CXMessengerChatMachine.sharedInstance
    fileprivate var fetchedCacheName: String {
        get { return "CXMessengerChat-\(String(describing: self.contact?.uid))" }
    }
    fileprivate var messageController: NSFetchedResultsController<NSFetchRequestResult>?

    fileprivate var dropdownMenu: CXNavigationDropdownMenu?
    
    // MARK: var in-out
    var contact: CXDMContact? {
        didSet {
            update()
        }
    }
    
    // MARK: loaded
    private var isLoaded: Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.automaticallyAdjustsScrollViewInsets = false
        self.edgesForExtendedLayout = [.top, .left, .right]
        self.setupDropdownMenu()
        self.setupLeftBarButtons()
        self.setupTableView()
        self.setupMessageController()
        self.setupSendMessageAction()
        self.setupKeyboardObserver()
        self.setupMessageObserver()
        self.isLoaded = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.update()
        doAfterDelayInSeconds(1.0) {
            if self.ifShowLastSection() {
                self.scrollToBottom(false)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.resetUnreadMessagesCount()
    }
    
    
    private var isFirstViewed: Bool = true
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if isFirstViewed {
            scrollToBottom(false)
            isFirstViewed = false;
        }
    }
    
    deinit {
        removeKeyboardObserver()
        removeMessageObserver()
    }
    
    // MARK: update
    func update() {
        if isLoaded {
            contactHeaderView.contact = contact
            dropdownTitleContentView.contact = contact
            scrollToBottom(false)
        }
    }
    
    // MARK: goto
    fileprivate func toProfile(_ contact: CXDMContact) {
        if let storyboard = self.storyboard {
            if let controller = storyboard.instantiateViewController(withIdentifier: "Profile") as? CXMessengerProfileController {
                controller.contact = contact
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }

    
}

// MARK: - Message Notification
extension CXMessengerChatController {

    @objc fileprivate func contactMessagesDidUpdate(_ notification: NSNotification) {
        if ifShowLastSection() {
            scrollToBottom()
        }
    }
    
    @objc fileprivate func contactUnreadMessagesCountDidUpdate(_ notification: NSNotification) {
        if let contactHash = notification.userInfo?["uid"] as? String,
            let contact = contact, contact.uid == contactHash {
            resetUnreadMessagesCount()
        }
    }
    
    func resetUnreadMessagesCount() {
        if let contact = contact,
            let count = contact.unreadMessagesCount?.intValue, count > 0,
            let contactHash = contact.uid {
            chatMachine.sendResetUnreadMessagesCountForContact(contactHash: contactHash)
        }
    }
    
}


// MARK: - Keyboard Notification
extension CXMessengerChatController {
    
    @objc fileprivate func keyboardDidShow(_ notification: NSNotification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardEndPositionY = endFrame.minY + (inputAccessoryView?.frame.maxY ?? 0)
            let controllerViewMaxPositionY = self.view.frame.maxY
            if keyboardEndPositionY < controllerViewMaxPositionY {
                scrollToBottom()
            }
        }
    }
    
    @objc fileprivate func keyboardDidHide(_ notification: NSNotification) {
        
    }

}


// MARK: - Scroll
extension CXMessengerChatController {

    fileprivate func ifShowLastSection() -> Bool {
        if let maxIndexPath = maxIndexPath(),
            let visibleIndexPathsList = tableView.indexPathsForVisibleRows {
            for visibleIndexPath in visibleIndexPathsList {
                if visibleIndexPath.section == maxIndexPath.section {
                    return true
                }
            }
        }
        return false
    }

    fileprivate func maxIndexPath() -> IndexPath? {
        if let sectionCount = messageController?.sections?.count, sectionCount > 0,
            let rowCount = messageController?.sections?[sectionCount-1].numberOfObjects, rowCount > 0 {
            let indexPath = IndexPath(row: rowCount-1, section: sectionCount-1)
            return indexPath
        }
        return nil
    }
    
    fileprivate func scrollToBottom(_ animated: Bool = true) {
        var contentOffsetY = tableView.contentSize.height - tableView.frame.height + tableView.contentInset.bottom;
        let contentOffsetYByTop = tableView.contentSize.height - tableView.frame.height + tableView.contentInset.bottom + tableView.contentInset.top;
        if contentOffsetYByTop < 0 {
            contentOffsetY -= contentOffsetYByTop
        }
        printLog(items:"scrollToBottom: \(contentOffsetY) (\(tableView.contentSize.height) - \(tableView.frame.height) + \(tableView.contentInset.bottom) (\(tableView.contentInset.top)) )")
        tableView.setContentOffset(CGPoint(x: 0.0, y: contentOffsetY), animated: animated)
    }
    

}


// MARK: - Setup
extension CXMessengerChatController {
    
    fileprivate func setupTableView() {
        tableView.register(UINib(nibName: "CXMessengerChatSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "section")

        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        unowned let blockSelf = self
        contactHeaderView.didTapHandler = {
            if let contact = blockSelf.contact {
                blockSelf.toProfile(contact)
            }
        }
        tableView.tableHeaderView = contactHeaderView
        tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.interactive;
        tableView.allowsSelection = false
        
        tableView.rowHeight = 70//UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70.0
        
        tableView.sectionHeaderHeight = 26
        tableView.estimatedSectionHeaderHeight = 26
        
        tableView.sectionFooterHeight = 0
        tableView.estimatedSectionFooterHeight = 0

    }
    
    fileprivate func setupDropdownMenu() {
        let dropdownMenuItems = [
            CXMessengerChatDropdownItem(title: Localized.MenuItemProfile(), imageName: "MSGIconProfile"),
//            CXMessengerChatDropdownItem(title: Localized.MenuItemFleet(), imageName: "MSGIconLiveFleet"),
//            CXMessengerChatDropdownItem(title: Localized.MenuItemBooking(), imageName: "MSGIconMakeBooking"),
            ]
        if let dropdownMenu = CXNavigationDropdownMenu(controller: self, items: dropdownMenuItems) {
            dropdownMenu.setTitleContentView(view: dropdownTitleContentView)
            dropdownMenu.registerNib(nib: UINib(nibName: "CXMessengerChatDropdownItemTableViewCell", bundle: nil))
            dropdownMenu.setItemHeight(height: 60)
            dropdownMenu.setStoredSelected(value: false)
            weak var blockSelf = self
            blockSelf?.dropdownMenu = dropdownMenu
            dropdownMenu.didSelectItemAtIndexHandler = { (menu: CXNavigationDropdownMenuViewController, indexPath: Int) -> () in
                switch indexPath {
                case 0:
                    if let contact = blockSelf?.contact {
                        menu.hideMenu(animation: false)
                        blockSelf?.toProfile(contact)
                    }
                default:
                    break
                }
            }
        }
    }
    
    fileprivate func setupLeftBarButtons() {
        var rightBarItems = [UIBarButtonItem]()
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "MSGPhoneIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CXMessengerChatController.phoneCallAction(_:)))
        rightBarItems.append(menuButton)
        
        
        self.navigationItem.rightBarButtonItems = rightBarItems

        
        let backButton = UIBarButtonItem(image: UIImage(named: "MSGBackIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CXMessengerChatController.goBack(_:)))
        self.navigationItem.leftBarButtonItem = backButton
    }

    @objc fileprivate func phoneCallAction(_ sender: Any) {
        contact?.makePhoneCall()
    }
    
    
    @objc fileprivate func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func setupSendMessageAction() {
        unowned let blockSelf = self
        chatInputAccessoryView.sendMessageActionHandler { (message) in
            if let contactHash = blockSelf.contact?.uid {
                _ = blockSelf.chatMachine.createAndSendMessageForContact(contactHash, text: message)
            }
        }
    }
    
    fileprivate func setupMessageController() {
        let predicate: NSPredicate?
        let sort: NSSortDescriptor?//[String: AnyObject]?
        let sectionNameKeyPath: String?
        if let contact = contact {
            let from = NSPredicate(format: "fromContact.uid == %@ AND toContact.uid == %@", chatMachine.user.uid!, contact.uid!)
            let to = NSPredicate(format: "fromContact.uid == %@ AND toContact.uid == %@", contact.uid!, chatMachine.user.uid!)
            predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [from, to])
            sort =  NSSortDescriptor(key: "timestamp", ascending: true)  // ["timestamp":"ASC"]
            sectionNameKeyPath = "sectionByDate"
        } else {
            predicate = NSPredicate(value: false)
            sort = nil
            sectionNameKeyPath = nil
        }
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: fetchedCacheName)
        self.messageController = CXDMMessage.newFetchedResultsControllerFor(fetchedCacheName,
                                                                            condition: predicate,
                                                                            batchSize: 0,
                                                                            sort: sort,
                                                                            sectionNameKeyPath: sectionNameKeyPath,
                                                                            context: self.chatMachine.mocUI)
        self.messageController?.delegate = self
        self.tableView.reloadData()
    }
    
    fileprivate func setupKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    fileprivate func removeKeyboardObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    fileprivate func setupMessageObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(contactMessagesDidUpdate(_:)), name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactMessagesDidUpdate), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(contactUnreadMessagesCountDidUpdate(_:)), name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: nil)
    }
    
    fileprivate func removeMessageObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactMessagesDidUpdate), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactsDidUpdate), object: nil)
    }


    
}


// MARK: - inputAccessoryView
extension CXMessengerChatController {
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override var inputAccessoryView: UIView? {
        get { return chatInputAccessoryView }
    }
    
}


// MARK: - UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate
extension CXMessengerChatController: NSFetchedResultsControllerDelegate {
    
    // UITableView
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.messageController?.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.messageController!.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = self.messageController!.object(at: indexPath) as? CXDMMessage
        let height = CXMessengerChatMessageCell.heightForMessage(message: message?.text, inTableView: tableView)
        return height
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 26
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        printLog(items: "indexPath \(indexPath.section)-\(indexPath.row)")
        if let message = self.messageController!.object(at: indexPath) as? CXDMMessage {
            let cellIdentifier = (message.fromContact == chatMachine.user) ? "UserMessageCell" : "ContactMessageCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CXMessengerChatMessageCell {
                cell.message = message
                return cell
            }
        }
        fatalError("not found Cell or data")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? CXMessengerChatMessageCell,
//            let message = cell.message {
//        }
    }
    
    private func tableViewSectionsName(rawName: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = CXDMMessage.formatSectionByDate
        if let date = dateFormatter.date(from: rawName) {
            let accuracy = date.accuracyToDate(Date())
            printLog(items: accuracy)
            switch accuracy {
            case .Second, .Minute, .Hour, .Day:
                return Localized.TableSectionToday()
            case .Week:
                return date.formattedWith("EEEE")
            case .Month, .Year:
                return date.formattedWith("dd MMMM yyyy")
            default:
                return date.formattedWith("dd MMMM yyyy")
            }
        }
        return rawName
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "section") as? CXMessengerChatSectionHeaderView {
            view.title = tableViewSectionsName(rawName: messageController!.sections![section].name)
            return view
        }
        fatalError("not found Header View or data")
    }
    
//    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return messageController.sections![section].name
//    }
    
    // NSFetchedResultsController
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            if let indexPath = indexPath,
                let cell = tableView.cellForRow(at: indexPath) as? CXMessengerChatMessageCell,
                let message = anObject as? CXDMMessage {
                cell.message = message
            }
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
        showPlaceholderIsNeed()
        doAfterDelayInSeconds(1.0) {
            if self.ifShowLastSection() {
                self.scrollToBottom(false)
            }
        }
    }
    
    func showPlaceholderIsNeed() {
        //placeholder
    }
}

