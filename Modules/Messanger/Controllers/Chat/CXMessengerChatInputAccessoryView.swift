//
//  CXMessengerChatInputAccessoryView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 07.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerChatInputAccessoryView: UIView, UITextViewDelegate {
    
    fileprivate let CHARASTERS_LIMIT = 350
    
    @IBOutlet private weak var textView: CXMessengerChatGrowingTextView!
    @IBOutlet private weak var placeholderLabel: UILabel!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var minHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var maxHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    
    @IBAction private func sendAction(_ sender: Any) {
        if let sendMessageAction = sendMessageAction {
            sendMessageAction(textView.text)
            textView.text = ""
            textDidChange()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.autoresizingMask = UIViewAutoresizing.flexibleHeight

        textView.delegate = self
        
        let h = textView.font!.lineHeight
        minHeightConstraint.constant = h*1 + 8*2
        maxHeightConstraint.constant = h*4 + 8*2
        heightConstraint.constant = minHeightConstraint.constant
        
        updateLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
    }

    private func textDidChange() {
        updateLayout()
        
        let textRect = textView.layoutManager.usedRect(for: textView.textContainer)
        let textHeight = textRect.size.height + textView.textContainerInset.top + textView.textContainerInset.bottom;
        heightConstraint.constant = textHeight
        
        setNeedsLayout()
        layoutIfNeeded()
    }

    override public var intrinsicContentSize: CGSize {
        get {
            let textSize = self.textView.sizeThatFits(CGSize(width: self.textView.bounds.width, height: CGFloat.greatestFiniteMagnitude))
            return CGSize(width: self.bounds.width, height: textSize.height)
        }
    }
    
    var placeholderText: String? {
        get { return placeholderLabel.text }
        set(value) { placeholderLabel.text = value }
    }
    
    typealias SendMessageActionHandler = (_ message: String) -> Void

    func sendMessageActionHandler(handler: @escaping SendMessageActionHandler) {
        sendMessageAction = handler
    }
    
    private var sendMessageAction: SendMessageActionHandler?
    
    func updateLayout() {
        placeholderLabel.isHidden = (textView.text.characters.count > 0)
        sendButton.isEnabled = (textView.text.characters.count > 0)
    }
    
    
    //
    func textViewDidChange(_ textView: UITextView) {
        textDidChange()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentCharacterCount = textView.text.characters.count
        let newLength = currentCharacterCount + text.characters.count - range.length
        print(range, currentCharacterCount, newLength)
        let canChange = newLength <= CHARASTERS_LIMIT
        if !canChange {
            var newText: String! = textView.text
            let newRange = newText.rangeFromNSRange(aRange: range)
            newText = newText.replacingCharacters(in: newRange, with: text)
            let newIndex = newText.index(newText.startIndex, offsetBy: CHARASTERS_LIMIT)
            newText = newText.substring(to: newIndex)
            textView.text = newText
        }
        return canChange
    }
}
