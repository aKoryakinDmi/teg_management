//
//  CXMessengerChatMessageCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 06.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

private func CXMessengerChatMessageCell_InitMessageLabelAttribytes() -> [String : AnyObject] {
    var attribytes = [String : AnyObject]()
    
    attribytes[NSFontAttributeName] = UIFont(SFUIType: .Regular, size: 14)!

    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = NSTextAlignment.left
    paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
    paragraphStyle.lineSpacing = 0.1
    attribytes[NSParagraphStyleAttributeName] = paragraphStyle
    
    return attribytes
}

private let CXMessengerChatMessageCell_MessageLabelAttribytes: [String : AnyObject] = CXMessengerChatMessageCell_InitMessageLabelAttribytes()

class CXMessengerChatMessageCell: UITableViewCell {

    @IBOutlet private weak var avatarView: CXMessengerAvatarView!
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.selectionStyle = UITableViewCellSelectionStyle.None
        avatarView.hiddenStatus = true
        messageLabel.textColor = MSGColor.text
        messageLabel.font = UIFont(SFUIType: .Regular, size: 14)
        dateLabel.textColor = MSGColor.disableText
        dateLabel.font = UIFont(SFUIType: .Regular, size: 12)
    }
    
    var message: CXDMMessage? {
        didSet { update() }
    }
    
    func update() {
        avatarView.contact = message?.fromContact
        var messageText = message?.text
        if let text = messageText, text.characters.count > 250 {
            let index: String.Index = text.index(text.startIndex, offsetBy: 250)
            messageText = text.substring(to: index)
        }
        messageLabel.text = messageText
        dateLabel.text = message?.dataTime?.formattedAsTime
    }
    
    class func heightForMessage(message: String?, inTableView tableView: UITableView) -> CGFloat {
        let widthShift: CGFloat = 135.0
        let heightShift: CGFloat = 28.0
        let heightMin: CGFloat = 70.0
        let heightMax: CGFloat = 250.0
        
        if let message = message {
            let width: CGFloat = tableView.frame.width - widthShift
            let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
            
            let boundingBox = message.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: CXMessengerChatMessageCell_MessageLabelAttribytes, context: nil)
            let result = max(heightMin,min(heightMax, boundingBox.height + heightShift))
            printLog(items: result)
            return result
        }
        return heightMin
        
    }

}



class CXMessengerChatMessageInCell: CXMessengerChatMessageCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        messageLabel.textColor = MSGColor.chatInText
    }
}

class CXMessengerChatMessageOutCell: CXMessengerChatMessageCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        messageLabel.textColor = MSGColor.chatOutText
    }
}


