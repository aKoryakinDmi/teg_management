//
//  CXMessengerRosterDropdownTitle.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.05.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerRosterDropdownTitleContentView: CXNavigationDropdownTitleContentView {
    override func awakeFromNib() {
        super.awakeFromNib()
        titleFont = UIFont(SFUIType: .Regular, size: 20)
        titleColor = MSGColor.navigationBarText
    }
}