//
//  CXMessengerRosterPlaceholderView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 25.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerRosterPlaceholderView: UIView {
    private let imageMargine: CGFloat = 20
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var cellsImageView: UIImageView!
    @IBOutlet private weak var cellsImageViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont(SFUIType: .Bold, size: 18)
        messageLabel.font = UIFont(SFUIType: .Regular, size: 14)
        titleLabel.textColor = MSGColor.disableText
        messageLabel.textColor = MSGColor.disableText
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutPlaceholderImage()
    }
    
    func layoutPlaceholderImage() {
        let maxImageViewHeight = self.frame.maxX - self.messageLabel.frame.minX - imageMargine
        if let image = UIImage(named: "MSGRosterPlaceholder") {
            let imageCellHeight = image.size.height //* image.scale
            let countCells = floor(maxImageViewHeight/imageCellHeight)
            cellsImageView.image = image
            cellsImageViewHeightConstraint.constant = countCells * imageCellHeight
        }
    }
    
    var title: String? {
        get { return titleLabel.text }
        set(value) { titleLabel.text = value}
    }

    var message: String? {
        get { return messageLabel.text }
        set(value) { messageLabel.text = value}
    }
    
}
