//
//  CXMessengerRosterController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.04.16.
//
//

import UIKit
import CoreData

private let Localized = CXLocalized.Messenger.Roster
private let MSGColor = MessengerSchemeColor.sharedInstance

private let supportUrl = URL(string: "http://www.teghelp.com/index.php?/Tickets/Submit")!

class CXMessengerRosterController: UITableViewController {
    
    @IBOutlet fileprivate var placeholderView: CXMessengerRosterPlaceholderView!
    
    let chatMachine = CXMessengerChatMachine.sharedInstance
    fileprivate var fetchedContactCacheName = "CXMessengerRoster"
    fileprivate var contactController: NSFetchedResultsController<NSFetchRequestResult>!
    
    fileprivate var needShowPlaceholder: Bool = true
    fileprivate var searchActive: Bool = false
    
    fileprivate var dropdownMenu: CXNavigationDropdownMenu?
    
    var avatarView: CXMessengerAvatarView?
    
    var contactsListGroup: CXMCMGroupType? = nil {
        didSet {
            updateContactsByGroup(contactsListGroup)
            filterContactsByGroup(contactsListGroup)
            if oldValue != contactsListGroup {
                let shiftTop = tableView.scrollIndicatorInsets.top
                tableView.setContentOffset(CGPoint(x: 0, y: -shiftTop), animated:true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatMachine.connect()
        
        addChatMachineObserver()
        
        setupContactController()
        filterContactsByGroup()
        
        setupDropdownMenu()

        searchDisplayController?.setupSearchCXApperance(tableView: tableView, placeholderText: Localized.SearchPlaceholder())
        

        tableView.addbackgroundViewAndShiftingSubview(withColor: MSGColor.navigationBarView)
        
        contactsListGroup = nil

        setupBarButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchDisplayController?.isActive == true {
           searchDisplayController?.searchBar.becomeFirstResponder()
        }
    }
    
    deinit {
        removeChatMachineObserver()
    }

    
    // MARK: Button Actions
    @objc fileprivate func goBack(_ sender: Any) {
        if let navigationController = navigationController as? CXMessengerNavigationController {
            navigationController.goBack()
        }
    }
   
    @objc fileprivate func avatarAction(_ sender: Any) {
        toProfile(contact: chatMachine.user)
    }
    
    func toProfile(contact: CXDMContact) {
        if let storyboard = storyboard {
            if let controller = storyboard.instantiateViewController(withIdentifier: "Profile") as? CXMessengerProfileController {
                controller.contact = contact
                navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func toChat(contact: CXDMContact) {
        if let storyboard = storyboard {
            if let controller = storyboard.instantiateViewController(withIdentifier: "Chat") as? CXMessengerChatController {
                controller.contact = contact
                navigationController?.pushViewController(controller, animated: true)
            }
        }
    }

    func toChat(contactHash: String) {
        if let contact: CXDMContact = CXDMContact.find(["uid": contactHash, "ownerUid": chatMachine.user.uid!], context: chatMachine.mocUI) {
            toChat(contact: contact)
        }
    }

    
    @objc fileprivate func openActionSheet(_ sender: Any) {
        let actionSheetItems = [
            //CXSheetActionItem(title: "Add Contact", imageName: "MSGAddContact"),
            //CXSheetActionItem(title: "Connection Requests", imageName: "MSGRequests"),
            CXSheetActionItem(title: Localized.MenuItemSettings(), imageName: "MSGSettings"),
            CXSheetActionItem(title: Localized.MenuItemSupport(), imageName: "MSGSupport"),
        ]
        if let actionSheet = CXActionSheet(controller: self, items: actionSheetItems) {
            weak var blockSelf = self
            actionSheet.didSelectItemAtIndexHandler = { (indexPath: Int) -> () in
                printLog(items: "Did select ActionSheet item at index: \(indexPath) \(String(describing: blockSelf))")
                switch indexPath {
                case 0: // Settings
                    if let controller = blockSelf?.storyboard?.instantiateViewController(withIdentifier: "Settings") as? CXMessengerSettingsController,
                        let user = blockSelf?.chatMachine.user {
                        controller.user = user
//                        blockSelf?.navigationController?.pushViewController(controller, animated: true)
                        
                        let nav = CXMessengerNavigationController(rootViewController: controller)
                        print(blockSelf!)
                        blockSelf?.present(nav, animated: true, completion: nil)
                    }
                    
                case 1: // Support
                    UIApplication.shared.openURL(supportUrl);
                default:
//                    blockSelf?.chatMachine.playReceivedMessageNotificationSound()
                    break
                }
            }
            actionSheet.present()
        }
    }
}

// MARK: - Segues
extension CXMessengerRosterController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Chat" {
            if let indexPath = tableView.indexPathForSelectedRow ?? searchDisplayController?.searchResultsTableView.indexPathForSelectedRow,
                let contact = contactController.object(at: indexPath) as? CXDMContact,
                let controller = segue.destination as? CXMessengerChatController {
                controller.contact = contact
            }
        }
    }
    
}

// MARK: - Setup
extension CXMessengerRosterController {
    fileprivate func addChatMachineObserver() {
        chatMachine.addSocketObserver(self,
                                      selectorDidOpen: #selector(socketManagerNotificationDidOpen(_:)),
                                      selectorDidFailWithError: #selector(socketManagerNotificationDidFailWithError(_:)),
                                      selectorDidCloseWithCode: #selector(socketManagerNotificationDidCloseWithCode(_:))
        )
        
        NotificationCenter.default.addObserver(self, selector: #selector(chatMachineNotificationUserDidUpdate(_:)), name:NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUserDidUpdate), object: nil)

        
        
    }
    
    fileprivate func removeChatMachineObserver() {
        chatMachine.removeSocketObserver(self)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUserDidUpdate), object: nil)
    }
    
    
    
    fileprivate func setupContactController() {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: fetchedContactCacheName)
        contactController = CXDMContact.newFetchedResultsControllerFor(fetchedContactCacheName,
                                                                            sort: ["lastMessage.timestamp": "DESC", "lastName": "ASC", "firstName": "ASC"],
                                                                            context: chatMachine.mocUI)
        contactController.delegate = self
    }
    
    fileprivate func getDropdownMenuItems() -> [CXMessengerRosterDropdownItem] {
        var items = [CXMessengerRosterDropdownItem]()
        items.append(CXMessengerRosterDropdownItem(title: Localized.MenuItemAllContacts(), group: nil, checkmarked: true))
        if chatMachine.user.canHasMyCompanyGroup {
            items.append(CXMessengerRosterDropdownItem(title: Localized.MenuItemMyCompany(), group: .MyCompany))
        }
        items.append(CXMessengerRosterDropdownItem(title: Localized.MenuItemBookingsInProgress(), group: .InProgress))
        items.append(CXMessengerRosterDropdownItem(title: Localized.MenuItemGeneralExchange(), group: .GeneralExchnange))
        return items
    }
    
    fileprivate func setupDropdownMenu() {
        if let dropdownMenu = CXNavigationDropdownMenu(controller: self, items: getDropdownMenuItems()) {
            dropdownMenu.titleColor = MSGColor.navigationBarText
            weak var blockSelf = self
            blockSelf?.dropdownMenu = dropdownMenu
            dropdownMenu.didSelectItemAtIndexHandler = { (menu: CXNavigationDropdownMenuViewController, indexPath: Int) -> () in
                if let item = menu.items[indexPath] as? CXMessengerRosterDropdownItem {
                    blockSelf?.contactsListGroup = item.group
                }
            }
        }
    }
    
    fileprivate func setupBarButtons() {
        setupRightBarButtons()
        setupLeftBarButtons()
    }
    
    fileprivate func setupRightBarButtons() {
        var rightBarItems = [UIBarButtonItem]()
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "MSGMenuIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CXMessengerRosterController.openActionSheet(_:)))
        rightBarItems.append(menuButton)
        
        if let avatarButton = setupAvatarButton() {
            rightBarItems.append(avatarButton)
        }
        
        navigationItem.rightBarButtonItems = rightBarItems
    }
    
    fileprivate func setupLeftBarButtons() {
        #if TypeProgect_cx4i
            let backButton = UIBarButtonItem(image: UIImage(named: "MSGBackIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(CXMessengerRosterController.goBack(_:)))
        #elseif TypeProgect_TEG_management
            let backButton = UIBarButtonItem(image: UIImage(named: "burger"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(CXMessengerRosterController.goBack(_:)))
        #endif
        navigationItem.leftBarButtonItem = backButton
    }
    
    
    fileprivate func setupAvatarButton() -> UIBarButtonItem? {
        if let tAvatarView = CXMessengerAvatarView.loadFromNib() {
            tAvatarView.contact = chatMachine.user
            tAvatarView.addTarget(target: self, action: #selector(CXMessengerRosterController.avatarAction(_:)))
            tAvatarView.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
            avatarView = tAvatarView
            let barButton = UIBarButtonItem(customView: tAvatarView)
            return barButton
        }
        return nil
    }
}

// MARK: - NSNotification
extension CXMessengerRosterController {
    
    // Soket Notification
    @objc func socketManagerNotificationDidOpen(_ notification: NSNotification) {
    }
    
    @objc func socketManagerNotificationDidFailWithError(_ notification: NSNotification) {
    }
    
    @objc func socketManagerNotificationDidCloseWithCode(_ notification: NSNotification) {
    }
    
    @objc func chatMachineNotificationUserDidUpdate(_ notification: NSNotification) {
        dropdownMenu?.update(items: getDropdownMenuItems())
    }

    
    // Core Data Notification
    @objc func chatMachineNotificationContactsDidUpdate(_ notification: NSNotification) {
    }
    
    @objc func chatMachineNotificationContactStatusDidUpdate(_ notification: NSNotification) {
    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate
extension CXMessengerRosterController: NSFetchedResultsControllerDelegate {

    private func actionCellAvatar(contact: CXDMContact?) {
        if let contact = contact {
            toProfile(contact: contact)
        }
    }
    // UITableView
    override func numberOfSections(in tableView: UITableView) -> Int {
        return contactController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = contactController.sections![section]
        needShowPlaceholder = (sectionInfo.numberOfObjects == 0) && (tableView == self.tableView)
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as? CXMessengerRosterContactCell ??
                 self.tableView.dequeueReusableCell(withIdentifier: "ContactCell") as? CXMessengerRosterContactCell,
         let contact = contactController.object(at: indexPath) as? CXDMContact {
            cell.contact = contact
            return cell
        }
        fatalError("not found Cell or data")
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let tableView = scrollView as? UITableView,
            let backgroundView = tableView.backgroundView {
            for view in backgroundView.subviews {
                if let shiftingTopView = view as? ShiftingTopView {
                    shiftingTopView.bottomPosition = -scrollView.contentOffset.y
                }
            }
        }
    }
    
    // NSFetchedResultsController
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            if let indexPath = indexPath,
                let cell = tableView.cellForRow(at: indexPath) as? CXMessengerRosterContactCell,
                let contact = anObject as? CXDMContact {
                cell.contact = contact
            }
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        showPlaceholderIsNeed()
    }
    
    func showPlaceholderIsNeed() {
        if needShowPlaceholder && !searchActive {
            var frame = tableView.bounds
            frame.size.height -= 120;
            placeholderView.frame = frame
            placeholderView.message = Localized.NoDataTitle()
            switch contactsListGroup {
            case .InProgress?:
                placeholderView.title = Localized.NoDataInBookingsInProgress()
            case .MyCompany?:
                placeholderView.title = Localized.NoDataInMyCompany()
            case .GeneralExchnange?:
                placeholderView.title = Localized.NoDataInGeneralExchange()
            default:
                placeholderView.title = Localized.NoDataInAnyGroup()
            }
            tableView.tableFooterView = placeholderView
        } else {
            tableView.tableFooterView = nil
        }
    }
}

// MARK: - UISearchBarDelegate
extension CXMessengerRosterController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBarBegin()
        // TODO: fix
        let v = UIView(frame: CGRect(x: 0, y: -20, width: searchBar.frame.width, height: 20))
        v.backgroundColor = MSGColor.navigationBarView
        searchBar.addSubview(v)
        if let text = searchBar.text {
            filterContentForSearchText(text)
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchText)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //searchBarEnd()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBarEnd()
    }
    
    func searchBarBegin() {
        searchActive = true
        showPlaceholderIsNeed()
    }
    func searchBarEnd() {
        searchActive = false
        filterContactsByGroup(contactsListGroup)
    }
    
    //MARK: Filters
    func updateContactsByGroup(_ group: CXMCMGroupType? = nil) {
        if let trueGroup = group {
            switch trueGroup {
            case .MyCompany:
                chatMachine.sendGetMyCompanyContacts()
            case .InProgress:
                chatMachine.sendGetInProgressContacts()
            case .GeneralExchnange:
                chatMachine.sendGetGeneralExchnangeContacts()
            }
        } else {
            chatMachine.sendGetAllContacts()
        }
    }
    
    func filterContactsByGroup(_ group: CXMCMGroupType? = nil) {
        printLog(items: "UserUid", chatMachine.user.uid!)
        let showGroupsList: [Int]
        if let group = group {
            showGroupsList = [group.rawValue]
        } else {
            showGroupsList = [
                CXMCMGroupType.InProgress.rawValue,
                CXMCMGroupType.MyCompany.rawValue,
                CXMCMGroupType.GeneralExchnange.rawValue,
            ]
        }
        let ownerPredicate = NSPredicate(format: "ownerUid == %@", chatMachine.user.uid!)
        let groupPredicate = NSPredicate(format: "ANY groups.uid IN %@", showGroupsList)
        contactController.fetchRequest.predicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [ownerPredicate,groupPredicate])

        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: fetchedContactCacheName)
        try! contactController.performFetch()
        tableView.reloadData()
        showPlaceholderIsNeed()
    }
    
    func filterContentForSearchText(_ searchText: String) {
        let searchText = searchText.trimmingCharacters(in: .whitespaces)
        if searchText.characters.count > 0 {
            let userPredicate = NSPredicate(format: "uid != %@", chatMachine.user.uid!)
            let ownerPredicate = NSPredicate(format: "ownerUid == %@", chatMachine.user.uid!)
            let words = searchText.components(separatedBy: .whitespaces)
            let predicates = words.map { NSPredicate(format: "firstName CONTAINS[cd] %@ OR lastName CONTAINS[cd] %@", $0,$0) }
            let searchPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: predicates)

            
            NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: fetchedContactCacheName)
            contactController.fetchRequest.predicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [userPredicate, ownerPredicate, searchPredicate])
            try! contactController.performFetch()
            tableView.reloadData()
            showPlaceholderIsNeed()
        } else {
            filterContactsByGroup(contactsListGroup)
        }
    }
}

