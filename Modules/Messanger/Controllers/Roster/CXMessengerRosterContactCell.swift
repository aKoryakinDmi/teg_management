//
//  CXMessengerRosterContactCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 09.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

public class CXMessengerRosterContactCell: UITableViewCell {
    
    @IBOutlet private weak var avatar: CXMessengerAvatarView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var companyLabel: UILabel!
    @IBOutlet private weak var companyImageView: UIImageView!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var badgeView: CXBadgeView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        avatar.isUserInteractionEnabled = false
        
        nameLabel.font = UIFont(SFUIType: .Regular, size: 20)
        messageLabel.font = UIFont(SFUIType: .Regular, size: 14)
        companyLabel.font = UIFont(SFUIType: .Regular, size: 13)
        timeLabel.font = UIFont(SFUIType: .Regular, size: 13)
        badgeView.font = UIFont(SFUIType: .Bold, size: 10)
        
        nameLabel.textColor = MSGColor.text
        messageLabel.textColor = MSGColor.text
        companyLabel.textColor = MSGColor.disableText
        timeLabel.textColor = MSGColor.text
        badgeView.textColor = MSGColor.text
        
        messageLabel.highlightedTextColor = MSGColor.disableText
        companyLabel.highlightedTextColor = MSGColor.disableText
        
        badgeView.badgeBackgroundColor = MSGColor.navigationBarView
        
        NotificationCenter.default.addObserver(self, selector: #selector(socketManagerNotificationContactMessagesDidUpdate(_:)), name:NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactMessagesDidUpdate), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(socketManagerNotificationContactUnreadMessagesCountDidUpdate(_:)), name:NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: nil)

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactMessagesDidUpdate), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: nil)
    }

    func addAvatarTarget(target: AnyObject?, action: Selector, forControlEvents controlEvents: UIControlEvents = UIControlEvents.touchUpInside) {
        avatar.addTarget(target: target, action: action, forControlEvents: controlEvents)
    }
    
    func didAvatarTapHandler(handler: ((_ contact: CXDMContact?) -> ())?) {
        weak var blockSelf = self
        if let handler = handler {
            avatar.didTapHandler =  { () -> () in
                handler(blockSelf?.contact)
            }
        } else {
            avatar.didTapHandler = nil
        }
    }
    
    var contact: CXDMContact? {
        didSet {
            update()
        }
    }
    
    func update() {
        nameLabel.text = contact?.fullName ?? ""
        companyLabel.text = contact?.fullCompany ?? ""
        companyImageView.isHidden = !((companyLabel.text?.characters.count)! > 0)
        messageLabel.text = contact?.uid ?? "" //contact.lastMessageText

        

        if let contact = contact {
            
            //messageLabel & timeLabel
            let lastMessage: CXDMMessage? = contact.lastMessage
            messageLabel.text = lastMessage?.text
            timeLabel.text = lastMessage?.dataTime?.formattedShortByPeriod()
            timeLabel.isHidden = (lastMessage?.dataTime == nil)
            
            // nameLabel(Font) & badgeView
            let count = contact.unreadMessagesCount?.intValue ?? 0
            badgeView.text = "\(count)"
            badgeView.isHidden = !(count > 0)
            nameLabel.font = UIFont(SFUIType: (count > 0) ? .Bold : .Regular, size: 20)
            messageLabel.textColor = (count > 0) ? MSGColor.text : MSGColor.disableText
        }
        avatar.contact = contact
    }
    
    private func updateLastMessage() {
//        contact?.detectLastMessage(<#T##ownerUid: String##String#>)
        if let lastMessage: CXDMMessage = contact?.lastMessage {
            
            messageLabel.text = lastMessage.text
            if let dataTime = lastMessage.dataTime {
                timeLabel.isHidden = false
                timeLabel.text =  dataTime.formattedByPeriod()
            } else {
                timeLabel.isHidden = true
            }
        } else {
            timeLabel.isHidden = true
        }
    }
    
    func socketManagerNotificationContactMessagesDidUpdate(_ notification: NSNotification) {
        if let uid = notification.userInfo?["uid"] as? String, uid == contact?.uid {
            update()
        }
    }
    func socketManagerNotificationContactUnreadMessagesCountDidUpdate(_ notification: NSNotification) {
        if let uid = notification.userInfo?["uid"] as? String, uid == contact?.uid {
            update()
        }
    }

}
