//
//  CXMessengerRosterDropdownItem.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

class CXMessengerRosterDropdownItem: CXDropdownItem {
    var group: CXMCMGroupType?    
    init(title: String, group: CXMCMGroupType?,  checkmarked: Bool = false) {
        super.init(title: title)
        self.checkmarked = checkmarked
        self.group = group
    }
}
