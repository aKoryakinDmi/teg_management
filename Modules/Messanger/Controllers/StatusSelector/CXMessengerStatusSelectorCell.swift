//
//  CXMessengerStatusSelectorCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 15.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerStatusSelectorCell: UITableViewCell {

    @IBOutlet private weak var statusBageView: CXMessengerStatusBadgeView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var checkmarkView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = MSGColor.text
        titleLabel.font = UIFont(SFUIType: .Regular, size: 20)
        statusBageView.statusFace = .Bold
    }
    
    func bind(status: CXMessengerStatus, checkmarked: Bool) {
        statusBageView.status = status
        checkmarkView.isHidden = !checkmarked
    }
    
    var status: CXMessengerStatus {
        get {
            return statusBageView.status
        }
        set(value) {
            statusBageView.status = value
            titleLabel.text = CXLocalized.Messenger.ChatStatus.Status(value)
        }
    }

    var checkmarked: Bool {
        get {
            return !checkmarkView.isHidden
        }
        set(value) {
            checkmarkView.isHidden = !value
        }
    }
    
}
