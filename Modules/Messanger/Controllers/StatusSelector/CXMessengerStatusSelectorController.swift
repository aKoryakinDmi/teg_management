//
//  CXMessengerStatusSelectorController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 14.04.16.
//
//

import Foundation

private let Localized = CXLocalized.Messenger.StatusSelector

class CXMessengerStatusSelectorController : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var status:CXMessengerStatus = .AwayFromDesk
    var contact: CXDMContact? {
        didSet {
            updateStatus(contact)
        }
    }
    
    let chatMachine = CXMessengerChatMachine.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        addMCMObserver()
        updateStatus(contact)
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        tableView?.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.estimatedRowHeight = 71
        
        tableView?.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 20))

        
        self.navigationItem.title = Localized.NavigationTitle()
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)

    }
    
    deinit {
        removeMCMObserver()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if #available(iOS 8.0, *) {
            return UITableViewAutomaticDimension
        } else {
            return 71.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for:indexPath) as? CXMessengerStatusSelectorCell
        if let cell = cell {
            switch indexPath.row {
            case 0:
                cell.status = .Avaliable
            case 1:
                cell.status = .AwayFromDesk
            case 2:
                cell.status = .NotAvaliable
            case 3:
                cell.status = .Offline
            default:
                cell.status = .Offline
            }
            cell.checkmarked = (status == cell.status)
            return cell
        }
        fatalError("Cell can not convert to registred class")
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? CXMessengerStatusSelectorCell {
            status = cell.status
        }
        tableView.reloadData()
        chatMachine.sendSetStatusForUser(status: status)
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - MCM Observer
extension CXMessengerStatusSelectorController {
    
    func addMCMObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(mcmContactStatusDidUpdate(_:)), name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)
    }
    
    func removeMCMObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)
    }
    
    @objc private func mcmContactStatusDidUpdate(_ notification: NSNotification) {
        if let _ = notification.userInfo?["all"] {
            updateStatus(contact)
        } else
            if let updatedUid = notification.userInfo?["uid"] as? String,
                let contactUid = contact?.uid {
                if updatedUid == contactUid {
                    updateStatus(contact)
                }
        }
    }
    
    func updateStatus(_ contact: CXDMContact?) {
        if let status = contact?.status {
            self.status = CXMessengerStatus(rawValue: status) ?? CXMessengerStatus.Offline
        } else {
            self.status = CXMessengerStatus.Offline
        }
        tableView?.reloadData()
    }
    
}
