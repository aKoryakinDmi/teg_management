//
//  CXMessengerProfileСommunicationView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

import Foundation

enum CXMessengerProfileСommunicationType: Int {
    case Chat
    case Phone
    case Mail
}

class CXMessengerProfileСommunicationView: UIView {
    
    var didTouchButtonHandler: ((_ buttonType: CXMessengerProfileСommunicationType) -> ())?

    func sendToHandlerTouchWithType(_ buttonType:CXMessengerProfileСommunicationType) {
        if let handler = self.didTouchButtonHandler {
            handler(buttonType)
        }
    }
    
    @IBAction func chatButtonAction(_ sender: Any) {
        sendToHandlerTouchWithType(.Chat)
    }

    @IBAction func phoneButtonAction(_ sender: Any) {
        sendToHandlerTouchWithType(.Phone)
    }

    @IBAction func mailButtonAction(_ sender: Any) {
        sendToHandlerTouchWithType(.Mail)
    }

}
