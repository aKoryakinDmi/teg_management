//
//  CXMessengerProfileLogoutCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance
 
class CXMessengerProfileLogoutCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = MSGColor.profileItemDestructText
        titleLabel.font = UIFont(SFUIType: .Regular, size: 20)
    }
}