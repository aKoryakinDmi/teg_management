//
//  CXMessengerProfileHeaderView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerProfileHeaderView: UIView {
        
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarView: CXMessengerAvatarView!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.textColor = MSGColor.profileNavigationBarText
        nameLabel.font = UIFont(SFUIType: .Regular, size: 20)
        placeLabel.textColor = MSGColor.profileHeaderText
        placeLabel.font = UIFont(SFUIType: .Regular, size: 15)
    }
    
    func addOnContainerView(view: UIView) {
        self.containerView.addSubview(view)
        view.addConstraintsToFillSuperview()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if let button = hitView as? UIButton {
            return button
        }
        return nil
    }
}
