//
//  CXMessengerProfileStatusView.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerProfileStatusView: UIView {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var badgeView: CXMessengerStatusBadgeView!
    @IBOutlet private weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = MSGColor.profileHeaderText
        titleLabel.font = UIFont(SFUIType: .Regular, size: 20)
        valueLabel.textColor = MSGColor.profileHeaderText
        valueLabel.font = UIFont(SFUIType: .Regular, size: 15)
        badgeView.statusFace = .Bold
        
        NotificationCenter.default.addObserver(self, selector: #selector(socketManagerNotificationContactStatusDidUpdate(_:)), name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: nil)
    }
    
    var didTouchButtonHandler: (() -> ())?
    
    private func sendToHandlerTouchWithType() {
        if let handler = self.didTouchButtonHandler {
            handler()
        }
    }
    
    @IBAction private func statusButtonAction(_ sender: Any) {
        sendToHandlerTouchWithType()
    }
    
    private var status:CXMessengerStatus {
        get {
            return badgeView.status
        }
        set(value) {
            badgeView.status = value
            valueLabel.text = CXLocalized.Messenger.ChatStatus.Status(value)
        }
    }
    
    // MARK: - Chat Machine
    var contact: CXDMContact? {
        didSet {
            self.updateStatus(self.contact)
        }
    }

    func socketManagerNotificationContactStatusDidUpdate(_ notification: NSNotification) {
        if let _ = notification.userInfo?["all"] {
            updateStatus(self.contact)
        } else
        if let updatedUid = notification.userInfo?["uid"] as? String,
            let contactUid = contact?.uid {
            if updatedUid == contactUid {
                updateStatus(self.contact)
            }
        }
    }

    func updateStatus(_ contact: CXDMContact?) {
        if let status = contact?.status {
            self.status = CXMessengerStatus(rawValue: status) ?? CXMessengerStatus.Offline
        } else {
            self.status = CXMessengerStatus.Offline
        }
    }
}
