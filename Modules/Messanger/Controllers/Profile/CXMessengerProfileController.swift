//
//  CXMessengerProfileController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

import Foundation
import MessageUI

private let Localized = CXLocalized.Messenger.Profile

class CXMessengerProfileController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet private weak var headerView: CXMessengerProfileHeaderView!
    @IBOutlet private weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var communicationView: CXMessengerProfileСommunicationView!
    @IBOutlet private var statusView: CXMessengerProfileStatusView!
    @IBOutlet private weak var tableView: UITableView!
    
    var restorationCache = [String: AnyObject]()
    var dafaultHeaderViewHeight: CGFloat = 240

    private var ownProfile: Bool? {
        get {
            if ((contact as? CXDMUser) != nil) {
                return true
            } else {
                return false
            }
        }
    }
    
    var contact: CXDMContact?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false

        headerView.avatarView.contact = contact
        headerView.nameLabel.text = contact?.fullName ?? ""
        headerView.placeLabel.text = contact?.timeZoneName

        dafaultHeaderViewHeight = headerViewHeightConstraint.constant
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 61
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: dafaultHeaderViewHeight))
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(dafaultHeaderViewHeight, 0, 0, 0)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if #available(iOS 9.0, *) { return true }
        else {
//            if let identifier = identifier {
                switch identifier {
                case "showStatusSelector":
                    if let controller = storyboard?.instantiateViewController(withIdentifier: "StatusSelector") as? CXMessengerStatusSelectorController {
                        controller.contact = contact
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                    return false
                default:
                    break
                }
//            }
        }
        return true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStatusSelector" {
            if let controller = segue.destination as? CXMessengerStatusSelectorController {
                controller.contact = contact
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearNavigationBar(controller: self.navigationController!)

        if ownProfile == true {
            self.headerView.avatarView.hiddenStatus = true
            self.headerView.addOnContainerView(view: statusView)
            statusView.contact = contact
            statusView.didTouchButtonHandler = {() -> () in
                print("button touch with type Status ")
            }
        } else {
            self.headerView.addOnContainerView(view: communicationView)
            weak var blockSelf = self
            communicationView.didTouchButtonHandler = {(buttonType: CXMessengerProfileСommunicationType) -> () in
                print("button touch with type \(buttonType)")
                switch buttonType {
                case .Chat:
                    if let viewControllers = blockSelf?.navigationController?.viewControllers, viewControllers.count > 1 {
                        for controller in viewControllers.reversed() {
                            if let controller = controller as? CXMessengerChatController {
                                controller.contact = blockSelf?.contact
                                blockSelf?.navigationController?.popToViewController(controller, animated: true)
                                return
                            }
                        }
                        if let controller = blockSelf?.storyboard?.instantiateViewController(withIdentifier: "Chat") as? CXMessengerChatController {
                            controller.contact = blockSelf?.contact
                            blockSelf?.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                    break
                case .Phone:
                    blockSelf?.contact?.makePhoneCall()
                    break
                case .Mail:
                    if let blockSelf = blockSelf {
                        blockSelf.contact?.makeEmailSend(controller: blockSelf)
                    }
                    break
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        restoretionNavigationBar(controller: self.navigationController!)
    }

    func clearNavigationBar(controller: UINavigationController) {
        restorationCache["NB.TintColor"] = controller.navigationBar.tintColor
        controller.navigationBar.tintColor = UIColor.white
        restorationCache["NB.BackgroundImage.MetricsDefault"] = controller.navigationBar.backgroundImage(for: UIBarMetrics.default)
        controller.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        restorationCache["NB.ShadowImage"] = controller.navigationBar.shadowImage
        controller.navigationBar.shadowImage = UIImage()
        
        controller.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    func restoretionNavigationBar(controller: UINavigationController) {
        let tintColor = restorationCache["NB.TintColor"] as? UIColor
        controller.navigationBar.tintColor = tintColor
        let image = restorationCache["NB.BackgroundImage.MetricsDefault"] as? UIImage
        controller.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        let shadowImage = restorationCache["NB.ShadowImage"] as? UIImage
        controller.navigationBar.shadowImage = shadowImage
    }
    
    // MARK: UITableViewDelegate & DataSource Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        //information
        //delete | logaut
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 5
        case 1: return 0 // dont show button
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if #available(iOS 8.0, *) {
            return UITableViewAutomaticDimension
        } else {
            return 83
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "InformationCell") as? CXMessengerProfileInformationCell {
                switch indexPath.row {
                case 0:
                    cell.title = Localized.InfoCellName()
                    cell.value = contact?.fullName ?? ""
                case 1:
                    cell.title = Localized.InfoCellEmail()
                    cell.value = contact?.email ?? ""
                case 2:
                    cell.title = Localized.InfoCellPhone()
                    cell.value = contact?.mainPhone ?? ""
                case 3:
                    cell.title = Localized.InfoCellCompanyName()
                    cell.value = contact?.companyName ?? ""
                default:
                    cell.title = ""
                    cell.value = ""
                }
                return cell
            }
        case 1:
            if ownProfile == true {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "LogoutCell") as? CXMessengerProfileLogoutCell {
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteContactCell") as? CXMessengerProfileDeleteContactCell {
                    return cell
                }
            }
        default:
            fatalError("Cell not define for indexPath \(indexPath)")
        }
        fatalError("Cell can not convert to registred class")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        headerViewHeightConstraint.constant = dafaultHeaderViewHeight - scrollView.contentOffset.y
    }

    
}

extension CXMessengerProfileController : MFMailComposeViewControllerDelegate {

    @objc internal func mailComposeController(_ controller: MFMailComposeViewController,
                                              didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
}
