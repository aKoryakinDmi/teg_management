//
//  CXMessengerProfileInformationCell.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class CXMessengerProfileInformationCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = MSGColor.profileItemTitleText
        titleLabel.font = UIFont(SFUIType: .Regular, size: 15)
        valueLabel.textColor = MSGColor.profileItemValueText
        valueLabel.font = UIFont(SFUIType: .Regular, size: 20)
        self.selectionStyle = .none
    }

    var title: String? {
        get { return titleLabel.text }
        set(value) { titleLabel.text = value }
    }

    var value: String? {
        get { return valueLabel.text }
        set(value) { valueLabel.text = value }
    }
}
