//
//  CXMessengerNavigationController.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.04.16.
//
//

import Foundation
import UIKit

private let MSGColor = MessengerSchemeColor.sharedInstance

@objc class CXMessengerNavigationController: UINavigationController {
    
    #if TypeProgect_cx4i
    var previousController: UIViewController?
    #endif

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }
    
    private func setup() {
        self.navigationBar.barTintColor = MSGColor.navigationBarView
        self.navigationBar.tintColor = MSGColor.navigationBarText
        self.navigationBar.setBackgroundImage(UIImage(named: "MSGNavigationBarBaclground"), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isHidden = false
//        self.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(SFUIType: .Regular, size: 14)!]
        self.navigationBar.isTranslucent = true
        self.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(SFUIType: .Regular, size: 20)!,
            NSForegroundColorAttributeName: MSGColor.navigationBarText,
        ]
    }
    
    func goBack() {
        #if TypeProgect_cx4i
            cx4iAppDelegate.getCX4iApplication().toState(self, previousController)
        #elseif TypeProgect_TEG_management
            self.menuContainerViewController.toggleLeftSideMenuCompletion({ 
                //code
            })
        #endif
    }
    
    override var supportedInterfaceOrientations:UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
}

