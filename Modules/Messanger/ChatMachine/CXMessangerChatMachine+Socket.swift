//
//  CXMessengerChatMachine+Socket.swift
//  cx4i
//
//  Created by EvGeniy Lell on 25.04.16.
//
//

import Foundation

typealias CXSocketSerialization = (_ response: AnyObject?) -> AnyObject?

class CXSocketContant: CustomStringConvertible {
    private let value:  String
    var extKey: String { get { return "\\\"\(value)\\\"" } }
    var key: String { get { return value } }
    func field(name:String) -> String { return "\\\"\(name)\\\":\\\"\(value)\\\"" }
    var description:String { return value }
    init(_ value: String) {
        self.value = value
    }
}

let CXSocketUUID = CXSocketContant("{UUID}")
let CXSocketUserHash = CXSocketContant("{userHash}")
let CXSocketSessionId = CXSocketContant("{sessionId}")
let CXSocketMessageId = CXSocketContant("{messageId}")
let CXSocketContactHash = CXSocketContant("{contactHash}")
let CXSocketMessageText = CXSocketContant("{messageText}")
let CXSocketTimeZone = CXSocketContant("{timeZone}")
let CXSocketTimeInterval = CXSocketContant("{timeInterval}")
let CXSocketTimeStamp = CXSocketContant("{timeStamp}")
let CXSocketFirstName = CXSocketContant("{firstName}")
let CXSocketLastName = CXSocketContant("{lastName}")
let CXSocketStatus = CXSocketContant("{status}")
let CXSocketSearch = CXSocketContant("{search}")
let CXSocketSound = CXSocketContant("{sound}")


func CXMessengerChatMachinePrepareMessage(messageObject: [String: Any]) -> String {
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: messageObject, options: [])
        let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
        let escapeJsonString = jsonString.replacingOccurrences(of: "\"", with: "\\\"")
        return "[\""+escapeJsonString+"\"]"
    } catch {
        print("MachineMessage Error:\(error)")
        return ""
    }
}
