//
//  CXMessengerChatMachine.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import SystemConfiguration
import Foundation
import CoreData

@objc class CXChatBridge: NSObject {
    class func connect() {
        CXMessengerChatMachine.sharedInstance.connect()
    }
    
    class func connectIfNeed() {
        CXMessengerChatMachine.sharedInstance.connectIfNeed()
    }
    
    class func disconnect() {
        CXMessengerChatMachine.sharedInstance.disconnect()
    }
    
    static let unreadMessagesCountDidChangedNotification = CXMessengerChatMachineNotificationUnreadMessagesCountDidChanged
    class func unreadMessagesCount() -> Int {
        _ = CXMessengerChatMachineNotificationUnreadMessagesCountDidChanged
        let moc = CXMessengerChatMachine.sharedInstance.mocUI
        let user = CXMessengerChatMachine.sharedInstance.user
        let contactsList:[CXDMContact] = CXDMContact.query(["ownerUid": user!.uid!], context: moc)
        var sum = 0
        for contact in contactsList {
            let count = contact.unreadMessagesCount?.intValue ?? 0
            sum += count
        }
        return sum
    }
    
}

class CXMessengerChatMachine {
    
    static let sharedInstance = CXMessengerChatMachine()
    
    let  coreDataManager = CXMessengerCoreDataManager()
    let  socketManager = CXSocket()
    
    fileprivate let errorHandler = CXErrorHandler.sharedInstance
    fileprivate var socketReachability: CXReachability? = nil
    fileprivate var internetReachability: CXReachability? = nil
    
    var completionQueueMOCList = [Thread: NSManagedObjectContext]()
    fileprivate let sendCompletionQueue = DispatchQueue(label: "chatMachine.sendCompletionQueue.serial")
    //dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)
    //dispatch_queue_create("chatMachine.sendCompletionQueue.serial", DISPATCH_QUEUE_SERIAL)
    
    // MARK: Reconnect
    private let reconnetTimeoutInterval: TimeInterval = 12
    fileprivate let numberTryConnect: UInt = 4
    
    var silentTasksCount: Int = 0 {
        didSet {
            if silentTasksCount < 0 { silentTasksCount = 0 }
            if silentTasksCount == 0 && socketManager.socket?.readyState == .OPEN {
                sendRegisterUserMessageForUser()
                sendRegisterStatusChangeForUser()
            }
        }
    }
    
    private var waitForHostAvailable: Bool = false
    fileprivate var reconnetTimer: Timer?
    
    fileprivate var countTryConnect: UInt = 0 {
        didSet {
            if countTryConnect > numberTryConnect {
                tryConnect = false
            }
        }
    }
    
    fileprivate var tryConnect: Bool = false {
        didSet {
            if tryConnect {
                if socketReachability?.isReachable() == false {
                    waitForHostAvailable = true
                }
                countTryConnect += 1
                if let reconnetTimer = reconnetTimer {
                    reconnetTimer.invalidate()
                    self.reconnetTimer = nil
                }
                reconnetTimer = Timer.scheduledTimer(timeInterval: reconnetTimeoutInterval, target: self, selector: #selector(reconnect(_:)), userInfo: nil, repeats: false)
            } else {
                if socketReachability?.isReachable() == true {
                    waitForHostAvailable = false
                }
                if let reconnetTimer = reconnetTimer {
                    reconnetTimer.invalidate()
                    self.reconnetTimer = nil
                    countTryConnect = 0
                }
            }
        }
    }
    
    @objc private func reconnect(_ timer: Timer) {
        connectIfNeed()
    }
    
    
    // MARK: -
    fileprivate(set) var lastUserStatus: String? = CXMessengerStatus.Offline.rawValue
    private(set) var user: CXDMUser!
    fileprivate func updateUser() {
        user = CXDMUser.findOrCreateByUid(uid: socketManager.parametrs.userHash!, context: mocBackground)
    }
    
    
    
    
    init() {
        sendCompletionQueue.sync() {
            self.user = CXDMUser.findOrCreateByUid(uid: "", context: self.mocBackground)
        }
        setStatusToAllContacts()
        setupReachability()
        addSocketObserver(self,
                          selectorDidOpen: #selector(socketManagerNotificationDidOpen(_:)),
                          selectorDidFailWithError: #selector(socketManagerNotificationDidFailWithError(_:)),
                          selectorDidCloseWithCode: #selector(socketManagerNotificationDidCloseWithCode(_:)))
        addMOCObserver()
        addApplicationObserver()
    }
    
    deinit {
        removeSocketObserver(self)
        removeMOCObserver()
        removeApplicationObserver()
        
        disconnect()
    }
    
    private func setupReachability() {
        do {
            #if TypeProgect_cx4i
                let hostname = Config.getChatHost().absoluteString
            #elseif TypeProgect_TEG_management
                let hostname = CXHTTPManager.Chat.hostUrl().absoluteString
            #endif
            socketReachability = try CXReachability.init(hostname: hostname)
            try socketReachability?.startNotifier()
            internetReachability = try CXReachability.reachabilityForInternetConnection()
        } catch {
            errorHandler.drawMessage(message: "Unable to create Reachability", backgroundColor: UIColor.red)
        }
        
        weak var blockSelf = self
        socketReachability?.whenReachable = { reachability in
            if let blockSelf = blockSelf {
                if blockSelf.waitForHostAvailable {
                    blockSelf.connect()
                }
            }
        }
    }
    
}

// MARK: - CoreData Helpers
extension CXMessengerChatMachine {
    var mocUI: NSManagedObjectContext {
        get {
            return coreDataManager.defaultManagedObjectContext
        }
    }
    
    
    var mocBackground: NSManagedObjectContext {
        get {
            let thread = Thread.current
            if let resultMOC = completionQueueMOCList[thread] {
                return resultMOC
            }
            let newMOC = coreDataManager.newBacgroundManagedObjectContext()
            completionQueueMOCList[thread] = newMOC
            return newMOC
        }
    }
    
    func saveMOC(moc: NSManagedObjectContext) {
        coreDataManager.saveBackgroundContext(context: moc)
        //        let main = NSThread.currentThread().isMainThread
        //        if main {
        //            coreDataManager.saveDefaultContext()
        //        } else {
        //            coreDataManager.saveBackgroundContext(moc)
        //        }
    }
    
    func UILoop(interval: TimeInterval = 0.001) {
        //        let loopUntil = NSDate(timeIntervalSinceNow: interval)
        //        let currentMode = NSRunLoop.currentRunLoop().currentMode
        //        NSRunLoop.currentRunLoop().runMode(currentMode ?? NSDefaultRunLoopMode, beforeDate: loopUntil)
    }
}

// MARK: - Socket Helpers
extension CXMessengerChatMachine {
    func connect() {
        socketManager.connect()
        if socketManager.parametrs.isValid() {
            updateUser()
            countTryConnect = 0
            tryConnect = true
        } else {
            disconnect()
        }
    }
    
    func connectIfNeed() {
        if tryConnect {
            if socketManager.socket?.readyState != .OPEN {
                socketManager.connect()
                if socketManager.parametrs.isValid() {
                    updateUser()
                    tryConnect = true
                } else {
                    disconnect(tryConnectAgain: true, reconnetTimerInvalidate: true)
                }
            }
        }
    }
    
    //    func disconnect() {
    //        socketManager.disconnect()
    //        tryConnect = false
    //    }
    
    func disconnect(tryConnectAgain tryConnect: Bool = false, reconnetTimerInvalidate: Bool = false) {
        socketManager.disconnect()
        self.tryConnect = tryConnect
        if reconnetTimerInvalidate {
            reconnetTimer?.invalidate()
            reconnetTimer = nil
        }
    }
    
    private func defaultSocketParametrs() -> [String: String] {
        return [
            CXSocketSessionId.key: socketManager.parametrs.jwt ?? "?",
            CXSocketUserHash.key: user.uid ?? "?",
            CXSocketTimeZone.key: user.timeZoneName ?? "?",
            CXSocketFirstName.key: user.firstName ?? "" ,
            CXSocketLastName.key: user.lastName ?? "",
        ]
    }
    
    func send(rawMessage: String, uuid: String? = nil, parameters: [String: String]? = nil, serialization: CXSocketSerialization? = nil, completion: CXSocketClosure? = nil) -> CXSocketTask{
        let uuid: String = (uuid != nil) ? uuid! : socketManager.randomStringUUID()
        var message = rawMessage.replacingOccurrences(of: CXSocketUUID.key, with: uuid)
        if let userParameters = parameters {
            for (key, value) in userParameters {
                message = message.replacingOccurrences(of: key, with: value)
            }
        }
        for (key, value) in defaultSocketParametrs() {
            message = message.replacingOccurrences(of: key, with: value)
        }
        weak var blockSelf = self
        let task = CXSocketTask(message: message, uuid: uuid, completion: { (task, response, error) in
            printLog(items: "response:\(String(describing: task.action))")
            if let blockSelf = blockSelf {
                if blockSelf.handleErrorSoket(error: error) == false {
                    blockSelf.sendCompletionQueue.async() {
                        if let completion = completion {
                            if let serialization = serialization {
                                completion(task, serialization(response), error)
                            } else {
                                completion(task, response, error)
                            }
                        }
                    }
                }
            }
        })
        socketManager.send(task: task)
        return task
    }
    
    var soketTasksList:Set<CXSocketTask> {
        get { return socketManager.tasksList }
    }
    
    func soketRemoveTask(task: CXSocketTask) {
        socketManager.tasksList.remove(task)
    }
    
}

// MARK: - Messenger Observer
extension CXMessengerChatMachine {
    
    func addMessengerObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(messangerUserDidUpdateNotification(_:)), name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUserDidUpdate), object: nil)
    }
    
    func removeMessengerObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUserDidUpdate), object: nil)
    }
    
    @objc private func messangerUserDidUpdateNotification(_ notification: Notification) {
        updateUser()
    }
    
}

// MARK: - MOC Observer
extension CXMessengerChatMachine {
    
    func addMOCObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(mocObjectsDidChangeNotification(_:)), name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: mocBackground)
    }
    
    func removeMOCObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: nil)
    }
    
    @objc private func mocObjectsDidChangeNotification(_ notification: NSNotification) {
        //        NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
        //        NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
        //        NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
        
    }
    
}

// MARK: - Socket Observer
extension CXMessengerChatMachine {
    
    func addSocketObserver(_ observer: AnyObject, selectorDidOpen: Selector, selectorDidFailWithError: Selector, selectorDidCloseWithCode: Selector) {
        let center = NotificationCenter.default
        center.addObserver(observer, selector: selectorDidOpen, name:NSNotification.Name(rawValue: CXSocketManagerNotificationDidOpen), object: nil)
        center.addObserver(observer, selector: selectorDidFailWithError, name:NSNotification.Name(rawValue: CXSocketManagerNotificationDidFailWithError), object: nil)
        center.addObserver(observer, selector: selectorDidCloseWithCode, name:NSNotification.Name(rawValue: CXSocketManagerNotificationDidCloseWithCode), object: nil)
    }
    
    func removeSocketObserver(_ observer: AnyObject) {
        let center = NotificationCenter.default
        center.removeObserver(observer, name: NSNotification.Name(rawValue: CXSocketManagerNotificationDidOpen), object: nil)
        center.removeObserver(observer, name: NSNotification.Name(rawValue: CXSocketManagerNotificationDidFailWithError), object: nil)
        center.removeObserver(observer, name: NSNotification.Name(rawValue: CXSocketManagerNotificationDidCloseWithCode), object: nil)
    }
    
    @objc func socketManagerNotificationDidOpen(_ notification: NSNotification) {
        tryConnect = false
        resetOptionSendingInMessages()
        sendRegisterDeliveredMessagesForUserUser()
        sendFirstUnsendetMessage()
        sendGetAllContacts()
        sendGetUser()
    }
    
    @objc func socketManagerNotificationDidFailWithError(_ notification: NSNotification) {
        if adapterToHandle() {
            if let error = notification.object as? NSError {
                if !handleErrorReachable() {
                    if !handleErrorWebSoket(error: error) {
                        errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketErrorUnknown(), backgroundColor: errorColor())
                    }
                }
            } else {
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketErrorUnknown(), backgroundColor: errorColor())
            }
        }
    }
    
    @objc func socketManagerNotificationDidCloseWithCode(_ notification: NSNotification) {
        if adapterToHandle() {
            if !handleCloseCode(code: notification.object as? SRStatusCode) {
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketCloseWithUnknownCode(), backgroundColor: errorColor())
            }
        }
    }
}

// MARK: - Application Observer
extension CXMessengerChatMachine {
    
    func addApplicationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackgroundNotification), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActiveNotification), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActiveNotification), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    func removeApplicationObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func applicationDidEnterBackgroundNotification() {
        //disconnect(tryConnectAgain: true, reconnetTimerInvalidate: true)
        print("applicationDidEnterBackgroundNotification")
    }
    
    @objc func applicationWillResignActiveNotification() {
        disconnect(tryConnectAgain: true, reconnetTimerInvalidate: true)
        print("applicationWillResignActiveNotification")
    }
    
    @objc func applicationDidBecomeActiveNotification() {
        silentTasksCount = 0
        connectIfNeed()
        print("applicationDidBecomeActiveNotification")
    }
    
    
}

// MARK: Error handle
extension CXMessengerChatMachine {
    
    fileprivate func errorColor() -> UIColor { return UIColor.red }
    fileprivate func handleErrorReachable() -> Bool {
        if socketReachability?.isReachable() == false {
            if internetReachability?.isReachable() == true {
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.HostNotAvailable(), backgroundColor: errorColor())
                return true
            } else {
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.NetworkNotAvailable(), backgroundColor: errorColor())
                return true
            }
        }
        return true
    }
    
    fileprivate func handleErrorSoket(error: CXSocketError?) -> Bool {
        if let error = error {
            switch error {
            case .InvalidResponse:
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketErrorInvalidResponse(), backgroundColor: errorColor())
            case .InvalidBody:
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketErrorInvalidResponse(), backgroundColor: errorColor())
            case .TaskCanceled:
                return true
            case .TimedOut(let task):
                let title = CXLocalized.ChatMachine.SocketErrorTimeoutFor(value: task.action ?? task.uuid)
                var userInfo = [String: Any]()
                userInfo["title"] = title
                userInfo["message"] = task.message
                errorHandler.drawMessage(message: title, userInfo: ["message": task.message], backgroundColor: errorColor())
            }
            return false
        }
        return false
    }
    
    fileprivate func handleErrorWebSoket(error: NSError?) -> Bool {
        if let error = error {
            if error.domain == CXSocketError.domain {
                _ = CXLocalized.ChatMachine.SocketError(error: error)
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketError(error: error), backgroundColor: errorColor())
            }
            errorHandler.drawMessage(message: CXLocalized.ChatMachine.WebSocketError(error: error), backgroundColor: errorColor())
            return true
        }
        return false
    }
    
    fileprivate func handleCloseCode(code: SRStatusCode?) -> Bool {
        if let code = code {
            switch code {
            case SRStatusCodeGoingAway:
                _ = CXLocalized.ChatMachine.SocketCloseWithGoingAway(code: code)
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketCloseWithGoingAway(code: code), backgroundColor: errorColor())
            default:
                errorHandler.drawMessage(message: CXLocalized.ChatMachine.SocketClose(code: code), backgroundColor: errorColor())
            }
            return true
        }
        return false
    }
    
    func adapterToHandle() -> Bool {
        tryConnect = true
        if tryConnect == false { // tryConnect will false if attempts have ended
            lastUserStatus = user.status
            setStatusToAllContacts()
            return true
        }
        return false
    }
}
