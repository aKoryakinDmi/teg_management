//
//  CXMessengerChatMachine+SocketSendMessage.swift
//  cx4i
//
//  Created by EvGeniy Lell on 11.05.16.
//
//

import Foundation
import CoreData
import AudioToolbox

//extension PrintLog { func setCXMessengerChatMachineSocketSendMessageLogLevel() { setLevel(.Debug) } }

//MARK: Notification
let CXMessengerChatMachineNotificationSendChatMessage = "CXMessengerChatMachineNotificationSendChatMessage"
let CXMessengerChatMachineNotificationMessagesDelivered = "CXMessengerChatMachineNotificationMessagesDelivered"

//MARK: Unread
let CXMessengerChatMachineMessage_SendChatMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "publish",
    "address": "chat.\(CXSocketContactHash.key)",
    "body": [
        "from": CXSocketUserHash.key,
        "to": CXSocketContactHash.key,
        "text": CXSocketMessageText.key,
        "messageId": CXSocketMessageId.key,
        "sessionId": CXSocketSessionId.key,
        "senderInfo": [
            "firstName": CXSocketFirstName.key,
            "lastName":  CXSocketLastName.key,
        ],
    ],
    "replyAddress": CXSocketUUID.key,
])


extension CXMessengerChatMachine {
    
    func resetOptionSendingInMessages() {
        let dmMessagesList: [CXDMMessage] = CXDMMessage.query([
            "fromContact.uid": user.uid!,
            "rawUid !=": NSNull(),
            "sending": true,
            ], context: mocBackground)
        for dmMessage in dmMessagesList {
            dmMessage.sending = NSNumber(value: false)
        }
        
        saveMOC(moc: mocBackground)
    }
    
    func sendFirstUnsendetMessage(toContactHash key: String? = nil) {

        var q:[String: Any] = [
            "fromContact.uid": user.uid!,
            "rawUid !=": NSNull(),
            "sending !=": true,
            ]
        if let toContactHash = key {
            q["toContact.uid"] = toContactHash
        }
        if let dmMessage: CXDMMessage = CXDMMessage.query(q, context: mocBackground, sort: [
                "timestamp": "ASC"
            ]).first {
            printLog(items: "Messege:\(dmMessage)")
            sendMessage(message: dmMessage, key: key)
        }
    }

    private func sendMessage(message: CXDMMessage, key: String? = nil) {
        sendRegisterDeliveredMessagesForUserUser()
        if message.sending?.boolValue != true {
            if let contactHash = message.toContact?.uid,
                var text = message.text,
                let rawUid = message.rawUid {
                text = text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                text = text.replacingOccurrences(of: "\\", with: "\\\\")
                text = text.replacingOccurrences(of: "\n", with: "\\\\n")
                message.sending = NSNumber(value: true)
                printLog(items: "Send Messege:\(message)")
                let task = send(rawMessage: CXMessengerChatMachineMessage_SendChatMessage, parameters:[
                    CXSocketContactHash.key: contactHash,
                    CXSocketMessageText.key: text,
                    CXSocketMessageId.key: rawUid
                    ])
                task.type = .Publish
                self.playSentMessageNotificationSound()
                if let key = key {
                    task.userInfo = ["contactHash": key as AnyObject]
                }
            }
        }
    }
    
    func createAndSendMessageForContact(_ contactHash: String, userHash : String? = nil,  text: String) -> CXDMMessage {
        let text = text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        let rawUid = socketManager.randomStringUUID()
        let dmMessage: CXDMMessage = CXDMMessage.findAndUpdateOrCreate(properties: [
            "timestamp": NSNumber(value: NSDate().timeIntervalSince1970 * 1000),
            "text": text,
            "sending": NSNumber(value: false),
            "rawUid": rawUid,
            ], context: mocBackground)
        let ownerUid: String = userHash ?? user.uid!
        dmMessage.fromContact = user
        dmMessage.fromContact = CXDMUser.findOrCreateByUid(uid: ownerUid, context: mocBackground)
        dmMessage.toContact = CXDMContact.findOrCreateByUid(uid: contactHash, ownerUid: ownerUid, context: mocBackground)
        dmMessage.toContact!.detectLastMessage(ownerUid: user.uid!)
        
        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactMessagesDidUpdate), object: self, userInfo: ["uid": dmMessage.toContact!.uid!])
            self.sendMessage(message: dmMessage)
        }
        
        return dmMessage//rawUid
    }
    


}
