//
//  CXMessangerChatMachine+Silent.swift
//  cx4i
//
//  Created by EvGeniy Lell on 30.05.16.
//
//

import Foundation
import CoreData


private var CXMessengerChatMachine_SilentMessage = Set<String>()

extension CXMessengerChatMachine {
    
    var isNeedSilentMode: Bool {
        get { return (UIApplication.shared.applicationState != .active && silentTasksCount > 0) }
    }
    
    func silentConnectIfNeed() {
        silentTasksCount += 1
        if socketManager.socket?.readyState != .OPEN {
            socketManager.connect()
        }
    }

    func silentDisconnectIfNeed() {
        silentTasksCount -= 1
        if !isNeedSilentMode {
            disconnect(tryConnectAgain: true, reconnetTimerInvalidate: true)
        }
    }

    func createAndSilentSendMessage(forContactHash contactHash: String, userHash: String, text: String ) {
        silentConnectIfNeed()
        sendRegisterDeliveredMessagesForUserUser()
        let message = createAndSendMessageForContact(contactHash, userHash: userHash, text: text)
        let waitTo = Date.init(timeIntervalSinceNow: 3)
        while waitTo.timeIntervalSince1970 > Date().timeIntervalSince1970 &&
            (message.rawUid?.characters.count)! > 0 {            
            RunLoop.current.run(mode: RunLoopMode.defaultRunLoopMode, before: Date.distantFuture)
        }
        silentDisconnectIfNeed()
    }
}
