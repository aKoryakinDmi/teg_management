//
//  CXMessengerCoreDataManager.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import Foundation

class CXMessengerCoreDataManager: CoreDataManager {
    
    static let sharedInstance = CXMessengerCoreDataManager()
    
    init() {
        super.init(modelName: "CXMessenger")
    }
    
}

