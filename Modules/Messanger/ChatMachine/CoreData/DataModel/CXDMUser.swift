//
//  CXDMUser.swift
//  cx4i
//
//  Created by EvGeniy Lell on 19.05.16.
//
//

import Foundation
import CoreData


class CXDMUser: CXDMContact {
    enum SearchRestriction: String {
        case Anyone     = "ANYONE"
        case MyCompany  = "MY_COMPANY"
    }

// Insert code here to add functionality to your managed object subclass

    var searchRestrictionAsType: SearchRestriction? {
        get {
            if let searchRestriction = searchRestriction {
                return SearchRestriction(rawValue: searchRestriction)
            }
            return nil
        }
        set(value) {
            searchRestriction = value?.rawValue
        }
    }

}

// MARK: Mapping
extension CXDMUser {
    
    internal override class func primaryKeys() -> [String]? {
        return ["uid"]
    }

    internal override class func mapping() -> [String: String]? {
        var mapping = super.mapping()
        mapping?["isSoundOn"] = "soundOn"
        mapping?["searchRestriction"] = "searchRestriction"
        return mapping
    }
    
    class func findOrCreateByUid(uid: String, context: NSManagedObjectContext) -> Self {
        return findOrCreate(["uid": uid, "ownerUid": uid], context: context)
    }
    
}

