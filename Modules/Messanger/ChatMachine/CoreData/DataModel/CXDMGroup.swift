//
//  CXDMGroup.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import Foundation
import CoreData


@objc(CXDMGroup)
class CXDMGroup: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    static func findOrCreateByUid(uid: Int, context: NSManagedObjectContext) -> Self {
        return self.findOrCreate(["uid": uid], context: context)
    }
    
}
