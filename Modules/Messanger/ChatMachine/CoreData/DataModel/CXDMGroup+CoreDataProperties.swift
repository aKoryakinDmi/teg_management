//
//  CXDMGroup+CoreDataProperties.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CXDMGroup {

    @NSManaged var uid: NSNumber?
    @NSManaged var contacts: NSSet?

}
