//
//  CXDMMessage.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import Foundation
import CoreData

@objc(CXDMMessage)
class CXDMMessage: NSManagedObject {

    var dataTime: Date? {
        get {
            if let timestamp = self.timestamp {
                return Date(timeIntervalSince1970: timestamp.doubleValue/1000)
            }
            return nil
        }
    }
    
    class var formatSectionByDate: String {
        get { return "yyyy.MM.dd" }
    }
    
    var sectionByDate: String? {
        get {
            return dataTime?.formattedWith(type(of: self).formatSectionByDate)
        }
    }
//    var sectionByDate: NSTimeInterval {
//        get {
//            if let dataTime = self.dataTime {
//                let dateFormatter = NSDateFormatter()
//                dateFormatter.dateFormat = "yyyy.MM.dd"
//                let dateString = dateFormatter.stringFromDate(dataTime)
//                if let date = dateFormatter.dateFromString(dateString) {
//                    let timeInterval = date.timeIntervalSince1970
//                    return timeInterval
//                }
//            }
//            return 0
//        }
//    }
    
}

// MARK: Mapping
extension CXDMMessage {
    
    internal override static func primaryKeys() -> [String]? {
        return ["timestamp"]
    }
    
    internal override static func mapping() -> [String: String]? {
        return [
            // server : client
//            "uuid": "uid",
            "text": "text",
            "sentTime": "timestamp",
        ]
    }
    
    static func findLastForContact(contact: CXDMContact, ownerUid: String, context: NSManagedObjectContext) -> CXDMMessage? {
        if let uid = contact.uid {
            return findLastForContactUid(contactUid: uid, ownerUid: ownerUid, context: context)
        }
        return nil
    }
    
    static func findLastForContactUid(contactUid: String, ownerUid: String, context: NSManagedObjectContext) -> CXDMMessage? {
        let condition = NSPredicate(format: "(fromContact.ownerUid == %@ AND fromContact.uid == %@) OR (toContact.ownerUid == %@ AND toContact.uid == %@)", ownerUid, contactUid, ownerUid, contactUid)
        let messagesList = self.query(condition, context: context, sort: ["timestamp":"DESC"], limit: 1)
        return messagesList.first as? CXDMMessage
    }

}

