//
//  CXDMMessage+CoreDataProperties.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CXDMMessage {

    @NSManaged var rawUid: String?
    @NSManaged var text: String?
    @NSManaged var timestamp: NSNumber?
    @NSManaged var sending: NSNumber?
    @NSManaged var fromContact: CXDMContact?
    @NSManaged var toContact: CXDMContact?

}
