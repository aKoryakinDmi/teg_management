//
//  CXDMContact+CoreDataProperties.swift
//  cx4i
//
//  Created by EvGeniy Lell on 03.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CXDMContact {

    @NSManaged var companyId: String?
    @NSManaged var companyName: String?
    @NSManaged var companySubscription: String?
    @NSManaged var connectedLoads: String?
    @NSManaged var countryCode2: String?
    @NSManaged var departmentName: String?
    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var jobTitle: String?
    @NSManaged var lastName: String?
    @NSManaged var phone1: String?
    @NSManaged var phone2: String?
    @NSManaged var serialNumber: String?
    @NSManaged var status: String?
    @NSManaged var timeZoneName: String?
    @NSManaged var uid: String?
    @NSManaged var ownerUid: String?
    @NSManaged var unreadMessagesCount: NSNumber?
    @NSManaged var contacts: NSSet?
    @NSManaged var groups: NSSet?
    @NSManaged var inMessages: NSSet?
    @NSManaged var owner: CXDMContact?
    @NSManaged var outMessages: NSSet?
    @NSManaged var lastMessage: CXDMMessage?

}
