//
//  CXDMContact.swift
//  cx4i
//
//  Created by EvGeniy Lell on 22.04.16.
//
//

import Foundation
import CoreData
import MessageUI


@objc(CXDMContact)
class CXDMContact: NSManagedObject, FullName, FullCompany {

    
}


// MARK: Mapping
extension CXDMContact {
    
    internal override class func primaryKeys() -> [String]? {
        return ["uid", "ownerUid"]
    }
    
    internal override class func mapping() -> [String: String]? {
        return [
            // server : client
            "firstName": "firstName",
            "lastName": "lastName",
            "userHash": "uid",
            "companyId": "companyId",
            "companyName": "companyName",
            "companySubscription": "companySubscription",
            "connectedLoads": "connectedLoads",
            "countryCode2": "countryCode2",
            "departmentName": "departmentName",
            "email": "email",
            "jobTitle": "jobTitle",
            "phone1": "phone1",
            "phone2": "phone2",
            "serialNumber": "serialNumber",
            "timeZoneName": "timeZoneName",
            "status": "status",
            "groups": "groups",
            "messages": "messages",
        ]
    }
    
    class func findOrCreateByUid(uid: String, ownerUid: String, context: NSManagedObjectContext) -> Self {
        return findOrCreate(["uid": uid, "ownerUid": ownerUid], context: context)
    }
    
    func detectLastMessage(ownerUid: String) {
        if let moc = self.managedObjectContext,
            let uid = self.uid {
            self.lastMessage = CXDMMessage.findLastForContactUid(contactUid: uid, ownerUid: ownerUid, context: moc)
        }
    }

}

extension CXDMContact {
    var companyDescription: String {
        get {
            var result = [String]()
            if companyName != nil {
                result.append(companyName!)
            }
            if countryCode2 != nil || companyId != nil {
                var subresult = [String]()
                if countryCode2 != nil  { subresult.append(countryCode2!) }
                if companyId != nil     { subresult.append(companyId!) }
                result.append("(\(subresult.joined(separator: " ")))")
            }
            return result.joined(separator: " ")
        }
    }
    
    var mainPhone: String? {
        if let phoneNumber = phone1 ?? phone2 {
            var phoneNumberClear = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
            phoneNumberClear = phoneNumberClear.trimmingCharacters(in: CharacterSet(charactersIn: " +"))
            return "+\(phoneNumberClear)"
        }
        return nil
    }
    
    func makePhoneCall() {
        if let mainPhone = mainPhone {
            if let phoneUrl = URL(string: "telprompt://\(mainPhone)") {
                UIApplication.shared.openURL(phoneUrl)
            }
        }
    }
    
    func makeEmailSend(controller: UIViewController, subject: String = "CX:", message: String = "", delegate: MFMailComposeViewControllerDelegate? = nil) {
        if let email = email, email.isValideEmail() {
            if !MFMailComposeViewController.canSendMail() {
                printLog(items:"Mail services are not available")
                return
            }
            let composeVC = MFMailComposeViewController()
            if let delegate = delegate  {
                composeVC.mailComposeDelegate = delegate
            } else if let delegate = controller as? MFMailComposeViewControllerDelegate {
                composeVC.mailComposeDelegate = delegate
            }
            
            // Configure the fields of the interface.
            composeVC.setToRecipients([email])
            composeVC.setSubject(subject)
            composeVC.setMessageBody(message, isHTML: false)
            
            // Present the view controller modally.
            controller.present(composeVC, animated: true, completion: nil)
            
        }
    }
    
    var cxStaus: CXMessengerStatus {
        get {
            if let status = status {
                return CXMessengerStatus(rawValue: status) ?? CXMessengerStatus.Offline
            }
            return CXMessengerStatus.Unknown
        }
    }
    
    var canHasMyCompanyGroup: Bool {
        get {
            if let companySubscription = companySubscription {
                return (companySubscription != "driver_plus")
            } else {
                return false
            }
        }
    }
}


// MARK: - Ralation Helpers
extension CXDMContact {
    
    func addGroup(value: CXDMGroup) {
        let items = self.mutableSetValue(forKey: "groups");
        items.add(value)
    }
    
    func removeGroup(value: CXDMGroup) {
        let items = self.mutableSetValue(forKey: "groups");
        items.remove(value)
    }
    
//    func addContact(value: CXDMContact) {
//        let items = self.mutableSetValueForKey("contacts");
//        items.addObject(value)
//    }
//    
//    func removeContact(value: CXDMContact) {
//        let items = self.mutableSetValueForKey("contacts");
//        items.removeObject(value)
//    }

}
