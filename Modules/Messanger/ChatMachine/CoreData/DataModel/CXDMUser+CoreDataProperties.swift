//
//  CXDMUser+CoreDataProperties.swift
//  cx4i
//
//  Created by EvGeniy Lell on 19.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CXDMUser {

    @NSManaged var soundOn: NSNumber?
    @NSManaged var searchRestriction: String?

}
