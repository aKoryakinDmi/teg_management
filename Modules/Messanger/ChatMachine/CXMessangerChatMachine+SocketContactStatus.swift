//
//  CXMessengerChatMachine+SocketContactStatus.swift
//  cx4i
//
//  Created by EvGeniy Lell on 23.04.16.
//
//

import Foundation

//extension PrintLog { func set_CXMessengerChatMachine_SocketContactStatus_LogLevel() { setLevel(.Debug) } }

// MARK: - Socket ContactStatus Methods

let CXMessengerChatMachineNotificationContactStatusDidUpdate = "CXMessengerChatMachineNotificationContactStatusDidUpdate"

let CXMessengerChatMachine_RegisterUserStatusChangeMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "register",
    "address" : "status.\(CXSocketContactHash.key)",
])

let CXMessengerChatMachine_UnregisterUserStatusChangeMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "unregister",
    "address" : "status.\(CXSocketContactHash.key)",
])

let CXMessengerChatMachine_GetUserStatusMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "user.status",
    "body": [
        "userHash": CXSocketContactHash.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
])

let CXMessengerChatMachine_SetUserStatusMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "publish",
    "address" : "statusChangedMobile.\(CXSocketUserHash.key)",
    "body": [
        "status": CXSocketStatus.key,
        "sessionId" : CXSocketSessionId.key,
    ],
])


extension CXMessengerChatMachine {
    
    private typealias CDStatusAtribut = String
    
    private enum SocketStatus: String {
        case Online = "online"
        case Away = "away"
        case Busy = "busy"
        case Offline = "offline"
        case Unknown = "unknown"
    }
    
    // MARK: CoreData
    private func updateDMContactStatus(contactHash: String, statusAtribut: CDStatusAtribut?) {
        if let statusAtribut = statusAtribut {
            let dmContact: CXDMContact = CXDMContact.findOrCreateByUid(uid: contactHash, ownerUid: user.uid!, context: mocBackground)
            let cxStatus: CXMessengerStatus
            let socketStatus = SocketStatus(rawValue: statusAtribut)
            switch socketStatus {
                case .Unknown?: cxStatus = CXMessengerStatus.Unknown
                case .Online?: cxStatus = CXMessengerStatus.Avaliable
                case .Away?: cxStatus = CXMessengerStatus.AwayFromDesk
                case .Busy?: cxStatus = CXMessengerStatus.NotAvaliable
                case .Offline?: cxStatus = CXMessengerStatus.Offline
                default: cxStatus = CXMessengerStatus.Unknown
            }
            dmContact.status = cxStatus.rawValue
            
            saveMOC(moc: mocBackground)
            DispatchQueue.main.async() {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: self, userInfo: ["uid": contactHash])
            }
        }
    }
    
    func setStatusToAllContacts(status:CXMessengerStatus = .Unknown) {
        let contactsLiat:[CXDMContact] = CXDMContact.all(mocBackground)
        for contact in contactsLiat {
            contact.status = status.rawValue
        }
        
        DispatchQueue.main.async() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: self, userInfo: ["all": ""])
        }
        
    }


    // MARK: Soket
    func sendGetStatusForUser() {
        sendGetStatusForContact(contactHash: user.uid!)
    }
    
    func sendGetStatusForContact(contactHash: String) {
        weak var blockSelf = self
        let task = send(rawMessage: CXMessengerChatMachine_GetUserStatusMessage, parameters: [
            CXSocketContactHash.key: contactHash,
        ], serialization: serializationAsUserStatus) { (task, response, error) in
            if let contactHash = task.userInfo?["contactHash"] as? String {
                blockSelf?.updateDMContactStatus(contactHash: contactHash, statusAtribut: response as? CDStatusAtribut)
            }
        }
        task.userInfo = ["contactHash": contactHash as AnyObject]
    }

    func sendSetStatusForUser(status:CXMessengerStatus = .Unknown) {
        var socketStatus: SocketStatus
        switch status {
            case .Unknown: socketStatus = SocketStatus.Unknown
            case .Avaliable: socketStatus = SocketStatus.Online
            case .AwayFromDesk: socketStatus = SocketStatus.Away
            case .NotAvaliable: socketStatus = SocketStatus.Busy
            case .Offline: socketStatus = SocketStatus.Offline
        }
        
        let dmUser: CXDMUser = CXDMUser.findOrCreateByUid(uid: user.uid!, context: mocBackground)
        dmUser.status = status.rawValue
        
        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactStatusDidUpdate), object: self, userInfo: ["uid": self.user.uid!])

            let task = self.send(rawMessage: CXMessengerChatMachine_SetUserStatusMessage, parameters: [
                CXSocketStatus.key: socketStatus.rawValue,
            ])
            task.type = .Publish
            
            printLog(items: task.message)
        }

    }

    private func registerUserStatusChangeStringUUID(userHash: String) -> String {
        return "status."+userHash
    }
    
    func sendRegisterStatusChangeForUser() {
        sendRegisterStatusChangeForContact(contactHash: user.uid!)
    }
    
    func sendRegisterStatusChangeForContact(contactHash: String) {
        if isNeedSilentMode { return }
        let uuid = registerUserStatusChangeStringUUID(userHash: contactHash)
        weak var blockSelf = self
        let task = send(rawMessage: CXMessengerChatMachine_RegisterUserStatusChangeMessage, uuid: uuid, parameters: [
            CXSocketContactHash.key: contactHash,
        ], serialization: serializationAsUserStatus) { (task, response, error) in
            if let contactHash = task.userInfo?["contactHash"] as? String {
                blockSelf?.updateDMContactStatus(contactHash: contactHash, statusAtribut: response as? CDStatusAtribut)
            }
        }
        task.type = .Register
        task.userInfo = ["contactHash": contactHash as AnyObject]
    }
    
    func sendUnregisterStatusChangeForUser() {
        sendUnregisterStatusChangeForContact(contactHash: user.uid!)
    }
    
    func sendUnregisterStatusChangeForContact(contactHash: String) {
        let uuid = registerUserStatusChangeStringUUID(userHash: contactHash)
        let task = send(rawMessage: CXMessengerChatMachine_UnregisterUserStatusChangeMessage, parameters: [
            CXSocketContactHash.key: contactHash,
        ], serialization: serializationAsUserStatus) { (task, response, error) in
            //self.updateCoreDataContactStatus(response as? String)
        }
        if let regTask = soketTasksList.filter(uuid: uuid).first {
            soketRemoveTask(task: regTask)            
        }
        task.userInfo = ["contactHash": contactHash as AnyObject]
    }
    
    // MARK: Soket - Serialization
    private func serializationAsUserStatus(response: AnyObject?) -> AnyObject?  {
        if let response = response as? [String: AnyObject],
            let rawStatus = response["status"] as? CDStatusAtribut {
            return rawStatus as AnyObject
        }
        return nil
    }
    
}

