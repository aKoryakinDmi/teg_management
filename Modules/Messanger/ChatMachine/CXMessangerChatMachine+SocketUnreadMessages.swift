//
//  CXMessengerChatMachine+SocketUnreadMessages.swift
//  cx4i
//
//  Created by EvGeniy Lell on 30.04.16.
//
//

import Foundation

//MARK: Notification
let CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate = "CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate"
let CXMessengerChatMachineNotificationUnreadMessagesCountDidChanged = "CXMessengerChatMachineNotificationUnreadMessagesCountDidChanged"

//MARK: Unread
let CXMessengerChatMachineMessage_GetUnreadMessagesCount = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "unread.messages.count",
    "body": [
        "type" : "GET",
        "userHash" : CXSocketUserHash.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
])

let CXMessengerChatMachineMessage_ResetUnreadMessagesCount = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "unread.messages.count",
    "body": [
        "type" : "RESET",
        "senderUserHash" : CXSocketContactHash.key,
        "userHash" : CXSocketUserHash.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
])


extension CXMessengerChatMachine {
    
    private typealias CDCountMessages = [[String: AnyObject]]

    // MARK: - CoreData
    private func updateDMUnreadMessages(task: CXSocketTask, messagesList: CDCountMessages?) {
        guard let messagesList = messagesList else { return }
        var contactCache = [String: CXDMContact]()
        func contactFromCacheByUid(uid: String) -> CXDMContact {
            if let dmContact = contactCache[uid] {
                return dmContact
            } else {
                UILoop()
                let dmContact: CXDMContact = CXDMContact.findOrCreateByUid(uid: uid, ownerUid: user.uid!, context: mocBackground)
                contactCache[uid] = dmContact
                return dmContact
            }
        }
        for unreadMessage in messagesList {
            if let uid = unreadMessage["userHash"] as? String,
                let count = unreadMessage["messagesCount"] as? Int {
                let dmContact: CXDMContact = contactFromCacheByUid(uid: uid)
                dmContact.unreadMessagesCount = count as NSNumber
            }
        }
        
        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            self.updateApplicationBadge()
            for (key, _) in contactCache {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: self, userInfo: ["uid": key])
            }
        }

    }
    
    // MARK: - Socket
    func sendGetUnreadMessagesCountForUserUser() {
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachineMessage_GetUnreadMessagesCount, serialization: serializationAsCountMessages) { (task, response, error) in
            blockSelf?.updateDMUnreadMessages(task: task, messagesList: response as? CDCountMessages)
        }
    }
    
    func sendResetUnreadMessagesCountForContact(contactHash: String) {
        resetLocalUnreadMessagesCountForContact(contactHash: contactHash)
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachineMessage_ResetUnreadMessagesCount, parameters: [
            CXSocketContactHash.key: contactHash,
        ]) { (task, response, error) in
            if error == nil {
                blockSelf?.sendGetUnreadMessagesCountForUserUser()
            }
        }
    }
    
    private func resetLocalUnreadMessagesCountForContact(contactHash: String) {
        let dmContact: CXDMContact = CXDMContact.findOrCreateByUid(uid: contactHash, ownerUid: user.uid!, context: mocBackground)
        dmContact.unreadMessagesCount = NSNumber(value: 0)
        
        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            self.updateApplicationBadge()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: self, userInfo: ["uid": contactHash])
        }
    }

    
    
    // MARK: Serialization
    private func serializationAsCountMessages(response: AnyObject?) -> AnyObject?  {
        if let response = response as? CDCountMessages {
            return response as AnyObject
        }
        return nil
    }
    
}
