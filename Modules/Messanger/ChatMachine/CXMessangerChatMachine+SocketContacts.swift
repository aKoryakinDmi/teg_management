 //
 //  CXMessengerChatMachine+SocketContacts.swift
 //  cx4i
 //
 //  Created by EvGeniy Lell on 22.04.16.
 //
 //
 
 import Foundation
 
 //extension PrintLog { func set_CXMessengerChatMachine_SocketContacts_LogLevel() { setLevel(.Debug) } }
 
 let CXMessengerChatMachineNotificationContactsDidUpdate = "CXMessengerChatMachineNotificationContactsDidUpdate"
 let CXMessengerChatMachineNotificationUserDidUpdate = "CXMessengerChatMachineNotificationUserDidUpdate"
 
 let CXMessengerChatMachine_GetGeneralExchnangeUsersMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "roster.groups",
    "body": [
        "action": "GET",
        "userHash": CXSocketUserHash.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
    ])
 
 let CXMessengerChatMachine_GetInProgressUsersMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "user.service",
    "body": [
        "action": "getInProgressUsersMobile",
        "userHash": CXSocketUserHash.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
    ])
 
 let CXMessengerChatMachine_GetMyCompanyUsersMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "user.service",
    "body": [
        "action": "getMyCompanyUsersMobile",
        "userHash": CXSocketUserHash.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
    ])
 
 let CXMessengerChatMachine_GetUserInfoMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "user.service",
    "body": [
        "action": "getUserInfoMobile",
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
    ])
 
 let CXMessengerChatMachine_GetContactInfoMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "user.service",
    "body": [
        "action": "getUserInfoMobile",
        "userHash": CXSocketContactHash.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
    ])
 
 let CXMessengerChatMachine_UpdateUserSettingsMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address": "user.service",
    "body": [
        "action": "updateUserSettings",
        "userHash": CXSocketUserHash.key,
        "settings": [
            "searchRestriction": CXSocketSearch.key,
            "isSoundOn": CXSocketSound.key,
        ],
        "sessionId": CXSocketSessionId.key,
    ],
    "replyAddress": CXSocketUUID.key
    ])
 
 
 public enum CXMCMGroupType: Int {
    case MyCompany          = 0
    case InProgress         = 1
    case GeneralExchnange   = 2
    
    static let ownRawValue: Int = 0xFF
    
 }
 
 extension CXMessengerChatMachine {
    
    private typealias CDContactAtributes = [String: Any]
    
    // MARK: CoreData
    private func updateDMUser(userAtributes: CDContactAtributes?) {
        if let userAtributes = userAtributes {
            var userWithOwnerAtributes = userAtributes
            userWithOwnerAtributes["ownerUid"] = user.uid!
            let dmUser: CXDMUser = CXDMUser.findAndUpdateOrCreate(properties: userWithOwnerAtributes, context: mocBackground)
            //   updateDMContact(contactAtributes: dmUser)
        }
        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUserDidUpdate), object: self)
        }
    }
    
    private func updateDMContact(contactAtributes: CDContactAtributes?) {
        if let contactAtributes = contactAtributes {
            let dmUser: CXDMContact = CXDMContact.findAndUpdateOrCreate(properties: contactAtributes, context: mocBackground)
            //          updateDMContact(contactAtributes: dmUser)
        }
        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactsDidUpdate), object: self)
        }
    }
    
    private func updateDMContactsList(contactsListAtributes: [CDContactAtributes]?, group: CXMCMGroupType) {
        if let contactsListAtributes = contactsListAtributes {
            let dmGroup: CXDMGroup = CXDMGroup.findOrCreateByUid(uid: group.rawValue, context: mocBackground)
            for userAtributes in contactsListAtributes {
                var userWithOwnerAtributes = userAtributes
                userWithOwnerAtributes["ownerUid"] = user.uid!
                let dmContact: CXDMContact = CXDMContact.findAndUpdateOrCreate(properties: userWithOwnerAtributes, context: mocBackground)
                UILoop()
                updateDMContact(contact: dmContact, group: dmGroup)
            }
        }
        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactsDidUpdate), object: self)
        }
    }
    
    private func updateDMContact(contact: CXDMContact?, group: CXDMGroup? = nil) {
        if let dmContact = contact {
            if let dmGroup = group {
                dmContact.addGroup(value: dmGroup)
            }
            if let contactHash = dmContact.uid {
                sendGetStatusForContact(contactHash: contactHash)
                sendRegisterStatusChangeForContact(contactHash: contactHash)
                if contactHash == user.uid {
                    sendRegisterUserMessageForUser()
                    sendGetUnreadMessagesCountForUserUser()
                } else {
                    sendGetLastHistoryMessagesForContact(contactHash: contactHash)
                }
            }
        }
    }
    
    // MARK: Soket
    func sendGetAllContacts() {
        sendGetMyCompanyContacts()
        sendGetInProgressContacts()
        sendGetGeneralExchnangeContacts()
    }
    
    func sendGetMyCompanyContacts() {
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachine_GetMyCompanyUsersMessage, serialization: serializationAsContacts) { (task, response, error) in
            blockSelf?.updateDMContactsList(contactsListAtributes: response as? [CDContactAtributes], group: .MyCompany)
        }
    }
    
    func sendGetInProgressContacts() {
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachine_GetInProgressUsersMessage, serialization: serializationAsContacts) { (task, response, error) in
            blockSelf?.updateDMContactsList(contactsListAtributes: response as? [CDContactAtributes], group: .InProgress)
        }
    }
    
    func sendGetGeneralExchnangeContacts() {
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachine_GetGeneralExchnangeUsersMessage, serialization: serializationAsRosterGroupsUsers) { (task, response, error) in
            blockSelf?.updateDMContactsList(contactsListAtributes: response as? [CDContactAtributes], group: .GeneralExchnange)
        }
    }
    
    func sendGetUser() {
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachine_GetUserInfoMessage, serialization: serializationAsUser) { (task, response, error) in
            blockSelf?.updateDMUser(userAtributes: response as? CDContactAtributes)
            if error == nil {
                blockSelf?.sendGetStatusForUser()
                blockSelf?.sendRegisterStatusChangeForUser()
            }
        }
    }
    
    func sendGetContact(contactHash: String) {
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachine_GetContactInfoMessage, parameters:[
            CXSocketContactHash.key:contactHash
        ], serialization: serializationAsUser) { (task, response, error) in
            blockSelf?.updateDMContact(contactAtributes: response as? CDContactAtributes)
        }
    }
    
    func sendUpdateUserSettings() {
        let task = send(rawMessage: CXMessengerChatMachine_UpdateUserSettingsMessage, parameters:[
            CXSocketSearch.key: user.searchRestriction ?? CXDMUser.SearchRestriction.Anyone.rawValue,
            CXSocketSound.extKey: String(user.soundOn?.boolValue ?? false),
            ])
        printLog(items: task.message, task.uuid)
    }
    
    
    // MARK: Soket - Serialization
    private func serializationAsContacts(response: AnyObject?) -> AnyObject?  {
        if let rawUsersList = response as? [CDContactAtributes] {
            return rawUsersList as AnyObject
        }
        return nil
    }
    
    private func serializationAsRosterGroupsUsers(response: AnyObject?) -> AnyObject?  {
        if let response = response as? [String: AnyObject] {
            var result = [CDContactAtributes]()
            for (_, rawGroupsList) in response {
                if let rawGroupsList = rawGroupsList as? [[String: AnyObject]] {
                    for rawGroup in rawGroupsList {
                        if let rawUsersList = rawGroup["members"] as? [CDContactAtributes] {
                            result.append(contentsOf: rawUsersList)
                        }
                    }
                }
            }
            return result as AnyObject
        }
        return nil
    }
    
    private func serializationAsUser(response: AnyObject?) -> AnyObject?  {
        if let response = response as? CDContactAtributes {
            return response as AnyObject
        }
        return nil
    }
 }
