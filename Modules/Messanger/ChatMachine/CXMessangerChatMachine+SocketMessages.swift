//
//  CXMessengerChatMachine+SocketMessages.swift
//  cx4i
//
//  Created by EvGeniy Lell on 25.04.16.
//
//

import Foundation
import AudioToolbox

//MARK: Notification
let CXMessengerChatMachineNotificationContactMessagesDidUpdate = "CXMessengerChatMachineNotificationContactMessagesDidUpdate"

//MARK: History
let CXMessengerChatMachineMessage_GetLastHistoryMessagesBeforeTimestamp = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "send",
    "address" : "read.chat.history.paging",
    "body": [
        "userFrom" : CXSocketUserHash.key,
        "userTo" : CXSocketContactHash.key,
        "beforeTimeStamp" : CXSocketTimeStamp.key,
        "sessionId" : CXSocketSessionId.key,
    ],
    "replyAddress" : CXSocketUUID.key
])

//MARK: Register chat
let CXMessengerChatMachine_RegisterUserMessagesMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "register",
    "address" : "chat.\(CXSocketUserHash.key)",
])

let CXMessengerChatMachine_UnregisterUserMessagesMessage = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "unregister",
    "address" : "chat.\(CXSocketUserHash.key)",
])

//MARK: Register delivered
let CXMessengerChatMachineMessage_DeliveredMessages = CXMessengerChatMachinePrepareMessage(messageObject: [
    "type": "register",
    "address" : "delivered.\(CXSocketUserHash.key)",
])

extension CXMessengerChatMachine {
    
    private typealias CDHistoryMessageAtributes = [String: AnyObject]
    private typealias CDDeliveredReport = [String: AnyObject]


    // MARK: - CoreData
    private func updateDMHistoryMessages(task: CXSocketTask, messagesList: [CDHistoryMessageAtributes]?) {
        if let messagesList = messagesList {
            var contactCache = [String: CXDMContact]()
            func contactFromCacheByUid(uid: String, sender: [String: AnyObject]?) -> CXDMContact {
                if let dmContact = contactCache[uid] {
                    return dmContact
                } else {
                    UILoop()
                    let dmContact: CXDMContact = CXDMContact.findOrCreateByUid(uid: uid, ownerUid: user.uid!, context: mocBackground)
                    if let sender = sender {
                        if dmContact.firstName == nil && dmContact.lastName == nil {
                            dmContact.update(sender, context: mocBackground)
                        }
                    }
                    contactCache[uid] = dmContact
                    return dmContact
                }
            }
            for rawMessage in messagesList {
                let dmMessage: CXDMMessage = CXDMMessage.findAndUpdateOrCreate(properties: rawMessage, context: mocBackground)
                // связывание отправитель - получатель
                if let fromUid = rawMessage["from"] as? String,
                    let toUid = rawMessage["to"] as? String {
                    let sender = rawMessage["senderInfo"] as? [String: AnyObject]
                    dmMessage.fromContact = contactFromCacheByUid(uid: fromUid, sender: sender)
                    dmMessage.toContact = contactFromCacheByUid(uid: toUid, sender: sender)
                    // увеличить кол-во не прочитанных если сообщение не из истории
                    // && если есть отправитель и он не владелец
                    let isFromHistory: Bool = rawMessage["isFromHistory"] as? Bool ?? false
                    if isFromHistory != true && fromUid != user.uid! {
                        var unreadMessagesCount: Int = dmMessage.fromContact?.unreadMessagesCount?.intValue ?? 0
                        unreadMessagesCount += 1
                        dmMessage.fromContact?.unreadMessagesCount = NSNumber(value: unreadMessagesCount)
                    }
                }
            }
            for (_, contact) in contactCache {
                if contact.uid != user.uid {
                    contact.detectLastMessage(ownerUid: user.uid!)
                }
            }
            saveMOC(moc: mocBackground)
            DispatchQueue.main.async() {
                self.updateApplicationBadge()
                for (key, _) in contactCache {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactMessagesDidUpdate), object: self, userInfo: ["uid": key])
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: self, userInfo: ["uid": key])
                }
            }

        }
    }
    
    private func updateDMDeliveredMessage(task: CXSocketTask, deliveredReport: CDDeliveredReport?) {
        printLog(items: "DeliveredReport:\n\(String(describing: deliveredReport))")
        if let rawUid = deliveredReport?["messageId"] as? String,
            let timestamp = deliveredReport?["timestamp"] as? Double,
            let dmMessage = CXDMMessage.find(["rawUid": rawUid], context: mocBackground) as? CXDMMessage {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationMessagesDelivered), object: self, userInfo: ["rawUid": rawUid])
            
            dmMessage.timestamp = timestamp as NSNumber
            dmMessage.rawUid = nil
            dmMessage.toContact!.detectLastMessage(ownerUid: user.uid!)
            //printLog(.Info, items: "Update Delivered Messege:\(dmMessage)")
            
            saveMOC(moc: mocBackground)
            DispatchQueue.main.async() {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactMessagesDidUpdate), object: self, userInfo: ["uid": dmMessage.toContact!.uid!])
                
                self.sendFirstUnsendetMessage(toContactHash: dmMessage.toContact?.uid)
            }
        } else {
            if let deliveredReport = deliveredReport {
                updateDMHistoryMessages(task: task, messagesList: [deliveredReport])
            }
        }
    }

    func appendUnreadMessages(count:Int = 1, toContactHash contactHash: String, withUserHash userHash: String) {
        let dmContact: CXDMContact = CXDMContact.findOrCreateByUid(uid: contactHash, ownerUid: userHash, context: mocBackground)
        var unreadMessagesCount = dmContact.unreadMessagesCount?.intValue ?? 0
        unreadMessagesCount += count
        dmContact.unreadMessagesCount = NSNumber(value: unreadMessagesCount)

        saveMOC(moc: mocBackground)
        DispatchQueue.main.async() {
            self.updateApplicationBadge()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationContactUnreadMessagesCountDidUpdate), object: self, userInfo: ["uid": contactHash])
        }
    }
    
    func updateApplicationBadge() {
        let contactsList:[CXDMContact] = CXDMContact.query(["ownerUid": user.uid!], context: mocBackground)
        var badgeNumber: Int = 0
        for contact in contactsList {
            let unreadMessagesCount = contact.unreadMessagesCount?.intValue ?? 0
            badgeNumber += unreadMessagesCount
        }
        UIApplication.shared.applicationIconBadgeNumber = badgeNumber
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUnreadMessagesCountDidChanged), object: self, userInfo: ["count": badgeNumber])
    }
    
    
    // MARK: - SOCKET
    // MARK: History
    func sendGetLastHistoryMessagesForContact(contactHash: String) {
        let lastMessage = CXDMMessage.findLastForContactUid(contactUid: contactHash, ownerUid: user.uid!, context: mocBackground)
        sendGetLastHistoryMessagesForContact(contactHash: contactHash, beforeTimestamp: String(format:"%.0f", lastMessage?.timestamp?.doubleValue ?? 0))
    }
    
    func sendGetLastHistoryMessagesForContact(contactHash: String, beforeTimestamp timestamp: String) {
        weak var blockSelf = self
        _ = send(rawMessage: CXMessengerChatMachineMessage_GetLastHistoryMessagesBeforeTimestamp, parameters: [
            CXSocketContactHash.key: contactHash,
            CXSocketTimeStamp.extKey: timestamp,
        ], serialization: serializationAsHistoryMessages) { (task, response, error) in
            blockSelf?.updateDMHistoryMessages(task: task, messagesList: response as? [CDHistoryMessageAtributes])
        }
    }
    
    //MARK: Delivered
    private func registerDeliveredMessagesUUID(userHash: String) -> String {
        return "delivered."+userHash
    }
    
    func sendRegisterDeliveredMessagesForUserUser() {
        let uuid = registerDeliveredMessagesUUID(userHash: user.uid!)
        weak var blockSelf = self
        let task = send(rawMessage: CXMessengerChatMachineMessage_DeliveredMessages, uuid: uuid, serialization: serializationAsDeliveredMessages) { (task, response, error) in
            blockSelf?.updateDMDeliveredMessage(task: task, deliveredReport: response as? CDDeliveredReport)
        }
        task.type = .Register
    }
    
    func playSentMessageNotificationSound() {
        if user.soundOn?.boolValue == true {
            AudioServicesPlaySystemSound(1004)
        }
    }

    //MARK: Register - unregister
    private func registerUserMessageStringUUID(userHash: String) -> String {
        return "chat."+userHash
    }
    
    func sendRegisterUserMessageForUser() {
        if isNeedSilentMode { return }
        weak var blockSelf = self
        let uuid = registerUserMessageStringUUID(userHash: user.uid!)
        let task = send(rawMessage: CXMessengerChatMachine_RegisterUserMessagesMessage, uuid: uuid, serialization: serializationAsPopapMessage as? CXSocketSerialization) { (task, response, error) in
            blockSelf?.updateDMHistoryMessages(task: task, messagesList: response as? [CDHistoryMessageAtributes])
            blockSelf?.playReceivedMessageNotificationSound()
        }
        task.type = .Register
        printLog(items: "RegisterUserMessage", task.message)
    }
    
    func playReceivedMessageNotificationSound() {
        if user.soundOn?.boolValue == true {
            AudioServicesPlaySystemSound(1307)
        }
    }
    
    func sendUnregisterUserMessageForUser() {
        let uuid = registerUserMessageStringUUID(userHash: user.uid!)
        _ = send(rawMessage: CXMessengerChatMachine_UnregisterUserMessagesMessage)
        if let regTask = soketTasksList.filter(uuid: uuid).first {
            soketRemoveTask(task: regTask)
        }
    }
    
    // MARK: Serialization
    private func serializationAsHistoryMessages(response: AnyObject?) -> AnyObject?  {
        if let messages = response as? [CDHistoryMessageAtributes] {
            return messages as AnyObject
        }
        return nil
    }

    private func serializationAsPopapMessage(response: AnyObject?) -> Any?  {
        if let messages = response as? CDHistoryMessageAtributes {
            return [messages]
        }
        return nil
    }
    
    private func serializationAsDeliveredMessages(response: AnyObject?) -> AnyObject?  {
        printLog(items: "AsDeliveredMessages:\n\(String(describing: response))")
        if let report = response as? CDDeliveredReport {
            return report as AnyObject
        }
        return nil
    }

}
