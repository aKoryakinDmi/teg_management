//
//  Messenger+CXLocalized.swift
//  cx4i
//
//  Created by EvGeniy Lell on 10.06.16.
//
//

import Foundation


extension CXLocalized {
    class var Messenger: CXLTMessenger.Type { get { return CXLTMessenger.self } }
}
class CXLTMessenger: CXLocalized.Table {
    override class func name() -> String { return "MessengerLocalizable" }
}
class CXLTBMessengerBlock: CXLTMessenger.Block {
    override class func table() -> CXLocalized.Table.Type { return CXLTMessenger.self }
}


// Messenger.Chat
extension CXLTMessenger {
    class var Chat: CXLTBChat.Type { get { return CXLTBChat.self } }
}
class CXLTBChat: CXLTBMessengerBlock {
    static func MenuItemProfile() -> String { return localized() }
    static func MenuItemFleet() -> String { return localized() }
    static func MenuItemBooking() -> String { return localized() }
    
    static func TableSectionToday() -> String { return localized() }
}


// Messenger.Profile
extension CXLTMessenger {
    class var Profile: CXLTBProfile.Type { get { return CXLTBProfile.self } }
}
class CXLTBProfile: CXLTBMessengerBlock {
    static func InfoCellName() -> String { return localized() }
    static func InfoCellEmail() -> String { return localized() }
    static func InfoCellPhone() -> String { return localized() }
    static func InfoCellCompanyName() -> String { return localized() }
}


// Messenger.Roster
extension CXLTMessenger {
    class var Roster: CXLTBRoster.Type { get { return CXLTBRoster.self } }
}
class CXLTBRoster: CXLTBMessengerBlock {
    static func SearchPlaceholder() -> String { return localized() }
    
    static func MenuItemSettings() -> String { return localized() }
    static func MenuItemSupport() -> String { return localized() }
    
    static func MenuItemAllContacts() -> String { return localized() }
    static func MenuItemMyCompany() -> String { return localized() }
    static func MenuItemBookingsInProgress() -> String { return localized() }
    static func MenuItemGeneralExchange() -> String { return localized() }
    
    static func NoDataTitle() -> String { return localized() }
    static func NoDataInMyCompany() -> String { return localized() }
    static func NoDataInBookingsInProgress() -> String { return localized() }
    static func NoDataInGeneralExchange() -> String { return localized() }
    static func NoDataInAnyGroup() -> String { return localized() }
}


// Messenger.Settings
extension CXLTMessenger {
    class var Settings: CXLTBSettings.Type { get { return CXLTBSettings.self } }
}
class CXLTBSettings: CXLTBMessengerBlock {
    static func NavigationTitle() -> String { return localized() }
    
    static func ConfidentialityHeader() -> String { return localized() }
    static func VisibleAny() -> String { return localized() }
    static func VisibleMyCompany() -> String { return localized() }
    
    static func NotificationHeader() -> String { return localized() }
    static func Sound() -> String { return localized() }
}


// Messenger.StatusSelector
extension CXLTMessenger {
    class var StatusSelector: CXLTBStatusSelector.Type { get { return CXLTBStatusSelector.self } }
}
class CXLTBStatusSelector: CXLTBMessengerBlock {
    static func NavigationTitle() -> String { return localized() }
}


// Messenger.StatusSelector
extension CXLTMessenger {
    class var ChatStatus: CXLTBChatStatus.Type { get { return CXLTBChatStatus.self } }
}
class CXLTBChatStatus: CXLTBMessengerBlock {
    static func NavigationTitle() -> String { return localized() }
    
    static func Status(_ status:CXMessengerStatus) -> String {
        switch status {
        case .Unknown:
            return Unknown()
        case .Offline:
            return Offline()
        case .NotAvaliable:
            return NotAvaliable()
        case .AwayFromDesk:
            return AwayFromDesk()
        case .Avaliable:
            return Avaliable()
        }
    }
    static func Unknown() -> String { return localized() }
    static func Offline() -> String { return localized() }
    static func NotAvaliable() -> String { return localized() }
    static func AwayFromDesk() -> String { return localized() }
    static func Avaliable() -> String { return localized() }

}




