//
//  MessengerScheme.swift
//  cx4i
//
//  Created by EvGeniy Lell on 13.06.16.
//
//

import Foundation

private let MessengerSchemaColor_SharedInstance = MessengerSchemeColor()

class MessengerSchemeColor {
    
    static let sharedInstance = MessengerSchemeColor()
    init() { setup() }
    
    var background = UIColor.white
    var text = UIColor.black
    var disableText = UIColor.gray
    
    // view - background
    var navigationBarView = UIColor.white
    var navigationBarSepatator = UIColor.gray
    var navigationBarText = UIColor.black
    var navigationBarSubText = UIColor.black
    var navigationBarSelectedText = UIColor.black
    
    // profile text
    var profileNavigationBarText = UIColor.black
    var profileHeaderText = UIColor.gray
    var profileItemTitleText = UIColor.black
    var profileItemValueText = UIColor.gray
    var profileItemDestructText = UIColor.red
    
    // chat
    var chatSectionHeaderBadgeView = UIColor.black
    var chatSectionHeaderText = UIColor.black
    var chatLinkText = UIColor.blue
    var chatInText = UIColor.black
    var chatOutText = UIColor.black
}

#if TypeProgect_cx4i
extension MessengerSchemeColor {
    func setup() {
        
        background = UIColor(hex:0xFFFFFF)
        text = UIColor(hex:0x242424)
        disableText = UIColor(hex:0xB0B5B9)
        
        // view - background
        navigationBarView = UIColor(hex:0xF8EA4d)
        navigationBarSepatator = UIColor(hex:0xC6A802)
        navigationBarText = UIColor(hex: 0x242424)
        navigationBarSubText = UIColor(hex:0xC6A802)
        navigationBarSelectedText = UIColor(hex:0x242424)
        
        // profile text
        profileNavigationBarText = UIColor(hex:0xFFFFFF)
        profileHeaderText = UIColor(hex:0xBEBEBE)
        profileItemTitleText = UIColor(hex:0x9F9F9E)
        profileItemValueText = UIColor(hex:0x454545)
        profileItemDestructText = UIColor(hex:0xF8645A)
        
        // chat
        chatSectionHeaderBadgeView = UIColor(hex:0xB0B5B9)
        chatSectionHeaderText = UIColor(hex:0xFFFFFF)
        chatLinkText = UIColor(hex:0xD06C00)
        chatInText = UIColor(hex:0x242424)
        chatOutText = UIColor(hex:0x242424)

    }
}
#endif


#if TypeProgect_TEG_management
extension MessengerSchemeColor {
    func setup() {
        
        background = CXColorBackground
        text = CXColorBlack //UIColor(hex:0x242424)
        disableText = CXColorSilver //UIColor(hex:0xB0B5B9)

        // view - background
        navigationBarView = CXColorMainTheme //UIColor(hex:0xC6A802)
        navigationBarSepatator = UIColor(hex:0x0d83b6)
        navigationBarText = CXColorWhite //UIColor(hex: 0x242424)
        navigationBarSubText = UIColor(hex:0x0d83b6)
        navigationBarSelectedText = CXColorWhite //UIColor(hex:0x242424)
        
        // profile text
        profileNavigationBarText = UIColor(hex:0xFFFFFF)
        profileHeaderText = CXColorPlaceholder //UIColor(hex:0xBEBEBE)
        profileItemTitleText = CXColorSilver //UIColor(hex:0x9F9F9E)
        profileItemValueText = CXColorBlack //UIColor(hex:0x454545)
        profileItemDestructText = CXColorRed //UIColor(hex:0xF8645A)
        // chat
        chatSectionHeaderBadgeView = UIColor(hex:0xb0b5b9) //CXColorSilver //UIColor(hex:0xB0B5B9)
        chatSectionHeaderText = UIColor(hex:0xFFFFFF)
        chatLinkText = CXColorLink //UIColor(hex:0xD06C00)
        chatInText = CXColorBlack
        chatOutText = CXColorWhite

    }
}
#endif
