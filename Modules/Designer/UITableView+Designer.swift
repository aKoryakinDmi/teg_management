//
//  UITableView+Designer.swift
//  cx4i
//
//  Created by EvGeniy Lell on 10.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

class ShiftingTopView: UIView {
    private var height: CGFloat
    private var bottomConstraint: NSLayoutConstraint?
    var bottomPosition: CGFloat = 0 {
        didSet {
            updateBottomConstraint()
        }
    }
    
    func updateBottomConstraint() {
        bottomConstraint?.constant = bottomPosition
    }
    
    init(withColor color: UIColor, height: CGFloat = 1000) {
        self.height = height
        super.init(frame: CGRect(x: 0, y: -height, width: height, height: height))
        backgroundColor = color
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if let superview = self.superview {
            translatesAutoresizingMaskIntoConstraints = false
            self.frame = CGRect(x: 0, y: -height, width:  superview.frame.width, height: height)
            let widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: superview, attribute: .width, multiplier: 1, constant: 0)
            let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
            let bottomConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: superview, attribute: .top, multiplier: 1, constant: 0)
            
            let constraints = [widthConstraint, heightConstraint, bottomConstraint ]
            superview.addConstraints(constraints)
            
            self.bottomConstraint = bottomConstraint
        }

    }
    
}


extension UITableView {
    func addbackgroundViewAndShiftingSubview(withColor color: UIColor, height: CGFloat = 1000) {
        let bgView = UIView(frame: self.frame)
        let shView = ShiftingTopView(withColor: MSGColor.navigationBarView, height: height)
        bgView.addSubview(shView)
        self.backgroundView = bgView
    }
}
