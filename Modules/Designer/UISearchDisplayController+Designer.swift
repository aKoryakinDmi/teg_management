//
//  UISearchDisplayController+Designer.swift
//  cx4i
//
//  Created by EvGeniy Lell on 10.04.16.
//
//

import Foundation

private let MSGColor = MessengerSchemeColor.sharedInstance

extension UISearchDisplayController {
    func setBackgroundColor(color: UIColor) {
        // searchBar : backgroundColor
        self.searchBar.backgroundColor = color
        self.searchBar.backgroundImage = UIImage(color: color)
        self.searchBar.barTintColor = color
        //UIImage(named: "MSGSearchBarBaclground")
        //UIImage(color: UIColor.redColor(), size: CGSizeMake(64, 64))
        self.searchBar.setBackgroundImage(UIImage(named: "MSGSearchBarBaclground"), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        //color: color, size: CGSizeMake(44, 44)
        self.searchBar.setSearchFieldBackgroundImage(UIImage(color: UIColor.clear, size: CGSize(width: 44, height: 44)), for: .normal)
    }
    
    func setTextColor(color: UIColor, placeholderColor: UIColor? = nil, placeholderText: String) {
        // searchBar : textColor (hack)
        if let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField {
            textFieldInsideSearchBar.textColor = color
            if let truePlaceholderColor = (placeholderColor != nil) ? placeholderColor : color {
                textFieldInsideSearchBar.attributedPlaceholder =
                    NSAttributedString(string: placeholderText /*"Search Contacts"*/, attributes: [NSForegroundColorAttributeName:truePlaceholderColor])
            }
        }
    }
    func setupSearchCXApperance(tableView:UITableView, placeholderText: String) {
        // tableView
        self.searchResultsTableView.rowHeight = tableView.rowHeight
        self.searchResultsTableView.estimatedRowHeight = tableView.estimatedRowHeight
//        self.searchResultsTableView.addSubview(withColor: MSGColor.navigationBarView)
        self.searchResultsTableView.contentInset = UIEdgeInsetsMake(164, 0, 0, 0)
        self.searchResultsTableView.clipsToBounds = true
//        self.searchResultsTableView.size
        
        tableView.tableHeaderView = self.searchBar
        
        // searchBar
        self.searchBar.isTranslucent = true //?
        self.searchBar.tintColor = MSGColor.navigationBarText
        self.setBackgroundColor(color: MSGColor.navigationBarView)
        self.setTextColor(color: MSGColor.navigationBarText, placeholderColor: MSGColor.navigationBarSepatator, placeholderText: placeholderText)
        
        self.searchBar.searchTextPositionAdjustment = UIOffsetMake(8, 0)
        // searchBar : button
        self.searchBar.setImage(UIImage(named: "MSGSearchIcon"), for: UISearchBarIcon.search, state: UIControlState.normal)
        self.searchBar.setImage(UIImage(named: "MSGClearIcon"), for: UISearchBarIcon.clear, state: UIControlState.normal)
        
    }

}
