//
//  UIColor+Designer.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.04.16.
//
//

import Foundation

extension UIColor {
//    static func ColorByProject(cx cx: UInt, tagm: UInt) -> UIColor {
//        return UIColor(hex:byProject(cx: cx, tagm: tagm))
//    }
//    
//    // view - background
//    public class func CXNavigationBarViewColor() -> UIColor {
//        return ColorByProject(cx: 0xF8EA4d, tagm: 0xF8EA4d)
//    }
//    public class func CXNavigationBarSepatatorColor() -> UIColor {
//        return UIColor(hex:0xC6A802)
//    }
//    public class func CXChatSectionHeaderBadgeViewColor() -> UIColor {
//        return UIColor(hex: 0xB0B5B9)
//    }
//    public class func CXRedBadgeViewColor() -> UIColor {
//        return UIColor(hex: 0xF15D5F)
//    }
//    
//    // text
//    public class func CXNavigationBarTextColor() -> UIColor {
//        return UIColor(hex: 0x242424)
//    }
//    public class func CXNavigationBarSelectedTextColor() -> UIColor {
//        return UIColor(hex: 0xC6A802, alpha: 0.8)
//    }
//    public class func CXTextColor() -> UIColor {
//        return UIColor(hex: 0x242424)
//    }
//    public class func CXDisableTextColor() -> UIColor {
//        return UIColor(hex: 0xB0B5B9)
//    }
//    // profile text
//    public class func CXProfileNavigationBarTextColor() -> UIColor {
//        return UIColor(hex: 0xFFFFFF)
//    }
//    public class func CXProfileHeaderTextColor() -> UIColor {
//        return UIColor(hex: 0xBEBEBE)
//    }
//    public class func CXProfileItemTitleTextColor() -> UIColor {
//        return UIColor(hex: 0x9F9F9E)
//    }
//    public class func CXProfileItemValueTextColor() -> UIColor {
//        return UIColor(hex: 0x454545)
//    }
//    public class func CXProfileItemDestructTextColor() -> UIColor {
//        return UIColor(hex: 0xF8645A)
//    }
//
//    public class func CXChatSectionHeaderTextColor() -> UIColor {
//        return UIColor(hex: 0xFFFFFF)
//    }
//    public class func CXChatLinkTextColor() -> UIColor {
//        return UIColor(hex: 0xD06C00)
//    }
//
}


extension UIColor {
   convenience init(hex: UInt, alpha: CGFloat = 1.0) {
      let red: UInt = (hex >> 16) & 0xff
      let green: UInt = (hex >> 8) & 0xff
      let blue: UInt = (hex >> 0) & 0xff
      self.init(red:CGFloat(red) / 255.0, green:CGFloat(green) / 255.0, blue:CGFloat(blue) / 255.0, alpha: alpha)
   }
}
