//
//  UIFont+Designer.swift
//  cx4i
//
//  Created by EvGeniy Lell on 04.04.16.
//
//

import Foundation

public enum FSUIDisplayFontType : String {
    case Black = "SFUIDisplay-Black"
    case Bold = "SFUIDisplay-Bold"
    case Heavy = "SFUIDisplay-Heavy"
    case Light = "SFUIDisplay-Light"
    case Medium = "SFUIDisplay-Medium"
    case Regular = "SFUIDisplay-Regular"
    case Semibold = "SFUIDisplay-Semibold"
    case Thin = "SFUIDisplay-Thin"
    case Ultralight = "SFUIDisplay-Ultralight"
}

extension UIFont {
    convenience init?(SFUIType:FSUIDisplayFontType, size: CGFloat) {
        self.init(name: SFUIType.rawValue, size: size)
    }
}