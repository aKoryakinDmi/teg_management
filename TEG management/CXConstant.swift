import UIKit

let CXGeneral                       = "CXGeneral"
let CXIdentifier                    = "CXIdentifier"
let CXJWT                           = "jwt"
let CXUsername                      = "username"
let CXPassword                      = "password"
let CXReuseIdentifierCluster        = "ADMapCluster"
let CXReuseIdentifierCell           = "CXLoad"
let CXReuseIdentifierAnnotation     = "CXAnnotation"
let CXReuseIdentifierAnnotationFrom = "AnnotationFrom"
let CXReuseIdentifierAnnotationTo   = "AnnotationTo"
let CXBehindTime                    = "CXBehindTime"
let CXRefreshInterval               = "CXRefreshInterval"
let CXSubject                       = "Subject"
let CXComments                      = "Comments"
let CXAnnotationDriver              = "CXAnnotationDriver"
let CXAnnotationFrom                = "CXAnnotationFrom"
let CXAnnotationTo                  = "CXAnnotationTo"
let CXAnnotationAvailable           = "CXAnnotationAvailable"
let CXAnnotationMaybe               = "CXAnnotationMaybe"
let CXAnnotationUnavailable         = "CXAnnotationUnavailable"
let CXAnnotationUnknown             = "CXAnnotationUnknown"
let CXNoLoads                       = "You do not currently have any bookings in progress"
let CXNoFilterLoads                 = "You do not currently have any bookings that meet the criteria for this status"
let CXTermsConditions               = "https://www.courierexchange.mobi/eula"
let CXDateFormat                    = "yyyy-MM-dd'T'HH:mm:ss'Z'"
let CXDateFormatToday               = "yyyy-MM-dd"
let CXDateFormatNotToday            = "HH:mm dd/MM/yy"
let CXDateFormatTime                = "HH:mm"

let CXKeyLoadId                         = "loadId"
let CXKeyDeliverBy                      = "deliverBy"
let CXKeyDistance                       = "distance"
let CXKeyETA                            = "eta"
let CXKeyDistanceInMiles                = "distanceInMiles"
let CXKeyOriginalETA                    = "originalEta"
let CXKeyFrom                           = "from"
let CXKeyTo                             = "to"
let CXKeyCoordinates                    = "coordinates"
let CXKeyLat                            = "lat"
let CXKeyLng                            = "lng"
let CXKeyPostcode                       = "postcode"
let CXKeyCompany                        = "company"
let CXKeyCompanyId                      = "companyId"
let CXKeyCountry                        = "country"
let CXKeyCountryCode2                   = "countryCode2"
let CXKeyName                           = "name"
let CXKeyFullAddress                    = "fullAddress"
let CXKeyTown                           = "town"
let CXKeyCompanySettings                = "companySettings"
let CXKeyBehindTime                     = "behindTime"
let CXKeyRefreshInterval                = "refreshInterval"
let CXKeyJWT                            = "jwt"
let CXKeyUser                           = "user"
let CXKeyUserID                         = "userId"
let CXKeyEmail                          = "email"
let CXKeyFirstName                      = "firstName"
let CXKeyLastName                       = "lastName"
let CXKeyPhone1                         = "phone1"
let CXKeySubcontractor                  = "subcontractor"
let CXKeyVehicle                        = "vehicle"
let CXKeyPhone                          = "phone"
let CXKeyLocation                       = "location"
let CXKeyLatitude                       = "latitude"
let CXKeyLongitude                      = "longitude"
let CXKeyTimestamp                      = "timestamp"
let CXKeyReadyAt                        = "readyAt"
let CXKeyStartTime                      = "startTime"
let CXKeyBodyType                       = "bodyType"
let CXKeyDefaultFromVehicle             = "defaultFromVehicle"
let CXKeyDefaultToVehicle               = "defaultToVehicle"
let CXKeyRadiusList                     = "radiusList"
let CXKeyUKRadiusList                   = "ukRadiusList"
let CXKeyVehicleFilters                 = "vehicleFilters"
let CXKeyVehicleSize                    = "vehicleSize"
let CXKeyDisplayVehicleSize             = "displayVehicleSize"
let CXKeyDisplayBodyType                = "displayBodyType"
let CXKeyAddress                        = "address"
let CXKeyRadius                         = "radius"
let CXKeyAPIKey                         = "apiKey"
let CXKeyStatus                         = "status"
let CXKeyStatusLastChanged              = "statusLastChanged"
let CXKeyCompanyDisplayId               = "companyDisplayId"
let CXKeyCompanyDisplayName             = "companyDisplayName"
let CXKeyContactCompanyId               = "contactCompanyId"
let CXKeyVehicleId                      = "vehicleId"
let CXKeyVehicleFilter: String          = "vehicleFilter"
let CXKeyLocationDisplayAddress: String = "locationDisplayAddress"
let CXKeyMessageText: String            = "messageText"
let CXKeyMessageId: String              = "messageId"
let CXKeyLivery: String                 = "livery"
let CXKeyOwnerDisplayName: String       = "ownerDisplayName"
let CXKeyOwnerDisplayId: String         = "ownerDisplayId"
let CXKeyVehicleDisplayBodyType: String = "vehicleDisplayBodyType"
let CXKeyVehicleDisplayName: String     = "vehicleDisplayName"
let CXKeyVehicleDisplaySize: String     = "vehicleDisplaySize"
let CXKeyMaxWeight: String              = "maxWeight"
let CXKeyValue: String                  = "value"
let CXKeyUnits: String                  = "units"
let CXKeyAvailable: String              = "AVAILABLE"
let CXKeyMaybe: String                  = "MAYBE_AVAILABLE"
let CXKeyUnavailable: String            = "UNAVAILABLE"
let CXKeyUnknown: String                = "UNKNOWN"
let CXKeyAll: String                    = "ALL"
let CXKeyDrivers: String                = "OUR_DRIVERS_AND_SUBBIES"
let CXKeyPreferred: String              = "PREFERRED_PARTNERS"
let CXKeyOther: String                  = "OTHER_DRIVERS"

let CXTabLive: Int = 0
let CXTabHome: Int = 1
let CXTabChat: Int = 2

let CXDefaultBehindTime: TimeInterval      = 15
let CXDefaultRefreshInterval: TimeInterval = 1
let CXDefaultSearchInterval: TimeInterval  = 0.3
let CXDefaultTimeout: TimeInterval         = 60
let CXSleepNSTimeInterval: TimeInterval    = 1.5

let CXMaxZoomLevel: Double         = 18
let CXMinZoomLevel: Double         = 2
let CXRestrictionZoomLevel: Double = 10

let CXColorMainTheme   = UIColor(hex: 0x06a0dd)
let CXColorBackground  = UIColor(hex: 0xf0f2f4)
let CXColorBorder      = UIColor(hex: 0xa9a9a8)
let CXColorLink        = UIColor(hex: 0x80afe5)
let CXColorLogOut      = UIColor(hex: 0x646668)
let CXColorPlaceholder = UIColor(hex: 0xc7c7cd)
let CXColorAvailable   = UIColor(hex: 0x51d63c)
let CXColorMaybe       = UIColor(hex: 0xf49e09)
let CXColorUnavailable = UIColor(hex: 0xf86459)
let CXColorUnknown     = UIColor(hex: 0xd4d6d7)
let CXColorBlack       = UIColor(hex: 0x454545)
let CXColorBlue        = UIColor(hex: 0x7fb8f1)
let CXColorGray        = UIColor(hex: 0xdadee2)
let CXColorGreen       = UIColor(hex: 0x42aa32)
let CXColorRed         = UIColor(hex: 0xee5e57)
let CXColorSilver      = UIColor(hex: 0x9f9f9e)
let CXColorWhite       = UIColor(hex: 0xffffff)
let CXColorYellow      = UIColor(hex: 0xf2d562)

enum CXLoadStatus: Int {
    case Total = 0
    case OnTime
    case BehindETA
    case Late
    case NotTracked
}

class CXSlider: UISlider {
    override func minimumValueImageRect(forBounds bounds: CGRect) -> CGRect {
        return super.minimumValueImageRect(forBounds: bounds).offsetBy(dx: 14, dy: 0)
    }
    
    override func maximumValueImageRect(forBounds bounds: CGRect) -> CGRect {
        return super.maximumValueImageRect(forBounds: bounds).offsetBy(dx: -14, dy: 0)
    }
}

class CXButton: UIButton {
    let imageViewDone = UIImageView(frame: CGRect.zero)
    
    func setImageDone(imageDone: UIImage) {
        imageViewDone.backgroundColor = UIColor.clear
        imageViewDone.image = imageDone
        imageViewDone.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageViewDone)
        self.addConstraint(NSLayoutConstraint(item: imageViewDone, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: -5))
        self.addConstraint(NSLayoutConstraint(item: imageViewDone, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 5))
        self.addConstraint(NSLayoutConstraint(item: imageViewDone, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: imageDone.size.width))
        self.addConstraint(NSLayoutConstraint(item: imageViewDone, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: imageDone.size.height))
    }
}

class CXAnnotation: MKPointAnnotation {
    var identifier: String?
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        self.init(red: (hex >> 16) & 0xff, green: (hex >> 8) & 0xff, blue: hex & 0xff)
    }
}

extension String {
    func convertToDictionary() -> [String: AnyObject]? {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            }
            catch let error as NSError {
                print(error)
            }
        }
        
        return nil
    }
    
    func isEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    func isToday() -> Bool {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = CXDateFormatToday
        if self.contains(dateFormat.string(from: Date())) {
            return true
        }
        return false
    }
    
    func fromBase64() -> String {
        let data = NSData(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
        return String(data: data! as Data, encoding: String.Encoding.utf8)!
    }
    
    func toBase64() -> String {
        let data = self.data(using: String.Encoding.utf8)
        return data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }
    
    func toMD5() -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.deallocate(capacity: digestLen)
        
        return String(format: hash as String)
    }
}

extension Double {
    func convertToMeters() -> Double {
        return self * 1.609344 * 1000
    }
    
    func convertToMiles() -> Double {
        return self * 0.000621371192
    }
}
