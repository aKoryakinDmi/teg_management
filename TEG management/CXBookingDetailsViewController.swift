import UIKit
import CoreLocation

class CXBookingDetailsViewController: UIViewController {
    struct Layout {
        static let completedViewContentHeight: CGFloat = 318
        static let labelOffset: CGFloat = 15
    }
    
    @IBOutlet weak var slider: CXSlider?
    @IBOutlet weak var viewContent: UIView?
    @IBOutlet weak var viewHalf: UIView?
    @IBOutlet weak var buttonCallHidden: UIButton?
    @IBOutlet weak var buttonViewRoute: UIButton?
    @IBOutlet weak var labelDelta: UILabel?
    @IBOutlet weak var labelETA: UILabel?
    @IBOutlet weak var labelFromTo: UILabel?
    @IBOutlet weak var labelCarrier: UILabel?
    @IBOutlet weak var labelCarrierValue: UILabel?
    @IBOutlet weak var labelMobile: UILabel?
    @IBOutlet weak var labelTotalMiles: UILabel?
    @IBOutlet weak var labelTotalMilesValue: UILabel?
    @IBOutlet weak var labelCompletedMiles: UILabel?
    @IBOutlet weak var labelCompletedMilesValue: UILabel?
    @IBOutlet weak var labelEstJourneyTime: UILabel?
    @IBOutlet weak var labelEstJourneyTimeValue: UILabel?
    @IBOutlet weak var labelPickUpTime: UILabel?
    @IBOutlet weak var labelPickUpTimeValue: UILabel?
    @IBOutlet weak var labelStartTime: UILabel?
    @IBOutlet weak var labelStartTimeValue: UILabel?
    @IBOutlet weak var labelMilesToGo: UILabel?
    @IBOutlet weak var labelMilesToGoValue: UILabel?
    @IBOutlet weak var labelDeliverByTime: UILabel?
    @IBOutlet weak var labelDeliverByTimeValue: UILabel?
    @IBOutlet weak var labelOriginalETA: UILabel?
    @IBOutlet weak var labelOriginalETAValue: UILabel?
    @IBOutlet weak var labelCurrentETA: UILabel?
    @IBOutlet weak var labelCurrentETAValue: UILabel?
    @IBOutlet weak var labelLastTrackedAt: UILabel?
    @IBOutlet weak var labelLastTrackedAtValue: UILabel?
    @IBOutlet weak var layoutConstraintViewContentHeight: NSLayoutConstraint?
    @IBOutlet weak var layoutConstraintPickUpTimeTop: NSLayoutConstraint?
    @IBOutlet weak var layoutConstraintDeliverByTimeTop: NSLayoutConstraint?
    @IBOutlet weak var layoutConstraintCurrentETATop: NSLayoutConstraint?
    var buttonCall: UIButton?
    var load: Dictionary<String, AnyObject>?
    var timer: Timer?
    var mobile: String?
    var x: Int?
    var progress: Float?
    var completed: Bool?
    var status: CXLoadStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = CXColorBackground
        
        self.title = "Booking Details"
        
        let buttonBack = UIButton(type: .custom)
        buttonBack.frame = CGRect(x: 0, y: 0, width: 10, height: 17)
        buttonBack.setImage(UIImage(named: "Back"), for: .normal)
        buttonBack.addTarget(self, action: #selector(CXBookingDetailsViewController.actionBack), for: .touchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem()
        leftBarButtonItem.customView = buttonBack
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        buttonCall = UIButton(type: .custom)
        buttonCall!.isEnabled = false
        buttonCall!.frame = CGRect(x: 0, y: 0, width: 16, height: 20)
        buttonCall!.setImage(UIImage(named: "phone"), for: .normal)
        buttonCall!.setImage(UIImage(named: "phone-disable"), for: .disabled)
        buttonCall!.addTarget(self, action: #selector(CXBookingDetailsViewController.actionCall(_:)), for: .touchUpInside)
        
        let rightBarButtonItem = UIBarButtonItem()
        rightBarButtonItem.customView = buttonCall
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(CXBookingDetailsViewController.actionSwipe(_:)))
        swipeGestureRecognizer.direction = .right
        viewHalf?.addGestureRecognizer(swipeGestureRecognizer)
        
        self.prepareContent()
        self.updateContent()
        if completed == false {
            self.executorLocation(animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.timerEnable()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timerDisable()
    }
    
    private func addressWithDistance(distance: Float, load: Dictionary<String, AnyObject>?) -> String {
        var address = ""
        if let town = (load?[CXKeyFrom] as! NSDictionary)[CXKeyTown] as? String {
            address += town
        } else {
            if let fullAddress = (load?[CXKeyFrom] as! NSDictionary)[CXKeyFullAddress] as? String {
                address += fullAddress
            }
        }
        if let postcode = (load?[CXKeyFrom] as! NSDictionary)[CXKeyPostcode] as? String {
            address += " (\(postcode))"
        }
        address += " - "
        if let town = (load?[CXKeyTo] as! NSDictionary)[CXKeyTown] as? String {
            address += "\(town)"
        } else {
            if let fullAddress = (load?[CXKeyTo] as! NSDictionary)[CXKeyFullAddress] as? String {
                address += fullAddress
            }
        }
        if let postcode = (load?[CXKeyTo] as! NSDictionary)[CXKeyPostcode] as? String {
            address += " (\(postcode))"
        }
        address += ", \(distance) miles"
        return address
    }
    
    private func differenceBetween(fromDate: Date, toDate: Date) -> String {
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([.day, .hour, .minute, .second])
        let components = calendar.dateComponents(unitFlags, from: fromDate, to: toDate)//, options: .WrapComponents)
        var difference = ""
        if components.day! > 0 {
            difference += "\(String(describing: components.day))d "
        }
        if components.hour! > 0 {
            difference += "\(String(describing: components.hour))h "
        }
        if components.minute! > 0 {
            difference += "\(String(describing: components.minute))m"
        }
        return difference
    }
    
    private func executorLocation(animated: Bool) {
        if animated == true {
            CXProgressHUD.sharedInstance.show()
        }
        let loadId = load?[CXKeyLoadId]
        CXHTTPManager.sharedInstance.executorLocation(loadId: String(describing: loadId!), completion: { (response, error) -> Void in
            if error == nil {
                let location = (response as! NSDictionary)[CXKeyLocation] as! NSDictionary
                if location[CXKeyTimestamp] != nil {
                    let timestamp = location[CXKeyTimestamp] as! TimeInterval
                    let dateTimestamp = Date(timeIntervalSince1970: timestamp / 1000)
                    let dateFormat = DateFormatter()
                    dateFormat.dateFormat = CXDateFormatTime
                    self.labelLastTrackedAtValue?.text = "\(dateFormat.string(from: dateTimestamp))"
                }
            }
            CXProgressHUD.sharedInstance.hide()
        })
    }
    
    fileprivate func loads(animated: Bool) {
        if animated == true {
            CXProgressHUD.sharedInstance.show()
        }
        let loadId = load?[CXKeyLoadId]
        CXHTTPManager.sharedInstance.inProgressWithCompletion { (response, error) -> Void in
            if error == nil {
                let arrayFiltered = (response as? Array)?.filter() { (element: Dictionary<String, AnyObject>) -> Bool in
                    if String(describing: element[CXKeyLoadId]!) == String(describing: loadId!) {
                        return true
                    }
                    return false
                }
                if (arrayFiltered?.count)! > 0 {
                    self.load = arrayFiltered?.first
                }
                if let status = self.load?[CXKeyStatus] as? String, status == "COMPLETED" {
                    self.completed = true
                }
                self.updateContent()
                if self.completed == false {
                    self.executorLocation(animated: false)
                }
            } else {
                CXErrorHandler.sharedInstance.handleError(error: error!)
                CXProgressHUD.sharedInstance.hide()
            }
        }
    }
    
    private func loadStatus() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = CXDateFormat
        
        var behindTime = CXDefaultBehindTime
        if CXUser.sharedInstance.userData(key: CXBehindTime) != nil {
            behindTime = CXUser.sharedInstance.userData(key: CXBehindTime) as! TimeInterval
        }
        
        x = 0
        progress = 0
        if load?[CXKeyETA] as? Dictionary<String, AnyObject> != nil {
            let deliverBy = load?[CXKeyDeliverBy] as! String
            let dateDeliverBy = dateFormat.date(from: deliverBy)
            let distance = load?[CXKeyDistance] as! Float
            let eta = (load?[CXKeyETA] as! NSDictionary)[CXKeyETA] as! String
            let dateETA = dateFormat.date(from: eta)
            let distanceInMiles = (load?[CXKeyETA] as! NSDictionary)[CXKeyDistanceInMiles] as! Float
            if distanceInMiles <= distance {
                progress = 1 - distanceInMiles / distance
            }
            if eta == deliverBy {
                status = .OnTime
            } else {
                let unitFlags = Set<Calendar.Component>([.minute])
                x = Calendar.current.dateComponents(unitFlags, from: dateETA!, to: dateDeliverBy!).minute
                if dateETA?.compare(dateDeliverBy!) == .orderedAscending {
                    status = .OnTime
                } else {
                    if dateETA?.compare((dateDeliverBy?.addingTimeInterval(behindTime * 60))!) == .orderedAscending {
                        status = .BehindETA
                    } else {
                        status = .Late
                    }
                }
            }
        } else {
            status = .NotTracked
        }
    }
    
    private func prepareContent() {
        if completed == true {
            labelCompletedMiles?.removeFromSuperview()
            labelCompletedMilesValue?.removeFromSuperview()
            labelEstJourneyTime?.removeFromSuperview()
            labelEstJourneyTimeValue?.removeFromSuperview()
            labelMilesToGo?.removeFromSuperview()
            labelMilesToGoValue?.removeFromSuperview()
            labelOriginalETA?.removeFromSuperview()
            labelOriginalETAValue?.removeFromSuperview()
            labelLastTrackedAt?.removeFromSuperview()
            labelLastTrackedAtValue?.removeFromSuperview()
            labelCurrentETA?.text = "Last ETA:"
            layoutConstraintViewContentHeight?.constant = Layout.completedViewContentHeight
            layoutConstraintPickUpTimeTop?.constant = Layout.labelOffset
            layoutConstraintDeliverByTimeTop?.constant = Layout.labelOffset
            layoutConstraintCurrentETATop?.constant = Layout.labelOffset
        }
        
        slider?.minimumTrackTintColor = CXColorGray
        slider?.maximumTrackTintColor = CXColorGray
        slider?.minimumValueImage = UIImage(named: "Gray_dot")
        slider?.maximumValueImage = UIImage(named: "gray_outline")
        slider?.setThumbImage(UIImage(named: "Gray_Car"), for: .normal)
        slider?.isUserInteractionEnabled = false
        viewContent?.layer.cornerRadius = 3
        buttonCallHidden?.isEnabled = false
        buttonViewRoute?.backgroundColor = CXColorMainTheme
        buttonViewRoute?.layer.cornerRadius = 3
        buttonViewRoute?.setTitleColor(CXColorWhite, for: .normal)
        labelDelta?.textColor = CXColorSilver
        labelETA?.textColor = CXColorBlack
        labelFromTo?.textColor = CXColorBlack
        labelCarrier?.textColor = CXColorSilver
        labelCarrierValue?.textColor = CXColorBlack
        labelMobile?.textColor = CXColorLink
        labelTotalMiles?.textColor = CXColorSilver
        labelTotalMilesValue?.textColor = CXColorBlack
        labelCompletedMiles?.textColor = CXColorSilver
        labelCompletedMilesValue?.textColor = CXColorBlack
        labelEstJourneyTime?.textColor = CXColorSilver
        labelEstJourneyTimeValue?.textColor = CXColorBlack
        labelPickUpTime?.textColor = CXColorSilver
        labelPickUpTimeValue?.textColor = CXColorBlack
        labelStartTime?.textColor = CXColorSilver
        labelStartTimeValue?.textColor = CXColorBlack
        labelMilesToGo?.textColor = CXColorSilver
        labelMilesToGoValue?.textColor = CXColorBlack
        labelDeliverByTime?.textColor = CXColorSilver
        labelDeliverByTimeValue?.textColor = CXColorBlack
        labelOriginalETA?.textColor = CXColorSilver
        labelOriginalETAValue?.textColor = CXColorBlack
        labelCurrentETA?.textColor = CXColorSilver
        labelCurrentETAValue?.textColor = CXColorBlack
        labelLastTrackedAt?.textColor = CXColorSilver
        labelLastTrackedAtValue?.text = ""
        labelLastTrackedAtValue?.textColor = CXColorBlack
    }
    
    private func reset() {
        labelCarrierValue?.text = ""
        labelMobile?.text = ""
        labelCompletedMilesValue?.text = ""
        labelEstJourneyTimeValue?.text = ""
        labelPickUpTimeValue?.text = ""
        labelStartTimeValue?.text = ""
        labelMilesToGoValue?.text = ""
        labelDeliverByTimeValue?.text = ""
        labelOriginalETAValue?.text = ""
        labelCurrentETAValue?.text = ""
    }
    
    private func timerEnable() {
        if timer != nil {
            self.timerDisable()
        }
        var refreshInterval = CXDefaultRefreshInterval
        if CXUser.sharedInstance.userData(key: CXRefreshInterval) != nil {
            refreshInterval = CXUser.sharedInstance.userData(key: CXRefreshInterval) as! TimeInterval
        }
        timer = Timer.scheduledTimer(timeInterval: refreshInterval * 60, target: self, selector: #selector(CXBookingDetailsViewController.actionUpdate), userInfo: nil, repeats: true)
    }
    
    private func timerDisable() {
        timer?.invalidate()
        timer = nil
    }
    
    private func updateContent() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = CXDateFormat
        
        self.reset()
        self.updateContentTop()
        self.updateContentCarrier()
        self.updateContentETA()
        
        let distance = load?[CXKeyDistance] as! Float
        labelFromTo?.text = self.addressWithDistance(distance: distance, load: load)
        labelTotalMilesValue?.text = "\(distance)"
        if var readyAt = load?[CXKeyReadyAt] as? String {
            dateFormat.dateFormat = CXDateFormat
            let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: readyAt)!)
            let dateReadyAt = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: readyAt)!)
            readyAt = dateFormat.string(from: dateReadyAt as Date)
            if readyAt.isToday() {
                dateFormat.dateFormat = CXDateFormatTime
                labelPickUpTimeValue?.text = "\(dateFormat.string(from: dateReadyAt))"
            } else {
                dateFormat.dateFormat = CXDateFormatNotToday
                labelPickUpTimeValue?.text = "\(dateFormat.string(from: dateReadyAt))"
            }
        }
        if var startTime = load?[CXKeyStartTime] as? String {
            dateFormat.dateFormat = CXDateFormat
            let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: startTime)!)
            let dateStartTime = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: startTime)!)
            startTime = dateFormat.string(from: dateStartTime as Date)
            if startTime.isToday() {
                dateFormat.dateFormat = CXDateFormatTime
                labelStartTimeValue?.text = "\(dateFormat.string(from: dateStartTime))"
            } else {
                dateFormat.dateFormat = CXDateFormatNotToday
                labelStartTimeValue?.text = "\(dateFormat.string(from: dateStartTime))"
            }
        }
        if var deliverBy = load?[CXKeyDeliverBy] as? String {
            dateFormat.dateFormat = CXDateFormat
            let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: deliverBy)!)
            let dateDeliverBy = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: deliverBy)!)
            deliverBy = dateFormat.string(from: dateDeliverBy as Date)
            if deliverBy.isToday() {
                dateFormat.dateFormat = CXDateFormatTime
                labelDeliverByTimeValue?.text = "\(dateFormat.string(from: dateDeliverBy))"
            } else {
                dateFormat.dateFormat = CXDateFormatNotToday
                labelDeliverByTimeValue?.text = "\(dateFormat.string(from: dateDeliverBy))"
            }
        }
    }
    
    private func updateContentTop() {
        if completed == true {
            slider?.minimumTrackTintColor = CXColorGray
            slider?.minimumValueImage = UIImage(named: "Gray_dot")
            slider?.value = 0
            slider?.setThumbImage(UIImage(named: "Gray_Car"), for: .normal)
            labelETA?.text = ""
            
            if var statusLastChanged = load?[CXKeyStatusLastChanged] as? String {
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = CXDateFormat
                let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: statusLastChanged)!)
                let dateStatusLastChanged = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: statusLastChanged)!)
                statusLastChanged = dateFormat.string(from: dateStatusLastChanged)
                if statusLastChanged.isToday() {
                    dateFormat.dateFormat = CXDateFormatTime
                    labelDelta?.text = "DELIVERED: \(dateFormat.string(from: dateStatusLastChanged))"
                } else {
                    dateFormat.dateFormat = CXDateFormatNotToday
                    labelDelta?.text = "DELIVERED: \(dateFormat.string(from: dateStatusLastChanged))"
                }
            }
            
            return
        }
        
        self.loadStatus()
        switch status! {
        case .Total, .OnTime:
            slider?.minimumTrackTintColor = CXColorGreen
            slider?.minimumValueImage = UIImage(named: "green_dot")
            slider?.setThumbImage(UIImage(named: "Green_Car"), for: .normal)
            if x == 0 {
                labelDelta?.text = "on time"
            } else {
                labelDelta?.text = "\(x!) min ahead of schedule"
            }
        case .BehindETA:
            slider?.minimumTrackTintColor = CXColorYellow
            slider?.minimumValueImage = UIImage(named: "yellow_dot")
            slider?.setThumbImage(UIImage(named: "Yellow_Car"), for: .normal)
            labelDelta?.text = "\(x!) min behind schedule"
        case .Late:
            slider?.minimumTrackTintColor = CXColorRed
            slider?.minimumValueImage = UIImage(named: "red_dot")
            slider?.setThumbImage(UIImage(named: "Red_Car"), for: .normal)
            labelDelta?.text = "\(x!) min behind schedule"
        case .NotTracked:
            slider?.minimumTrackTintColor = CXColorGray
            slider?.minimumValueImage = UIImage(named: "Gray_dot")
            slider?.setThumbImage(UIImage(named: "Gray_Car"), for: .normal)
            labelDelta?.text = "N/A"
            labelETA?.text = "ETA: N/A"
        }
        slider?.value = progress!
    }
    
    private func updateContentCarrier() {
        let subcontractor = load?[CXKeySubcontractor] as! NSDictionary
        if subcontractor[CXKeyName] != nil {
            labelCarrierValue?.text = "\(subcontractor[CXKeyName] as! String)"
        } else {
            if subcontractor[CXKeyVehicle] != nil {
                labelCarrierValue?.text = "\(subcontractor[CXKeyVehicle] as! String)"
            }
        }
        buttonCall!.isEnabled = false
        buttonCallHidden?.isEnabled = false
        if subcontractor[CXKeyPhone] != nil {
            buttonCall!.isEnabled = true
            buttonCallHidden?.isEnabled = true
            mobile = subcontractor[CXKeyPhone] as? String
            labelMobile?.text = "+\(mobile!)"
        }
    }
    
    private func updateContentETA() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = CXDateFormat
        
        let distance = load?[CXKeyDistance] as! Float
        if load?[CXKeyETA] as? Dictionary<String, AnyObject> != nil {
            let distanceInMiles = (load?[CXKeyETA] as! NSDictionary)[CXKeyDistanceInMiles] as! Float
            if var eta = (load?[CXKeyETA] as! NSDictionary)[CXKeyETA] as? String {
                let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: eta)!)
                let dateETA = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: eta)!)
                eta = dateFormat.string(from: dateETA)
                if eta.isToday() {
                    dateFormat.dateFormat = CXDateFormatTime
                    labelETA?.text = "ETA: \(dateFormat.string(from: dateETA))"
                    labelCurrentETAValue?.text = "\(dateFormat.string(from: dateETA))"
                } else {
                    dateFormat.dateFormat = CXDateFormatNotToday
                    labelETA?.text = "ETA: \(dateFormat.string(from: dateETA))"
                    labelCurrentETAValue?.text = "\(dateFormat.string(from: dateETA))"
                }
            }
            var originalETA = (load?[CXKeyETA] as! NSDictionary)[CXKeyOriginalETA] as? String
            
            labelCompletedMilesValue?.text = "\(distance - distanceInMiles)"
            if let startTime = load?[CXKeyStartTime] as? String {
                dateFormat.dateFormat = CXDateFormat
                let dateStartTime = dateFormat.date(from: startTime)
                if originalETA != nil {
                    let dateOriginalETA = dateFormat.date(from: originalETA!)
                    labelEstJourneyTimeValue?.text = self.differenceBetween(fromDate: dateStartTime!, toDate: dateOriginalETA!)
                }
            }
            labelMilesToGoValue?.text = "\(distanceInMiles)"
            if originalETA != nil {
                dateFormat.dateFormat = CXDateFormat
                let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: originalETA!)!)
                let dateOriginalETA = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: originalETA!)!)
                originalETA = dateFormat.string(from: dateOriginalETA)
                if originalETA!.isToday() {
                    dateFormat.dateFormat = CXDateFormatTime
                    labelOriginalETAValue?.text = "\(dateFormat.string(from: dateOriginalETA))"
                } else {
                    dateFormat.dateFormat = CXDateFormatNotToday
                    labelOriginalETAValue?.text = "\(dateFormat.string(from: dateOriginalETA))"
                }
            }
        }
    }
}

extension CXBookingDetailsViewController {
    @IBAction func actionCall(_ sender: Any) {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "+\(mobile!)", message: nil, preferredStyle: .alert)
            var action = UIAlertAction(title: "Cancel", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            action = UIAlertAction(title: "Call", style: .default) { (action) in
                if UIApplication.shared.canOpenURL(URL(string: "tel:\(self.mobile!.replacingOccurrences(of: " ", with: ""))")!) {
                    UIApplication.shared.openURL(URL(string: "tel:\(self.mobile!.replacingOccurrences(of: " ", with: ""))")!)
                }
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            if UIApplication.shared.canOpenURL(URL(string: "tel:\(self.mobile!.replacingOccurrences(of: " ", with: ""))")!) {
                UIApplication.shared.openURL(URL(string: "tel:\(self.mobile!.replacingOccurrences(of: " ", with: ""))")!)
            }
        }
    }
    
    @IBAction func actionViewRoute(_ sender: Any) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXBookingRoute") as! CXBookingRouteViewController
        viewController.load = load
        viewController.status = status
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func actionSwipe(_ gestureRecognizer: UISwipeGestureRecognizer) {
        self.actionBack()
    }
    
    func actionUpdate() {
        self.loads(animated: true)
    }
}
