//
//  CXTabBarViewController.swift
//  TEG management
//
//  Created by EvGeniy Lell on 23.06.16.
//
//

import Foundation

class CXTabBarViewController: UITabBarController {
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(CXTabBarViewController.updateChatBadge(_:)), name:NSNotification.Name(rawValue: CXMessengerChatMachineNotificationUnreadMessagesCountDidChanged), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateChatBadge(_ notification: NSNotification) {
        if let count = notification.userInfo?["count"] as? NSInteger,
            let chatBarItem = chatBarItem() {
            //            chatBarItem.color
            if count == 0 {
                chatBarItem.badgeValue = nil
            } else {
                chatBarItem.badgeValue = String(count)
            }
        }
    }
    
    private func chatBarItem() -> UITabBarItem? {
        return tabBarItem(CXTabChat)
    }
    
    private func tabBarItem(_ index: NSInteger) -> UITabBarItem? {
        if let viewControllers = viewControllers, viewControllers.count > index {
            let chatController = viewControllers[index]
            return chatController.tabBarItem
        }
        return nil
    }
}
