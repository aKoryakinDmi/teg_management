import UIKit

class CXHomeViewController: UIViewController, UIAlertViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    struct Layout {
        static let collectionCellOffset: CGFloat = 8
        static let collectionCellHeight: CGFloat = 80
        static let buttonFilterOffset: CGFloat = 6
        static let buttonFilterImageOffsetX: CGFloat = 5
        static let buttonFilterImageOffsetY: CGFloat = 15
        static let labelFilterHeight: CGFloat = 16
    }
    
    @IBOutlet weak var viewContent: UIView?
    @IBOutlet weak var viewFilter: UIView?
    @IBOutlet weak var collection: UICollectionView?
    @IBOutlet weak var labelMessage: UILabel?
    @IBOutlet weak var layoutConstraintViewContentLeading: NSLayoutConstraint?
    @IBOutlet weak var layoutConstraintViewFilterHeight: NSLayoutConstraint?
    var menu: CXMenu?
    var buttonFilterBlue: CXButton?
    var buttonFilterGray: CXButton?
    var buttonFilterGreen: CXButton?
    var buttonFilterRed: CXButton?
    var buttonFilterYellow: CXButton?
    var buttonHidden: UIButton?
    var labelFilterBlue: UILabel?
    var labelFilterGray: UILabel?
    var labelFilterGreen: UILabel?
    var labelFilterRed: UILabel?
    var labelFilterYellow: UILabel?
    var layoutConstraintMenu: NSLayoutConstraint?
    var timer: Timer?
    var arrayLoads: Array<AnyObject>?
    var arrayLoadsFiltered: Array<AnyObject>?
    var loading: Bool?
    var filter: CXLoadStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = CXColorBackground
        
        self.navigationController?.navigationBar.barTintColor = CXColorMainTheme
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "SFUIDisplay-Regular", size: 19)!, NSForegroundColorAttributeName: CXColorWhite]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.hidesBackButton = true
        self.title = "Bookings In Progress"
        
        let buttonMenu = UIButton(type: .custom)
        buttonMenu.setImage(UIImage(named: "burger"), for: .normal)
        buttonMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonMenu.addTarget(self, action: #selector(CXHomeViewController.actionMenu), for: .touchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem()
        leftBarButtonItem.customView = buttonMenu
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        let buttonUpdate = UIButton(type: .custom)
        buttonUpdate.setImage(UIImage(named: "update"), for: .normal)
        buttonUpdate.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonUpdate.addTarget(self, action: #selector(CXHomeViewController.actionUpdate), for: .touchUpInside)
        
        let rightBarButtonItem = UIBarButtonItem()
        rightBarButtonItem.customView = buttonUpdate
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.prepareContent()
        
        loading = false
        filter = .Total
        if arrayLoads == nil {
            arrayLoads = Array()
        }
        if arrayLoadsFiltered == nil {
            arrayLoadsFiltered = Array()
        }
        
        self.drawFilter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loads(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(CXHomeViewController.applicationDidBecomeActive(_:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        self.timerEnable()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        
        self.timerDisable()
    }
    
    fileprivate func addressWithDistance(_ distance: Float, load: Dictionary<String, AnyObject>) -> String {
        var address = ""
        if (load[CXKeyFrom] as! NSDictionary)[CXKeyTown] != nil {
            let town = (load[CXKeyFrom] as! NSDictionary)[CXKeyTown] as! String
            address += town
        } else {
            if (load[CXKeyFrom] as! NSDictionary)[CXKeyFullAddress] != nil {
                let fullAddress = (load[CXKeyFrom] as! NSDictionary)[CXKeyFullAddress] as! String
                address += fullAddress
            }
        }
        if (load[CXKeyFrom] as! NSDictionary)[CXKeyPostcode] != nil {
            let postcode = (load[CXKeyFrom] as! NSDictionary)[CXKeyPostcode] as! String
            address += " (\(postcode))"
        }
        address += " - "
        if (load[CXKeyTo] as! NSDictionary)[CXKeyTown] != nil {
            let town = (load[CXKeyTo] as! NSDictionary)[CXKeyTown] as! String
            address += "\(town)"
        } else {
            if (load[CXKeyTo] as! NSDictionary)[CXKeyFullAddress] != nil {
                let fullAddress = (load[CXKeyTo] as! NSDictionary)[CXKeyFullAddress] as! String
                address += fullAddress
            }
        }
        if (load[CXKeyTo] as! NSDictionary)[CXKeyPostcode] != nil {
            let postcode = (load[CXKeyTo] as! NSDictionary)[CXKeyPostcode] as! String
            address += " (\(postcode))"
        }
        address += ", \(distance) miles"
        return address
    }
    
    private func drawFilter() {
        let size: CGFloat = (UIScreen.main.bounds.width - Layout.buttonFilterOffset * 6) / 5;
        layoutConstraintViewFilterHeight?.constant = size + Layout.buttonFilterOffset * 3 + Layout.labelFilterHeight * 2
        
        buttonFilterBlue = self.buttonFilter()
        buttonFilterBlue!.backgroundColor = CXColorBlue
        buttonFilterBlue!.addTarget(self, action: #selector(CXHomeViewController.actionFilterBlue), for: .touchUpInside)
        buttonFilterBlue!.setImageDone(imageDone: UIImage(named: "Done_Blue")!)
        viewFilter?.addSubview(buttonFilterBlue!)
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterBlue!, attribute: .leading, relatedBy: .equal, toItem: viewFilter!, attribute: .leading, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterBlue!, attribute: .top, relatedBy: .equal, toItem: viewFilter!, attribute: .top, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterBlue!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterBlue!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        
        labelFilterBlue = self.labelFilter()
        labelFilterBlue!.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        labelFilterBlue!.text = "Total"
        viewFilter?.addSubview(labelFilterBlue!)
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterBlue!, attribute: .leading, relatedBy: .equal, toItem: viewFilter!, attribute: .leading, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterBlue!, attribute: .top, relatedBy: .equal, toItem: buttonFilterBlue, attribute: .bottom, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterBlue!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterBlue!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelFilterHeight))
        
        buttonFilterGreen = self.buttonFilter()
        buttonFilterGreen!.backgroundColor = CXColorGreen
        buttonFilterGreen!.imageViewDone.isHidden = true
        buttonFilterGreen!.addTarget(self, action: #selector(CXHomeViewController.actionFilterGreen), for: .touchUpInside)
        buttonFilterGreen!.setImageDone(imageDone: UIImage(named: "Done_Green")!)
        viewFilter?.addSubview(buttonFilterGreen!)
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGreen!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterBlue, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGreen!, attribute: .top, relatedBy: .equal, toItem: viewFilter!, attribute: .top, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGreen!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGreen!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        
        labelFilterGreen = self.labelFilter()
        labelFilterGreen!.text = "On Time"
        viewFilter?.addSubview(labelFilterGreen!)
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGreen!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterBlue, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGreen!, attribute: .top, relatedBy: .equal, toItem: buttonFilterBlue, attribute: .bottom, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGreen!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGreen!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelFilterHeight))
        
        buttonFilterYellow = self.buttonFilter()
        buttonFilterYellow!.backgroundColor = CXColorYellow
        buttonFilterYellow!.imageViewDone.isHidden = true
        buttonFilterYellow!.addTarget(self, action: #selector(CXHomeViewController.actionFilterYellow), for: .touchUpInside)
        buttonFilterYellow!.setImageDone(imageDone: UIImage(named: "Done_Yellow")!)
        viewFilter?.addSubview(buttonFilterYellow!)
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterYellow!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterGreen, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterYellow!, attribute: .top, relatedBy: .equal, toItem: viewFilter!, attribute: .top, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterYellow!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterYellow!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        
        labelFilterYellow = self.labelFilter()
        labelFilterYellow!.text = "Behind ETA"
        viewFilter?.addSubview(labelFilterYellow!)
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterYellow!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterGreen, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterYellow!, attribute: .top, relatedBy: .equal, toItem: buttonFilterBlue, attribute: .bottom, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterYellow!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterYellow!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelFilterHeight))
        
        buttonFilterRed = self.buttonFilter()
        buttonFilterRed!.backgroundColor = CXColorRed
        buttonFilterRed!.imageViewDone.isHidden = true
        buttonFilterRed!.addTarget(self, action: #selector(CXHomeViewController.actionFilterRed), for: .touchUpInside)
        buttonFilterRed!.setImageDone(imageDone: UIImage(named: "Done_Red")!)
        viewFilter?.addSubview(buttonFilterRed!)
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterRed!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterYellow, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterRed!, attribute: .top, relatedBy: .equal, toItem: viewFilter!, attribute: .top, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterRed!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterRed!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        
        labelFilterRed = self.labelFilter()
        labelFilterRed!.text = "Late"
        viewFilter?.addSubview(labelFilterRed!)
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterRed!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterYellow, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterRed!, attribute: .top, relatedBy: .equal, toItem: buttonFilterBlue, attribute: .bottom, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterRed!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterRed!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelFilterHeight))
        
        buttonFilterGray = self.buttonFilter()
        buttonFilterGray!.backgroundColor = CXColorGray
        buttonFilterGray!.imageViewDone.isHidden = true
        buttonFilterGray!.addTarget(self, action: #selector(CXHomeViewController.actionFilterGray), for: .touchUpInside)
        buttonFilterGray!.setImageDone(imageDone: UIImage(named: "Done_Gray")!)
        viewFilter?.addSubview(buttonFilterGray!)
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGray!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterRed, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGray!, attribute: .top, relatedBy: .equal, toItem: viewFilter!, attribute: .top, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGray!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: buttonFilterGray!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        
        labelFilterGray = self.labelFilter()
        labelFilterGray!.text = "Not Tracked\nNot Started"
        viewFilter?.addSubview(labelFilterGray!)
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGray!, attribute: .leading, relatedBy: .equal, toItem: buttonFilterRed, attribute: .trailing, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGray!, attribute: .top, relatedBy: .equal, toItem: buttonFilterBlue, attribute: .bottom, multiplier: 1, constant: Layout.buttonFilterOffset))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGray!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size))
        self.view.addConstraint(NSLayoutConstraint(item: labelFilterGray!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelFilterHeight * 2))
        
        self.updateContent()
    }
    
    private func drawHiddenButton(offset: CGFloat) {
        self.buttonHidden = UIButton(type: .custom)
        self.buttonHidden!.frame = CGRect.zero
        self.buttonHidden!.translatesAutoresizingMaskIntoConstraints = false
        self.buttonHidden!.addTarget(self, action: #selector(CXHomeViewController.actionMenuHidden), for: .touchUpInside)
        self.view.addSubview(self.buttonHidden!)
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .leading, relatedBy: .equal, toItem: self.view!, attribute: .leading, multiplier: 1, constant: offset))
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .top, relatedBy: .equal, toItem: self.view!, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .trailing, relatedBy: .equal, toItem: self.view!, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .bottom, relatedBy: .equal, toItem: self.view!, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.layoutIfNeeded()
    }
    
    func drawMenu() {
        let offset: CGFloat = self.view.frame.width * 0.75
        if menu != nil {
            buttonHidden!.removeFromSuperview()
            self.view.layoutIfNeeded()
            layoutConstraintMenu!.constant = -offset
            layoutConstraintViewContentLeading!.constant = 0
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.view.layoutIfNeeded()
                self.resetTopAndBottomBar()
            }, completion: {(finished: Bool) -> Void in
                self.menu?.removeFromSuperview()
                self.menu = nil
            })
        } else {
            self.prepareMenu(offset: offset)
            layoutConstraintMenu!.constant = 0
            layoutConstraintViewContentLeading!.constant = offset
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.view.layoutIfNeeded()
                self.navigationController!.navigationBar.frame = CGRect(x: offset, y: 20, width: self.navigationController!.navigationBar.frame.width, height: self.navigationController!.navigationBar.frame.height)
                self.navigationController!.tabBarController?.tabBar.frame = CGRect(x: offset, y: self.navigationController!.tabBarController!.tabBar.frame.origin.y, width: self.navigationController!.tabBarController!.tabBar.frame.width, height: self.navigationController!.tabBarController!.tabBar.frame.height)
            }, completion: {(finished: Bool) -> Void in
                self.drawHiddenButton(offset: offset)
            })
        }
    }
    
    func loads(_ animated: Bool) {
        if loading == false {
            loading = true
            if animated == true {
                CXProgressHUD.sharedInstance.show()
            }
            CXHTTPManager.sharedInstance.inProgressWithCompletion { (response, error) -> Void in
                self.loading = false
                if error == nil {
                    self.arrayLoads = response as? Array
                    self.updateContent()
                } else {
                    CXErrorHandler.sharedInstance.handleError(error: error!)
                }
                CXProgressHUD.sharedInstance.hide()
            }
        }
    }
    
    fileprivate func login(_ showInitial: Bool) {
        if showInitial == true {
            CXProgressHUD.sharedInstance.showInitial()
        } else {
            CXProgressHUD.sharedInstance.show()
        }
        CXHTTPManager.sharedInstance.managerLoginWithCompletion { (response, error) -> Void in
            if error == nil {
                CXUser.sharedInstance.setUserData(userData: response as! Dictionary<String, AnyObject>)
                
                self.loading = false
                self.loads(false)
            } else {
                CXProgressHUD.sharedInstance.hide()
                CXErrorHandler.sharedInstance.handleError(error: error!)
            }
        }
    }
    
    fileprivate func logout() {
        CXProgressHUD.sharedInstance.show()
        CXHTTPManager.sharedInstance.logoutWithCompletion { (response, error) -> Void in
            if error == nil {
                self.navigationController!.tabBarController?.navigationController!.popToRootViewController(animated: true)
                self.resetTopAndBottomBar()
            } else {
                CXErrorHandler.sharedInstance.handleError(error: error!)
            }
            CXProgressHUD.sharedInstance.hide()
        }
    }
    
    private func prepareContent() {
        labelMessage!.numberOfLines = 0
        labelMessage!.text = CXNoLoads
        labelMessage!.textColor = CXColorBlack
    }
    
    private func prepareMenu(offset: CGFloat) {
        menu = CXMenu(frame: CGRect.zero)
        menu!.buttonLive!.addTarget(self, action: #selector(CXHomeViewController.actionLive), for: .touchUpInside)
        menu!.buttonHome!.addTarget(self, action: #selector(CXHomeViewController.actionHome), for: .touchUpInside)
        menu!.buttonProfile!.addTarget(self, action: #selector(CXHomeViewController.actionProfile), for: .touchUpInside)
        menu!.buttonSupport!.addTarget(self, action: #selector(CXHomeViewController.actionSupport), for: .touchUpInside)
        menu!.buttonTermsConditions!.addTarget(self, action: #selector(CXHomeViewController.actionTermsConditions), for: .touchUpInside)
        menu!.buttonLogOut!.addTarget(self, action: #selector(CXHomeViewController.actionLogOut), for: .touchUpInside)
        
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(CXHomeViewController.actionSwipe(_:)))
        swipeGestureRecognizer.direction = .left
        view.addGestureRecognizer(swipeGestureRecognizer)
        
        self.view.addSubview(menu!)
        layoutConstraintMenu = NSLayoutConstraint(item: menu!, attribute: .leading, relatedBy: .equal, toItem: self.view!, attribute: .leading, multiplier: 1, constant: -offset)
        self.view.addConstraint(layoutConstraintMenu!)
        self.view.addConstraint(NSLayoutConstraint(item: menu!, attribute: .top, relatedBy: .equal, toItem: self.view!, attribute: .top, multiplier: 1, constant: -64))
        self.view.addConstraint(NSLayoutConstraint(item: menu!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: offset))
        self.view.addConstraint(NSLayoutConstraint(item: menu!, attribute: .bottom, relatedBy: .equal, toItem: self.view!, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.layoutIfNeeded()
    }
    
    private func resetTopAndBottomBar() {
        self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 20, width: self.navigationController!.navigationBar.frame.width, height: self.navigationController!.navigationBar.frame.height)
        self.navigationController!.tabBarController?.tabBar.frame = CGRect(x: 0, y: self.navigationController!.tabBarController!.tabBar.frame.origin.y, width: self.navigationController!.tabBarController!.tabBar.frame.width, height: self.navigationController!.tabBarController!.tabBar.frame.height)
    }
    
    fileprivate func setOnTimeCell(_ cell: CXLoadCollectionViewCell) {
        cell.slider?.minimumTrackTintColor = CXColorGreen
        cell.slider?.minimumValueImage = UIImage(named: "green_dot")
        cell.slider?.setThumbImage(UIImage(named: "Green_Car"), for: .normal)
        cell.labelDelta?.text = "on time"
    }
    
    fileprivate func setBehindETACell(_ cell: CXLoadCollectionViewCell) {
        cell.slider?.minimumTrackTintColor = CXColorYellow
        cell.slider?.minimumValueImage = UIImage(named: "yellow_dot")
        cell.slider?.setThumbImage(UIImage(named: "Yellow_Car"), for: .normal)
    }
    
    fileprivate func setLateCell(_ cell: CXLoadCollectionViewCell) {
        cell.slider?.minimumTrackTintColor = CXColorRed
        cell.slider?.minimumValueImage = UIImage(named: "red_dot")
        cell.slider?.setThumbImage(UIImage(named: "Red_Car"), for: .normal)
    }
    
    fileprivate func setNotTrackedCell(_ cell: CXLoadCollectionViewCell) {
        cell.slider?.minimumTrackTintColor = CXColorGray
        cell.slider?.minimumValueImage = UIImage(named: "Gray_dot")
        cell.slider?.setThumbImage(UIImage(named: "Gray_Car"), for: .normal)
        cell.labelDelta?.text = "N/A"
        cell.labelETA?.text = "ETA: N/A"
    }
    
    fileprivate func setCompletedCell(_ cell: CXLoadCollectionViewCell) {
        cell.slider?.minimumTrackTintColor = CXColorGray
        cell.slider?.minimumValueImage = UIImage(named: "Gray_dot")
        cell.slider?.value = 0
        cell.slider?.setThumbImage(UIImage(named: "Gray_Car"), for: .normal)
        cell.labelETA?.text = ""
    }
    
    private func sortLoads() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = CXDateFormat
        
        arrayLoadsFiltered?.sort() { (a, b) -> Bool in
            let dictA = a as! Dictionary<String, AnyObject>
            let dictB = b as! Dictionary<String, AnyObject>
            
            if let status = dictA[CXKeyStatus] as? String, status == "COMPLETED" {
                return false
            }
            if let status = dictB[CXKeyStatus] as? String, status == "COMPLETED" {
                return true
            }
            
            var dateA: Date?
            var dateB: Date?
            if dictA[CXKeyETA] != nil {
                let eta = (dictA[CXKeyETA] as! NSDictionary)[CXKeyETA] as! String
                dateA = dateFormat.date(from: eta)
            }
            if dictB[CXKeyETA] != nil {
                let eta = (dictB[CXKeyETA] as! NSDictionary)[CXKeyETA] as! String
                dateB = dateFormat.date(from: eta)
            }
            if dateA == nil {
                return false
            }
            if dateB == nil {
                return true
            }
            return dateA?.compare(dateB! as Date) == .orderedAscending
        }
    }
    
    private func timerEnable() {
        if timer != nil {
            self.timerDisable()
        }
        var refreshInterval = CXDefaultRefreshInterval
        if CXUser.sharedInstance.userData(key: CXRefreshInterval) != nil {
            refreshInterval = CXUser.sharedInstance.userData(key: CXRefreshInterval) as! TimeInterval
        }
        timer = Timer.scheduledTimer(timeInterval: refreshInterval * 60, target: self, selector: #selector(CXHomeViewController.loads(_:)), userInfo: nil, repeats: true)
    }
    
    private func timerDisable() {
        timer?.invalidate()
        timer = nil
    }
    
    fileprivate func updateContent() {
        buttonFilterBlue!.imageViewDone.isHidden = true
        buttonFilterBlue!.titleLabel?.font = UIFont(name: "SFUIDisplay-Light", size: 32)
        buttonFilterGray!.imageViewDone.isHidden = true
        buttonFilterGray!.titleLabel?.font = UIFont(name: "SFUIDisplay-Light", size: 32)
        buttonFilterGreen!.imageViewDone.isHidden = true
        buttonFilterGreen!.titleLabel?.font = UIFont(name: "SFUIDisplay-Light", size: 32)
        buttonFilterRed!.imageViewDone.isHidden = true
        buttonFilterRed!.titleLabel?.font = UIFont(name: "SFUIDisplay-Light", size: 32)
        buttonFilterYellow!.imageViewDone.isHidden = true
        buttonFilterYellow!.titleLabel?.font = UIFont(name: "SFUIDisplay-Light", size: 32)
        labelFilterBlue!.font = UIFont(name: "SFUIDisplay-Regular", size: 12)
        labelFilterGray!.font = UIFont(name: "SFUIDisplay-Regular", size: 12)
        labelFilterGreen!.font = UIFont(name: "SFUIDisplay-Regular", size: 12)
        labelFilterRed!.font = UIFont(name: "SFUIDisplay-Regular", size: 12)
        labelFilterYellow!.font = UIFont(name: "SFUIDisplay-Regular", size: 12)
        switch filter! {
        case .Total:
            buttonFilterBlue!.imageViewDone.isHidden = false
            buttonFilterBlue!.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 32)
            labelFilterBlue!.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        case .OnTime:
            buttonFilterGreen!.imageViewDone.isHidden = false
            buttonFilterGreen!.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 32)
            labelFilterGreen!.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        case .BehindETA:
            buttonFilterYellow!.imageViewDone.isHidden = false
            buttonFilterYellow!.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 32)
            labelFilterYellow!.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        case .Late:
            buttonFilterRed!.imageViewDone.isHidden = false
            buttonFilterRed!.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 32)
            labelFilterRed!.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        case .NotTracked:
            buttonFilterGray!.imageViewDone.isHidden = false
            buttonFilterGray!.titleLabel?.font = UIFont(name: "SFUIDisplay-Medium", size: 32)
            labelFilterGray!.font = UIFont(name: "SFUIDisplay-Medium", size: 12)
        }
        
        self.updateLoads()
    }
    
    private func updateLoads() {
        arrayLoadsFiltered?.removeAll()
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = CXDateFormat
        
        var behindTime = CXDefaultBehindTime
        if CXUser.sharedInstance.userData(key: CXBehindTime) != nil {
            behindTime = CXUser.sharedInstance.userData(key: CXBehindTime) as! TimeInterval
        }
        
        var indexBlue = 0
        var indexGray = 0
        var indexGreen = 0
        var indexRed = 0
        var indexYellow = 0
        
        for load in arrayLoads! {
            indexBlue += 1
            
            if let status = load[CXKeyStatus] as? String, status == "COMPLETED" {
                indexGray += 1
                if filter == .NotTracked {
                    arrayLoadsFiltered?.append(load)
                }
                
                continue
            }
            
            if load[CXKeyETA] as? Dictionary<String, AnyObject> != nil {
                let deliverBy = load[CXKeyDeliverBy] as! String
                let dateDeliverBy = dateFormat.date(from: deliverBy)
                let eta = (load[CXKeyETA] as! NSDictionary)[CXKeyETA] as! String
                let dateETA = dateFormat.date(from: eta)
                if eta == deliverBy {
                    indexGreen += 1
                    if filter == .OnTime {
                        arrayLoadsFiltered?.append(load)
                    }
                } else {
                    if dateETA?.compare(dateDeliverBy!) == .orderedAscending {
                        indexGreen += 1
                        if filter == .OnTime {
                            arrayLoadsFiltered?.append(load)
                        }
                    } else {
                        if dateETA?.compare((dateDeliverBy?.addingTimeInterval(behindTime * 60))!) == .orderedAscending {
                            indexYellow += 1
                            if filter == .BehindETA {
                                arrayLoadsFiltered?.append(load)
                            }
                        } else {
                            indexRed += 1
                            if filter == .Late {
                                arrayLoadsFiltered?.append(load)
                            }
                        }
                    }
                }
            } else {
                indexGray += 1
                if filter == .NotTracked {
                    arrayLoadsFiltered?.append(load)
                }
            }
        }
        if filter == .Total {
            arrayLoadsFiltered = arrayLoads
        }
        
        if arrayLoadsFiltered?.count == 0 {
            if arrayLoads?.count == 0 {
                labelMessage?.text = CXNoLoads
            } else {
                labelMessage?.text = CXNoFilterLoads
            }
            labelMessage?.isHidden = false
        } else {
            labelMessage?.isHidden = true
        }
        
        buttonFilterBlue?.setTitle(String(indexBlue), for: .normal)
        buttonFilterGray?.setTitle(String(indexGray), for: .normal)
        buttonFilterGreen?.setTitle(String(indexGreen), for: .normal)
        buttonFilterRed?.setTitle(String(indexRed), for: .normal)
        buttonFilterYellow?.setTitle(String(indexYellow), for: .normal)
        
        self.sortLoads()
        
        collection?.reloadData()
    }
}

extension CXHomeViewController {
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.logout()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayLoadsFiltered!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CXReuseIdentifierCell, for: indexPath as IndexPath) as! CXLoadCollectionViewCell
        cell.slider?.value = 0
        
        var behindTime = CXDefaultBehindTime
        if CXUser.sharedInstance.userData(key: CXBehindTime) != nil {
            behindTime = CXUser.sharedInstance.userData(key: CXBehindTime) as! TimeInterval
        }
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = CXDateFormat
        
        let load = arrayLoadsFiltered![indexPath.item] as! Dictionary<String, AnyObject>
        let deliverBy = load[CXKeyDeliverBy] as! String
        let dateDeliverBy = dateFormat.date(from: deliverBy)
        let distance = load[CXKeyDistance] as! Float
        if load[CXKeyETA] != nil {
            let distanceInMiles = (load[CXKeyETA] as! NSDictionary)[CXKeyDistanceInMiles] as! Float
            if distanceInMiles <= distance {
                let progress = 1 - distanceInMiles / distance
                cell.slider?.value = progress
            }
            
            if var eta = (load[CXKeyETA] as! NSDictionary)[CXKeyETA] as? String {
                var dateETA = dateFormat.date(from: eta)
                if eta == deliverBy {
                    self.setOnTimeCell(cell)
                } else {
                    let unitFlags = Set<Calendar.Component>([.minute])
                    let x = Calendar.current.dateComponents(unitFlags, from: dateETA!, to: dateDeliverBy!).minute
                    if dateETA?.compare(dateDeliverBy!) == .orderedAscending {
                        self.setOnTimeCell(cell)
                        cell.labelDelta?.text = "\(String(describing: x)) min ahead of schedule"
                    } else {
                        if dateETA?.compare((dateDeliverBy?.addingTimeInterval(behindTime * 60))!) == .orderedAscending {
                            self.setBehindETACell(cell)
                            cell.labelDelta?.text = "\(String(describing: x)) min behind schedule"
                        } else {
                            self.setLateCell(cell)
                            cell.labelDelta?.text = "\(String(describing: x)) min behind schedule"
                        }
                    }
                }
                let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: eta)!)
                dateETA = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: eta)!)
                eta = dateFormat.string(from: dateETA!)
                if eta.isToday() {
                    dateFormat.dateFormat = CXDateFormatTime
                    cell.labelETA?.text = "ETA: \(dateFormat.string(from: dateETA!))"
                } else {
                    dateFormat.dateFormat = CXDateFormatNotToday
                    cell.labelETA?.text = "ETA: \(dateFormat.string(from: dateETA!))"
                }
            }
        } else {
            self.setNotTrackedCell(cell)
        }
        
        if let status = load[CXKeyStatus] as? String, status == "COMPLETED" {
            self.setCompletedCell(cell)
            if var statusLastChanged = load[CXKeyStatusLastChanged] as? String {
                dateFormat.dateFormat = CXDateFormat
                let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: statusLastChanged)!)
                let dateStatusLastChanged = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: statusLastChanged)!)
                statusLastChanged = dateFormat.string(from: dateStatusLastChanged)
                if statusLastChanged.isToday() {
                    dateFormat.dateFormat = CXDateFormatTime
                    cell.labelDelta?.text = "DELIVERED: \(dateFormat.string(from: dateStatusLastChanged))"
                } else {
                    dateFormat.dateFormat = CXDateFormatNotToday
                    cell.labelDelta?.text = "DELIVERED: \(dateFormat.string(from: dateStatusLastChanged))"
                }
            }
        }
        
        cell.labelFromTo?.text = self.addressWithDistance(distance, load: load)
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - Layout.collectionCellOffset * 2, height: Layout.collectionCellHeight)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        let load = arrayLoadsFiltered![indexPath.item] as! Dictionary<String, AnyObject>
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXBookingDetails") as! CXBookingDetailsViewController
        viewController.completed = false
        if let status = load[CXKeyStatus] as? String, status == "COMPLETED" {
            viewController.completed = true
        }
        viewController.load = load
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y <= -Layout.collectionCellOffset * 5 {
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.loads(true)
        }
    }
}

extension CXHomeViewController {
    func actionFilterBlue() {
        filter = .Total
        self.updateContent()
    }
    
    func actionFilterGray() {
        filter = .NotTracked
        self.updateContent()
    }
    
    func actionFilterGreen() {
        filter = .OnTime
        self.updateContent()
    }
    
    func actionFilterRed() {
        filter = .Late
        self.updateContent()
    }
    
    func actionFilterYellow() {
        filter = .BehindETA
        self.updateContent()
    }
    
    func actionHome() {
        self.actionMenu()
    }
    
    func actionLive() {
        self.actionMenu()
        
        self.navigationController!.tabBarController?.selectedIndex = CXTabLive
    }
    
    func actionLogOut() {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Log Out", message: "Are you sure you want to log out?", preferredStyle: .alert)
            var action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.logout()
            }
            alertController.addAction(action)
            action = UIAlertAction(title: "Cancel", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: "Log Out", message: "Are you sure you want to log out?", delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "Cancel").show()
        }
    }
    
    func actionMenu() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({
            //code
        })
        return
        
        let navigationController = self.navigationController!.tabBarController?.viewControllers![CXTabLive] as! UINavigationController
        let viewController = navigationController.viewControllers.first as! CXLiveViewController
        viewController.drawMenu()
        
        self.drawMenu()
    }
    
    func actionMenuHidden() {
        self.actionMenu()
    }
    
    func actionProfile() {
        self.actionMenu()
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXProfile") as! CXProfileViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    func actionSupport() {
        self.actionMenu()
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXSupport") as! CXSupportViewController
        let navigationController = UINavigationController(rootViewController: viewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func actionSwipe(_ gestureRecognizer: UISwipeGestureRecognizer) {
        self.actionMenu()
    }
    
    func actionTermsConditions() {
        UIApplication.shared.openURL(URL(string: CXTermsConditions)!)
        
        self.actionMenu()
    }
    
    func actionUpdate() {
        self.loads(true)
    }
    
    func applicationDidBecomeActive(_ notification: Notification) {
        print("applicationDidBecomeActive")
        self.login(false)
    }
}

extension CXHomeViewController {
    fileprivate func buttonFilter() -> CXButton {
        let buttonFilter = CXButton(type: .custom)
        buttonFilter.frame = CGRect.zero
        buttonFilter.clipsToBounds = false
        buttonFilter.layer.cornerRadius = 4
        buttonFilter.titleLabel?.font = UIFont(name: "SFUIDisplay-Light", size: 32)
        buttonFilter.translatesAutoresizingMaskIntoConstraints = false
        buttonFilter.setTitleColor(CXColorBlack, for: .normal)
        return buttonFilter
    }
    
    fileprivate func labelFilter() -> UILabel {
        let labelFilter = UILabel()
        labelFilter.backgroundColor = UIColor.clear
        labelFilter.font = UIFont(name: "SFUIDisplay-Regular", size: 12)
        labelFilter.frame = CGRect.zero
        labelFilter.numberOfLines = 2
        labelFilter.textAlignment = .center
        labelFilter.textColor = CXColorBlack
        labelFilter.translatesAutoresizingMaskIntoConstraints = false
        return labelFilter
    }
}
