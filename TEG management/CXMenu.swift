import UIKit

class CXMenu: UIView {
   @IBOutlet weak var viewContent: UIView?
   @IBOutlet weak var buttonLive: UIButton?
   @IBOutlet weak var buttonHome: UIButton?
   @IBOutlet weak var buttonProfile: UIButton?
   @IBOutlet weak var buttonSupport: UIButton?
   @IBOutlet weak var buttonTermsConditions: UIButton?
   @IBOutlet weak var buttonLogOut: UIButton?
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      loadViewFromNib()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      loadViewFromNib()
   }
   
   private func loadViewFromNib() {
      let bundle = Bundle(for: type(of: self))
      let nib = UINib(nibName: "CXMenu", bundle: bundle)
      let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
      view.frame = CGRect.zero
      self.addSubview(view)
    
      self.backgroundColor = CXColorGray
      self.translatesAutoresizingMaskIntoConstraints = false
      
      self.buttonLive?.setTitleColor(CXColorBlack, for: .normal)
      self.buttonHome?.setTitleColor(CXColorBlack, for: .normal)
      self.buttonProfile?.setTitleColor(CXColorBlack, for: .normal)
      self.buttonSupport?.setTitleColor(CXColorBlack, for: .normal)
      self.buttonTermsConditions?.setTitleColor(CXColorBlack, for: .normal)
      self.buttonLogOut?.setTitleColor(CXColorLogOut, for: .normal)
   }
}
