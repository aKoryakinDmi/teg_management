import UIKit

class CXSupportViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
   struct Layout {
      static let commentOffset: CGFloat = 20
      static let labelWidth: CGFloat = 128
      static let labelHeight: CGFloat = 20
      static let labelOffset: CGFloat = 7
      static let labelTop: CGFloat = 13
   }
   
   @IBOutlet weak var viewContent: UIView?
   @IBOutlet weak var textFieldSubject: UITextField?
   @IBOutlet weak var textViewComment: UITextView?
   @IBOutlet weak var layoutConstraintTextViewCommentBottom: NSLayoutConstraint?
   var labelSubject: UILabel?
   var labelComment: UILabel?
   weak var layoutConstraintSubjectTop: NSLayoutConstraint?
   weak var layoutConstraintCommentTop: NSLayoutConstraint?
   
   deinit {
      NotificationCenter.default.removeObserver(self)
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.navigationItem.hidesBackButton = true
      self.title = "Support"
      self.navigationController?.navigationBar.barTintColor = CXColorMainTheme
      self.navigationController?.navigationBar.isHidden = false
      self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "SFUIDisplay-Regular", size: 19)!, NSForegroundColorAttributeName: CXColorWhite]
      self.navigationController?.navigationBar.isTranslucent = false
      
      let buttonBack = UIButton(type: .custom)
      buttonBack.setImage(UIImage(named: "CloseWhite"), for: .normal)
      buttonBack.frame = CGRect(x :0, y: 0, width: 16, height: 16)
      buttonBack.addTarget(self, action: #selector(CXSupportViewController.actionBack), for: .touchUpInside)
      
      let leftBarButtonItem = UIBarButtonItem()
      leftBarButtonItem.customView = buttonBack
      self.navigationItem.leftBarButtonItem = leftBarButtonItem
      
      let buttonSend = UIButton(type: .custom)
    buttonSend.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
      buttonSend.titleLabel?.font = UIFont(name: "SFUIDisplay-Regular", size: 15)
      buttonSend.setTitle("Send", for: .normal)
      buttonSend.setTitleColor(CXColorWhite, for: .normal)
      buttonSend.addTarget(self, action: #selector(CXSupportViewController.actionSend), for: .touchUpInside)
      
      let rightBarButtonItem = UIBarButtonItem()
      rightBarButtonItem.customView = buttonSend
      self.navigationItem.rightBarButtonItem = rightBarButtonItem
      
      self.prepareContent()
      
      let gesture = UITapGestureRecognizer(target: self, action: #selector(CXSupportViewController.actionTap(_:)))
      self.view.addGestureRecognizer(gesture)
      
      NotificationCenter.default.addObserver(self, selector: #selector(CXSupportViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(CXSupportViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      textFieldSubject!.becomeFirstResponder()
   }
   
   fileprivate func checkTextFieldsEmpty() -> Bool {
      guard let subject = textFieldSubject?.text, !subject.isEmpty else {
         self.showAlertWithTitle(title: "Error", message: "Enter subject.")
         
         return false
      }
      guard let comment = textViewComment?.text, comment != CXComments else {
         self.showAlertWithTitle(title: "Error", message: "Enter comments.")
         
         return false
      }
      
      return true
   }
   
   fileprivate func drawLabelSubject() {
      guard let subject = textFieldSubject!.text, !subject.isEmpty else {
         labelSubject?.removeFromSuperview()
         labelSubject = nil
         
         return
      }
      if labelSubject == nil {
         labelSubject = self.labelPlaceholder()
         labelSubject?.text = CXSubject
         self.viewContent!.addSubview(labelSubject!)
         self.view.addConstraint(NSLayoutConstraint(item: labelSubject!, attribute: .leading, relatedBy: .equal, toItem: viewContent!, attribute: .leading, multiplier: 1, constant: 0))
         layoutConstraintSubjectTop = NSLayoutConstraint(item: labelSubject!, attribute: .top, relatedBy: .equal, toItem: textFieldSubject!, attribute: .top, multiplier: 1, constant: -Layout.labelTop)
         self.view.addConstraint(layoutConstraintSubjectTop!)
         self.view.addConstraint(NSLayoutConstraint(item: labelSubject!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelWidth))
         self.view.addConstraint(NSLayoutConstraint(item: labelSubject!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
         self.view.layoutIfNeeded()
         layoutConstraintSubjectTop!.constant -= Layout.labelOffset
         UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
            self.labelSubject?.alpha = 1
         })
      }
   }
   
   fileprivate func drawLabelComment() {
      guard let comment = textViewComment!.text, !comment.isEmpty else {
         labelComment?.removeFromSuperview()
         labelComment = nil
         
         return
      }
      if labelComment == nil {
         labelComment = self.labelPlaceholder()
         labelComment?.text = CXComments
         self.viewContent!.addSubview(labelComment!)
         self.view.addConstraint(NSLayoutConstraint(item: labelComment!, attribute: .leading, relatedBy: .equal, toItem: viewContent!, attribute: .leading, multiplier: 1, constant: 0))
         layoutConstraintCommentTop = NSLayoutConstraint(item: labelComment!, attribute: .top, relatedBy: .equal, toItem: textViewComment!, attribute: .top, multiplier: 1, constant: -Layout.labelTop)
         self.view.addConstraint(layoutConstraintCommentTop!)
         self.view.addConstraint(NSLayoutConstraint(item: labelComment!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelWidth))
         self.view.addConstraint(NSLayoutConstraint(item: labelComment!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
         self.view.layoutIfNeeded()
         layoutConstraintCommentTop!.constant -= Layout.labelOffset
         UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
            self.labelComment?.alpha = 1
         })
      }
   }
   
   fileprivate func hideKeyboard() {
      textFieldSubject!.resignFirstResponder()
      textViewComment!.resignFirstResponder()
   }
   
   fileprivate func labelPlaceholder() -> UILabel {
      let labelPlaceholder = UILabel(frame: CGRect.zero)
      labelPlaceholder.alpha = 0
      labelPlaceholder.font = UIFont(name: "SFUIDisplay-Regular", size: 15)
      labelPlaceholder.textAlignment = .left
      labelPlaceholder.textColor = CXColorBorder
      labelPlaceholder.translatesAutoresizingMaskIntoConstraints = false
      return labelPlaceholder
   }
   
   private func prepareContent() {
      textFieldSubject!.layer.borderColor = CXColorBorder.cgColor
      textFieldSubject!.layer.borderWidth = 1
      textFieldSubject!.layer.cornerRadius = 3
      textFieldSubject!.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFieldSubject!.frame.height))
      textFieldSubject!.leftViewMode = UITextFieldViewMode.always
      textFieldSubject!.textColor = CXColorBorder
      textFieldSubject!.addTarget(self, action: #selector(CXSupportViewController.textFieldDidChange(_:)), for: .editingChanged)
      
      textViewComment!.layer.borderColor = CXColorBorder.cgColor
      textViewComment!.layer.borderWidth = 1
      textViewComment!.layer.cornerRadius = 3
      textViewComment!.text = CXComments
      textViewComment!.textColor = CXColorPlaceholder
      textViewComment!.textContainer.lineFragmentPadding = 15
   }
   
   fileprivate func sendSupportEmail() {
      let subject = textFieldSubject?.text
      let comment = textViewComment?.text
      CXProgressHUD.sharedInstance.show()
      CXHTTPManager.sharedInstance.sendSupportEmail(subject: subject!, comment: comment!) { (response, error) -> Void in
         if error == nil {
            self.actionBack()
         } else {
            CXErrorHandler.sharedInstance.handleError(error: error!)
         }
         CXProgressHUD.sharedInstance.hide()
      }
   }
   
   private func showAlertWithTitle(title: String?, message: String?) {
      if #available(iOS 8.0, *) {
         let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
         let action = UIAlertAction(title: "OK", style: .default) { (action) in
            
         }
         alertController.addAction(action)
         self.present(alertController, animated: true, completion: nil)
      } else {
         UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK").show()
      }
   }
}

extension CXSupportViewController {
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      
      return true
   }
   
   func textViewDidBeginEditing(_ textView: UITextView) {
      if textView.text == CXComments {
         textView.text = ""
         textView.textColor = CXColorBorder
      }
   }
   
   func textViewDidEndEditing(_ textView: UITextView) {
      if textView.text.isEmpty {
         textView.text = CXComments
         textView.textColor = CXColorPlaceholder
      }
   }
   
   func textViewDidChange(_ textView: UITextView) {
      self.drawLabelComment()
   }
}

extension CXSupportViewController {
   func actionBack() {
      self.dismiss(animated: true, completion: nil)
   }
   
   func actionSend() {
      if self.checkTextFieldsEmpty() {
         self.sendSupportEmail()
      }
   }
   
   func actionTap(_ gesture: UITapGestureRecognizer) {
      self.hideKeyboard()
   }
   
   func keyboardWillShow(_ notification: NSNotification) {
      if let keyboardHeight = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue {
         self.view.layoutIfNeeded()
         layoutConstraintTextViewCommentBottom?.constant = keyboardHeight.height + Layout.commentOffset
         UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
         })
      }
   }
   
   func keyboardWillHide(_ notification: NSNotification) {
      self.view.layoutIfNeeded()
      layoutConstraintTextViewCommentBottom?.constant = Layout.commentOffset
      UIView.animate(withDuration: 0.2, animations: { () -> Void in
         self.view.layoutIfNeeded()
      })
   }
   
   func textFieldDidChange(_ textField: UITextField) {
      self.drawLabelSubject()
   }
}
