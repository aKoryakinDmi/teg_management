import UIKit

class CXProfileViewController: UIViewController {
    struct Layout {
        static let viewBackgroundHeight: CGFloat = 190
        static let labelHeight: CGFloat = 20
        static let labelOffsetLeading: CGFloat = 17
        static let labelOffsetTop: CGFloat = 15
        static let labelOffsetBottom: CGFloat = 2
    }
    
    @IBOutlet weak var viewBackground: UIView?
    @IBOutlet weak var viewContent: UIView?
    @IBOutlet weak var labelAddress: UILabel?
    @IBOutlet weak var labelName: UILabel?
    @IBOutlet weak var imageAvatar: UIImageView?
    @IBOutlet weak var layoutConstraintViewBackgroundHeight: NSLayoutConstraint?
    var point: CGPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(CXProfileViewController.actionPan(gestureRecognizer:)))
        self.view.addGestureRecognizer(gesture)
        
        self.prepareContent()
        self.picture()
        self.drawContent()
    }
    
    private func drawContent() {
        var viewLast: UIView = viewBackground!
        if CXUser.sharedInstance.userData(key: CXKeyFirstName) != nil {
            let labelTitle = self.labelTitle()
            labelTitle.text = "Name"
            viewContent!.addSubview(labelTitle)
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .top, relatedBy: .equal, toItem: viewLast, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetTop))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            
            let labelValue = self.labelValue()
            labelValue.text = "\(CXUser.sharedInstance.userData(key: CXKeyFirstName)!) \(CXUser.sharedInstance.userData(key: CXKeyLastName)!)"
            viewContent!.addSubview(labelValue)
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .top, relatedBy: .equal, toItem: labelTitle, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetBottom))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            
            viewLast = labelValue
        }
        if CXUser.sharedInstance.userData(key: CXKeyEmail) != nil {
            let labelTitle = self.labelTitle()
            labelTitle.text = "Email"
            viewContent!.addSubview(labelTitle)
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .top, relatedBy: .equal, toItem: viewLast, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetTop))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            
            let labelValue = self.labelValue()
            labelValue.text = "\(CXUser.sharedInstance.userData(key: CXKeyEmail)!)"
            viewContent!.addSubview(labelValue)
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .top, relatedBy: .equal, toItem: labelTitle, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetBottom))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            
            viewLast = labelValue
        }
        if CXUser.sharedInstance.userData(key: CXKeyPhone1) != nil {
            let labelTitle = self.labelTitle()
            labelTitle.text = "Phone"
            viewContent!.addSubview(labelTitle)
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .top, relatedBy: .equal, toItem: viewLast, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetTop))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            
            let labelValue = self.labelValue()
            labelValue.text = "+\(CXUser.sharedInstance.userData(key: CXKeyPhone1)!)"
            viewContent!.addSubview(labelValue)
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .top, relatedBy: .equal, toItem: labelTitle, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetBottom))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            
            viewLast = labelValue
        }
        if CXUser.sharedInstance.userData(key: CXKeyName) != nil {
            let labelTitle = self.labelTitle()
            labelTitle.text = "Company Name"
            viewContent!.addSubview(labelTitle)
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .top, relatedBy: .equal, toItem: viewLast, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetTop))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelTitle, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            
            let labelValue = self.labelValue()
            viewContent!.addSubview(labelValue)
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .leading, relatedBy: .equal, toItem: viewContent, attribute: .leading, multiplier: 1, constant: Layout.labelOffsetLeading))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .top, relatedBy: .equal, toItem: labelTitle, attribute: .bottom, multiplier: 1, constant: Layout.labelOffsetBottom))
            self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .trailing, relatedBy: .equal, toItem: viewContent, attribute: .trailing, multiplier: 1, constant: -Layout.labelOffsetLeading))
            var content = "\(CXUser.sharedInstance.userData(key: CXKeyName)!) (\(CXUser.sharedInstance.userData(key: CXKeyCountryCode2)!) \(CXUser.sharedInstance.userData(key: CXKeyCompanyId)!))"
            let size = CGSize(width: UIScreen.main.bounds.width - Layout.labelOffsetLeading * 2, height: CGFloat.greatestFiniteMagnitude)
            var rect = (content as NSString).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: labelValue.font], context: nil)
            if rect.height > Layout.labelHeight {
                content = "\(CXUser.sharedInstance.userData(key: CXKeyName)!)\n(\(CXUser.sharedInstance.userData(key: CXKeyCountryCode2)!) \(CXUser.sharedInstance.userData(key: CXKeyCompanyId)!))"
                rect = (content as NSString).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: labelValue.font], context: nil)
                labelValue.numberOfLines = 0
                self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: rect.height))
            } else {
                self.view.addConstraint(NSLayoutConstraint(item: labelValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            }
            labelValue.text = content
            
            viewLast = labelValue
        }
    }
    
    private func labelTitle() -> UILabel {
        let labelFilter = UILabel()
        labelFilter.backgroundColor = UIColor.clear
        labelFilter.font = UIFont(name: "SFUIDisplay-Regular", size: 15)
        labelFilter.frame = CGRect.zero
        labelFilter.textAlignment = .left
        labelFilter.textColor = CXColorSilver
        labelFilter.translatesAutoresizingMaskIntoConstraints = false
        return labelFilter
    }
    
    private func labelValue() -> UILabel {
        let labelFilter = UILabel()
        labelFilter.backgroundColor = UIColor.clear
        labelFilter.font = UIFont(name: "SFUIDisplay-Regular", size: 19)
        labelFilter.frame = CGRect.zero
        labelFilter.textAlignment = .left
        labelFilter.textColor = CXColorBlack
        labelFilter.translatesAutoresizingMaskIntoConstraints = false
        return labelFilter
    }
    
    private func picture() {
        CXHTTPManager.sharedInstance.pictureWithCompletion { (response, error) -> Void in
            if error == nil {
                let folder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
                self.imageAvatar?.image = UIImage(contentsOfFile: folder.appendingPathComponent((response! as AnyObject).lastPathComponent))
            }
        }
    }
    
    private func prepareContent() {
        let country = CXUser.sharedInstance.userData(key: CXKeyCountry) as! String
        let town = CXUser.sharedInstance.userData(key: CXKeyTown) as! String
        let textAttachment = NSTextAttachment()
        textAttachment.image = UIImage(named: "location")
        let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(attachment: textAttachment))
        attributedString.append(NSAttributedString(string: " \(country), \(town)"))
        labelAddress?.attributedText = attributedString
        labelAddress?.textColor = CXColorSilver
        labelName?.text = "\(CXUser.sharedInstance.userData(key: CXKeyFirstName)!) \(CXUser.sharedInstance.userData(key: CXKeyLastName)!)"
        imageAvatar?.layer.cornerRadius = 27
    }
}

extension CXProfileViewController {
    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func actionPan(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .changed {
            let translation = gestureRecognizer.translation(in: gestureRecognizer.view)
            layoutConstraintViewBackgroundHeight?.constant = Layout.viewBackgroundHeight + translation.y
            let minHeight = Layout.viewBackgroundHeight * 0.84
            if (layoutConstraintViewBackgroundHeight?.constant)! < minHeight {
                layoutConstraintViewBackgroundHeight?.constant = minHeight
            }
            self.view.layoutIfNeeded()
        } else {
            if gestureRecognizer.state == .ended {
                layoutConstraintViewBackgroundHeight?.constant = Layout.viewBackgroundHeight
                UIView.animate(withDuration: 0.3, animations: {() -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
}
