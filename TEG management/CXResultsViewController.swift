import MapKit

private let CXResultsFontDefault = UIFont(name: "SFUIDisplay-Light", size: 12)
private let CXResultsFontSelected = UIFont(name: "SFUIDisplay-Medium", size: 12)

class CXResultsViewController: UIViewController, FBClusteringManagerDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate {
    @IBOutlet weak var map: MKMapView?
    @IBOutlet weak var viewStatus: UIView?
    @IBOutlet weak var viewCriterias: UIView?
    @IBOutlet weak var viewClusterVehicles: UIView?
    @IBOutlet weak var table: UITableView?
    @IBOutlet weak var buttonStatusAvailable: UIButton?
    @IBOutlet weak var buttonStatusMaybe: UIButton?
    @IBOutlet weak var buttonStatusUnavailable: UIButton?
    @IBOutlet weak var buttonStatusUnknown: UIButton?
    @IBOutlet weak var buttonCriteriaAll: UIButton?
    @IBOutlet weak var buttonCriteriaDrivers: UIButton?
    @IBOutlet weak var buttonCriteriaPreferred: UIButton?
    @IBOutlet weak var buttonCriteriaOther: UIButton?
    @IBOutlet weak var labelClusterVehicles: UILabel?
    var clusteringManager: FBClusteringManager?
    var originalRegion: MKCoordinateRegion?
    var timer: Timer?
    var arrayVehicleFilters: [AnyObject] = []
    var arrayVehicles: [AnyObject] = []
    var arrayClusterVehicles: [AnyObject] = []
    var cachedImages: [String: UIImage] = [:]
    var params: Dictionary<String, String>?
    var coordinate: CLLocationCoordinate2D?
    var initial: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = CXColorBackground
        
        self.title = "Results"
        
        let buttonBack = UIButton(type: .custom)
        buttonBack.frame = CGRect(x: 0, y: 0, width: 10, height: 17)
        buttonBack.setImage(UIImage(named: "Back"), for: .normal)
        buttonBack.addTarget(self, action: #selector(CXBookingDetailsViewController.actionBack), for: .touchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem()
        leftBarButtonItem.customView = buttonBack
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        let buttonUpdate = UIButton(type: .custom)
        buttonUpdate.setImage(UIImage(named: "update"), for: .normal)
        buttonUpdate.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonUpdate.addTarget(self, action: #selector(CXHomeViewController.actionUpdate), for: .touchUpInside)
        
        let rightBarButtonItem = UIBarButtonItem()
        rightBarButtonItem.customView = buttonUpdate
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.prepareContent()
        self.drawContent()
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(CXResultsViewController.actionPinch(_:)))
        pinchGesture.delegate = self
        pinchGesture.delaysTouchesBegan = true
        map?.addGestureRecognizer(pinchGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CXResultsViewController.actionTap(_:)))
        tapGesture.numberOfTapsRequired = 2
        map?.addGestureRecognizer(tapGesture)
        
        clusteringManager = FBClusteringManager(annotations: [])
        clusteringManager?.delegate = self
        
        self.setCoordinate()
        guard coordinate != nil else {
            return
        }
        
        map?.centerCoordinate = coordinate!
        self.drawRadius()
        self.checkNoResults()
        self.updateFilters()
        self.updateAnnotations()
        
        initial = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if initial == true {
            initial = false
            self.reset()
        }
        
        self.timerEnable()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.timerDisable()
    }
    
    fileprivate func cachedImageForText(text: String) -> UIImage {
        if let image = cachedImages[text] {
            return image
        }
        
        let image = self.generateImageWithText(text: text)
        cachedImages[text] = image
        
        return image
    }
    
    private func checkNoResults() {
        if arrayVehicles.count == 0 {
            self.showAlert()
        }
    }
    
    private func drawContent() {
        var separator = self.separator()
        buttonCriteriaAll?.addSubview(separator)
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .top, relatedBy: .equal, toItem: buttonCriteriaAll, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .trailing, relatedBy: .equal, toItem: buttonCriteriaAll, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .bottom, relatedBy: .equal, toItem: buttonCriteriaAll, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1))
        
        separator = self.separator()
        buttonCriteriaDrivers?.addSubview(separator)
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .top, relatedBy: .equal, toItem: buttonCriteriaDrivers, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .trailing, relatedBy: .equal, toItem: buttonCriteriaDrivers, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .bottom, relatedBy: .equal, toItem: buttonCriteriaDrivers, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1))
        
        if arrayVehicleFilters.count > 3 {
            separator = self.separator()
            buttonCriteriaPreferred?.addSubview(separator)
            self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .top, relatedBy: .equal, toItem: buttonCriteriaPreferred, attribute: .top, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .trailing, relatedBy: .equal, toItem: buttonCriteriaPreferred, attribute: .trailing, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .bottom, relatedBy: .equal, toItem: buttonCriteriaPreferred, attribute: .bottom, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1))
        } else {
            buttonCriteriaPreferred?.isHidden = true
            buttonCriteriaPreferred?.removeFromSuperview()
            
            self.view.addConstraint(NSLayoutConstraint(item: buttonCriteriaDrivers!, attribute: .trailing, relatedBy: .equal, toItem: buttonCriteriaOther!, attribute: .leading, multiplier: 1, constant: 0))
        }
        
        separator = self.separator()
        viewClusterVehicles?.addSubview(separator)
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .leading, relatedBy: .equal, toItem: viewClusterVehicles, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .top, relatedBy: .equal, toItem: viewClusterVehicles, attribute: .top, multiplier: 1, constant: 44))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .trailing, relatedBy: .equal, toItem: viewClusterVehicles, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: separator, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1))
    }
    
    private func drawRadius() {
        guard let radius = params![CXKeyRadius] else {
            return
        }
        map?.add(MKCircle(center: coordinate!, radius: CLLocationDistance(radius)!))
    }
    
    private func filteredLocations() -> Array<AnyObject> {
        var array: [AnyObject] = []
        for vehicle in arrayVehicles {
            guard let location = vehicle[CXKeyLocation] else {
                continue
            }
            let loc = location as! [String: Any]
            guard let status = loc[CXKeyStatus] as? String else {
                continue
            }
            
            if status == CXKeyAvailable && buttonStatusAvailable?.isSelected == true {
                array.append(vehicle)
            }
            if status == CXKeyMaybe && buttonStatusMaybe?.isSelected == true {
                array.append(vehicle)
            }
            if status == CXKeyUnavailable && buttonStatusUnavailable?.isSelected == true {
                array.append(vehicle)
            }
            if status == CXKeyUnknown && buttonStatusUnknown?.isSelected == true {
                array.append(vehicle)
            }
        }
        
        return array
    }
    
    private func generateImageWithText(text: String) -> UIImage {
        let image = UIImage(named: "blue_circle")!
        
        let imageView = UIImageView(image: image)
        imageView.backgroundColor = UIColor.clear
        imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        label.backgroundColor = UIColor.clear
        label.font = UIFont(name: "SFUIDisplay-Regular", size: 15)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.text = text
        
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageWithText = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imageWithText!
    }
    
    fileprivate func maxZoomLevel() -> Double {
        var maxZoomLevel = CXRestrictionZoomLevel
        if buttonCriteriaDrivers?.isSelected == true {
            maxZoomLevel = CXMaxZoomLevel
        }
        return maxZoomLevel
    }
    
    private func prepareContent() {
        viewStatus?.layer.cornerRadius = 3
        viewCriterias?.layer.cornerRadius = 3
        viewClusterVehicles?.layer.cornerRadius = 3
        buttonStatusAvailable?.isSelected = true
        buttonStatusMaybe?.isSelected = true
        buttonStatusUnavailable?.isSelected = true
        buttonStatusUnknown?.isSelected = true
        buttonCriteriaAll?.titleLabel?.font = CXResultsFontSelected
        buttonCriteriaDrivers?.titleLabel?.textAlignment = .center
        buttonCriteriaPreferred?.titleLabel?.textAlignment = .center
        buttonCriteriaOther?.titleLabel?.textAlignment = .center
    }
    
    fileprivate func reset() {
        guard coordinate != nil else {
            return
        }
        guard let radius = params![CXKeyRadius] else {
            return
        }
        
        let center = CLLocationCoordinate2DMake(coordinate!.latitude, coordinate!.longitude)
        let distance = CLLocationDistance(radius)! * 2.1
        map?.setRegion(MKCoordinateRegionMakeWithDistance(center, distance, distance), animated: false)
        
        if let latitudeDelta = map?.region.span.latitudeDelta {
            let center = CLLocationCoordinate2DMake(coordinate!.latitude - latitudeDelta * -0.1, coordinate!.longitude)
            map?.setCenter(center, animated: false)
        }
    }
    
    private func separator() -> UIView {
        let separator = UIView()
        separator.backgroundColor = CXColorBackground
        separator.frame = CGRect.zero
        separator.translatesAutoresizingMaskIntoConstraints = false
        return separator
    }
    
    private func setCoordinate() {
        guard let lat = params![CXKeyLat] else {
            return
        }
        guard let lng = params![CXKeyLng] else {
            return
        }
        
        coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(lat)!, CLLocationDegrees(lng)!)
    }
    
    private func showAlert() {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Warning", message: "Your search returned 0 results, please try changing your criteria and searching again.", preferredStyle: .alert)
            let action = UIAlertAction(title: "Search Again", style: .default) { (action) in
                self.actionBack()
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: "Warning", message: "Your search returned 0 results, please try changing your criteria and searching again.", delegate: self, cancelButtonTitle: "Search Again").show()
        }
    }
    
    private func timerEnable() {
        if timer != nil {
            self.timerDisable()
        }
        var refreshInterval = CXDefaultRefreshInterval
        if CXUser.sharedInstance.userData(key: CXRefreshInterval) != nil {
            refreshInterval = CXUser.sharedInstance.userData(key: CXRefreshInterval) as! TimeInterval
        }
        timer = Timer.scheduledTimer(timeInterval: refreshInterval * 60, target: self, selector: #selector(CXResultsViewController.actionUpdate), userInfo: nil, repeats: true)
    }
    
    private func timerDisable() {
        timer?.invalidate()
        timer = nil
    }
    
    func updateAnnotations() {
        clusteringManager?.removeAnnotations(clusteringManager?.allAnnotations())
        
        let arrayFiltered = self.filteredLocations()
        if arrayFiltered.count == 0 {
            return
        }
        
        var arrayAnnotations: [CXAnnotation] = []
        for vehicle in arrayFiltered {
            guard let location = vehicle[CXKeyLocation] as? Dictionary<String, AnyObject> else {
                continue
            }
            guard let latitude = location[CXKeyLatitude] as? Double else {
                continue
            }
            guard let longitude = location[CXKeyLongitude] as? Double else {
                continue
            }
            
            let pointAnnotation = CXAnnotation()
            pointAnnotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
            if let status = location[CXKeyStatus] as? String {
                switch status {
                case CXKeyAvailable:
                    pointAnnotation.title = CXAnnotationAvailable
                case CXKeyMaybe:
                    pointAnnotation.title = CXAnnotationMaybe
                case CXKeyUnavailable:
                    pointAnnotation.title = CXAnnotationUnavailable
                case CXKeyUnknown:
                    pointAnnotation.title = CXAnnotationUnknown
                default:
                    pointAnnotation.title = CXAnnotationUnknown
                }
            }
            
            if let vehicleId = location[CXKeyVehicleId] as? Int {
                pointAnnotation.identifier = String(vehicleId)
            }
            arrayAnnotations.append(pointAnnotation)
        }
        
        clusteringManager?.setAnnotations(arrayAnnotations)
        self.mapView(map!, regionDidChangeAnimated: false)
    }
    
    func updateFilters() {
        var statusAvailable = 0
        var statusMaybe = 0
        var statusUnavailable = 0
        var statusUnknown = 0
        for vehicle in arrayVehicles {
            guard let location = vehicle[CXKeyLocation] as? Dictionary<String, AnyObject> else {
                continue
            }
            guard let status = location[CXKeyStatus] as? String else {
                continue
            }
            
            switch status {
            case CXKeyAvailable:
                statusAvailable += 1
            case CXKeyMaybe:
                statusMaybe += 1
            case CXKeyUnavailable:
                statusUnavailable += 1
            case CXKeyUnknown:
                statusUnknown += 1
            default:
                statusUnknown += 1
            }
        }
        buttonStatusAvailable?.setTitle("Available (\(statusAvailable))", for: .normal)
        buttonStatusMaybe?.setTitle("Maybe Available (\(statusMaybe))", for: .normal)
        buttonStatusUnavailable?.setTitle("Unavailable (\(statusUnavailable))", for: .normal)
        buttonStatusUnknown?.setTitle("Unknown (\(statusUnknown))", for: .normal)
    }
    
    fileprivate func vehicleLocation(_ reset: Bool) {
        CXProgressHUD.sharedInstance.show()
        CXHTTPManager.sharedInstance.vehicleLocations(params: params!, completion: { (response, error) -> Void in
            if error == nil {
                self.arrayVehicles = response as! [AnyObject]
                self.updateFilters()
                self.updateAnnotations()
                if reset == true {
                    self.reset()
                }
            } else {
                CXErrorHandler.sharedInstance.handleError(error: error! as NSError)
            }
            CXProgressHUD.sharedInstance.hide()
        })
    }
}

extension CXResultsViewController {
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.actionBack()
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func cellSizeFactorForCoordinator(coordinator: FBClusteringManager!) -> CGFloat {
        return 1.5
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        OperationQueue().addOperation({
            let scale = Double(mapView.bounds.width) / mapView.visibleMapRect.size.width
            let annotations = self.clusteringManager?.clusteredAnnotations(within: mapView.visibleMapRect, withZoomScale: scale)
            self.clusteringManager?.displayAnnotations(annotations, on: mapView)
        })
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: CXReuseIdentifierAnnotation)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: CXReuseIdentifierAnnotation)
        } else {
            annotationView!.annotation = annotation
        }
        
        if let annotation = annotation as? FBAnnotationCluster {
            let quantity = annotation.annotations.count
            annotationView!.image = self.cachedImageForText(text: String(quantity))
        } else {
            let status = annotation.title!
            switch status! {
            case CXAnnotationAvailable:
                annotationView!.image = UIImage(named: "green_pin")
            case CXAnnotationMaybe:
                annotationView!.image = UIImage(named: "pin_yellow")
            case CXAnnotationUnavailable:
                annotationView!.image = UIImage(named: "red_pin")
            case CXAnnotationUnknown:
                annotationView!.image = UIImage(named: "pin_gray")
            default:
                annotationView!.image = UIImage(named: "pin_gray")
            }
        }
        
        annotationView?.canShowCallout = false
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.deselectAnnotation(view.annotation, animated: false)
        
        if let annotation = view.annotation as? FBAnnotationCluster {
            let arrayAnnotations = annotation.annotations as! [CXAnnotation]
            let maxZoomLevel = self.maxZoomLevel()
            if Int(round(mapView.zoomLevel())) == Int(maxZoomLevel) {
                viewClusterVehicles?.isHidden = false
                arrayClusterVehicles = arrayAnnotations
                table?.reloadData()
                let quantity = annotation.annotations.count
                labelClusterVehicles?.text = "(\(quantity))"
            } else {
                let firstAnnotation = arrayAnnotations[0]
                var bottomRight = firstAnnotation.coordinate
                var topLeft = firstAnnotation.coordinate
                for annotation in arrayAnnotations {
                    if annotation.coordinate.latitude > bottomRight.latitude {
                        bottomRight.latitude = annotation.coordinate.latitude
                    }
                    if annotation.coordinate.longitude > bottomRight.longitude {
                        bottomRight.longitude = annotation.coordinate.longitude
                    }
                    if annotation.coordinate.latitude < topLeft.latitude {
                        topLeft.latitude = annotation.coordinate.latitude
                    }
                    if annotation.coordinate.longitude < topLeft.longitude {
                        topLeft.longitude = annotation.coordinate.longitude
                    }
                }
                let center = CLLocationCoordinate2DMake((bottomRight.latitude + topLeft.latitude) * 0.5, (bottomRight.longitude + topLeft.longitude) * 0.5)
                let locationLeft = CLLocation(latitude: topLeft.latitude, longitude: topLeft.longitude)
                let locationRight = CLLocation(latitude: bottomRight.latitude, longitude: bottomRight.longitude)
                let distance = locationLeft.distance(from: locationRight) * 1.2
                let region = MKCoordinateRegionMakeWithDistance(center, distance, distance)
                if mapView.zoomLevel(for: region) > maxZoomLevel {
                    mapView.setCenter(center, zoomLevel: maxZoomLevel, animated: false)
                } else {
                    if mapView.zoomLevel(for: region) < CXMinZoomLevel {
                        mapView.setCenter(center, zoomLevel: CXMinZoomLevel, animated: false)
                    } else {
                        mapView.setRegion(region, animated: false)
                    }
                }
            }
            
            return
        }
        
        for vehicle in arrayVehicles {
            guard let location = vehicle[CXKeyLocation] as? Dictionary<String, AnyObject> else {
                continue
            }
            guard let left = location[CXKeyVehicleId] as? Int else {
                continue
            }
            guard let right = (view.annotation as! CXAnnotation).identifier else {
                continue
            }
            guard let contactCompanyId = vehicle[CXKeyContactCompanyId] as? Int else {
                continue
            }
            
            if String(left) == right {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXDetails") as! CXDetailsViewController
                viewController.contactCompanyId = String(contactCompanyId)
                viewController.vehicleId = String(left)
                self.present(viewController, animated: true, completion: nil)
                
                return
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circle = MKCircleRenderer(overlay: overlay)
        circle.fillColor = CXColorMainTheme.withAlphaComponent(0.2)
        circle.lineWidth = 2
        circle.strokeColor = CXColorMainTheme
        return circle
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayClusterVehicles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: CXReuseIdentifierCell)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CXReuseIdentifierCell)
        }
        
        let annotation = arrayClusterVehicles[indexPath.row] as! CXAnnotation
        let status = annotation.title
        switch status! {
        case CXAnnotationAvailable:
            cell?.imageView?.image = UIImage(named: "small_green")
        case CXAnnotationMaybe:
            cell?.imageView?.image = UIImage(named: "small_yellow")
        case CXAnnotationUnavailable:
            cell?.imageView?.image = UIImage(named: "small_red")
        case CXAnnotationUnknown:
            cell?.imageView?.image = UIImage(named: "small_gray")
        default:
            cell?.imageView?.image = UIImage(named: "small_gray")
        }
        
        cell?.textLabel?.font = UIFont(name: "SFUIDisplay-Regular", size: 17)
        cell?.textLabel?.text = ""
        
        guard let right = annotation.identifier else {
            return cell!
        }
        
        for vehicle in arrayVehicles {
            guard let location = vehicle[CXKeyLocation] as? Dictionary<String, AnyObject> else {
                continue
            }
            guard let left = location[CXKeyVehicleId] as? Int else {
                continue
            }
            
            if String(left) == right {
                if let companyDisplayName = vehicle[CXKeyCompanyDisplayName] as? String {
                    cell?.textLabel?.text = companyDisplayName
                }
                
                break
            }
        }
        
        cell?.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pointAnnotation = arrayClusterVehicles[indexPath.row] as! CXAnnotation
        
        guard let right = pointAnnotation.identifier else {
            return
        }
        
        for vehicle in arrayVehicles {
            guard let location = vehicle[CXKeyLocation] as? Dictionary<String, AnyObject> else {
                continue
            }
            guard let left = location[CXKeyVehicleId] as? Int else {
                continue
            }
            guard let contactCompanyId = vehicle[CXKeyContactCompanyId] as? Int else {
                continue
            }
            
            if String(left) == right {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXDetails") as! CXDetailsViewController
                viewController.contactCompanyId = String(contactCompanyId)
                viewController.vehicleId = String(left)
                self.present(viewController, animated: true, completion: nil)
                
                break
            }
        }
        
        self.actionClose([])
    }
}

extension CXResultsViewController {
    @IBAction func actionClose(_ sender: Any) {
        viewClusterVehicles?.isHidden = true
    }
    
    @IBAction func actionCriteria(_ sender: Any) {
        let send = sender as? UIButton
        if send?.isSelected == false {
            buttonCriteriaAll?.isSelected = false
            buttonCriteriaAll?.titleLabel?.font = CXResultsFontDefault
            buttonCriteriaDrivers?.isSelected = false
            buttonCriteriaDrivers?.titleLabel?.font = CXResultsFontDefault
            buttonCriteriaPreferred?.isSelected = false
            buttonCriteriaPreferred?.titleLabel?.font = CXResultsFontDefault
            buttonCriteriaOther?.isSelected = false
            buttonCriteriaOther?.titleLabel?.font = CXResultsFontDefault
            send?.isSelected = true
            send?.titleLabel?.font = CXResultsFontSelected
            
            if send == buttonCriteriaAll {
                params![CXKeyVehicleFilter] = CXKeyAll
            }
            if send == buttonCriteriaDrivers {
                params![CXKeyVehicleFilter] = CXKeyDrivers
            }
            if send == buttonCriteriaPreferred {
                params![CXKeyVehicleFilter] = CXKeyPreferred
            }
            if send == buttonCriteriaOther {
                params![CXKeyVehicleFilter] = CXKeyOther
            }
            self.vehicleLocation(true)
        }
    }
    
    @IBAction func actionReset(_ sender: Any) {
        self.reset()
    }
    
    @IBAction func actionStatus(_ sender: Any) {
        let send = sender as? UIButton
        send?.isSelected = !(send?.isSelected)!
        
        self.updateAnnotations()
    }
    
    func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func actionUpdate() {
        self.vehicleLocation(false)
    }
    
    func actionPinch(_ gesture: UIPinchGestureRecognizer) {
        print("Zoom level: \(String(describing: map?.zoomLevel()))")
        if gesture.state == .began {
            originalRegion = map?.region
        }
        if gesture.state == .changed {
            var latitudeDelta = originalRegion!.span.latitudeDelta
            var longitudeDelta = originalRegion!.span.longitudeDelta
            latitudeDelta = latitudeDelta / Double(gesture.scale)
            longitudeDelta = longitudeDelta / Double(gesture.scale)
            let region = MKCoordinateRegionMake(originalRegion!.center, MKCoordinateSpanMake(latitudeDelta, longitudeDelta))
            let maxZoomLevel = self.maxZoomLevel()
            if (map?.zoomLevel(for: region))! > maxZoomLevel {
                map?.setCenter(map!.centerCoordinate, zoomLevel: maxZoomLevel, animated: true)
            } else {
                if (map?.zoomLevel(for: region))! < CXMinZoomLevel {
                    map?.setCenter(map!.centerCoordinate, zoomLevel: CXMinZoomLevel, animated: true)
                } else {
                    map?.setRegion(region, animated: false)
                }
            }
        }
    }
    
    func actionTap(_ gesture: UITapGestureRecognizer) {
        guard var region =  map?.region else {
            return
        }
        guard var span = map?.region.span else {
            return
        }
        guard let center = map?.convert(gesture.location(in: map), toCoordinateFrom: map) else {
            return
        }
        
        region.center = center
        span.latitudeDelta *= 0.5
        span.longitudeDelta *= 0.5
        region.span = span
        let maxZoomLevel = self.maxZoomLevel()
        if (map?.zoomLevel(for: region))! > maxZoomLevel {
            map?.setCenter(map!.centerCoordinate, zoomLevel: maxZoomLevel, animated: true)
        } else {
            map?.setRegion(region, animated: true)
        }
    }
}
