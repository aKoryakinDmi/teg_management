import UIKit
import MessageUI

class CXDetailsViewController: UIViewController, MFMailComposeViewControllerDelegate, UIScrollViewDelegate {
    struct Layout {
        static let viewBackgroundHeight: CGFloat = 174
    }
    
    @IBOutlet weak var viewBackground: UIView?
    @IBOutlet weak var viewContent: UIView?
    @IBOutlet weak var viewContentMiddle: UIView?
    @IBOutlet weak var scroll: UIScrollView?
    @IBOutlet weak var labelAddress: UILabel?
    @IBOutlet weak var labelStatus: UILabel?
    @IBOutlet weak var labelCompanyNameTitle: UILabel?
    @IBOutlet weak var labelCompanyNameValue: UILabel?
    @IBOutlet weak var labelVehicleSizeTitle: UILabel?
    @IBOutlet weak var labelVehicleSizeValue: UILabel?
    @IBOutlet weak var labelBodyTypeTitle: UILabel?
    @IBOutlet weak var labelBodyTypeValue: UILabel?
    @IBOutlet weak var labelLiveryTitle: UILabel?
    @IBOutlet weak var labelLiveryValue: UILabel?
    @IBOutlet weak var labelVehicleReferenceTitle: UILabel?
    @IBOutlet weak var labelVehicleReferenceValue: UILabel?
    @IBOutlet weak var labelMaxWeightTitle: UILabel?
    @IBOutlet weak var labelMaxWeightValue: UILabel?
    @IBOutlet weak var labelEmailTitle: UILabel?
    @IBOutlet weak var labelEmailValue: UILabel?
    @IBOutlet weak var labelPhoneTitle: UILabel?
    @IBOutlet weak var labelPhoneValue: UILabel?
    @IBOutlet weak var imageAvatar: UIImageView?
    @IBOutlet weak var layoutConstraintViewBackgroundHeight: NSLayoutConstraint?
    var info: [String: Any]?
    var contactCompanyId: String?
    var vehicleId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareContent()
        self.trackedVehicleInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let width = viewContent?.frame.width else {
            return
        }
        guard let height = viewContent?.frame.height else {
            return
        }
        scroll?.contentSize = CGSize(width: width, height: height)
    }
    
    fileprivate func prepareContent() {
        viewContentMiddle?.backgroundColor = CXColorBackground
        labelAddress?.textColor = CXColorSilver
        labelCompanyNameTitle?.textColor = CXColorSilver
        labelCompanyNameValue?.textColor = CXColorBlack
        labelVehicleSizeTitle?.textColor = CXColorSilver
        labelVehicleSizeValue?.textColor = CXColorBlack
        labelBodyTypeTitle?.textColor = CXColorSilver
        labelBodyTypeValue?.textColor = CXColorBlack
        labelLiveryTitle?.textColor = CXColorSilver
        labelLiveryValue?.textColor = CXColorBlack
        labelVehicleReferenceTitle?.textColor = CXColorSilver
        labelVehicleReferenceValue?.textColor = CXColorBlack
        labelMaxWeightTitle?.textColor = CXColorSilver
        labelMaxWeightValue?.textColor = CXColorBlack
        labelEmailTitle?.textColor = CXColorSilver
        labelEmailValue?.textColor = CXColorLink
        labelPhoneTitle?.textColor = CXColorSilver
        labelPhoneValue?.textColor = CXColorLink
        imageAvatar?.layer.cornerRadius = 27
    }
    
    fileprivate func showAlert() {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Warning", message: "Your device could not send email. Please check email configuration and try again.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: "Warning", message: "Your device could not send email. Please check email configuration and try again.", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    private func trackedVehicleInfo() {
        guard contactCompanyId != nil else {
            return
        }
        guard vehicleId != nil else {
            return
        }
        
        CXProgressHUD.sharedInstance.show()
        CXHTTPManager.sharedInstance.trackedVehicleInfo(vehicleId: vehicleId!, contactCompanyId: contactCompanyId!, completion: { (response, error) -> Void in
            if error == nil {
                self.info = response as? [String : Any]
                self.updateContent()
            } else {
                CXErrorHandler.sharedInstance.handleError(error: error!)
            }
            CXProgressHUD.sharedInstance.hide()
        })
    }
    
    private func updateContent() {
        guard info != nil else {
            return
        }
        
        if let locationDisplayAddress = info![CXKeyLocationDisplayAddress] as? String {
            let textAttachment = NSTextAttachment()
            textAttachment.image = UIImage(named: "location")
            let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(attachment: textAttachment))
            attributedString.append(NSAttributedString(string: " \(locationDisplayAddress)"))
            labelAddress?.attributedText = attributedString
        }
        var status = CXKeyUnknown
        labelStatus?.text = ""
        if let location = info![CXKeyLocation] as? Dictionary<String, AnyObject> {
            status = location[CXKeyStatus] as! String
            if status != CXKeyUnknown {
                if let messageText = location[CXKeyMessageText] as? String {
                    labelStatus?.text = messageText
                }
            }
            switch status {
            case CXKeyAvailable:
                labelStatus?.textColor = CXColorAvailable
            case CXKeyMaybe:
                labelStatus?.textColor = CXColorMaybe
            case CXKeyUnavailable:
                labelStatus?.textColor = CXColorUnavailable
            case CXKeyUnknown:
                labelStatus?.textColor = CXColorUnknown
            default:
                labelStatus?.textColor = CXColorUnknown
            }
        }
        if var ownerDisplayName = info![CXKeyOwnerDisplayName] as? String {
            if let ownerDisplayId = info![CXKeyOwnerDisplayId] as? String {
                ownerDisplayName = "\(ownerDisplayName) (\(ownerDisplayId))"
            }
            labelCompanyNameValue?.text = ownerDisplayName
        }
        if let vehicleDisplaySize = info![CXKeyVehicleDisplaySize] as? String {
            labelVehicleSizeValue?.text = vehicleDisplaySize
        }
        if let vehicleDisplayBodyType = info![CXKeyVehicleDisplayBodyType] as? String {
            labelBodyTypeValue?.text = vehicleDisplayBodyType
        }
        if let livery = info![CXKeyLivery] as? Int {
            if livery == 0 {
                labelLiveryValue?.text = "No"
            } else {
                labelLiveryValue?.text = "Yes"
            }
        }
        if let vehicleDisplayName = info![CXKeyVehicleDisplayName] as? String {
            labelVehicleReferenceValue?.text = vehicleDisplayName
        }
        labelMaxWeightValue?.text = "Not Supplied"
        if let maxWeight = info![CXKeyMaxWeight] as? Dictionary<String, AnyObject> {
            var text = ""
            if let maxWeightValue = maxWeight[CXKeyValue] {
                text = "\(maxWeightValue) "
            }
            if let maxWeightUnits = maxWeight[CXKeyUnits] as? String {
                text = "\(text)\(maxWeightUnits)"
            }
            labelMaxWeightValue?.text = text
        }
        if let email = info![CXKeyEmail] as? String {
            labelEmailValue?.text = email
        }
        if let phone = info![CXKeyPhone] as? String {
            labelPhoneValue?.text = "+\(phone)"
        }
    }
}

extension CXDetailsViewController {
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        layoutConstraintViewBackgroundHeight?.constant = Layout.viewBackgroundHeight - scrollView.contentOffset.y
        let minHeight = Layout.viewBackgroundHeight
        if (layoutConstraintViewBackgroundHeight?.constant)! < minHeight {
            layoutConstraintViewBackgroundHeight?.constant = minHeight
        }
        self.view.layoutIfNeeded()
    }
}

extension CXDetailsViewController {
    @IBAction func actionCall(_ sender: Any) {
        guard info != nil else {
            return
        }
        guard let mobile = info![CXKeyPhone] as? String, !mobile.isEmpty else {
            return
        }
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "+\(mobile)", message: nil, preferredStyle: .alert)
            var action = UIAlertAction(title: "Cancel", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            action = UIAlertAction(title: "Call", style: .default) { (action) in
                if UIApplication.shared.canOpenURL(URL(string: "tel:\(mobile.replacingOccurrences(of: " ", with: ""))")!) {
                    UIApplication.shared.openURL(URL(string: "tel:\(mobile.replacingOccurrences(of: " ", with: ""))")!)
                }
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            if UIApplication.shared.canOpenURL(URL(string: "tel:\(mobile.replacingOccurrences(of: " ", with: ""))")!) {
                UIApplication.shared.openURL(URL(string: "tel:\(mobile.replacingOccurrences(of: " ", with: ""))")!)
            }
        }
    }
    
    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionEmail(_ sender: Any) {
        guard info != nil else {
            return
        }
        guard let email = info![CXKeyEmail] as? String, !email.isEmpty else {
            return
        }
        
        let mailComposeViewController = MFMailComposeViewController()
        mailComposeViewController.mailComposeDelegate = self
        mailComposeViewController.setToRecipients([email])
        
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showAlert()
        }
    }
}
