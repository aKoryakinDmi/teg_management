import UIKit

class CXLoadCollectionViewCell: UICollectionViewCell {
   @IBOutlet weak var slider: CXSlider?
   @IBOutlet weak var labelDelta: UILabel?
   @IBOutlet weak var labelETA: UILabel?
   @IBOutlet weak var labelFromTo: UILabel?
   
   override func awakeFromNib() {
      self.layer.cornerRadius = 3
      
      slider?.minimumTrackTintColor = CXColorGray
      slider?.maximumTrackTintColor = CXColorGray
      slider?.minimumValueImage = UIImage(named: "Gray_dot")
      slider?.maximumValueImage = UIImage(named: "gray_outline")
      slider?.setThumbImage(UIImage(named: "Gray_Car"), for: .normal)
      slider?.isUserInteractionEnabled = false
      labelDelta?.textColor = CXColorSilver
      labelETA?.textColor = CXColorBlack
      labelFromTo?.textColor = CXColorBlack
   }
}
