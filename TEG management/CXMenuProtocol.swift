//
//  CXMenuProtocol.swift
//  TEG management
//
//  Created by EvGeniy Lell on 16.06.16.
//
//

import Foundation

protocol CXMenuProtocol {
    var menu: CXMenu? { get set }
    var buttonHidden: UIButton? { get set }
    func drawHiddenButton(offset: CGFloat)
    func drawMenu()
}