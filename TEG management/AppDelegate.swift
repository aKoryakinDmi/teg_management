import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    static let sharedInstance = UIApplication.shared.delegate as! AppDelegate
    fileprivate var slideMenu: MFSideMenuContainerViewController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        UITabBar.appearance().tintColor = CXColorMainTheme
        //UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: CXColorMainTheme], forState:.Normal)
        
        MessengerSchemeColor.sharedInstance.setup()
        
        ApnController.sharedInstance.initPushNotifications(application: application)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.slideMenu = MFSideMenuContainerViewController()
        let leftMenuController = storyboard.instantiateViewController(withIdentifier: "CXLeftMenu") as! CXLeftMenuController
        slideMenu.leftMenuViewController = leftMenuController
        
        let welcomeViewController = storyboard.instantiateViewController(withIdentifier: "CXWelcome") as! CXWelcomeViewController
        let navigationController = UINavigationController(rootViewController: welcomeViewController)
        slideMenu.centerViewController = navigationController
        
        self.window?.rootViewController = slideMenu
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// UITabBarController
extension AppDelegate {
    func mainTabBarController() -> UITabBarController? {
        if let sideMenuController = slideMenu,
            let centerController = sideMenuController.centerViewController as? UINavigationController {
            let count = centerController.viewControllers.count
            if count > 0 {
                if let tabBarController = centerController.viewControllers[count-1] as? UITabBarController {
                    return tabBarController
                }
            }
        }
        return nil
    }
}
// APN Notification
extension AppDelegate {
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        ApnController.sharedInstance.didRegisterForRemoteNotificationsWithDeviceToken(deviceToken: deviceToken)
    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        ApnController.sharedInstance.didFailToRegisterForRemoteNotificationsWithError(error: error)
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        ApnController.sharedInstance.didReceiveRemoteNotification(userInfo: userInfo)
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        ApnController.sharedInstance.didReceiveRemoteNotification(userInfo: userInfo)
        completionHandler(.newData);
    }
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [NSObject : AnyObject], withResponseInfo responseInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        ApnController.sharedInstance.handleActionWithIdentifier(identifier: identifier, userInfo: userInfo, withResponseInfo: responseInfo)
        completionHandler()
    }
}
