import UIKit

private let CXDefaultRadius = "30"
private let CXDefaultTableHeight: CGFloat = 228

class CXLiveViewController: UIViewController, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    struct Layout {
        static let labelWidth: CGFloat = 150
        static let labelHeight: CGFloat = 20
        static let labelOffset: CGFloat = 7
        static let labelTop: CGFloat = 13
    }
    
    struct Picker {
        static let searchRadius: Int = 100
        static let vehicleSizeFrom: Int = 101
        static let vehicleSizeTo: Int = 102
        static let bodyType: Int = 103
    }
    
    @IBOutlet weak var viewContent: UIView?
    @IBOutlet weak var viewCriterias: UIView?
    @IBOutlet weak var table: UITableView?
    @IBOutlet weak var textFieldTownName: UITextField?
    @IBOutlet weak var textFieldSearchRadius: UITextField?
    @IBOutlet weak var textFieldVehicleSizeFrom: UITextField?
    @IBOutlet weak var textFieldVehicleSizeTo: UITextField?
    @IBOutlet weak var textFieldBodyType: UITextField?
    @IBOutlet weak var buttonSearch: UIButton?
    @IBOutlet weak var buttonClear: UIButton?
    @IBOutlet weak var labelSearchRadius: UILabel?
    @IBOutlet weak var labelVehicleSizeFrom: UILabel?
    @IBOutlet weak var labelVehicleSizeTo: UILabel?
    @IBOutlet weak var labelBodyType: UILabel?
    @IBOutlet weak var labelNoResults: UILabel?
    @IBOutlet weak var layoutConstraintViewContentLeading: NSLayoutConstraint?
    @IBOutlet weak var layoutConstraintTableHeight: NSLayoutConstraint?
    var menu: CXMenu?
    var buttonHidden: UIButton?
    var labelTownName: UILabel?
    var layoutConstraintMenu: NSLayoutConstraint?
    weak var layoutConstraintLoginTop: NSLayoutConstraint?
    var timer: Timer?
    var arraySearch: [AnyObject] = []
    var arraySearchRadius: [String] = []
    var arrayVehicleSize: [String] = []
    var arrayVehicleSizeFiltered: [String] = []
    var arrayBodyType: [String] = []
    var lam: [String: Any]?
    var location: [String: Any]?
    var initial: Bool?
    var loading: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = CXColorBackground
        
        self.navigationController?.navigationBar.barTintColor = CXColorMainTheme
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "SFUIDisplay-Regular", size: 19)!, NSForegroundColorAttributeName: CXColorWhite]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.hidesBackButton = true
        self.title = "Search Live Capacity"
        
        let buttonMenu = UIButton(type: .custom)
        buttonMenu.setImage(UIImage(named: "burger"), for: .normal)
        buttonMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonMenu.addTarget(self, action: #selector(CXHomeViewController.actionMenu), for: .touchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem()
        leftBarButtonItem.customView = buttonMenu
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        self.prepareContent()
        self.setPickers()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(CXSupportViewController.actionTap(_:)))
        gesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(gesture)
        
        loading = false
        
        self.resetContent()
        if initial == true {
            self.login(showInitial: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CXLiveViewController.applicationDidBecomeActive(_:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CXLiveViewController.applicationWillResignActive(_:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CXLiveViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CXLiveViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    private func drawHiddenButton(offset: CGFloat) {
        self.buttonHidden = UIButton(type: .custom)
        self.buttonHidden!.frame = CGRect.zero
        self.buttonHidden!.translatesAutoresizingMaskIntoConstraints = false
        self.buttonHidden!.addTarget(self, action: #selector(CXLiveViewController.actionMenuHidden), for: .touchUpInside)
        self.view.addSubview(self.buttonHidden!)
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .leading, relatedBy: .equal, toItem: self.view!, attribute: .leading, multiplier: 1, constant: offset))
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .top, relatedBy: .equal, toItem: self.view!, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .trailing, relatedBy: .equal, toItem: self.view!, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.buttonHidden!, attribute: .bottom, relatedBy: .equal, toItem: self.view!, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.layoutIfNeeded()
    }
    
    func drawMenu() {
        let offset: CGFloat = self.view.frame.width * 0.75
        if menu != nil {
            buttonHidden!.removeFromSuperview()
            self.view.layoutIfNeeded()
            layoutConstraintMenu!.constant = -offset
            layoutConstraintViewContentLeading!.constant = 0
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.view.layoutIfNeeded()
                self.resetTopAndBottomBar()
            }, completion: { (finished: Bool) -> Void in
                self.menu?.removeFromSuperview()
                self.menu = nil
            })
        } else {
            self.prepareMenu(offset: offset)
            layoutConstraintMenu!.constant = 0
            layoutConstraintViewContentLeading!.constant = offset
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.view.layoutIfNeeded()
                self.navigationController!.navigationBar.frame = CGRect(x: offset, y: 20, width: self.navigationController!.navigationBar.frame.width, height: self.navigationController!.navigationBar.frame.height)
                self.navigationController!.tabBarController?.tabBar.frame = CGRect(x: offset, y: self.navigationController!.tabBarController!.tabBar.frame.origin.y, width: self.navigationController!.tabBarController!.tabBar.frame.width, height: self.navigationController!.tabBarController!.tabBar.frame.height)
            }, completion: { (finished: Bool) -> Void in
                self.drawHiddenButton(offset: offset)
            })
        }
    }
    
    fileprivate func drawLabelTown() {
        guard let townName = textFieldTownName!.text, !townName.isEmpty else {
            labelTownName?.removeFromSuperview()
            labelTownName = nil
            labelNoResults?.isHidden = true
            
            return
        }
        if labelTownName == nil {
            labelTownName = self.labelPlaceholder()
            labelTownName?.text = "Town or Postcode"
            self.viewCriterias!.addSubview(labelTownName!)
            self.view.addConstraint(NSLayoutConstraint(item: labelTownName!, attribute: .leading, relatedBy: .equal, toItem: textFieldTownName!, attribute: .leading, multiplier: 1, constant: 0))
            layoutConstraintLoginTop = NSLayoutConstraint(item: labelTownName!, attribute: .top, relatedBy: .equal, toItem: textFieldTownName!, attribute: .top, multiplier: 1, constant: -Layout.labelTop)
            self.view.addConstraint(layoutConstraintLoginTop!)
            self.view.addConstraint(NSLayoutConstraint(item: labelTownName!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelWidth))
            self.view.addConstraint(NSLayoutConstraint(item: labelTownName!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            self.view.layoutIfNeeded()
            layoutConstraintLoginTop!.constant -= Layout.labelOffset
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.view.layoutIfNeeded()
                self.labelTownName?.alpha = 1
            })
        }
    }
    
    fileprivate func hideKeyboard() {
        textFieldTownName?.resignFirstResponder()
        textFieldSearchRadius?.resignFirstResponder()
        textFieldVehicleSizeFrom?.resignFirstResponder()
        textFieldVehicleSizeTo?.resignFirstResponder()
        textFieldBodyType?.resignFirstResponder()
    }
    
    fileprivate func hideLocationContent() {
        table?.isHidden = true
        labelNoResults?.isHidden = true
    }
    
    fileprivate func labelPlaceholder() -> UILabel {
        let labelPlaceholder = UILabel(frame: CGRect.zero)
        labelPlaceholder.alpha = 0
        labelPlaceholder.font = UIFont(name: "SFUIDisplay-Regular", size: 15)
        labelPlaceholder.textAlignment = .left
        labelPlaceholder.textColor = CXColorBorder
        labelPlaceholder.translatesAutoresizingMaskIntoConstraints = false
        return labelPlaceholder
    }
    
    fileprivate func lamSearchParameters(animated: Bool) {
        if loading == false {
            loading = true
            if animated == true {
                CXProgressHUD.sharedInstance.show()
            }
            CXHTTPManager.sharedInstance.lamSearchParametersWithCompletion { (response, error) -> Void in
                self.loading = false
                if error == nil {
                    self.lam = response as? [String: Any]
                    self.resetContent()
                } else {
                    CXErrorHandler.sharedInstance.handleError(error: error! as NSError)
                }
                CXProgressHUD.sharedInstance.hide()
            }
        }
    }
    
    fileprivate func login(showInitial: Bool) {
        if showInitial == true {
            CXProgressHUD.sharedInstance.showInitial()
        } else {
            CXProgressHUD.sharedInstance.show()
        }
        CXHTTPManager.sharedInstance.managerLoginWithCompletion { (response, error) -> Void in
            if error == nil {
                CXUser.sharedInstance.setUserData(userData: response as! Dictionary<String, AnyObject>)
                
                self.loading = false
                self.lamSearchParameters(animated: false)
            } else {
                CXProgressHUD.sharedInstance.hide()
                CXErrorHandler.sharedInstance.handleError(error: error!)
                self.navigationController?.tabBarController?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    fileprivate func logout() {
        CXProgressHUD.sharedInstance.show()
        CXHTTPManager.sharedInstance.logoutWithCompletion { (response, error) -> Void in
            self.navigationController?.tabBarController?.navigationController?.popToRootViewController(animated: true)
            if error == nil {
                self.resetTopAndBottomBar()
            } else {
                CXErrorHandler.sharedInstance.handleError(error: error!)
            }
            CXProgressHUD.sharedInstance.hide()
        }
    }
    
    private func params() -> Dictionary<String, String> {
        let lat = String(describing: location![CXKeyLat]!)
        let lng = String(describing: location![CXKeyLng]!)
        let value = Double(textFieldSearchRadius!.text!)!.convertToMeters()
        let radius = String(value)
        var minVehicle = ""
        var maxVehicle = ""
        if let vehicleSize = lam![CXKeyVehicleSize] as? Array<Dictionary<String, String>> {
            for size in vehicleSize {
                let title = size[CXKeyDisplayVehicleSize]
                if title == textFieldVehicleSizeFrom?.text {
                    minVehicle = size[CXKeyAPIKey]!
                }
                if title == textFieldVehicleSizeTo?.text {
                    maxVehicle = size[CXKeyAPIKey]!
                }
            }
        }
        
        guard textFieldBodyType?.text?.isEmpty != true else {
            return [CXKeyLat: lat, CXKeyLng: lng, CXKeyRadius: radius, "minVehicle": minVehicle, "maxVehicle": maxVehicle, CXKeyVehicleFilter: CXKeyAll]
        }
        
        var bodyType = ""
        if let arrayBodyType = lam![CXKeyBodyType] as? Array<Dictionary<String, String>> {
            for body in arrayBodyType {
                let title = body[CXKeyDisplayBodyType]
                if title == textFieldBodyType?.text {
                    bodyType = body[CXKeyAPIKey]!
                }
            }
        }
        
        return [CXKeyLat: lat, CXKeyLng: lng, CXKeyRadius: radius, "minVehicle": minVehicle, "maxVehicle": maxVehicle, CXKeyBodyType: bodyType, CXKeyVehicleFilter: CXKeyAll]
    }
    
    private func pickerView() -> UIPickerView {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }
    
    private func prepareContent() {
        viewCriterias?.layer.cornerRadius = 3
        table?.layer.borderColor = CXColorBorder.cgColor
        table?.layer.borderWidth = 1
        table?.layer.cornerRadius = 3
        textFieldTownName?.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFieldTownName!.frame.height))
        textFieldTownName?.leftViewMode = UITextFieldViewMode.always
        textFieldTownName?.layer.borderColor = CXColorBorder.cgColor
        textFieldTownName?.layer.borderWidth = 1
        textFieldTownName?.layer.cornerRadius = 3
        textFieldTownName?.textColor = CXColorBlack
        textFieldTownName?.addTarget(self, action: #selector(CXLiveViewController.textFieldDidChange(_:)), for: .editingChanged)
        textFieldSearchRadius?.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFieldTownName!.frame.height))
        textFieldSearchRadius?.leftViewMode = UITextFieldViewMode.always
        var image = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: textFieldSearchRadius!.frame.height))
        image.contentMode = .center
        image.image = UIImage(named: "dropdown")
        textFieldSearchRadius?.rightView = image
        textFieldSearchRadius?.rightViewMode = UITextFieldViewMode.always
        textFieldSearchRadius?.layer.borderColor = CXColorBorder.cgColor
        textFieldSearchRadius?.layer.borderWidth = 1
        textFieldSearchRadius?.layer.cornerRadius = 3
        textFieldSearchRadius?.textColor = CXColorBlack
        textFieldVehicleSizeFrom?.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFieldTownName!.frame.height))
        textFieldVehicleSizeFrom?.leftViewMode = UITextFieldViewMode.always
        image = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: textFieldVehicleSizeFrom!.frame.height))
        image.contentMode = .center
        image.image = UIImage(named: "dropdown")
        textFieldVehicleSizeFrom?.rightView = image
        textFieldVehicleSizeFrom?.rightViewMode = UITextFieldViewMode.always
        textFieldVehicleSizeFrom?.layer.borderColor = CXColorBorder.cgColor
        textFieldVehicleSizeFrom?.layer.borderWidth = 1
        textFieldVehicleSizeFrom?.layer.cornerRadius = 3
        textFieldVehicleSizeFrom?.textColor = CXColorBlack
        textFieldVehicleSizeTo?.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFieldTownName!.frame.height))
        textFieldVehicleSizeTo?.leftViewMode = UITextFieldViewMode.always
        image = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: textFieldVehicleSizeTo!.frame.height))
        image.contentMode = .center
        image.image = UIImage(named: "dropdown")
        textFieldVehicleSizeTo?.rightView = image
        textFieldVehicleSizeTo?.rightViewMode = UITextFieldViewMode.always
        textFieldVehicleSizeTo?.layer.borderColor = CXColorBorder.cgColor
        textFieldVehicleSizeTo?.layer.borderWidth = 1
        textFieldVehicleSizeTo?.layer.cornerRadius = 3
        textFieldVehicleSizeTo?.textColor = CXColorBlack
        textFieldBodyType?.leftView = UIView(frame: CGRect(x :0, y: 0, width: 15, height: textFieldTownName!.frame.height))
        textFieldBodyType?.leftViewMode = UITextFieldViewMode.always
        image = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: textFieldBodyType!.frame.height))
        image.contentMode = .center
        image.image = UIImage(named: "dropdown")
        textFieldBodyType?.rightView = image
        textFieldBodyType?.rightViewMode = UITextFieldViewMode.always
        textFieldBodyType?.layer.borderColor = CXColorBorder.cgColor
        textFieldBodyType?.layer.borderWidth = 1
        textFieldBodyType?.layer.cornerRadius = 3
        textFieldBodyType?.textColor = CXColorBlack
        buttonSearch?.backgroundColor = CXColorMainTheme
        buttonSearch?.layer.cornerRadius = 3
        buttonSearch?.setTitleColor(CXColorWhite, for: .normal)
        labelSearchRadius?.textColor = CXColorBorder
        labelVehicleSizeFrom?.textColor = CXColorBorder
        labelVehicleSizeTo?.textColor = CXColorBorder
        labelBodyType?.textColor = CXColorBorder
        labelNoResults?.layer.borderColor = CXColorBorder.cgColor
        labelNoResults?.layer.borderWidth = 1
        labelNoResults?.layer.cornerRadius = 3
        labelNoResults?.numberOfLines = 0
        labelNoResults?.textColor = CXColorBlack
        layoutConstraintTableHeight?.constant = CXDefaultTableHeight
    }
    
    private func prepareMenu(offset: CGFloat) {
        menu = CXMenu(frame: CGRect.zero)
        menu!.buttonLive!.addTarget(self, action: #selector(CXLiveViewController.actionLive), for: .touchUpInside)
        menu!.buttonHome!.addTarget(self, action: #selector(CXLiveViewController.actionHome), for: .touchUpInside)
        menu!.buttonProfile!.addTarget(self, action: #selector(CXLiveViewController.actionProfile), for: .touchUpInside)
        menu!.buttonSupport!.addTarget(self, action: #selector(CXLiveViewController.actionSupport), for: .touchUpInside)
        menu!.buttonTermsConditions!.addTarget(self, action: #selector(CXLiveViewController.actionTermsConditions), for: .touchUpInside)
        menu!.buttonLogOut!.addTarget(self, action: #selector(CXLiveViewController.actionLogOut), for: .touchUpInside)
        
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(CXLiveViewController.actionSwipe(_:)))
        swipeGestureRecognizer.direction = .left
        view.addGestureRecognizer(swipeGestureRecognizer)
        
        self.view.addSubview(menu!)
        layoutConstraintMenu = NSLayoutConstraint(item: menu!, attribute: .leading, relatedBy: .equal, toItem: self.view!, attribute: .leading, multiplier: 1, constant: -offset)
        self.view.addConstraint(layoutConstraintMenu!)
        self.view.addConstraint(NSLayoutConstraint(item: menu!, attribute: .top, relatedBy: .equal, toItem: self.view!, attribute: .top, multiplier: 1, constant: -64))
        self.view.addConstraint(NSLayoutConstraint(item: menu!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: offset))
        self.view.addConstraint(NSLayoutConstraint(item: menu!, attribute: .bottom, relatedBy: .equal, toItem: self.view!, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.layoutIfNeeded()
    }
    
    fileprivate func resetContent() {
        labelNoResults?.isHidden = true
        table?.isHidden = true
        textFieldTownName?.text = ""
        textFieldSearchRadius?.isEnabled = false
        textFieldSearchRadius?.text = CXDefaultRadius
        textFieldVehicleSizeFrom?.text = ""
        if let defaultFromVehicle = lam?[CXKeyDefaultFromVehicle] as? Dictionary<String, String> {
            if let displayVehicleSize = defaultFromVehicle[CXKeyDisplayVehicleSize] {
                textFieldVehicleSizeFrom?.text = displayVehicleSize
            }
        }
        textFieldVehicleSizeTo?.text = ""
        if let defaultToVehicle = lam?[CXKeyDefaultToVehicle] as? Dictionary<String, String> {
            if let displayVehicleSize = defaultToVehicle[CXKeyDisplayVehicleSize] {
                textFieldVehicleSizeTo?.text = displayVehicleSize
            }
        }
        arrayVehicleSize.removeAll()
        if let vehicleSize = lam?[CXKeyVehicleSize] as? Array<Dictionary<String, String>> {
            for size in vehicleSize {
                arrayVehicleSize.append(size[CXKeyDisplayVehicleSize]!)
            }
        }
        textFieldBodyType?.text = ""
        arrayBodyType.removeAll()
        if let bodyType = lam?[CXKeyBodyType] as? Array<Dictionary<String, String>> {
            for body in bodyType {
                arrayBodyType.append(body[CXKeyDisplayBodyType]!)
            }
            if arrayBodyType.count > 0 {
                arrayBodyType.insert("", at: 0)
            }
            textFieldBodyType?.text = arrayBodyType.first
        }
    }
    
    private func resetTopAndBottomBar() {
        self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 20, width: self.navigationController!.navigationBar.frame.width, height: self.navigationController!.navigationBar.frame.height)
        self.navigationController!.tabBarController?.tabBar.frame = CGRect(x: 0, y: self.navigationController!.tabBarController!.tabBar.frame.origin.y, width: self.navigationController!.tabBarController!.tabBar.frame.width, height: self.navigationController!.tabBarController!.tabBar.frame.height)
    }
    
    func search() {
        guard let text = textFieldTownName?.text, text.characters.count > 1 else {
            return
        }
        
        CXHTTPManager.sharedInstance.locationCompletion(address: text, completion: { (response, error) -> Void in
            self.arraySearch.removeAll()
            self.table?.isHidden = true
            self.labelNoResults?.isHidden = true
            if error == nil {
                self.arraySearch = response as! Array<AnyObject>
                if self.arraySearch.count != 0 {
                    self.table?.isHidden = false
                } else {
                    self.labelNoResults?.isHidden = false
                }
            }
            self.table?.reloadData()
        })
    }
    
    fileprivate func setLocation() {
        textFieldTownName?.resignFirstResponder()
        self.hideLocationContent()
        
        textFieldTownName?.text = location![CXKeyAddress] as? String
        
        arraySearchRadius.removeAll()
        textFieldSearchRadius?.text = CXDefaultRadius
        
        guard let county = location![CXKeyCountry] as? String else {
            return
        }
        
        var array: [AnyObject] = []
        if county == "GB" {
            array = self.lam![CXKeyUKRadiusList] as! Array<AnyObject>
        } else {
            array = self.lam![CXKeyRadiusList] as! Array<AnyObject>
        }
        for radius in array {
            let value = radius as! Double
            arraySearchRadius.append(String(Int(round(value.convertToMiles()))))
        }
        textFieldSearchRadius?.isEnabled = true
    }
    
    private func setPickers() {
        var pickerView = self.pickerView()
        pickerView.tag = Picker.searchRadius
        textFieldSearchRadius?.inputView = pickerView
        
        var toolBar = self.toolBar()
        textFieldSearchRadius?.inputAccessoryView = toolBar
        
        pickerView = self.pickerView()
        pickerView.tag = Picker.vehicleSizeFrom
        textFieldVehicleSizeFrom?.inputView = pickerView
        
        toolBar = self.toolBar()
        textFieldVehicleSizeFrom?.inputAccessoryView = toolBar
        
        pickerView = self.pickerView()
        pickerView.tag = Picker.vehicleSizeTo
        textFieldVehicleSizeTo?.inputView = pickerView
        
        toolBar = self.toolBar()
        textFieldVehicleSizeTo?.inputAccessoryView = toolBar
        
        pickerView = self.pickerView()
        pickerView.tag = Picker.bodyType
        textFieldBodyType?.inputView = pickerView
        
        toolBar = self.toolBar()
        textFieldBodyType?.inputAccessoryView = toolBar
    }
    
    fileprivate func showAlert() {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Error", message: "Please enter a location to perform a search.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: "Error", message: "Please enter a location to perform a search.", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    fileprivate func timerEnable() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: CXDefaultSearchInterval, target: self, selector: #selector(CXLiveViewController.search), userInfo: nil, repeats: false)
    }
    
    private func toolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        let barButtonItemDoneSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButtonItemDone = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(CXSupportViewController.actionTap(_:)))
        toolBar.setItems([barButtonItemDoneSpace, barButtonItemDone], animated: false)
        return toolBar
    }
    
    fileprivate func vehicleLocation() {
        self.hideKeyboard()
        
        let params = self.params()
        CXProgressHUD.sharedInstance.show()
        CXHTTPManager.sharedInstance.vehicleLocations(params: params, completion: { (response, error) -> Void in
            if error == nil {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXResults") as! CXResultsViewController
                if let arrayVehicleFilters = self.lam?[CXKeyVehicleFilters] as? [AnyObject] {
                    viewController.arrayVehicleFilters = arrayVehicleFilters
                }
                viewController.arrayVehicles = response as! [AnyObject]
                viewController.params = params
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                CXErrorHandler.sharedInstance.handleError(error: error!)
            }
            CXProgressHUD.sharedInstance.hide()
        })
    }
}

extension CXLiveViewController {
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.logout()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == Picker.searchRadius {
            return arraySearchRadius.count
        }
        if pickerView.tag == Picker.vehicleSizeFrom {
            return arrayVehicleSizeFiltered.count
        }
        if pickerView.tag == Picker.vehicleSizeTo {
            return arrayVehicleSizeFiltered.count
        }
        if pickerView.tag == Picker.bodyType {
            return arrayBodyType.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == Picker.searchRadius {
            return arraySearchRadius[row]
        }
        if pickerView.tag == Picker.vehicleSizeFrom {
            return arrayVehicleSizeFiltered[row]
        }
        if pickerView.tag == Picker.vehicleSizeTo {
            return arrayVehicleSizeFiltered[row]
        }
        if pickerView.tag == Picker.bodyType {
            return arrayBodyType[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == Picker.searchRadius {
            textFieldSearchRadius?.text = arraySearchRadius[row]
        }
        if pickerView.tag == Picker.vehicleSizeFrom {
            textFieldVehicleSizeFrom?.text = arrayVehicleSizeFiltered[row]
        }
        if pickerView.tag == Picker.vehicleSizeTo {
            textFieldVehicleSizeTo?.text = arrayVehicleSizeFiltered[row]
        }
        if pickerView.tag == Picker.bodyType {
            textFieldBodyType?.text = arrayBodyType[row]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: CXReuseIdentifierCell)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CXReuseIdentifierCell)
        }
        let location = arraySearch[indexPath.row]
        cell?.textLabel?.text = location[CXKeyAddress] as? String
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.location = arraySearch[indexPath.row] as? [String : Any]
        self.setLocation()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textFieldTownName {
            self.search()
        }
        
        if textField == textFieldSearchRadius {
            guard let radius = textField.text else {
                return true
            }
            guard let index = arraySearchRadius.index(of: radius) else {
                return true
            }
            
            let pickerView = textField.inputView as? UIPickerView
            pickerView?.selectRow(index, inComponent: 0, animated: false)
        }
        
        if textField == textFieldVehicleSizeFrom {
            guard let to = textFieldVehicleSizeTo?.text else {
                return true
            }
            
            arrayVehicleSizeFiltered.removeAll()
            for size in arrayVehicleSize {
                if size != to {
                    arrayVehicleSizeFiltered.append(size)
                } else {
                    arrayVehicleSizeFiltered.append(size)
                    
                    guard let from = textField.text else {
                        return true
                    }
                    guard let index = arrayVehicleSizeFiltered.index(of: from) else {
                        return true
                    }
                    
                    let pickerView = textField.inputView as? UIPickerView
                    pickerView?.selectRow(index, inComponent: 0, animated: false)
                    
                    return true
                }
            }
        }
        
        if textField == textFieldVehicleSizeTo {
            guard let from = textFieldVehicleSizeFrom?.text else {
                return true
            }
            
            arrayVehicleSizeFiltered = arrayVehicleSize
            var size = arrayVehicleSizeFiltered.first
            while(size != from) {
                arrayVehicleSizeFiltered.removeFirst()
                size = arrayVehicleSizeFiltered.first
            }
            
            guard let to = textField.text else {
                return true
            }
            guard let index = arrayVehicleSizeFiltered.index(of: to) else {
                return true
            }
            
            let pickerView = textField.inputView as? UIPickerView
            pickerView?.selectRow(index, inComponent: 0, animated: false)
        }
        
        if textField == textFieldBodyType {
            guard let bodyType = textField.text else {
                return true
            }
            guard let index = arrayBodyType.index(of: bodyType) else {
                return true
            }
            
            let pickerView = textField.inputView as? UIPickerView
            pickerView?.selectRow(index, inComponent: 0, animated: false)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldTownName {
            table?.isHidden = true
        }
        textField.resignFirstResponder()
        
        return false
    }
}

extension CXLiveViewController {
    @IBAction func actionSearch(_ sender: Any) {
        self.hideKeyboard()
        self.hideLocationContent()
        
        guard let address = location?[CXKeyAddress] as? String else {
            self.showAlert()
            
            return
        }
        guard textFieldTownName?.text == address else {
            self.showAlert()
            
            return
        }
        
        self.vehicleLocation()
    }
    
    @IBAction func actionClear(_ sender: Any) {
        self.hideKeyboard()
        self.hideLocationContent()
        self.resetContent()
    }
    
    func actionHome() {
        self.actionMenu()
        
        self.navigationController!.tabBarController?.selectedIndex = CXTabHome
    }
    
    func actionLive() {
        self.actionMenu()
    }
    
    func actionLogOut() {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Log Out", message: "Are you sure you want to log out?", preferredStyle: .alert)
            var action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.logout()
            }
            alertController.addAction(action)
            action = UIAlertAction(title: "Cancel", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: "Log Out", message: "Are you sure you want to log out?", delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "Cancel").show()
        }
    }
    
    func actionMenu() {
        
        self.menuContainerViewController.toggleLeftSideMenuCompletion({
            //code
        })
        return
        
        let navigationController = self.navigationController!.tabBarController?.viewControllers![CXTabHome] as! UINavigationController
        let viewController = navigationController.viewControllers.first as! CXHomeViewController
        viewController.drawMenu()
        
        self.drawMenu()
    }
    
    func actionMenuHidden() {
        self.actionMenu()
    }
    
    func actionProfile() {
        self.actionMenu()
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXProfile") as! CXProfileViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    func actionSupport() {
        self.actionMenu()
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXSupport") as! CXSupportViewController
        let navigationController = UINavigationController(rootViewController: viewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func actionSwipe(_ gestureRecognizer: UISwipeGestureRecognizer) {
        self.actionMenu()
    }
    
    func actionTap(_ gesture: UITapGestureRecognizer) {
        if textFieldTownName?.isFirstResponder == true {
            return
        }
        
        self.hideKeyboard()
        self.hideLocationContent()
    }
    
    func actionTermsConditions() {
        UIApplication.shared.openURL(URL(string: CXTermsConditions)!)
        
        self.actionMenu()
    }
    
    func applicationDidBecomeActive(_ notification: Notification) {
        print("applicationDidBecomeActive")
        self.login(showInitial: false)
    }
    
    func applicationWillResignActive(_ notification: Notification) {
        self.hideKeyboard()
    }
    
    func keyboardWillShow(_ notification: Notification) {
        guard let keyboardHeight = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else {
            return
        }
        guard let y = table?.frame.origin.y else {
            return
        }
        
        self.view.layoutIfNeeded()
        let height = UIScreen.main.bounds.height
        layoutConstraintTableHeight?.constant = height - keyboardHeight.height - y - 64
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.view.layoutIfNeeded()
        layoutConstraintTableHeight?.constant = CXDefaultTableHeight
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField == textFieldTownName {
            self.drawLabelTown()
            self.timerEnable()
            
            return
        }
    }
}
