import MapKit

class CXBookingRouteViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var map: MKMapView?
    @IBOutlet weak var buttonReset: UIButton?
    var originalRegion: MKCoordinateRegion?
    var route: MKRoute?
    var load: Dictionary<String, AnyObject>?
    var driverCoordinate: CLLocationCoordinate2D?
    var fromCoordinate: CLLocationCoordinate2D?
    var toCoordinate: CLLocationCoordinate2D?
    var status: CXLoadStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Booking Route"
        
        let buttonBack = UIButton(type: .custom)
        buttonBack.setImage(UIImage(named: "Back"), for: .normal)
        buttonBack.frame = CGRect(x: 0, y: 0, width: 10, height: 17)
        buttonBack.addTarget(self, action: #selector(CXBookingRouteViewController.actionBack), for: .touchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem()
        leftBarButtonItem.customView = buttonBack
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        let buttonUpdate = UIButton(type: .custom)
        buttonUpdate.setImage(UIImage(named: "update"), for: .normal)
        buttonUpdate.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonUpdate.addTarget(self, action: #selector(CXBookingRouteViewController.actionUpdate), for: .touchUpInside)
        
        let rightBarButtonItem = UIBarButtonItem()
        rightBarButtonItem.customView = buttonUpdate
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(CXBookingRouteViewController.actionPinch(_:)))
        pinchGesture.delegate = self
        pinchGesture.delaysTouchesBegan = true
        map?.addGestureRecognizer(pinchGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CXBookingRouteViewController.actionTap(_:)))
        tapGesture.numberOfTapsRequired = 2
        map?.addGestureRecognizer(tapGesture)
        
        self.updateCoordinates()
        self.direction()
        self.executorLocation()
    }
    
    fileprivate func annotation(_ annotation: MKAnnotation, identifier: String) -> MKAnnotationView {
        var annotationView = map?.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        } else {
            annotationView!.annotation = annotation
        }
        return annotationView!
    }
    
    private func coordinateBottomRight() -> CLLocationCoordinate2D {
        var bottomRight = fromCoordinate!
        let left = toCoordinate!
        var right = fromCoordinate!
        if driverCoordinate != nil {
            right = driverCoordinate!
        }
        
        bottomRight.latitude = right.latitude
        if left.latitude > right.latitude {
            bottomRight.latitude = left.latitude
        }
        bottomRight.longitude = right.longitude
        if left.longitude > right.longitude {
            bottomRight.longitude = left.longitude
        }
        return bottomRight
    }
    
    private func coordinateTopLeft() -> CLLocationCoordinate2D {
        var topLeft = toCoordinate!
        let left = toCoordinate!
        var right = fromCoordinate!
        if driverCoordinate != nil {
            right = driverCoordinate!
        }
        
        topLeft.latitude = right.latitude
        if left.latitude < right.latitude {
            topLeft.latitude = left.latitude
        }
        topLeft.longitude = right.longitude
        if left.longitude < right.longitude {
            topLeft.longitude = left.longitude
        }
        return topLeft
    }
    
    fileprivate func executorLocation() {
        let loadId = load![CXKeyLoadId]
        CXHTTPManager.sharedInstance.executorLocation(loadId: String(describing: loadId!), completion: { (response, error) -> Void in
            if error == nil {
                let location = (response as! NSDictionary)[CXKeyLocation] as! NSDictionary
                self.driverCoordinate = CLLocationCoordinate2DMake(location[CXKeyLatitude] as! CLLocationDegrees, location[CXKeyLongitude] as! CLLocationDegrees)
            } else {
                self.showAlert()
            }
            self.updateAnnotations()
        })
    }
    
    private func direction() {
        CXProgressHUD.sharedInstance.show()
        
        guard fromCoordinate != nil else {
            CXProgressHUD.sharedInstance.hide()
            return
        }
        
        guard toCoordinate != nil else {
            CXProgressHUD.sharedInstance.hide()
            return
        }
        
        let directionsRequest = MKDirectionsRequest()
        directionsRequest.source = MKMapItem(placemark: MKPlacemark(coordinate: fromCoordinate!, addressDictionary: nil))
        directionsRequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: toCoordinate!, addressDictionary: nil))
        directionsRequest.transportType = .automobile
        let directions = MKDirections(request: directionsRequest)
        directions.calculate { (response, error) -> Void in
            if error == nil {
                self.route = response?.routes[0]
                self.map?.removeOverlays(self.map!.overlays)
                self.map?.add(self.route!.polyline)
            }
            CXProgressHUD.sharedInstance.hide()
        }
    }
    
    fileprivate func reset() {
        var array: [MKAnnotation] = []
        for annotation in map!.annotations {
            guard let identifier = (annotation as! CXAnnotation).identifier else {
                return
            }
            
            if driverCoordinate != nil {
                if identifier == CXAnnotationDriver {
                    array.append(annotation)
                }
            } else {
                if identifier == CXAnnotationFrom {
                    array.append(annotation)
                }
            }
            if identifier == CXAnnotationTo {
                array.append(annotation)
            }
        }
        
        guard route != nil else {
            map?.showAnnotations(array, animated: true)
            return
        }
        
        self.resetWithRoute()
    }
    
    fileprivate func resetWithRoute() {
        var bottomRight = coordinateBottomRight()
        var topLeft = coordinateTopLeft()
        
        var index = 0
        if driverCoordinate != nil {
            let locationDriver = CLLocation(latitude: bottomRight.latitude, longitude: bottomRight.longitude)
            var i = 0
            var nearestDistance = Double(CGFloat.greatestFiniteMagnitude)
            for step in route!.steps {
                let coordinate = step.polyline.coordinate
                let locationStep = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                let distance = locationStep.distance(from: locationDriver)
                if distance < nearestDistance {
                    nearestDistance = distance
                    index = i
                }
                
                i += 1
            }
        }
        
        for i in index..<route!.steps.count {
            let step = route!.steps[i]
            if step.polyline.coordinate.latitude > bottomRight.latitude {
                bottomRight.latitude = step.polyline.coordinate.latitude
            }
            if step.polyline.coordinate.longitude > bottomRight.longitude {
                bottomRight.longitude = step.polyline.coordinate.longitude
            }
            if step.polyline.coordinate.latitude < topLeft.latitude {
                topLeft.latitude = step.polyline.coordinate.latitude
            }
            if step.polyline.coordinate.longitude < topLeft.longitude {
                topLeft.longitude = step.polyline.coordinate.longitude
            }
        }
        
        let center = CLLocationCoordinate2DMake((bottomRight.latitude + topLeft.latitude) * 0.5, (bottomRight.longitude + topLeft.longitude) * 0.5)
        let locationLeft = CLLocation(latitude: topLeft.latitude, longitude: topLeft.longitude)
        let locationRight = CLLocation(latitude: bottomRight.latitude, longitude: bottomRight.longitude)
        let distance = locationLeft.distance(from: locationRight)
        let region = MKCoordinateRegionMakeWithDistance(center, distance, distance)
        map?.setRegion(region, animated: true)
    }
    
    private func showAlert() {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Warning", message: "The current location of the load cannot be displayed as tracking data is not being supplied by the vehicle", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: "Warning", message: "The current location of the load cannot be displayed as tracking data is not being supplied by the vehicle", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    fileprivate func titleDriver() -> String {
        var title = ""
        if load?[CXKeyETA] as? Dictionary<String, AnyObject> != nil {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = CXDateFormat
            
            let distanceInMiles = (load?[CXKeyETA] as! NSDictionary)[CXKeyDistanceInMiles] as! Float
            if var eta = (load?[CXKeyETA] as! NSDictionary)[CXKeyETA] as? String {
                let seconds = NSTimeZone.local.secondsFromGMT(for: dateFormat.date(from: eta)!)
                let dateETA = Date(timeInterval: TimeInterval(seconds), since: dateFormat.date(from: eta)!)
                eta = dateFormat.string(from: dateETA)
                if eta.isToday() {
                    dateFormat.dateFormat = CXDateFormatTime
                    title = "Miles to go: \(distanceInMiles). ETA: \(dateFormat.string(from: dateETA))"
                } else {
                    dateFormat.dateFormat = CXDateFormatNotToday
                    title = "Miles to go: \(distanceInMiles). ETA: \(dateFormat.string(from: dateETA))"
                }
            }
        }
        return title
    }
    
    fileprivate func titleFrom() -> String {
        var title = "From: "
        if (load?[CXKeyFrom] as! NSDictionary)[CXKeyTown] != nil {
            let town = (load?[CXKeyFrom] as! NSDictionary)[CXKeyTown] as! String
            title += town
        } else {
            if (load?[CXKeyFrom] as! NSDictionary)[CXKeyFullAddress] != nil {
                let fullAddress = (load?[CXKeyFrom] as! NSDictionary)[CXKeyFullAddress] as! String
                title += fullAddress
            }
        }
        if (load?[CXKeyFrom] as! NSDictionary)[CXKeyPostcode] != nil {
            let postcode = (load?[CXKeyFrom] as! NSDictionary)[CXKeyPostcode] as! String
            title += " (\(postcode))"
        }
        return title
    }
    
    fileprivate func titleTo() -> String {
        var title = "To: "
        if (load?[CXKeyTo] as! NSDictionary)[CXKeyTown] != nil {
            let town = (load?[CXKeyTo] as! NSDictionary)[CXKeyTown] as! String
            title += town
        } else {
            if (load?[CXKeyTo] as! NSDictionary)[CXKeyFullAddress] != nil {
                let fullAddress = (load?[CXKeyTo] as! NSDictionary)[CXKeyFullAddress] as! String
                title += fullAddress
            }
        }
        if (load?[CXKeyTo] as! NSDictionary)[CXKeyPostcode] != nil {
            let postcode = (load?[CXKeyTo] as! NSDictionary)[CXKeyPostcode] as! String
            title += " (\(postcode))"
        }
        return title
    }
    
    private func updateAnnotations() {
        map?.removeAnnotations(map!.annotations)
        
        var pointAnnotation = CXAnnotation()
        if driverCoordinate != nil {
            pointAnnotation.coordinate = driverCoordinate!
            pointAnnotation.identifier = CXAnnotationDriver
            map?.addAnnotation(pointAnnotation)
        }
        
        if fromCoordinate != nil {
            pointAnnotation = CXAnnotation()
            pointAnnotation.coordinate = fromCoordinate!
            pointAnnotation.identifier = CXAnnotationFrom
            map?.addAnnotation(pointAnnotation)
        }
        
        if toCoordinate != nil {
            pointAnnotation = CXAnnotation()
            pointAnnotation.coordinate = toCoordinate!
            pointAnnotation.identifier = CXAnnotationTo
            map?.addAnnotation(pointAnnotation)
        }
        
        map?.showAnnotations(map!.annotations, animated: true)
    }
    
    private func updateCoordinates() {
        let coordinatesFrom = (load![CXKeyFrom] as! NSDictionary)[CXKeyCoordinates] as! NSDictionary
        fromCoordinate = CLLocationCoordinate2DMake(coordinatesFrom[CXKeyLat] as! CLLocationDegrees, coordinatesFrom[CXKeyLng] as! CLLocationDegrees)
        let coordinatesTo = (load![CXKeyTo] as! NSDictionary)[CXKeyCoordinates] as! NSDictionary
        toCoordinate = CLLocationCoordinate2DMake(coordinatesTo[CXKeyLat] as! CLLocationDegrees, coordinatesTo[CXKeyLng] as! CLLocationDegrees)
    }
}

extension CXBookingRouteViewController {
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: MKAnnotationView?
        
        guard let identifier = (annotation as! CXAnnotation).identifier else {
            return annotationView
        }
        
        if identifier == CXAnnotationDriver {
            annotationView = self.annotation(annotation, identifier: CXReuseIdentifierAnnotation)
            annotationView!.image = UIImage(named: "black_car")
            (annotation as! CXAnnotation).title = self.titleDriver()
        }
        if identifier == CXAnnotationFrom {
            annotationView = self.annotation(annotation, identifier: CXReuseIdentifierAnnotationFrom)
            annotationView!.image = UIImage(named: "start")
            annotationView!.centerOffset = CGPoint(x: annotationView!.image!.size.width * 0.5 - 1, y: -annotationView!.image!.size.height * 0.5)
            (annotation as! CXAnnotation).title = self.titleFrom()
        }
        if identifier == CXAnnotationTo {
            annotationView = self.annotation(annotation, identifier: CXReuseIdentifierAnnotationTo)
            annotationView!.image = UIImage(named: "finish")
            annotationView!.centerOffset = CGPoint(x: annotationView!.image!.size.width * 0.5 - 1, y: -annotationView!.image!.size.height * 0.5)
            (annotation as! CXAnnotation).title = self.titleTo()
        }
        
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(polyline: route!.polyline)
        polylineRenderer.lineWidth = 5
        polylineRenderer.strokeColor = CXColorUnavailable
        return polylineRenderer
    }
}

extension CXBookingRouteViewController {
    @IBAction func actionReset(_ sender: Any) {
        self.reset()
    }
    
    func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func actionUpdate() {
        self.executorLocation()
    }
    
    func actionPinch(_ gesture: UIPinchGestureRecognizer) {
        print("Zoom level: \(String(describing: map?.zoomLevel()))")
        if gesture.state == .began {
            originalRegion = map?.region
        }
        if gesture.state == .changed {
            var latitudeDelta = originalRegion!.span.latitudeDelta
            var longitudeDelta = originalRegion!.span.longitudeDelta
            latitudeDelta = latitudeDelta / Double(gesture.scale)
            longitudeDelta = longitudeDelta / Double(gesture.scale)
            let region = MKCoordinateRegionMake(originalRegion!.center, MKCoordinateSpanMake(latitudeDelta, longitudeDelta))
            if (map?.zoomLevel(for: region))! > CXMaxZoomLevel {
                map?.setCenter(map!.centerCoordinate, zoomLevel: CXMaxZoomLevel, animated: true)
            } else {
                if (map?.zoomLevel(for: region))! < CXMinZoomLevel {
                    map?.setCenter(map!.centerCoordinate, zoomLevel: CXMinZoomLevel, animated: true)
                } else {
                    map?.setRegion(region, animated: false)
                }
            }
        }
    }
    
    func actionTap(_ gesture: UITapGestureRecognizer) {
        guard var region =  map?.region else {
            return
        }
        guard var span = map?.region.span else {
            return
        }
        guard let center = map?.convert(gesture.location(in: map), toCoordinateFrom: map) else {
            return
        }
        
        region.center = center
        span.latitudeDelta *= 0.5
        span.longitudeDelta *= 0.5
        region.span = span
        if (map?.zoomLevel(for: region))! > CXMaxZoomLevel {
            map?.setCenter(map!.centerCoordinate, zoomLevel: CXMaxZoomLevel, animated: true)
        } else {
            map?.setRegion(region, animated: true)
        }
    }
}
