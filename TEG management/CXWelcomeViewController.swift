import UIKit

class CXWelcomeViewController: UIViewController, UIAlertViewDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate {
    struct Layout {
        static let labelWidth: CGFloat = 80
        static let labelHeight: CGFloat = 20
        static let labelOffset: CGFloat = 7
        static let labelTop: CGFloat = 13
        static let textFieldLoginTop: CGFloat = 80
        static let textFieldLoginTopOffset: CGFloat = 16
    }
    
    @IBOutlet weak var viewContent: UIView?
    @IBOutlet weak var textFieldLogin: UITextField?
    @IBOutlet weak var textFieldPassword: UITextField?
    @IBOutlet weak var buttonDisclaimer: UIButton?
    @IBOutlet weak var buttonForgot: UIButton?
    @IBOutlet weak var buttonLogin: UIButton?
    @IBOutlet weak var buttonTermsConditions: UIButton?
    @IBOutlet weak var labelInfo: UILabel?
    @IBOutlet weak var labelVersion: UILabel?
    @IBOutlet weak var layoutConstraintLogoHeight: NSLayoutConstraint?
    @IBOutlet weak var layoutConstraintViewContentAlignCenterY: NSLayoutConstraint?
    @IBOutlet weak var layoutConstraintTextFieldLoginTop: NSLayoutConstraint?
    var labelLogin: UILabel?
    var labelPassword: UILabel?
    weak var actionSubmit: AnyObject?
    weak var layoutConstraintLoginTop: NSLayoutConstraint?
    weak var layoutConstraintPasswordTop: NSLayoutConstraint?
    
    typealias LiveSetting = (initial: Bool, lam: [String: Any]?)
    class func setupTabBarController(live: LiveSetting) -> UITabBarController {
        
        // Main
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let liveViewController = mainStoryboard.instantiateViewController(withIdentifier: "CXLive") as! CXLiveViewController
        liveViewController.initial = live.initial
        liveViewController.lam = live.lam
        let liveTab = UINavigationController(rootViewController: liveViewController)
        liveTab.tabBarItem = UITabBarItem(title: "Search Live Capacity", image: UIImage(named: "live"), tag: 0)
        
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CXHome") as! CXHomeViewController
        let homeTab = UINavigationController(rootViewController: homeViewController)
        homeTab.tabBarItem = UITabBarItem(title: "Bookings In Progress", image: UIImage(named: "bookings"), tag: 0)
        
        // Messenger
        let chatStoryboard = UIStoryboard(name: "Messenger", bundle: nil)
        
        let chatTab = chatStoryboard.instantiateInitialViewController() as! CXMessengerNavigationController
        chatTab.tabBarItem = UITabBarItem(title: "Messenger", image: UIImage(named: "chat_tabIcn"), tag: 0)
        
        let baseController = mainStoryboard.instantiateViewController(withIdentifier: "CXTabBar") as! UITabBarController
        baseController.viewControllers = [liveTab, homeTab, chatTab]
        baseController.navigationController?.navigationBar.isHidden = true
        
        return baseController
    }
    
    private func pushTabBarController(_ live: LiveSetting) -> Bool {
        if CXUser.sharedInstance.username() != nil {
            let tabBarController = CXWelcomeViewController.setupTabBarController(live: live)
            self.navigationController?.pushViewController(tabBarController, animated: true)
            return true
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let liveSetting = LiveSetting(initial: true, lam: nil)
        _ = pushTabBarController(liveSetting)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.prepareContent()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(CXWelcomeViewController.actionTap(_:)))
        self.view.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CXWelcomeViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CXWelcomeViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        self.menuContainerViewController.panMode = MFSideMenuPanModeDefault
    }
    
    fileprivate func checkTextFieldsEmpty() -> Bool {
        guard let login = textFieldLogin?.text, !login.isEmpty else {
            self.showAlertWithTitle(title: "Error", message: "Enter your username.")
            
            return false
        }
        guard let password = textFieldPassword?.text, !password.isEmpty else {
            self.showAlertWithTitle(title: "Error", message: "Enter your password.")
            
            return false
        }
        
        return true
    }
    
    fileprivate func drawLabelLogin() {
        guard let login = textFieldLogin!.text, !login.isEmpty else {
            labelLogin?.removeFromSuperview()
            labelLogin = nil
            
            return
        }
        if labelLogin == nil {
            labelLogin = self.labelPlaceholder()
            labelLogin?.text = "Username"
            self.viewContent!.addSubview(labelLogin!)
            self.view.addConstraint(NSLayoutConstraint(item: labelLogin!, attribute: .leading, relatedBy: .equal, toItem: viewContent!, attribute: .leading, multiplier: 1, constant: 0))
            layoutConstraintLoginTop = NSLayoutConstraint(item: labelLogin!, attribute: .top, relatedBy: .equal, toItem: textFieldLogin!, attribute: .top, multiplier: 1, constant: -Layout.labelTop)
            self.view.addConstraint(layoutConstraintLoginTop!)
            self.view.addConstraint(NSLayoutConstraint(item: labelLogin!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelWidth))
            self.view.addConstraint(NSLayoutConstraint(item: labelLogin!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            self.view.layoutIfNeeded()
            layoutConstraintLoginTop!.constant -= Layout.labelOffset
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                self.view.layoutIfNeeded()
                self.labelLogin?.alpha = 1
            })
        }
    }
    
    fileprivate func drawLabelPassword() {
        guard let password = textFieldPassword!.text, !password.isEmpty else {
            labelPassword?.removeFromSuperview()
            labelPassword = nil
            
            return
        }
        if labelPassword == nil {
            labelPassword = self.labelPlaceholder()
            labelPassword?.text = "Password"
            self.viewContent!.addSubview(labelPassword!)
            self.view.addConstraint(NSLayoutConstraint(item: labelPassword!, attribute: .leading, relatedBy: .equal, toItem: viewContent!, attribute: .leading, multiplier: 1, constant: 0))
            layoutConstraintPasswordTop = NSLayoutConstraint(item: labelPassword!, attribute: .top, relatedBy: .equal, toItem: textFieldPassword!, attribute: .top, multiplier: 1, constant: -Layout.labelTop)
            self.view.addConstraint(layoutConstraintPasswordTop!)
            self.view.addConstraint(NSLayoutConstraint(item: labelPassword!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelWidth))
            self.view.addConstraint(NSLayoutConstraint(item: labelPassword!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Layout.labelHeight))
            self.view.layoutIfNeeded()
            layoutConstraintPasswordTop!.constant -= Layout.labelOffset
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                self.view.layoutIfNeeded()
                self.labelPassword?.alpha = 1
            })
        }
    }
    
    fileprivate func hideKeyboard() {
        textFieldLogin!.resignFirstResponder()
        textFieldPassword!.resignFirstResponder()
    }
    
    fileprivate func forgotPassword(_ email: String) {
        CXHTTPManager.sharedInstance.forgotPasswordWithEmail(email: email, completion: { (response, error) -> Void in
            if error == nil {
                self.showAlertWithTitle(title: "Success", message: "An email has been sent to: \(email). It includes details of how to reset your password. For security, the link in the email will expire after 30 minutes.")
            } else {
                if (error!._userInfo?[AFNetworkingOperationFailingURLResponseErrorKey] as? HTTPURLResponse)?.statusCode == 422 {
                    self.showAlertWithTitle(title: "Error", message: "The email address has either been entered incorrectly or is not recognised. Please check and try again.")
                } else {
                    CXErrorHandler.sharedInstance.handleError(error: error! as NSError)
                }
            }
        })
    }
    
    fileprivate func labelPlaceholder() -> UILabel {
        let labelPlaceholder = UILabel(frame: CGRect.zero)
        labelPlaceholder.alpha = 0
        labelPlaceholder.font = UIFont(name: "SFUIDisplay-Regular", size: 15)
        labelPlaceholder.textAlignment = .left
        labelPlaceholder.textColor = CXColorBorder
        labelPlaceholder.translatesAutoresizingMaskIntoConstraints = false
        return labelPlaceholder
    }
    
    fileprivate func login() {
        self.hideKeyboard()
        CXProgressHUD.sharedInstance.show()
        CXUser.sharedInstance.signIn(login: textFieldLogin!.text!, password: textFieldPassword!.text!)
        weak var blockSelf = self
        CXHTTPManager.sharedInstance.managerLoginWithCompletion { (response, error) -> Void in
            if error == nil {
                CXUser.sharedInstance.setUserData(userData: response as! [String : Any])
                
                CXHTTPManager.sharedInstance.lamSearchParametersWithCompletion { (response, error) -> Void in
                    if error == nil {
                        let liveSetting = LiveSetting(initial: false, lam: response as? [String : Any])
                        _ = blockSelf?.pushTabBarController(liveSetting)
                    } else {
                        CXErrorHandler.sharedInstance.handleError(error: error! as NSError)
                    }
                    CXProgressHUD.sharedInstance.hide()
                }
            } else {
                CXProgressHUD.sharedInstance.hide()
                CXErrorHandler.sharedInstance.handleError(error: error! as NSError)
                if let blockSelf = blockSelf {
                    blockSelf.navigationController?.popToViewController(blockSelf, animated: true)
                }
            }
        }
    }
    
    fileprivate func prepareContent() {
        textFieldLogin!.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFieldLogin!.frame.height))
        textFieldLogin!.leftViewMode = UITextFieldViewMode.always
        textFieldLogin!.layer.borderColor = CXColorBorder.cgColor
        textFieldLogin!.layer.borderWidth = 1
        textFieldLogin!.layer.cornerRadius = 3
        textFieldLogin!.textColor = CXColorBlack
        textFieldLogin!.addTarget(self, action: #selector(CXWelcomeViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        textFieldPassword!.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFieldLogin!.frame.height))
        textFieldPassword!.leftViewMode = UITextFieldViewMode.always
        textFieldPassword!.layer.borderColor = CXColorBorder.cgColor
        textFieldPassword!.layer.borderWidth = 1
        textFieldPassword!.layer.cornerRadius = 3
        textFieldPassword!.textColor = CXColorBlack
        textFieldPassword!.addTarget(self, action: #selector(CXWelcomeViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        buttonLogin?.backgroundColor = CXColorMainTheme
        buttonLogin?.layer.cornerRadius = 3
        buttonLogin?.setTitleColor(CXColorWhite, for: .normal)
        
        buttonForgot?.setTitleColor(CXColorMainTheme, for: .normal)
        
        labelInfo!.numberOfLines = 2
        labelInfo!.textColor = CXColorSilver
        let attributedString = NSMutableAttributedString(string: "By clicking the 'Log In' button, you are agreeing to our Terms & Conditions & Disclaimer")
        attributedString.addAttribute(NSForegroundColorAttributeName, value: CXColorMainTheme, range: (attributedString.string as NSString).range(of: "Terms & Conditions"))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: CXColorMainTheme, range: (attributedString.string as NSString).range(of: "Disclaimer"))
        labelInfo!.attributedText = attributedString
        
        guard let bundleShortVersionString = Bundle.main.infoDictionary!["CFBundleShortVersionString"] else {
            return
        }
        guard let bundleVersion = Bundle.main.infoDictionary!["CFBundleVersion"] else {
            return
        }
        
        labelVersion?.text = "version: \(bundleShortVersionString) - \(bundleVersion)"
        labelVersion?.textColor = CXColorBorder
    }
    
    fileprivate func showAlertWithTitle(title: String?, message: String?) {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
}

extension CXWelcomeViewController {
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.alertViewStyle == .default {
            if buttonIndex == 0 {
                self.actionTermsConditions(nil)
            }
        } else {
            if buttonIndex == 1 {
                let textField = alertView.textField(at: 0)
                let email = textField?.text
                self.forgotPassword(email!)
            }
        }
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField == textFieldPassword {
            self.login()
        }
        
        return true
    }
}

extension CXWelcomeViewController {
    @IBAction func actionDisclaimer(_ sender: Any) {
        let alertController = UIAlertController(title: "Disclaimer", message: "You are agree to not use this application whilst driving as it is potentially dangerous and may be illegal. Please only use this application when it is safe to do so and when the law permits. Please view our full Terms & Conditions.", preferredStyle: .alert)
        var action = UIAlertAction(title: "Terms & Conditions", style: .default) { (action) in
            self.actionTermsConditions(nil)
        }
        alertController.addAction(action)
        action = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func actionForgot(_ sender: Any) {
        let alertController = UIAlertController(title: "Forgot Password?", message: "Enter your email address:", preferredStyle: .alert)
        alertController.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.autocapitalizationType = UITextAutocapitalizationType.none
            textField.autocorrectionType = UITextAutocorrectionType.no
            textField.keyboardType = UIKeyboardType.emailAddress
            textField.placeholder = "Your email"
            textField.addTarget(self, action: #selector(CXWelcomeViewController.textFieldDidChange(_:)), for: .editingChanged)
        })
        var action = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
        }
        alertController.addAction(action)
        action = UIAlertAction(title: "Submit", style: .default) { (action) in
            let email = alertController.textFields!.first?.text
            self.forgotPassword(email!)
        }
        action.isEnabled = false
        actionSubmit = action
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        if self.checkTextFieldsEmpty() {
            self.login()
        }
    }
    
    @IBAction func actionTermsConditions(_ sender: Any?) {
        UIApplication.shared.openURL(URL(string: CXTermsConditions)!)
    }
    
    func actionTap(_ gesture: UITapGestureRecognizer) {
        self.hideKeyboard()
    }
    
    func keyboardWillShow(_ notification: NSNotification) {
        self.view.layoutIfNeeded()
        let height = UIScreen.main.bounds.height
        if height < 568 {
            layoutConstraintViewContentAlignCenterY!.constant = -((height - viewContent!.frame.height) / 2)
            layoutConstraintTextFieldLoginTop!.constant = Layout.textFieldLoginTopOffset
        } else {
            if height < 667 {
                layoutConstraintViewContentAlignCenterY!.constant = -((height - viewContent!.frame.height) / 2)
                layoutConstraintTextFieldLoginTop!.constant = Layout.textFieldLoginTopOffset
            } else {
                if height < 736 {
                    layoutConstraintViewContentAlignCenterY!.constant = -((height - viewContent!.frame.height) / 4)
                    layoutConstraintTextFieldLoginTop!.constant = Layout.textFieldLoginTopOffset + ((height - viewContent!.frame.height) / 4)
                } else {
                    layoutConstraintViewContentAlignCenterY!.constant = -((height - viewContent!.frame.height) / 4)
                    layoutConstraintTextFieldLoginTop!.constant = Layout.textFieldLoginTopOffset + ((height - viewContent!.frame.height) / 4)
                }
            }
        }
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        self.view.layoutIfNeeded()
        layoutConstraintViewContentAlignCenterY!.constant = 0
        layoutConstraintTextFieldLoginTop!.constant = Layout.textFieldLoginTop
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField == textFieldLogin {
            self.drawLabelLogin()
            return
        }
        if textField == textFieldPassword {
            self.drawLabelPassword()
            return
        }
        
        if textField.text?.isEmail() == true {
            if #available(iOS 8.0, *) {
                (actionSubmit as! UIAlertAction).isEnabled = true
            }
        } else {
            if #available(iOS 8.0, *) {
                (actionSubmit as! UIAlertAction).isEnabled = false
            }
        }
    }
}
