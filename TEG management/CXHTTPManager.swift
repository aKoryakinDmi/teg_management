import UIKit

typealias CXClosure = (_ response: Any?, _ error: Error?) -> Void

#if ConfigurationProgect_Dev //DEV_SERVER
let WS_HOST = "dev-cx.transportexchangegroup.com"
    //   private let CHAT_HOST = "dev-cx.transportexchangegroup.com";
#elseif ConfigurationProgect_Live //LIVE_SERVER
let WS_HOST = "cx.transportexchangegroup.com"
    //   private let CHAT_HOST = "cx.transportexchangegroup.com";
#endif

let WS_PROTOCOL = "https"
let WS_PATH = "/trackme-ws/v2/"

//private let CHAT_K3DC = "{3DC}"
//private let CHAT_K8CC = "{8CC}"
//private let CHAT_PROTOCOL = "wss"
//private let CHAT_PATH = "/mobilewss/\(CHAT_K3DC)/\(CHAT_K8CC)/websocket"
//private let CHAT_BASE = "\(CHAT_PROTOCOL)://\(CHAT_HOST)\(CHAT_PATH)"

let CXID = "ID"
let CXVID = "VID"
let CXCCID = "CCID"
let CXBase = "\(WS_PROTOCOL)://\(WS_HOST)\(WS_PATH)"
let CXDefaultUsername = "selfRegistrar"
let CXDefaultPassword = "selfPassw0rd"


class CXHTTPManager {
    class Chat {
        private enum Key: String {
            case k3DC = "{3DC}"
            case k8CC = "{8CC}"
        }
        #if ConfigurationProgect_Dev
        private static let host = "dev-cx.transportexchangegroup.com"
        #elseif ConfigurationProgect_Live
        private static let host = "cx.transportexchangegroup.com"
        #endif
        private static let scheme = "wss"
        private static let path = "/mobilewss/\(Key.k3DC.rawValue)/\(Key.k8CC.rawValue)/websocket"
        class func hostUrl() -> URL {
            let reuslt = "\(scheme)://\(host)"
            return URL(string: reuslt)!
        }
        class func baseUrl(a3ds: String, a8CC: String) -> URL {
            var tPath = path
            tPath = tPath.replacingOccurrences(of: Key.k3DC.rawValue, with: a3ds)
            tPath = tPath.replacingOccurrences(of: Key.k8CC.rawValue, with: a8CC)
            let reuslt = "\(scheme)://\(host)\(tPath)"
            return URL(string: reuslt)!
        }
    }
    
    
    struct Method {
        static let executorLocation: String = "load/\(CXID)/executorLocation"
        static let forgotPassword: String = "forgotPassword"
        static let inProgress: String = "inProgress"
        static let lamSearchParameters: String = "lamSearchParameters"
        static let locationCompletion: String = "locationCompletion"
        static let logout: String = "logout"
        static let managerLogin: String = "managerLogin"
        static let sendSupportEmail: String = "sendSupportEmail"
        static let trackedVehicleInfo: String = "trackedVehicleInfo/\(CXVID)/companyId/\(CXCCID)"
        static let userInfoPicture: String = "userInfo/picture"
        static let vehicleLocations: String = "vehicleLocations"
    }
    
    static let sharedInstance = CXHTTPManager()
    
    let manager = AFHTTPSessionManager(baseURL: URL(string: CXBase))
    
    private init() {
        manager.setTaskDidReceiveAuthenticationChallenge { (session, task, challenge, credential) -> URLSession.AuthChallengeDisposition in
            if challenge.previousFailureCount > 0 || CXUser.sharedInstance.username() == nil {
                return .cancelAuthenticationChallenge
            } else {
                if task.originalRequest?.url?.absoluteString.contains(Method.forgotPassword) == true {
                    credential?.pointee = URLCredential(user: CXDefaultUsername, password: CXDefaultPassword.toMD5(), persistence: .none)
                    return .useCredential
                } else {
                    credential?.pointee = URLCredential(user: CXUser.sharedInstance.username()!, password: CXUser.sharedInstance.password()!, persistence: .none)
                    return .useCredential
                }
            }
        }
    }
    
    func executorLocation(loadId: String, completion: @escaping CXClosure) {
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.get(Method.executorLocation.replacingOccurrences(of: CXID, with: loadId), parameters: nil, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func forgotPasswordWithEmail(email: String, completion: @escaping CXClosure) {
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.post(Method.forgotPassword, parameters: ["email": email], progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func inProgressWithCompletion(completion: @escaping CXClosure) {
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.get(Method.inProgress, parameters: nil, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func lamSearchParametersWithCompletion(completion: @escaping CXClosure) {
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.get(Method.lamSearchParameters, parameters: nil, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func locationCompletion(address: String, completion: @escaping CXClosure) {
        let params = ["address": address]
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.get(Method.locationCompletion, parameters: params, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func logoutWithCompletion(completion: @escaping CXClosure) {
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.post(Method.logout, parameters: nil, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            CXUser.sharedInstance.signOut()
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func managerLoginWithCompletion(completion: @escaping CXClosure) {
        var params = [
            "clientType": "cx4a",
            "clientOsVersion": [
                "phoneSystem": UIDevice.current.systemName,
                "version": UIDevice.current.systemVersion
            ],
            "additionalInfo": "additional"
            ] as [String : Any]
        
        let bundleIdentifier = (Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String) ?? ""
        #if ConfigurationProgect_AppStore
            let appIdentifier = bundleIdentifier.components(separatedBy: ".").last ?? ""
        #else
            let appIdentifier = (bundleIdentifier.components(separatedBy: ".").last ?? "") + "-debug"
        #endif
        
        if let deviceToken = ApnController.sharedInstance.deviceToken {
            params["notificationInfo"] = [
                "app": appIdentifier,
                "deviceToken": deviceToken,
                "platform": "ios"
            ]
        }
        print(params)
        
        let data = try! JSONSerialization.data(withJSONObject: params, options:JSONSerialization.WritingOptions(rawValue: 0))
        let json = String(data: data, encoding: String.Encoding.utf8)
        print(json!)
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.post(Method.managerLogin, parameters: ["loginRequest": json!], progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            CXUser.sharedInstance.signOut()
            completion(nil, error)
        })
    }
    
    func sendSupportEmail(subject: String, comment: String, completion: @escaping CXClosure) {
        let params = ["email": "\(CXUser.sharedInstance.userData(key: CXKeyEmail)!)", "subject": subject, "comment": "\(comment)\n\nEnvironment:\n*Phone Model = \(UIDevice.current.model)\n*iOS version = \(UIDevice.current.systemVersion)\n*App version code = \(Bundle.main.infoDictionary!["CFBundleVersion"]!)\n*App version name = \(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)\n*Date = \(Date())"]
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.post(Method.sendSupportEmail, parameters: params, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func trackedVehicleInfo(vehicleId: String, contactCompanyId: String, completion: @escaping CXClosure) {
        var method = Method.trackedVehicleInfo.replacingOccurrences(of: CXVID, with: vehicleId)
        method = method.replacingOccurrences(of: CXCCID, with: contactCompanyId)
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.get(method, parameters: nil, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
    
    func pictureWithCompletion(completion: @escaping CXClosure) {
        let request = URLRequest(url: URL(string: "\(CXBase + Method.userInfoPicture)")!)
        
        let downloadTask = manager.downloadTask(with: request, progress: nil, destination: { (targetPath, response) -> URL in
            let folder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            return URL(fileURLWithPath: folder).appendingPathComponent("avatar.png")
        }, completionHandler: { (response, filePath, error) -> Void in
            if error == nil {
                print(filePath!)
                completion(filePath, nil)
            } else {
                print(error!)
                completion(nil, error!)
            }
        })
        downloadTask.resume()
    }
    
    func vehicleLocations(params: [String: String], completion: @escaping CXClosure)  {
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.get(Method.vehicleLocations, parameters: params, progress: { (progress) -> Void in
            
        }, success: { (task, response) -> Void in
            print(response!)
            completion(response, nil)
        }, failure: { (task, error) -> Void in
            print(error)
            completion(nil, error)
        })
    }
}
