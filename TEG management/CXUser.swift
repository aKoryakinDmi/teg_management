import UIKit

class CXUser {
    static let sharedInstance = CXUser()
    
    let keychainItemWrapper = KeychainItemWrapper(identifier: CXIdentifier, accessGroup: nil)
    
    func username() -> String? {
        return (keychainItemWrapper[CXUsername] as? String)
    }
    
    func password() -> String? {
        return (keychainItemWrapper[CXPassword] as? String)
    }
    
    func signIn(login: String, password: String) {
        keychainItemWrapper[CXUsername] = login as AnyObject
        keychainItemWrapper[CXPassword] = password.toMD5() as AnyObject
    }
    
    func signOut() {
        keychainItemWrapper[CXUsername] = nil
        keychainItemWrapper[CXPassword] = nil
        AppLogin.sharedInstance.logout()
    }
    
    func setUserData(userData: [String: Any]) {
        UserDefaults.standard.set(userData[CXKeyJWT], forKey: CXKeyJWT)
        
        let company = userData[CXKeyCompany] as! Dictionary<String, AnyObject>
        UserDefaults.standard.set(company[CXKeyCompanyId], forKey: CXKeyCompanyId)
        UserDefaults.standard.set(company[CXKeyCountry], forKey: CXKeyCountry)
        UserDefaults.standard.set(company[CXKeyCountryCode2], forKey: CXKeyCountryCode2)
        UserDefaults.standard.set(company[CXKeyName], forKey: CXKeyName)
        UserDefaults.standard.set(company[CXKeyTown], forKey: CXKeyTown)
        
        let companySettings = userData[CXKeyCompanySettings] as! Dictionary<String, AnyObject>
        UserDefaults.standard.set(companySettings[CXKeyBehindTime] as! TimeInterval, forKey: CXBehindTime)
        UserDefaults.standard.set(companySettings[CXKeyRefreshInterval] as! TimeInterval, forKey: CXRefreshInterval)
        
        let user = userData[CXKeyUser] as! Dictionary<String, AnyObject>
        UserDefaults.standard.set(user[CXKeyEmail], forKey: CXKeyEmail)
        UserDefaults.standard.set(user[CXKeyFirstName], forKey: CXKeyFirstName)
        UserDefaults.standard.set(user[CXKeyLastName], forKey: CXKeyLastName)
        UserDefaults.standard.set(user[CXKeyPhone1], forKey: CXKeyPhone1)
        UserDefaults.standard.set(user[CXKeyUserID], forKey: CXKeyUserID)
        
        UserDefaults.standard.synchronize()
        
        if let jwt = self.userData(key: CXKeyJWT) as? String,
            let userUid = self.userData(key: CXKeyUserID) as? NSNumber {
            AppLogin.sharedInstance.login(jwt: jwt, userUid: String(userUid.intValue))
            CXMessengerChatMachine.sharedInstance.connect()
        }
    }
    
    func userData(key: String) -> Any? {
        return UserDefaults.standard.object(forKey: key)
    }
}
