import UIKit

class CXProgressHUD: UIView {
    struct Layout {
        static let logoHeight: CGFloat = 136
        static let logoWidth: CGFloat = 136
    }
    
    static let sharedInstance = CXProgressHUD()
    
    var car: UIImageView?
    var navigator: UIImageView?
    var timer: Timer?
    
    private func drawAnimation() {
        self.drawCar()
        self.drawNavigator(loading: true)
    }
    
    private func drawCar() {
        UIView.animate(withDuration: 0.08, animations: { () -> Void in
            self.car?.transform = CGAffineTransform(scaleX: 1, y: 1.04)
        }, completion: { (finished) -> Void in
            UIView.animate(withDuration: 0.08, animations: { () -> Void in
                self.car?.transform = CGAffineTransform(scaleX: 1, y: 0.96)
            }, completion: { (finished) -> Void in
                if self.car != nil
                {
                    self.drawCar()
                }
            })
        })
    }
    
    private func drawNavigator(loading: Bool) {
        if loading {
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.navigator?.transform = CGAffineTransform(rotationAngle: (30 * CGFloat(Double.pi)) / 180)
            }, completion: { (finished) -> Void in
                if self.navigator != nil
                {
                    self.drawNavigator(loading: false)
                }
            })
        } else {
            UIView.animate(withDuration: 0.8, animations: { () -> Void in
                self.navigator?.transform = CGAffineTransform(rotationAngle: (-30 * CGFloat(Double.pi)) / 180)
            }, completion: { (finished) -> Void in
                UIView.animate(withDuration: 0.8, animations: { () -> Void in
                    self.navigator?.transform = CGAffineTransform(rotationAngle: (30 * CGFloat(Double.pi)) / 180)
                }, completion: { (finished) -> Void in
                    if self.navigator != nil {
                        self.drawNavigator(loading: false)
                    }
                })
            })
        }
    }
    
    func show() {
        self.hide()
        
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        UIApplication.shared.keyWindow!.addSubview(self)
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        car = UIImageView(image: UIImage(named: "Car"))
        let carWidth = car!.image?.size.width
        let carHeight = car!.image?.size.height
        car!.backgroundColor = UIColor.clear
        car!.frame = CGRect(x: (width - carWidth!) / 2, y: (height - carHeight!) / 2, width: carWidth!, height: carHeight!)
        self.addSubview(car!)
        
        navigator = UIImageView(image: UIImage(named: "Navigator"))
        let navigatorWidth = navigator!.image?.size.width
        let navigatorHeight = navigator!.image?.size.height
        navigator!.backgroundColor = UIColor.clear
        navigator!.frame = CGRect(x: (width - navigatorWidth!) / 2, y: (height - navigatorHeight!) / 2, width: navigatorWidth!, height: navigatorHeight!)
        self.addSubview(navigator!)
        
        self.drawAnimation()
    }
    
    func hide() {
        timer?.invalidate()
        for view in self.subviews
        {
            view.layer.removeAllAnimations()
            view.removeFromSuperview()
        }
        car = nil
        navigator = nil
        self.layer.removeAllAnimations()
        self.removeFromSuperview()
    }
}

extension CXProgressHUD {
    func showInitial() {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        UIApplication.shared.keyWindow!.addSubview(self)
        
        self.backgroundColor = CXColorMainTheme
        self.frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        let logo = UIImageView(image: UIImage(named: "logo_white"))
        logo.backgroundColor = UIColor.clear
        logo.frame = CGRect(x: (width - Layout.logoWidth) * 0.5, y: (height - Layout.logoHeight) * 0.5, width: Layout.logoWidth, height: Layout.logoHeight)
        self.addSubview(logo)
        
        timer = Timer.scheduledTimer(timeInterval: CXSleepNSTimeInterval, target: self, selector: #selector(CXProgressHUD.hideInitial), userInfo: nil, repeats: false)
    }
    
    func hideInitial() {
        self.show()
    }
}
