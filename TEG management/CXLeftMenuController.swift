//
//  CXLeftMenuController.swift
//  TEG management
//
//  Created by EvGeniy Lell on 16.06.16.
//
//

import Foundation

class CXLeftMenuController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tb: UITabBarController?
    
    typealias MenuItemAction = ()->()
    class MenuItem {
        let iconName: String
        let title: String
        let action: MenuItemAction
        init(iconName: String, title: String, action: @escaping MenuItemAction) {
            self.iconName = iconName
            self.title = title
            self.action = action
        }
    }
    
    fileprivate func menuState(_ state: MFSideMenuState) {
        if let sideMenuController = self.parent as? MFSideMenuContainerViewController {
            sideMenuController.menuState = state
        }
    }
    private func tabBarController() -> UITabBarController? {
        if let sideMenuController = self.parent as? MFSideMenuContainerViewController,
            let centerController = sideMenuController.centerViewController as? UINavigationController {
            let count = centerController.viewControllers.count
            if count > 0 {
                if let tabBarController = centerController.viewControllers[count-1] as? UITabBarController {
                    tb = tabBarController
                    return tabBarController
                }
            }
        }
        return nil
    }
    private func activeController() -> UIViewController? {
        if let tabBarController = self.tabBarController() {
            let index = tabBarController.selectedIndex
            var controller = tabBarController.viewControllers?[index]
            if let navigationController = controller as? UINavigationController {
                controller = navigationController.viewControllers.first
            }
            return controller
        }
        return nil
    }
    private var menuItemsList: [MenuItem]!
    private func createMenuItemsList() -> [MenuItem] {
        weak var blockSelf = self
        return [
            MenuItem(iconName: "menu-globe", title: "Search Live Capacity", action: {
                blockSelf?.menuState(MFSideMenuStateClosed)
                blockSelf?.tabBarController()?.selectedIndex = CXTabLive
            }),
            MenuItem(iconName: "menu-bookings", title: "Bookings In Progress", action: {
                blockSelf?.menuState(MFSideMenuStateClosed)
                blockSelf?.tabBarController()?.selectedIndex = CXTabHome
            }),
            MenuItem(iconName: "chat_menuIcn", title: "Messenger", action: {
                blockSelf?.menuState(MFSideMenuStateClosed)
                blockSelf?.tabBarController()?.selectedIndex = CXTabChat
            }),
            MenuItem(iconName: "profile", title: "Profile", action: {
                blockSelf?.menuState(MFSideMenuStateClosed)
                if let activeController = blockSelf?.activeController() {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXProfile") as! CXProfileViewController
                    activeController.present(viewController, animated: true, completion: nil)
                }
            }),
            MenuItem(iconName: "support", title: "Support", action: {
                blockSelf?.menuState(MFSideMenuStateClosed)
                if let activeController = blockSelf?.activeController() {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CXSupport") as! CXSupportViewController
                    let navigationController = UINavigationController(rootViewController: viewController)
                    activeController.present(navigationController, animated: true, completion: nil)
                }

            }),
            MenuItem(iconName: "terms", title: "Terms & Conditions", action: {
                UIApplication.shared.openURL(URL(string: CXTermsConditions)!)
                blockSelf?.menuState(MFSideMenuStateClosed)
            }),
        ]
    }
    

    @IBOutlet private weak var buttonLogOut: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = tabBarController()
        menuItemsList = createMenuItemsList()
        view.backgroundColor = CXColorGray
        tableView.isScrollEnabled = false
        tableView.sectionHeaderHeight = 40
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        tableView.rowHeight = 64
        tableView.separatorColor = CXColorBlack
        tableView.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0)
        
        buttonLogOut.addTarget(self, action: #selector(CXLeftMenuController.actionLogOut), for: .touchUpInside)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as? CXLeftMenuControllerMenuCell {
            cell.menuItem = menuItemsList[indexPath.row]
            return cell
        }
        fatalError("Cell not found")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) as? CXLeftMenuControllerMenuCell {
            cell.menuItem?.action()
        }
    }
}

// TODO: need remove to ~LoginController
extension CXLeftMenuController: UIAlertViewDelegate {
    func actionLogOut() {
        menuState(MFSideMenuStateClosed)
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Log Out", message: "Are you sure you want to log out?", preferredStyle: .alert)
            var action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.logout()
            }
            alertController.addAction(action)
            action = UIAlertAction(title: "Cancel", style: .default) { (action) in
                
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            UIAlertView(title: "Log Out", message: "Are you sure you want to log out?", delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "Cancel").show()
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0 {
            self.logout()
        }
    }
    private func logout() {
        CXProgressHUD.sharedInstance.show()
        CXHTTPManager.sharedInstance.logoutWithCompletion { (response, error) -> Void in
            DispatchQueue.main.async() {
//                self.tabBarController?.navigationController?.popToRootViewController(animated: true)
                self.tb?.navigationController?.popToRootViewController(animated: true)
            }
            if error == nil {
//                dispatch_async(dispatch_get_main_queue()) {
//                    self.tabBarController()?.navigationController?.popToRootViewControllerAnimated(true)
//                    self.resetTopAndBottomBar()
//                }
            } else {
                CXErrorHandler.sharedInstance.handleError(error: error! as NSError)
            }
            CXProgressHUD.sharedInstance.hide()
        }
    }
    private func resetTopAndBottomBar() {
        // TODO: wtf???
//        self.navigationController!.navigationBar.frame = CGRectMake(0, 20, self.navigationController!.navigationBar.frame.width, self.navigationController!.navigationBar.frame.height)
//        self.navigationController!.tabBarController?.tabBar.frame = CGRectMake(0, self.navigationController!.tabBarController!.tabBar.frame.origin.y, self.navigationController!.tabBarController!.tabBar.frame.width, self.navigationController!.tabBarController!.tabBar.frame.height)
    }


}

class CXLeftMenuControllerMenuCell: UITableViewCell {
    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = CXColorBlack
        titleLabel.font = UIFont(name: ".SFUIText-Regular", size: 16)
        separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        selectionStyle = .none
    }
    
    var menuItem: CXLeftMenuController.MenuItem? {
        didSet {
            titleLabel.text = menuItem?.title ?? ""
            let icon = UIImage(named: menuItem?.iconName ?? "")
            iconView.image = icon
        }
    }
}
